import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import json

import markdown
from csv import writer
import time
import os

with open('../../aps_staff.md', 'r') as f:
    text = f.read()
    html = markdown.markdown(text)

with open('aps_staff_lst.html', 'w') as f:
    f.write(html)

# Read the list with all the links
aps_list_path = os.getcwd() + "/aps_staff_lst.html"

aps_list_html_page = BeautifulSoup(open(aps_list_path, encoding="utf8"), "html.parser")

# All the links to aps websites
all_links_aps = []

for a in aps_list_html_page.find_all('a', href=True):
    all_links_aps.append(a['href'])

for link in all_links_aps:

    html = urllib.request.urlopen(link).read()
    soup = BeautifulSoup(html, 'html.parser')
    scripts = soup.find_all('script')
    
    found_data = 0

    for script in scripts:
        result = script.text.find('aps_directory_data')
        if (result != -1):
            raw_data = script.text.strip()[21:-1]
            
    data = json.loads(raw_data)

    all_educators = []

    for key in data:
        position = str(data[key]['title'])        
        position = position.replace('\'','')
    
        position = position.replace("[","")
        position = position.replace("]","")
        