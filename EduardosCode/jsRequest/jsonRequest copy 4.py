import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import json
from collections import OrderedDict

import markdown
from csv import writer
import time
import os

with open('../../aps_staff.md', 'r') as f:
    text = f.read()
    html = markdown.markdown(text)

with open('aps_staff_lst.html', 'w') as f:
    f.write(html)

# Read the list with all the links
aps_list_path = os.getcwd() + "/aps_staff_lst.html"

aps_list_html_page = BeautifulSoup(open(aps_list_path, encoding="utf8"), "html.parser")

# All the links to aps websites
all_links_aps = []

for a in aps_list_html_page.find_all('a', href=True):
    all_links_aps.append(a['href'])

def arrayToString(array):
    items = str(array)
    items = items.replace("[", "")
    items = items.replace("]", "")
    items = items.replace("\'", "")
    return items


class Educator:
    def __init__(self, firstName, lastName, aps_id, email, worksite, title, category):
        self.firstName = firstName
        self.lastName =  lastName
        self.aps_id = aps_id 
        self.email = email
        self.worksite = worksite
        self.title = title
        self.category = category
        
        

with open('aps_data.csv', 'w', encoding='utf8', newline='') as f:
    thewriter = writer(f)
    header = ['First Name', 'Last Name', 'APS ID','Email', 'Worksite', 'Title', 'Category']
    thewriter.writerow(header)
    all_educators = []
    for link in all_links_aps:
            html = urllib.request.urlopen(link).read()
            soup = BeautifulSoup(html, 'html.parser')
            scripts = soup.find_all('script')
            
            found_data = 0

            for script in scripts:
                result = script.text.find('aps_directory_data')
                if (result != -1):
                    raw_data = script.text.strip()[21:-1]
                    
            data = json.loads(raw_data)

            for key in data:
                first_name = data[key]['name_f']
                last_name = data[key]['name_l']
                aps_id = data[key]['apsid']
                email = data[key]['email']
           
                worksite = arrayToString(data[key]['site'])
                worksite = worksite.replace("5001", "Virtual Elementary School")
                worksite = worksite.replace("5002", "Virtual Middle School")
                worksite = worksite.replace("5003", "Virtual High School")
                try:
                    title = arrayToString(data[key]['title'])
                except:
                    title = 'n/a'

                category = arrayToString(data[key]['category'])             

                all_educators.append(Educator(first_name, last_name, aps_id, email, worksite, title, category))
            
        
        
    
    cleaned_data = []
    '''for each in all_educators:
        for teacher in cleaned_data:
             if (each.aps_id != teacher.aps_id):
                 cleaned_data.append(each.aps_id)'''
    
    '''all_aps_ids = []
    for each in all_educators: 
        all_aps_ids.append(each.aps_id)
    cleaned_ids = set (all_aps_ids)'''

    
    '''for each in cleaned_ids:
        if all_educators.index(each) > -1: 
            cleaned_data.append(all_educators[all_educators.index(each)])'''
   
    all_educators.sort(key=lambda item: item.aps_id[1:])
 
    for each in all_educators:
        instances = all_educators.count(each.aps_id)
        index = all_educators.index(each)
        while instances > 1:
            if all_educators[index].worksite == all_educators[index+1].worksite:
                all_educators.remove(all_educators[index+1])
        
    #there is also an OrderedDict.fromkeys(list) method which returns the list w/o duplicates and in the same order
        
 
        def __eq__(self, other):
            return self.author_name==other.author_name\
                and self.title==other.title
        
    for each in all_educators:
        header = [each.firstName, each.lastName, each.aps_id, each.email, each.worksite, each.title, each.category]
        thewriter.writerow(header)
    #you wanna try, yeah 
      
        

        
            

      

    

    





    
    
    
    
            
            
            





            
            
            
           