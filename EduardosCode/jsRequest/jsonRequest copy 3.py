First Name,Last Name,APS ID,Email,Worksite,Title,Category
Kertenia,Lynch,E10740,kertenia.lynch@apsva.us,Abingdon ES,Administrative Assistant,Staff
Marial,Gerald,E10754,maria.gerald@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kim,Crittenden,E10862,kim.crittenden@apsva.us,Abingdon ES,Teacher,Instructional Staff
Anne,Oliveira,E11466,anne.oliveira@apsva.us,Abingdon ES,Assistant Principal,Administrative Staff
Steve,Morse,E1204,steve.morse@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kathi,Aagaard,E13649,kathi.aagaard@apsva.us,Abingdon ES,Teacher,Instructional Staff
Jolynn,Lopez,E13650,jolynn.lopez@apsva.us,Abingdon ES,Administrative Assistant,Staff
Kelly,Leahey,E14020,kelly.leahey@apsva.us,Abingdon ES,Teacher,Instructional Staff
Caty,Branco,E14689,caty.branco@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Carolyn,Togan,E14777,carolyn.togan@apsva.us,Abingdon ES,Custodian,Facilities Staff
Nelson,Salmeron,E14883,nelson.salmeron@apsva.us,Abingdon ES,Maintenance Supervisor,Facilities Staff
Ethel,Edmond,E15385,ethel.edmond@apsva.us,Abingdon ES,Staff,Staff
Linda,Ellis,E15423,linda.ellis@apsva.us,Abingdon ES,Teacher,Instructional Staff
Andrew,Wojciechowski,E1555,andrew.wojciechowski@apsva.us,Abingdon ES,Teacher,Instructional Staff
Gabriela,Perez,E15773,gabriela.perez@apsva.us,Abingdon ES,Administrative Assistant,Staff
Anne,Carroll,E16002,anne.carroll@apsva.us,Abingdon ES,Teacher,Instructional Staff
Suzanne,Butler,E19340,suzanne.butler@apsva.us,Abingdon ES,n/a,Unclassified
Lori,Criado,E19966,lori.criado@apsva.us,Abingdon ES,Teacher,Instructional Staff
Meghan,Fatouros,E20105,meghan.fatouros@apsva.us,Abingdon ES,Teacher,Instructional Staff
Monica,Day,E20322,monica.day@apsva.us,Abingdon ES,Teacher,Instructional Staff
MaryClare,Moller,E20333,maryclare.moller@apsva.us,Abingdon ES,Teacher,Instructional Staff
Rachel,Nesbitt,E20666,rachel.nesbitt@apsva.us,Abingdon ES,Teacher,Instructional Staff
Andebet,Tessema,E20741,andebet.tessema@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Delmy,Reyes,E20896,delmy.reyes@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Kimberly,Johnson,E21332,kimberly.johnson@apsva.us,Abingdon ES,Teacher,Instructional Staff
Sara,Edmonds,E21479,sara.edmonds@apsva.us,Abingdon ES,Teacher,Instructional Staff
Maria,Deolazo,E21510,maria.deolazo@apsva.us,Abingdon ES,Teacher,Instructional Staff
Sirena,Butler,E21551,sirena.butler@apsva.us,Abingdon ES,Instructional Assistant,Staff
Shitaye,Yoseph,E21618,shitu.yoseph@apsva.us,Abingdon ES,Instructional Assistant,Staff
Theresa,Agyare,E22381,theresa.agyare@apsva.us,Abingdon ES,Staff,Staff
Corinne,Kramer,E22420,corinne.kramer@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Clare,Mclean,E22497,clare.mclean@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Fernando,Guillen,E22557,fernando.guillen@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Mary,Cantwell,E23671,mary.cantwell@apsva.us,Abingdon ES,Teacher,Instructional Staff
Hari,Regmi,E23865,hari.regmi@apsva.us,Abingdon ES,Custodian,Facilities Staff
Julia,Olanyk,E24111,julia.olanyk@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Sarah,Mullinax,E24253,sarah.mullinax@apsva.us,Abingdon ES,Teacher,Instructional Staff
Christina,Simpson,E24697,christina.simpson@apsva.us,Abingdon ES,Administrative Assistant,Staff
Suzanne,Szigeti,E24772,suzanne.szigeti@apsva.us,Abingdon ES,Teacher,Instructional Staff
Luz,Jarzen,E24806,luz.jarzen@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kimberlee,Gorecki,E24821,kimberlee.gorecki@apsva.us,Abingdon ES,Teacher,Instructional Staff
Amanda,Loayza Vela,E24875,amanda.loayza@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Julia,Winkler,E24903,julia.winkler@apsva.us,Abingdon ES,Teacher,Instructional Staff
Deirdre,Groh,E24951,deirdre.groh@apsva.us,Abingdon ES,Teacher,Instructional Staff
Jose,Turcios-Alvarez,E25335,jose.turciosalvarez@apsva.us,Abingdon ES,Custodian,Facilities Staff
Jennifer,Crawford,E25390,jennifer.crawford@apsva.us,Abingdon ES,Teacher,Instructional Staff
Renita,Upshur,E25722,renita.upshur@apsva.us,Abingdon ES,Teacher,Instructional Staff
Sarah,Tyson,E25780,sarah.tyson@apsva.us,Abingdon ES,Teacher,Instructional Staff
Paulette,Bunce,E25833,paulette.bunce@apsva.us,Abingdon ES,Teacher,Instructional Staff
Lynne,Lee,E25866,lynne.lee@apsva.us,Abingdon ES,Teacher,Instructional Staff
Leslie,Whiteside Elgendi,E25983,leslie.elgendi@apsva.us,Abingdon ES,Staff,Staff
Julia,Stewart,E26070,julia.stewart@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Eleanor,Splan,E26587,eleanor.splan2@apsva.us,Abingdon ES,Teacher,Instructional Staff
Naomi,Holly,E26782,naomi.holly@apsva.us,Abingdon ES,Teacher,Instructional Staff
Nicole,Jondahl,E26873,nicole.jondahl@apsva.us,Abingdon ES,Teacher,Instructional Staff
Lauren,Muscarella,E26899,lauren.muscarella@apsva.us,Abingdon ES,Teacher,Instructional Staff
DeLorenzo,Keels,E27096,delorenzo.keels@apsva.us,Abingdon ES,Staff,Staff
Tdenek,Ayalew,E27332,tdenek.ayalew@apsva.us,Abingdon ES,Staff,Staff
Olympia,Carranza,E27734,olympia.carranza@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kathryn,Coan,E28193,kathryn.coan@apsva.us,Abingdon ES,Teacher,Instructional Staff
Rocio,Villalobos,E28201,rocio.villalobos@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Vasthy,Delgado,E28515,vasthy.delgado2@apsva.us,Abingdon ES,Teacher,Instructional Staff
Wendy,Zamora Moran,E28873,wendy.zamoramoran@apsva.us,Abingdon ES,Custodian,Facilities Staff
Chelsea,Sachse,E28952,chelsea.sachse@apsva.us,Abingdon ES,Teacher,Instructional Staff
Melissa,Zandonella,E28998,melissa.zandonella@apsva.us,Abingdon ES,Teacher,Instructional Staff
Katharine,McGinn,E29085,kate.mcginn@apsva.us,Abingdon ES,Teacher,Instructional Staff
Frantzie,Cadet,E29176,frantzie.cadet@apsva.us,Abingdon ES,Teacher,Instructional Staff
Estela,Tice,E29342,estela.tice@apsva.us,Abingdon ES,Teacher,Instructional Staff
Jose,Sorto,E29512,jose.sorto@apsva.us,Abingdon ES,Custodian,Facilities Staff
Amanda,Domfe,E29520,amanda.domfe2@apsva.us,Abingdon ES,Teacher,Instructional Staff
Bianka,Snorgrass,E29817,bianka.snorgrass2@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kenneth,Cox,E29875,ken.cox@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Betzabe,Olivera,E30033,betzabe.olivera@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Giselle,Mercado,E30035,giselle.mercado@apsva.us,Abingdon ES,Staff,Staff
Elizabeth,Katz,E30105,elizabeth.katz@apsva.us,Abingdon ES,Teacher,Instructional Staff
Chelsea,Callan,E30121,chelsea.callan@apsva.us,Abingdon ES,Teacher,Instructional Staff
Victor,Spadaro,E30208,victor.spadaro@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kimberly,Stewart,E30219,kimberly.stewart@apsva.us,Abingdon ES,Teacher,Instructional Staff
Arminda,Hidalgo Acosta,E30269,arminda.hidalgoa@apsva.us,Abingdon ES,Instructional Assistant,Staff
Krishna,Patury,E30465,krishna.patury@apsva.us,Abingdon ES,Teacher,Instructional Staff
Pattaraporn,Wannalo,E30493,pattaraporn.wannalo@apsva.us,Abingdon ES,Staff,Staff
Shahenaz,Abdelbaset,E30552,shahenaz.abdelbaset@apsva.us,Abingdon ES,Instructional Assistant,Staff
Jessy,Bracamonte Peraza,E30740,jessy.bracamonte@apsva.us,Abingdon ES,Instructional Assistant,Staff
Samantha,Marra,E30797,samantha.marra@apsva.us,Abingdon ES,Teacher,Instructional Staff
Ikhtyar,Ali,E30810,ikhtyar.ali@apsva.us,Abingdon ES,Staff,Staff
Kathryn,Breen,E30931,kathryn.breen@apsva.us,Abingdon ES,Teacher,Instructional Staff
Janet,Kingsley,E31004,janet.kingsley@apsva.us,Abingdon ES,Teacher,Instructional Staff
David,Horak,E31058,david.horak@apsva.us,Abingdon ES,Principal,Unclassified
Alexis,Johnson,E31070,alexis.johnson@apsva.us,Abingdon ES,n/a,Unclassified
Betsy,Hailstone,E31136,betsy.hailstone@apsva.us,Abingdon ES,Teacher,Instructional Staff
Emily,Bennett,E31182,emily.bennett3@apsva.us,Abingdon ES,Teacher,Instructional Staff
Allison,Barnes,E31269,allison.barnes2@apsva.us,Abingdon ES,Teacher,Instructional Staff
Paige,Kearnan,E31281,paige.kearnan@apsva.us,Abingdon ES,Teacher,Instructional Staff
Boae,Kim,E31397,boae.kim@apsva.us,Abingdon ES,Teacher,Instructional Staff
Lauren,Meehan,E31413,lauren.meehan@apsva.us,Abingdon ES,Teacher,Instructional Staff
Michele,Micael,E31417,michele.micael@apsva.us,Abingdon ES,Assistant Principal,Administrative Staff
Jeremy,Blaine,E31659,jeremy.blaine@apsva.us,Abingdon ES,Teacher,Instructional Staff
Maria,Smith,E31762,maria.smith@apsva.us,Abingdon ES,Teacher,Instructional Staff
Alison,West,E31788,alison.wexelman@apsva.us,Abingdon ES,Teacher,Instructional Staff
Rashida,Jordan,E31802,rashida.jordan@apsva.us,Abingdon ES,Teacher,Instructional Staff
Brianna,Carey,E32115,brianna.carey@apsva.us,Abingdon ES,Teacher,Instructional Staff
Giovanna,Koesterer,E32116,giovanna.koesterer@apsva.us,Abingdon ES,Teacher,Instructional Staff
Emma,Carter,E32475,emma.carter@apsva.us,Abingdon ES,Staff,Staff
Rajani,Taylor,E32476,rajani.taylor@apsva.us,Abingdon ES,Staff,Staff
Meredith,Rose,E32566,meredith.rose@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kelly,Penfield,E32586,kelly.penfield@apsva.us,Abingdon ES,Teacher,Instructional Staff
Amy,Burd,E32610,amy.burd@apsva.us,Abingdon ES,Teacher,Instructional Staff
Anna,Strachan,E32611,anna.strachan@apsva.us,Abingdon ES,Teacher,Instructional Staff
Madeline,Reicherter,E32670,madeline.reicherter@apsva.us,Abingdon ES,Teacher,Instructional Staff
Jonah,Klixbull,E32706,jonah.klixbull@apsva.us,Abingdon ES,Teacher,Instructional Staff
Renee,Edwards,E32711,renee.edwards@apsva.us,Abingdon ES,Teacher,Instructional Staff
Rebecca,Madrona,E32775,rebecca.madrona@apsva.us,Abingdon ES,Teacher,Instructional Staff
Hadley,Lopez-Solis,E32820,hadley.lopezsolis@apsva.us,Abingdon ES,Instructional Assistant,Staff
Julianna,Horneman,E32948,julianna.horneman@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Jennifer,Midgley,E33007,jennifer.midgley@apsva.us,Abingdon ES,Teacher,Instructional Staff
Evonne,Wasieleski,E33048,evonne.wasieleski@apsva.us,Abingdon ES,Teacher,Instructional Staff
Lori,Willden,E33204,lori.willden@apsva.us,Abingdon ES,Teacher,Instructional Staff
AnTon,Wallace,E33213,anton.wallace@apsva.us,Abingdon ES,Staff,Staff
Anna,Richey,E33262,anna.richey@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Helen,Ryan,E33394,helen.ryan@apsva.us,Abingdon ES,Administrative Assistant,Staff
Kotraina,Roper,E33477,kotraina.roper@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Asmaa,Hossny,E33509,asmaa.hossny@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Allan,Brown,E3866,allan.brown@apsva.us,Abingdon ES,Teacher,Instructional Staff
Beata,Chambers,E4211,beata.chambers@apsva.us,Abingdon ES,Teacher,Instructional Staff
Rosemary,Duenas,E4286,rosemary.duenas@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Lorena,Reyes-Andrade,E4732,lorena.reyes@apsva.us,Abingdon ES,Teacher,Instructional Staff
Lina,Veizaga,E4735,lina.veizaga@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Lavonne,Spinner,E4998,lavonne.spinner@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Sonjia,Davis,E5534,sonjia.davis@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kim,Taylor,E5619,kim.taylor@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Kerry,Abbott,E641,kerry.abbott@apsva.us,Abingdon ES,Teacher,Instructional Staff
Marylydia,Perez,E7627,marylydia.perez@apsva.us,Abingdon ES,Teacher,Instructional Staff
Kimberly,Kerby,E7893,kimberly.kerby@apsva.us,Abingdon ES,Teacher,Instructional Staff
Jenn,Howard,E888,jenn.howard@apsva.us,Abingdon ES,Teacher,Instructional Staff
Lola,Palacios,E9071,lola.palacios@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Joe,Reed,E927,joe.reed@apsva.us,Abingdon ES,Teacher,Instructional Staff
Fily,Barrientos,E9283,fily.barrientos@apsva.us,Abingdon ES,Instructional Assistant,Instructional Staff
Michael,Collazos,E9849,michael.collazos@apsva.us,Abingdon ES,Teacher,Instructional Staff
Bobby,Lassiter,E10382,bobby.lassiter@apsva.us,Arlington Mill HS,Staff,Staff
Barbara,Thompson,E11335,barbara.thompson@apsva.us,Arlington Mill HS,Principal,Unclassified
Elizabeth,Minihan,E11806,elizabeth.minihan@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Ronald,Melkis,E1283,ronald.melkis@apsva.us,Arlington Mill HS,n/a,Unclassified
Corey,Meyers,E13640,corey.meyers@apsva.us,Arlington Mill HS,n/a,Unclassified
Manny,Sanchez,E13678,manny.sanchez@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Amy,Langrehr,E15859,amy.langrehr@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Paula,Lamina,E15918,paula.lamina@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Geraldine,Maskelony,E16060,geraldine.maskelony@apsva.us,Arlington Mill HS,n/a,Unclassified
Daniel,Pope,E1803,daniel.pope@apsva.us,Arlington Mill HS,Instructional Assistant,Instructional Staff
Teresa,Ghiglino,E19021,teresa.ghiglino@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Jody,Gilmore,E20627,jody.gilmore@apsva.us,Arlington Mill HS,n/a,Unclassified
Geraldine,Oliveto,E21561,geraldine.oliveto@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Anthony,Spanos,E21836,anthony.spanos@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Lara,Macdonald,E2217,lara.macdonald@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Maria,Rosario,E23091,maria.rosario@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Robin,Liten-Tejada,E2371,robin.litentejada@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Sharon,Solorzano,E23966,sharon.solorzano@apsva.us,Arlington Mill HS,n/a,Unclassified
Patricia,Sanguinetti,E24033,patricia.sanguinetti@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Tracy,Santasine,E24313,tracy.santasine@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Jamie,Odeneal,E25280,jamie.odeneal@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Tom,Szczerbinski,E253,tom.szczerbinski@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Natalia,Salazar,E2599,natalia.salazar@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Holly,Marvin,E26091,holly.marvin@apsva.us,Arlington Mill HS,n/a,Unclassified
Veronica,Bartlett,E26237,veronica.bartlett@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Bryan,Tubbs-Herring,E26457,bryan.tubbsherring@apsva.us,Arlington Mill HS,Staff,Staff
Heesang,Kim,E26662,heesang.kim@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Daniel,Castillo,E26664,daniel.castillo@apsva.us,Arlington Mill HS,Instructional Assistant,Instructional Staff
Yeny,Amaya Velasquez,E27189,yeny.amayavelasquez@apsva.us,Arlington Mill HS,Custodian,Facilities Staff
Tara,Colgan,E27412,tara.colgan@apsva.us,Arlington Mill HS,Staff,Staff
Mindy,Straley,E28126,mindy.straley@apsva.us,Arlington Mill HS,n/a,Unclassified
Andreina,Jimenez,E28172,andreina.salinas@apsva.us,Arlington Mill HS,Registrar,Staff
Miriam,Thomas,E29006,miriam.thomas@apsva.us,Arlington Mill HS,n/a,Unclassified
Miruna,Stanica,E29122,miruna.stanica@apsva.us,Arlington Mill HS,n/a,Unclassified
Lan,Lai,E29589,lan.lai@apsva.us,Arlington Mill HS,Custodian,Facilities Staff
Dawn,Bursk,E30031,dawn.bursk@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Joseph,Ellington,E30401,joseph.ellington@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Ariel,Arias,E30971,ariel.arias@apsva.us,Arlington Mill HS,Administrative Assistant,Staff
Kyra,Walker,E31061,kyra.walker@apsva.us,Arlington Mill HS,n/a,Unclassified
Miguel,Perez-Calleja,E32184,miguel.perez@apsva.us,Arlington Mill HS,n/a,Unclassified
Halit,Rosario,E32707,omar.rosario@apsva.us,Arlington Mill HS,n/a,Unclassified
Keshia,Henderson,E32867,keshia.henderson@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Jennifer,Hauver,E32869,jennifer.hauver@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Deisy,Estevez,E32977,deisy.estevez@apsva.us,Arlington Mill HS,Instructional Assistant,Instructional Staff
Sandra,Short,E5756,sandra.short@apsva.us,Arlington Mill HS,Administrative Assistant,Staff
Claudia,Vasquez,E6516,claudia.vasquez@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Gail,Bailey,E6966,gail.bailey@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
David,Hernandez,E7088,david.hernandez@apsva.us,Arlington Mill HS,Staff,Staff
Mirela,Geagla,E7477,mirela.geagla@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Cindy,Schall,E7621,cindy.schall@apsva.us,Arlington Mill HS,Assistant Principal,Administrative Staff
Maria,Clinger,E8549,maria.clinger@apsva.us,Arlington Mill HS,Teacher,Instructional Staff
Mary,Begley,E11187,mary.begley@apsva.us,Arlington Science Focus School,Principal,Unclassified
Jeanie,Chai,E12026,jeanie.chai@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Thomas,McGuire,E1428,thomas.mcguire@apsva.us,Arlington Science Focus School,Instructional Assistant,Instructional Staff
Janet,Freeman,E14462,janet.freeman@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Elva,Turcios,E14873,elva.turcios@apsva.us,Arlington Science Focus School,Custodian,Facilities Staff
Jennifer,Hall,E15401,jennifer.hall@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Melissa,Holston,E16156,melissa.holston@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Meg,Schryver,E16802,meg.schryver@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Olga,Almengor,E19026,olga.almengor@apsva.us,Arlington Science Focus School,Maintenance Supervisor,Facilities Staff
Barbara,Jones,E19551,barbara.jones@apsva.us,Arlington Science Focus School,Assistant Principal,Administrative Staff
Kathryn,Kaiser,E19859,kathryn.kaiser@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Tulio,Marin,E19985,tulio.marin@apsva.us,Arlington Science Focus School,Custodian,Facilities Staff
Cassi,Leonard,E20380,cassi.leonard@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Stephanie,Lin,E21463,stephanie.lin@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Sydney,Lewis,E22080,sydney.lewis@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Abby,Tolkan,E22213,abby.tolkan@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Jarvin,Winger,E22384,jarvin.winger@apsva.us,Arlington Science Focus School,Instructional Assistant,Instructional Staff
Bobbie,Fullen,E22968,bobbie.fullen@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Megan,Todd,E23009,megan.todd@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Tradicia,Greene,E23218,tradicia.greene@apsva.us,Arlington Science Focus School,Instructional Assistant,Instructional Staff
Kelly,Kallassy,E24537,kelly.kallassy@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Selam,Negash,E24886,selam.negash@apsva.us,Arlington Science Focus School,Instructional Assistant,Instructional Staff
Rebecca,Strong,E25906,rebecca.strong@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Halee,Wilson,E25915,halee.wilson@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Brittany,Oman,E26766,brittany.oman@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Lisa,Gallagher,E26807,lisa.gallagher@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Emily,Foster,E26888,emily.foster@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Beth,Hooser,E26955,beth.hooser@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Emily,Currie,E26968,emily.currie@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Meskerem,Bekele,E27214,meskerem.bekele@apsva.us,Arlington Science Focus School,Staff,Staff
Fatima,Laamiri,E27294,fatima.laamiri@apsva.us,Arlington Science Focus School,Instructional Assistant,Staff
Janis,James-Northover,E27443,janis.jamesnorthover@apsva.us,Arlington Science Focus School,Staff,Staff
Omayma,Ghabosh,E27621,omayma.ghabosh@apsva.us,Arlington Science Focus School,Instructional Assistant,Staff
Sarah,Kiel,E27747,sarah.kiel@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Hannah,Odachowski,E27826,hannah.odachowski@apsva.us,Arlington Science Focus School,Instructional Assistant,Instructional Staff
Lori,Mintzer,E29179,lori.mintzer@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Nicole,Brown,E29449,nicole.brown@apsva.us,Arlington Science Focus School,Staff,Staff
Tayech,Meseret,E29489,tayech.meseret@apsva.us,Arlington Science Focus School,Instructional Assistant,Staff
Kathleen,Maxey,E29973,kathleen.maxey@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Olivia,Anderson,E30047,olivia.anderson@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Erika,Dixon,E30098,erika.dixon@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Sarah,Hansrote,E30111,sarah.hansrote@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Aileen,Mavity,E30113,aileen.mavity@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Olivia,Proietti,E30130,olivia.proietti@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Melissa,Idell,E30145,melissa.idell@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Michelle,Shine,E30194,michelle.shine@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Darnicha,Hopwood,E30497,darnicha.hopwood@apsva.us,Arlington Science Focus School,Instructional Assistant,Instructional Staff
Jazmynn,Wilson,E31074,jazmynn.wilson@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Raphael,Villacrusis,E31097,raphael.villacrusis@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Stacia,Griffin,E31175,stacia.griffin@apsva.us,Arlington Science Focus School,Administrative Assistant,Staff
Christian,Weddle,E31606,christian.weddle@apsva.us,Arlington Science Focus School,Staff,Staff
Shavon,Moore,E31696,shavon.moore@apsva.us,Arlington Science Focus School,Staff,Staff
Michael,Bosse,E32054,michael.bosse@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Matthew,McAndrew,E32083,matthew.mcandrew@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Atasha,Jackson,E32229,atasha.jackson@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Ryan,Coates,E32381,ryan.coates@apsva.us,Arlington Science Focus School,Custodian,Facilities Staff
Alison,Yeich,E32585,alison.yeich@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Rachael,Roberts,E32644,rachael.roberts@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Paula,Todd,E32672,paula.todd@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Elizabeth,Uffelman,E32763,elizabeth.uffelman@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Katherine,Wiberg,E32774,katherine.wiberg@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Kyle,McCarthy,E32889,kyle.mccarthy@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Ann,Cadiz Rivera,E32912,ann.cadizrivera@apsva.us,Arlington Science Focus School,Administrative Assistant,Staff
Adena,Porter,E32946,adena.porter@apsva.us,Arlington Science Focus School,Instructional Assistant,Staff
Davaadorj,Danzannyam,E32951,davaadorj.danzannyam@apsva.us,Arlington Science Focus School,Instructional Assistant,Staff
Janice,O'Day,E33059,janice.oday@apsva.us,Arlington Science Focus School,Staff,Staff
Kelly,Kniseley,E4936,kelly.kniseley@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Danielle,Anctil,E536,danielle.anctil@apsva.us,Arlington Science Focus School,Teacher,Instructional Staff
Seseg,Frankosky,E5417,seseg.frankosky@apsva.us,Arlington Science Focus School,Instructional Assistant,Instructional Staff
Teresa,Smiroldo,E5457,teresa.smiroldo@apsva.us,Arlington Science Focus School,Administrative Assistant,Staff
Teresa,Short,E7964,teresa.short@apsva.us,Arlington Science Focus School,Instructional Assistant,Instructional Staff
Chris,Jones,E1113,chris.jones@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Patricia,Demsky,E11188,patricia.demsky@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Liz,Gephardt,E11328,liz.gephardt@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Ellen,Hemstreet,E12243,ellen.hemstreet@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Allison,Gordon,E13273,allison.gordon@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Amy,Slavin,E13791,amy.slavin@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Marta,Arias,E14882,marta.arias@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Helene,Stainback,E1551,helene.stainback@apsva.us,Ashlawn ES,Administrative Assistant,Staff
Kousar,Chatha,E15520,kousar.chatha@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Mamit,Gebreegziabher,E15632,mamit.gebreegziabher@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Holly,Fischer,E15715,holly.fischer@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Kelly,Adams,E15850,kelly.adams@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Mignon,Dwyer,E15956,mignon.dwyer@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Dina,Pham,E16078,dina.pham@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Cindy,Skinner,E1611,cindy.skinner@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Sulma,Diaz,E16224,sulma.diaz@apsva.us,Ashlawn ES,Custodian,Facilities Staff
Alison,Sheahan,E16276,alison.sheahan@apsva.us,Ashlawn ES,Staff,Staff
Sean,Kennedy,E16761,sean.kennedy@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Emily Small,Ward,E16906,emily.small@apsva.us,Ashlawn ES,Administrative Assistant,Staff
Wendy,Ellison,E175,wendy.ellison@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Victoria,Paris,E19163,victoria.paris@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Elizabeth,Howard,E19265,beth.howard@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Celia,Arnade,E193,celia.arnade@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Cuc,Sheets,E19773,cuc.sheets@apsva.us,Ashlawn ES,Staff,Staff
Latasha,Reaves,E19902,latasha.reaves@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Jamie,Daly,E20035,jamie.daly@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Margaret,Ehlers,E20265,margaret.ehlers@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Hannah,Magnolia,E20447,hannah.magnolia@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Amy,Fong,E20483,amy.fong@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Maria,White,E20542,maria.white@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Leslie,Argueta,E20596,leslie.argueta@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Abida,Adhi,E20715,abida.adhi@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Shannon,Brady,E20960,shannon.brady@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Lindsay,Cronin,E21320,lindsay.cronin@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Jennifer,Ellington,E21420,jennifer.ellington@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Meghan,Neary,E21462,meghan.neary@apsva.us,Ashlawn ES,"Assistant Principal, Principal",Administrative Staff
Lauren,Walsh,E21606,lauren.walsh@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Jaim,Foster,E22045,jaim.foster@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Dana,Crepeau,E2250,dana.crepeau@apsva.us,Ashlawn ES,n/a,Unclassified
Milagros,Jimenez,E22501,milagros.jimenez@apsva.us,Ashlawn ES,Teacher,Instructional Staff
June,Prakash,E22511,june.prakash@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Jeremy,Strohl,E22851,jeremy.strohl@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Dana,Dougherty,E23104,dana.dougherty@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Manjit,Chase,E23233,manjit.chase@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Suzanne,Boothby,E23448,suzanne.boothby@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Antonio,Metts,E23510,antonio.metts@apsva.us,Ashlawn ES,Custodian,Facilities Staff
Kevin,Hjelm,E24764,kevin.hjelm@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Katie,Avila,E24914,katie.avila@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Susan,DeMatties,E24948,susan.dematties@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Christopher,Bangs,E25030,christopher.bangs@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Nghia,Luu,E25474,nghia.luu@apsva.us,Ashlawn ES,Custodian,Facilities Staff
Patrick,Carmack,E25753,patrick.carmack@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Jason,Mastaler,E25791,jason.mastaler@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Maria,Morales Nebreda,E25857,maria.moralesnebreda@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Patricia,Butler,E25936,patricia.butler@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Vanessa,Sanchez,E26114,vanessa.sanchez@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Jaime,Taylor,E26240,jaime.taylor@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Johanna,Garay,E26464,johanna.garay2@apsva.us,Ashlawn ES,Staff,Staff
Erik,Kamenski,E26775,erik.kamenski@apsva.us,Ashlawn ES,n/a,Unclassified
Amelva,Spaine,E27251,amelva.spaine@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Paul,Pavelich,E27321,paul.pavelich@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Christian,Rios Bustos,E27329,christian.riosbustos@apsva.us,Ashlawn ES,Staff,Staff
Alexis,Carrillo,E27466,alexis.carrillo@apsva.us,Ashlawn ES,Staff,Staff
Ismail,El Boukri,E27486,ismail.elboukri@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Donna,Bertsch,E27844,donna.bertsch@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Christina,Stever,E27986,christina.stever@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Cristela,Luna,E28112,cristela.luna@apsva.us,Ashlawn ES,Principals Assistant,Staff
Nancy,Hawkins,E2813,nancy.hawkins@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Margaret,Bleicher,E28426,margaret.bleicher@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Montana,Wrigley,E28437,montana.wrigley2@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Kristi,Henshaw,E28558,kristi.henshaw@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Charlotte,Hoffer,E28945,charlotte.hoffer@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Emily,Lingenfelter,E28977,emily.lingenfelter@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Ofelia,Oronoz,E29065,ofelia.oronoz@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Isabelle,Isakson,E29112,isabelle.isakson@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Michelle,Grant,E29154,michelle.grant@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Rachel,Walsh,E29199,rachel.walsh@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Wendy,Ward,E29392,wendy.ward2@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Naudia,Key,E29423,naudia.key@apsva.us,Ashlawn ES,Staff,Staff
Timeka,Eskridge,E29490,timeka.eskridge@apsva.us,Ashlawn ES,Staff,Staff
Mary,McDonnell,E30353,mary.mcdonnell@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Ryan,Dallas,E30370,ryan.dallas@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Theresa,Vickers,E30400,theresa.vickers@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Elizabeth,Granny,E30608,elizabeth.granny@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Madeline,Long,E30731,madeline.long@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Norma,Argueta,E31146,norma.argueta@apsva.us,Ashlawn ES,Custodian,Facilities Staff
Alex,Baldwin,E31286,alex.baldwin@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Nibal,Hammoudeh,E31471,nibal.hammoudeh@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Liseth,Gonzalez,E31770,liseth.gonzalez@apsva.us,Ashlawn ES,Maintenance Supervisor,Facilities Staff
Jacqueline,Berrios,E31971,jacqueline.berrios@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Joshua,Readshaw,E32032,joshua.readshaw@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Deanna,Bell,E32300,deanna.bell@apsva.us,Ashlawn ES,Staff,Staff
Ivonne,Velazquez Pulido,E32308,ivonne.velazquez@apsva.us,Ashlawn ES,Staff,Staff
Danyelle,Dyson,E32332,danyelle.dyson@apsva.us,Ashlawn ES,Staff,Staff
Jessica,Harmon,E32568,jessica.harmon@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Laura,Zielinski,E32646,laura.zielinski@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Kanokorn,Gunyon,E32810,kanokorn.gunyon@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Amira,Damrani,E32914,amira.damrani@apsva.us,Ashlawn ES,Staff,Staff
Zahra,Qodia,E33443,zahra.qodia@apsva.us,Ashlawn ES,Staff,Staff
Eric,Sokolove,E396,eric.sokolove@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Lisa,Craddock,E4140,lisa.craddock@apsva.us,Ashlawn ES,Staff,Staff
Anjanette,Brooks,E4146,anjanette.brooks@apsva.us,Ashlawn ES,Staff,Staff
Aundrea,Moore,E4217,aundrea.moore@apsva.us,Ashlawn ES,Administrative Assistant,Staff
Brenda,Rios,E5988,brenda.durand@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Terry,Hill,E8268,terry.hill@apsva.us,Ashlawn ES,Instructional Assistant,Instructional Staff
Lang,Tien,E8273,lang.tien@apsva.us,Ashlawn ES,Custodian,Facilities Staff
Megan,Bransford,E9206,megan.bransford@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Kimberly,Klaus,E9860,kimberly.klaus@apsva.us,Ashlawn ES,Teacher,Instructional Staff
Donald,Martin,E10496,donald.martin@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Mike,Krage,E11928,mike.krage@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Veronica,Perez Perea,E15235,veronica.perez@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Monica,Hirschberg,E1556,monica.hirschberg@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Tiffani,Ker,E15727,tiffani.ker@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Shaunice,Henderson,E15927,shaunice.henderson@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Toni,Wexler,E16708,toni.wexler@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Courtney,Riley,E19352,courtney.riley@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Janette,Mason,E19520,janette.mason@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Heather,Blake,E19566,heather.blake@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Amanuel,Haile,E19589,amanuel.haile@apsva.us,Arlington Traditional School,Custodian,Facilities Staff
Ariel,Blessed,E19812,ariel.blessed@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Allison,Nowak,E20042,allison.nowak@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Antonelly,Bradiel,E20116,antonelly.bradiel@apsva.us,Arlington Traditional School,Instructional Assistant,Staff
Anna,Barba,E21250,anna.barba@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Wala,Elamin,E21537,wala.elamin@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Allison,Ransom,E21560,allison.ransom@apsva.us,Arlington Traditional School,Staff,Staff
Rasheeda,Bridgeforth,E21665,rasheeda.bridgeforth@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Jennifer,Messman,E21973,jen.messman@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Jill,Congelio,E22007,jill.congelio@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Stephanie,Byrne,E22217,stephanie.byrne@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Tori,Blyler,E23110,tori.blyler@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Marcus,Holladay,E23164,marcus.holladay@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Melissa,Blumenthal,E23479,melissa.blumenthal@apsva.us,Arlington Traditional School,Administrative Assistant,Staff
Caitlin,Franz,E23779,caitlin.franz@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Miriam,Capellan,E23986,miriam.capellan@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Wafa,Ahmed,E24323,wafa.ahmed@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Lora,Shelly,E24424,lora.shelly@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Elizabeth,Snead,E2445,elizabeth.snead@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Victoria,Metz,E2462,victoria.metz@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Amy,Parente,E24970,amy.parente@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Benjamin,Greenwell,E25504,benjamin.greenwell@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Lynna,Lam,E25582,lynna.lam@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Chloe,Ferogh,E25708,chloe.ferogh@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Holly,Beglinger,E26529,holly.beglinger2@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Maria,Barreda,E26879,maria.barreda@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Nathan,Pipke,E27005,nathan.pipke@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Kathryn,Peterson,E27067,kathryn.peterson@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Michael,Rumerman,E27225,michael.rumerman@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Karen,Lewis,E27446,karen.lewis2@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Joselin,Carballo,E27771,joselin.carballo@apsva.us,Arlington Traditional School,Administrative Assistant,Staff
Amber,Mollet,E27851,amber.mollet@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Sarah,McReynolds,E27854,sarah.mcreynolds@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Antonina,Wilson,E28017,antonina.wilson@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Kidist,Feseha,E28081,kidist.feseha@apsva.us,Arlington Traditional School,Instructional Assistant,Staff
Nicole,Davies,E28096,nicole.davies@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Gina,Memmott,E28138,gina.memmott@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Sheila,Skipper,E28253,sheila.skipper@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Nellie,Earley,E28455,nellie.earley2@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Rodolfo,Barrientos,E28561,rodolfo.barrientos@apsva.us,Arlington Traditional School,Instructional Assistant,Staff
Nahdeya,Quarles,E28777,nahdeya.quarles@apsva.us,Arlington Traditional School,Staff,Staff
Julia,McCoy,E28795,julia.mccoy2@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Inga,Schoenbrun,E29178,inga.schoenbrun@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Sandra,Ormeno,E29180,sandra.ormeno@apsva.us,Arlington Traditional School,Administrative Assistant,Staff
Kathy,Patino,E29471,kathy.patino@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
David,Butala,E29688,david.butala@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Christy,Swain,E3047,christy.swain@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Diana,Lee,E30506,diana.lee2@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Adity,Barua,E30525,adity.barua@apsva.us,Arlington Traditional School,Instructional Assistant,Staff
Heather,Stocking,E30617,heather.stocking@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Alexis,Marsella,E30935,alexis.marsella2@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Carol,Halvorson,E31272,carol.halvorson@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Asnakech,Worku-Kereta,E31544,asnakech.workukereta@apsva.us,Arlington Traditional School,Instructional Assistant,Staff
Hebatallah,Nada,E31719,hebatallah.nada@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Marissa,Works,E32033,marissa.works@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Maureen,Boucher,E32039,maureen.boucher@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Alexandra,Cozell,E32052,alexandra.cozell@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Elizabeth,Cornwell,E32121,elizabeth.cornwell@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Elizabeth,Meydenbauer,E32288,e.meydenbauer2@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Eunice,Drayton,E32327,eunice.drayton@apsva.us,Arlington Traditional School,Custodian,Facilities Staff
Jennifer,Leach,E32460,jennifer.leach@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Jennifer,Snider,E32539,jennifer.snider2@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Jenna,Love,E32609,jenna.love@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Melissa,Szymanski,E32621,melissa.szymanski@apsva.us,Arlington Traditional School,n/a,Unclassified
Helen,Tesfahunegn,E32630,helen.tesfahunegn@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Abigail,Willard,E32669,abigail.willard@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Elizabeth,Maranto,E32693,elizabeth.maranto@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Hana,Epstein,E32755,hana.epstein@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Theresa,Chapman,E3301,theresa.chapman@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Haggar,Kodua,E33113,haggar.kodua@apsva.us,Arlington Traditional School,Custodian,Facilities Staff
Kevin,Barnes,E33585,kevin.barnes@apsva.us,Arlington Traditional School,Custodian,Facilities Staff
Ashley,Berger,E4101,ashley.berger@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Cynthia,Long,E4293,cynthia.long@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Amara,Chin,E4793,amara.chin@apsva.us,Arlington Traditional School,Maintenance Supervisor,Facilities Staff
Cassandra,Bates,E4918,cassandra.bates@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Rista,Brown,E5194,rista.brown@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Rex,Godwin,E6275,rex.godwin@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Pilar,Friedman,E6447,pilar.friedman@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Barbara,Spalding-Campbell,E6898,barbara.spalding@apsva.us,Arlington Traditional School,Administrative Assistant,Staff
Terry,Dwyer,E732,terry.dwyer@apsva.us,Arlington Traditional School,Teacher,Instructional Staff
Holly,Hawthorne,E8107,holly.hawthorne@apsva.us,Arlington Traditional School,Principal,Unclassified
Todd,Mcdaniel,E8865,todd.mcdaniel@apsva.us,Arlington Traditional School,Custodian,Facilities Staff
Matthew,Carey,E8948,matthew.carey@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Lisa,Payne,E8997,lisa.payne@apsva.us,Arlington Traditional School,Administrative Assistant,Staff
Sileap,Sok,E9689,sileap.sok@apsva.us,Arlington Traditional School,Instructional Assistant,Instructional Staff
Julia,Sondheim,E10410,julia.sondheim@apsva.us,Barcroft ES,Teacher,Instructional Staff
Krystal,Spraggins,E10610,krystal.spraggins@apsva.us,Barcroft ES,Teacher,Instructional Staff
Marel,Sitron,E11669,marel.sitron@apsva.us,Barcroft ES,Teacher,Instructional Staff
Cynthia,Edwards,E12032,cynthia.edwards@apsva.us,Barcroft ES,Staff,Staff
Myrel,Umila,E145,myrel.umila@apsva.us,Barcroft ES,Teacher,Instructional Staff
Melinda,Metz,E14794,melinda.metz@apsva.us,Barcroft ES,Teacher,Instructional Staff
Aissata,Diallo,E15500,aissata.diallo@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Loren,Valcourt,E15802,loren.valcourt@apsva.us,Barcroft ES,Teacher,Instructional Staff
Asmeret,Tesfai,E15880,asmeret.tesfai@apsva.us,Barcroft ES,Staff,Staff
Susan,Spranger,E1672,susan.spranger@apsva.us,Barcroft ES,Teacher,Instructional Staff
Ana,Soto,E17124,ana.soto@apsva.us,Barcroft ES,Staff,Staff
Jacqueline,Pippins,E21114,jacqueline.pippins@apsva.us,Barcroft ES,Teacher,Instructional Staff
Thea,Aldrich,E21186,thea.aldrich@apsva.us,Barcroft ES,Teacher,Instructional Staff
Grayson,Contreras,E21530,grayson.romero@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Nicole,Fischer,E22123,nicole.fischer@apsva.us,Barcroft ES,Teacher,Instructional Staff
Katherine,Lopez,E22206,katherine.lopez@apsva.us,Barcroft ES,Teacher,Instructional Staff
Cesar,Celis,E22214,cesar.celis@apsva.us,Barcroft ES,Principals Assistant,Staff
Janet,Dorn,E22473,janet.dorn@apsva.us,Barcroft ES,Teacher,Instructional Staff
Margaret,Nice,E22790,margaret.nice@apsva.us,Barcroft ES,Teacher,Instructional Staff
Tasean,Monroe,E23688,tasean.monroe@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Margaret,Batten,E23895,margaret.batten@apsva.us,Barcroft ES,Teacher,Instructional Staff
Kathryn,Smith,E23913,kathryn.smith@apsva.us,Barcroft ES,Teacher,Instructional Staff
Vonda,Ward,E24007,vonda.ward@apsva.us,Barcroft ES,Teacher,Instructional Staff
Maribel,Vilela,E24177,maribel.vilela@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Margolinda,Bonilla,E24226,margolinda.bonilla@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Niva,Barua,E24259,niva.barua@apsva.us,Barcroft ES,Staff,Staff
Candice,Barrett,E24599,candice.barrett@apsva.us,Barcroft ES,Teacher,Instructional Staff
Jennifer,Everdale,E24635,jennifer.everdale@apsva.us,Barcroft ES,Teacher,Instructional Staff
David,Groh,E24837,david.groh@apsva.us,Barcroft ES,Teacher,Instructional Staff
Michele,Hamblin,E24848,michele.hamblin@apsva.us,Barcroft ES,Teacher,Instructional Staff
Jennifer,Steinhoff,E24906,jennifer.steinhoff@apsva.us,Barcroft ES,Teacher,Instructional Staff
Leslie,Jones,E25149,leslie.jones@apsva.us,Barcroft ES,Staff,Staff
Katherine,Cicala,E25302,katherine.cicala@apsva.us,Barcroft ES,Teacher,Instructional Staff
Elizabeth,Mejia,E25724,elizabeth.mejia@apsva.us,Barcroft ES,Teacher,Instructional Staff
Denice,Arroyo,E25763,denice.arroyo@apsva.us,Barcroft ES,Teacher,Instructional Staff
Holly,Marvin,E26091,holly.marvin@apsva.us,Barcroft ES,Teacher,Instructional Staff
Jennifer,Klapper,E26141,jennifer.klapper@apsva.us,Barcroft ES,Teacher,Instructional Staff
Johanna,Garay,E26464,johanna.garay2@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Jamie,Osmulski,E26738,jamie.osmulski@apsva.us,Barcroft ES,Teacher,Instructional Staff
Gabriela,Rivas,E26866,gabriela.rivas@apsva.us,Barcroft ES,Assistant Principal,Administrative Staff
Jeanell,Pearson,E27027,jeanell.pearson@apsva.us,Barcroft ES,Staff,Staff
Betelehem,Abera,E27048,betelehem.abera2@apsva.us,Barcroft ES,Staff,Staff
Luisa,Echevarria,E27088,luisa.echevarria@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Mariam,Abdelgadir,E27255,mariam.abdelgadir@apsva.us,Barcroft ES,Staff,Staff
Ellie,Ellison,E27642,ellie.ellison@apsva.us,Barcroft ES,Teacher,Instructional Staff
Senait,Meskel,E27877,senait.meskel@apsva.us,Barcroft ES,Staff,Staff
Elizabeth,Aspacio,E27956,elizabeth.aspacio@apsva.us,Barcroft ES,Teacher,Instructional Staff
Shawniquawn,Jackson,E28026,shawniquawn.jackson@apsva.us,Barcroft ES,Staff,Staff
Senayit,Mekonnen,E28113,senayit.mekonnen@apsva.us,Barcroft ES,Staff,Staff
Clarence,Hooper,E28246,clarence.hooper@apsva.us,Barcroft ES,Custodian,Facilities Staff
Amanda,Erra,E28984,amanda.erra@apsva.us,Barcroft ES,Teacher,Instructional Staff
Kia,Kinsler,E29240,kia.kinsler@apsva.us,Barcroft ES,Teacher,Instructional Staff
Alicia,Cherry,E2972,alicia.cherry@apsva.us,Barcroft ES,Teacher,Instructional Staff
Zachary,Norrbom,E30078,zachary.norrbom@apsva.us,Barcroft ES,Teacher,Instructional Staff
Angela,Estay,E30088,angela.estay@apsva.us,Barcroft ES,Teacher,Instructional Staff
Amanda,Cornacchio,E30146,amanda.cornacchio@apsva.us,Barcroft ES,Teacher,Instructional Staff
Stephanie,Dinnen,E30224,stephanie.dinnen@apsva.us,Barcroft ES,Teacher,Instructional Staff
Christian,Barrera,E30260,christian.barrera@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Ambreen,Kiani,E30286,ambreen.kiani@apsva.us,Barcroft ES,Staff,Staff
Savannah,Stapley,E30557,savannah.stapley@apsva.us,Barcroft ES,Teacher,Instructional Staff
Reeann,Aune,E30560,reeann.aune@apsva.us,Barcroft ES,Teacher,Instructional Staff
Laura,Londono Iza,E30626,laura.londonoiza@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Nawal,Tawfik,E30968,nawal.tawfik@apsva.us,Barcroft ES,Staff,Staff
Ann,Gwynn,E31139,ann.gwynn@apsva.us,Barcroft ES,Teacher,Instructional Staff
Tracy,Louis-Charles,E31141,tracy.louischarles@apsva.us,Barcroft ES,Teacher,Instructional Staff
Terri,Shonerd,E31174,terri.shonerd@apsva.us,Barcroft ES,Teacher,Instructional Staff
Khawla,Bougarfaoui,E31542,khawla.bougarfaoui@apsva.us,Barcroft ES,Staff,Staff
Faiza,Moutamtia,E31750,faiza.moutamtia@apsva.us,Barcroft ES,Staff,Staff
Juan,Ribera Mendoza,E31879,juan.riberamendoza@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Ashlee,Demastus,E32073,ashlee.demastus2@apsva.us,Barcroft ES,Teacher,Instructional Staff
Christie,Labrie,E32146,christie.labrie@apsva.us,Barcroft ES,Teacher,Instructional Staff
Marie-Francoise,Vivarelli,E32239,marie.vivarelli@apsva.us,Barcroft ES,Teacher,Instructional Staff
Theresa,Basoah,E32311,theresa.basoah@apsva.us,Barcroft ES,Staff,Staff
Bernadette,Snead,E32453,bernadette.snead@apsva.us,Barcroft ES,Teacher,Instructional Staff
Josi,Rogers,E32508,josi.rogers@apsva.us,Barcroft ES,Teacher,Instructional Staff
Alexandra,McKenrick,E32620,alexandra.mckenrick@apsva.us,Barcroft ES,Teacher,Instructional Staff
Max,Adamo,E32680,max.adamo2@apsva.us,Barcroft ES,Teacher,Instructional Staff
Raymond,Murphey,E32760,raymond.murphey@apsva.us,Barcroft ES,Teacher,Instructional Staff
Elizabeth,Uffelman,E32763,elizabeth.uffelman@apsva.us,Barcroft ES,Teacher,Instructional Staff
Denise,Brown,E3282,denise.brown@apsva.us,Barcroft ES,Teacher,Instructional Staff
Fatiha,Mansouri,E32976,fatiha.mansouri@apsva.us,Barcroft ES,Staff,Staff
Soudalath,Phomsopha,E32981,soudalath.phomsopha@apsva.us,Barcroft ES,Staff,Staff
Chaymaa,Yassine,E32993,chaymaa.yassine@apsva.us,Barcroft ES,Staff,Staff
Sanae,Bakhti,E33295,sanae.bakhti@apsva.us,Barcroft ES,Staff,Staff
Soraida,Ferrel Mordagon De Sejas,E33482,soraida.ferrel@apsva.us,Barcroft ES,Staff,Staff
Ghizlane,Lemtai,E33520,ghizlane.lemtai@apsva.us,Barcroft ES,Staff,Staff
Diana,Nogales Borda,E33545,diana.nogalesborda@apsva.us,Barcroft ES,Staff,Staff
Judy,Apostolico-Buck,E4239,judy.apostolicobuck@apsva.us,Barcroft ES,Principal,Unclassified
James,Hainer-Violand,E4515,james.hainer@apsva.us,Barcroft ES,Teacher,Instructional Staff
Maria,Contreras,E4972,maria.contreras@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Allison,Andrews,E5075,allison.andrews@apsva.us,Barcroft ES,Teacher,Instructional Staff
Daysi,Rivera,E5376,daysi.blanco@apsva.us,Barcroft ES,Teacher,Instructional Staff
Marina,Benitez,E6702,marina.benitez@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Lisa,Jackson,E7032,lisa.jackson@apsva.us,Barcroft ES,Teacher,Instructional Staff
Karla,Mora,E7313,karla.mora@apsva.us,Barcroft ES,Administrative Assistant,Staff
Orlando,Garcia,E7721,orlando.garcia@apsva.us,Barcroft ES,Administrative Assistant,Staff
Susan Holland,Cottrell-Holland,E7863,susan.cottrell@apsva.us,Barcroft ES,Teacher,Instructional Staff
Quan,Tran,E7899,quan.tran@apsva.us,Barcroft ES,Custodian,Facilities Staff
Joseph,Preko,E7907,joseph.preko@apsva.us,Barcroft ES,Custodian,Facilities Staff
Tammy,Nelson,E8376,tammy.nelson@apsva.us,Barcroft ES,Maintenance Supervisor,Facilities Staff
Rosario,Rodriguez,E8388,rosario.rodriguez@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Krista,Bouton,E850,krista.bouton@apsva.us,Barcroft ES,Teacher,Instructional Staff
Elsie,Kuncar,E9503,elsie.kuncar@apsva.us,Barcroft ES,Teacher,Instructional Staff
Nury,Castillo-Zelaya,E9900,nury.castillo@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Fatima,Remadan,E9967,fatima.remadan@apsva.us,Barcroft ES,Instructional Assistant,Instructional Staff
Yasmin,Escalante,E10001,yasmin.escalante@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Rodolfo,Rivas,E10162,rodolfo.rivas@apsva.us,Barrett ES,Custodian,Facilities Staff
Andrea,Donovan,E11240,andrea.donovan@apsva.us,Barrett ES,Teacher,Instructional Staff
Tonique,Mason,E11372,tonique.mason@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Maria,Kaplan,E1197,maria.kaplan@apsva.us,Barrett ES,"Administrative Assistant, Instructional Assistant",Instructional Staff
Martha,Miranda,E12202,martha.miranda@apsva.us,Barrett ES,Teacher,Instructional Staff
Cristina,Torres,E12824,cristina.torres@apsva.us,Barrett ES,Teacher,Instructional Staff
Cindy,McGonigle,E12989,cindy.mcgonigle@apsva.us,Barrett ES,Teacher,Instructional Staff
Connie,Quinzio,E1342,connie.quinzio@apsva.us,Barrett ES,Staff,Staff
Laurie,Sullivan,E13918,laurie.sullivan@apsva.us,Barrett ES,Teacher,Instructional Staff
Angela,Settles,E14841,angela.settles@apsva.us,Barrett ES,Teacher,Instructional Staff
Jonathan,Velsey,E14850,jonathan.velsey@apsva.us,Barrett ES,n/a,Unclassified
Blanca,Castillo,E15575,blanca.castillo@apsva.us,Barrett ES,Custodian,Facilities Staff
Sandra,Espinoza,E15591,sandra.espinoza@apsva.us,Barrett ES,Administrative Assistant,Staff
Elizabeth,Sullivan,E16315,elizabeth.rente@apsva.us,Barrett ES,Teacher,Instructional Staff
Asli,Hundessa,E16633,asli.hundessa@apsva.us,Barrett ES,Administrative Assistant,Staff
Marissa,Mulholland,E16757,marissa.mulholland@apsva.us,Barrett ES,Teacher,Instructional Staff
Jake,Ramsay,E16943,jake.ramsay@apsva.us,Barrett ES,Teacher,Instructional Staff
Johnw,Stewart,E17093,johnw.stewart@apsva.us,Barrett ES,Teacher,Instructional Staff
Mariacarmen,Enriquez,E1754,mariacarmen.enriquez@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Gregory,D'Addario,E19207,greg.daddario@apsva.us,Barrett ES,Teacher,Instructional Staff
Suzanne,Butler,E19340,suzanne.butler@apsva.us,Barrett ES,Teacher,Instructional Staff
Stephanie,Matadial,E19363,stephanie.matadial@apsva.us,Barrett ES,Principals Assistant,Staff
Elizabeth,Halligan,E19489,elizabeth.halligan@apsva.us,Barrett ES,Teacher,Instructional Staff
Julie,Kaufman,E19506,julie.kaufman@apsva.us,Barrett ES,Teacher,Instructional Staff
Arturo,Ramirez,E19705,arturo.ramirez@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Gabby,Guadamuz,E19889,gabby.guadamuz@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Zoila,Suria,E20073,zoila.suria@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Michelle,Jackson,E20111,michelle.jackson@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Nancy,Hensley,E20293,nancy.hensley@apsva.us,Barrett ES,Teacher,Instructional Staff
Kenyon,Spann,E20374,kenyon.spann@apsva.us,Barrett ES,Teacher,Instructional Staff
Nancy,Costello,E20530,nancy.costello@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Lauren,Smith Janzen,E20544,lauren.janzen@apsva.us,Barrett ES,Teacher,Instructional Staff
Rosalba,Lopez,E20793,rosalba.lopez@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Christina,Torres,E20856,christina.torres@apsva.us,Barrett ES,Teacher,Instructional Staff
Meaghan,Heiges,E2099,meaghan.heiges@apsva.us,Barrett ES,Teacher,Instructional Staff
Kevin,Wege,E21413,kevin.wege@apsva.us,Barrett ES,Teacher,Instructional Staff
Jeannie,Landgraf,E21503,jeannie.landgraf@apsva.us,Barrett ES,Teacher,Instructional Staff
Michael,Kennedy,E22252,michael.kennedy@apsva.us,Barrett ES,Teacher,Instructional Staff
Ashley,Giglio,E22803,ashley.giglio@apsva.us,Barrett ES,Teacher,Instructional Staff
Hilary,Sigal,E22925,hilary.sigal@apsva.us,Barrett ES,Teacher,Instructional Staff
Grecia,Machicado,E23201,grecia.machicado@apsva.us,Barrett ES,Teacher,Instructional Staff
Margaret,Ayers,E23208,margaret.ayers@apsva.us,Barrett ES,Teacher,Instructional Staff
Zachary,Porter,E23452,zachary.porter@apsva.us,Barrett ES,Teacher,Instructional Staff
Elaine,Fargo,E23502,elaine.fargo@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Christina,Jeffers,E23544,christina.jeffers@apsva.us,Barrett ES,Teacher,Instructional Staff
Allison,Platz,E23910,allison.platz@apsva.us,Barrett ES,Teacher,Instructional Staff
Heather,Weir,E23943,heather.weir@apsva.us,Barrett ES,Teacher,Instructional Staff
Anastasia,Romero,E23989,anastasia.romero@apsva.us,Barrett ES,Teacher,Instructional Staff
Lillian,Wallace,E24412,lillian.wallace@apsva.us,Barrett ES,Staff,Staff
Emily,Maldonado,E24805,emily.maldonado@apsva.us,Barrett ES,Teacher,Instructional Staff
Abigail,Crain,E25139,abigail.crain@apsva.us,Barrett ES,Teacher,Instructional Staff
Diana,Bustamante Osorio,E25276,diana.osorio@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Jessica,Idol,E25678,jessica.idol@apsva.us,Barrett ES,Teacher,Instructional Staff
Amanda,Thiel,E25818,amanda.thiel@apsva.us,Barrett ES,Teacher,Instructional Staff
Erin,Cassedy,E25822,erin.cassedy@apsva.us,Barrett ES,Teacher,Instructional Staff
Morgan,Welsh,E25903,morgan.welsh@apsva.us,Barrett ES,Teacher,Instructional Staff
Christine,Cunningham,E25913,christine.cunningham@apsva.us,Barrett ES,Teacher,Instructional Staff
Jennifer,Flores,E261,jennifer.flores@apsva.us,Barrett ES,Teacher,Instructional Staff
Brittany,Utterback,E26326,brittany.utterback@apsva.us,Barrett ES,Teacher,Instructional Staff
Ariel,Jones,E26550,ariel.jones@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Jennifer,Buckley,E26584,jennifer.buckley@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Maria,Blanco Melendez,E26599,maria.blanco@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Gladys,Buendia,E27089,gladys.buendia@apsva.us,Barrett ES,Staff,Staff
Jennifer,Owen,E27283,jennifer.owen@apsva.us,Barrett ES,Teacher,Instructional Staff
Lulit,Tessema,E27335,lulit.tessema@apsva.us,Barrett ES,Staff,Staff
Ashley,Hollander,E27790,ashley.hollander@apsva.us,Barrett ES,Teacher,Instructional Staff
Wendy,Mastaler,E27850,wendy.mastaler@apsva.us,Barrett ES,Teacher,Instructional Staff
Catherine,Han,E27962,catherine.han@apsva.us,Barrett ES,Principal,Administrative Staff
Monae,Blount,E28142,monae.blount@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Giselle,Olaya,E28239,giselle.olaya@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Lauren,Ferraro,E2829,lauren.ferraro@apsva.us,Barrett ES,Teacher,Instructional Staff
Rehab,"Beshri, Ali",E28396,rehab.ali2@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Nuria,Loza,E28409,nuria.guevaraloza2@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Erika,Saravia,E28829,erika.saravia@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Heidi,Kulick,E28908,heidi.kulick@apsva.us,Barrett ES,Teacher,Instructional Staff
Kate,Delenick,E29173,kate.delenick@apsva.us,Barrett ES,Teacher,Instructional Staff
Claire,Kauffman,E29397,claire.kauffman@apsva.us,Barrett ES,Teacher,Instructional Staff
Zozan,Siso,E29404,zozan.siso2@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Sheyla Ramallo,Fuentes Ramallo,E29407,sheyla.ramallo@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Loyda,Alcantara,E29462,loyda.alcantara@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Adela,Jaldin De Herrera,E30055,adela.jaldin@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Katherine,Carlisle Maestri,E30201,katherine.carlisle@apsva.us,Barrett ES,Teacher,Instructional Staff
Noreen,Syeda,E30345,noreen.syeda@apsva.us,Barrett ES,Staff,Staff
Mariam,Alwarith,E30607,mariam.alwarith@apsva.us,Barrett ES,n/a,Unclassified
Bryna,Stewart,E30718,bryna.stewart@apsva.us,Barrett ES,Teacher,Instructional Staff
Semira,Mohammed,E30721,semira.mohammed@apsva.us,Barrett ES,Staff,Staff
Erika,Porro,E31385,erika.porro@apsva.us,Barrett ES,Staff,Staff
Nicole,Briggs,E31605,nicole.briggs@apsva.us,Barrett ES,Staff,Staff
Anna,Bowser,E31690,anna.bowser2@apsva.us,Barrett ES,Teacher,Instructional Staff
Nathaniel,Ho,E31767,nathaniel.ho@apsva.us,Barrett ES,Teacher,Instructional Staff
Naima,Riad,E31775,naima.riad@apsva.us,Barrett ES,Staff,Staff
Ruth,Rosales Herrera,E31912,ruth.rosalesherrera@apsva.us,Barrett ES,Staff,Staff
Eleni,Alexopoulou,E31941,eleni.alexopoulou@apsva.us,Barrett ES,Teacher,Instructional Staff
Michael,Wills,E32028,michael.wills@apsva.us,Barrett ES,Teacher,Instructional Staff
Jacqueline,Laurell,E32029,jacqueline.laurell@apsva.us,Barrett ES,Teacher,Instructional Staff
Emma,Bailey,E32053,emma.bailey@apsva.us,Barrett ES,Teacher,Instructional Staff
Marissa,Szalkowski,E32122,marissa.szalkowski@apsva.us,Barrett ES,Teacher,Instructional Staff
Ashley,Omekam,E32154,ashley.omekam@apsva.us,Barrett ES,Teacher,Instructional Staff
Eboni,Pasley,E32220,eboni.pasley@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Kleyry,Oyuela Corrales,E32379,kleyry.oyuela@apsva.us,Barrett ES,Staff,Staff
Jessica,Alfaro,E32477,jessica.alfaro@apsva.us,Barrett ES,Staff,Staff
Danna,Samuels,E32570,danna.samuels@apsva.us,Barrett ES,Teacher,Instructional Staff
Amanda,Chartrand,E32652,amanda.chartrand@apsva.us,Barrett ES,Teacher,Instructional Staff
Nancy,Davis,E32665,nancy.davis@apsva.us,Barrett ES,Teacher,Instructional Staff
Gregory,Stickeler,E32696,gregory.stickeler@apsva.us,Barrett ES,n/a,Unclassified
Sofia,Soulas,E32701,sofia.soulas@apsva.us,Barrett ES,Teacher,Instructional Staff
Isabelle,Kingsley,E32817,isabelle.kingsley@apsva.us,Barrett ES,n/a,Unclassified
Judith,Annancy,E32928,judith.annancy@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Gabriela,Quiroz,E33199,gabriela.quiroz@apsva.us,Barrett ES,Teacher,Instructional Staff
Nancy,Villatoro Romero,E33232,nancy.villatoro@apsva.us,Barrett ES,Staff,Staff
Tamara,Gross,E33315,tamara.gross@apsva.us,Barrett ES,Staff,Staff
Amin,Littman,E3431,amin.littman@apsva.us,Barrett ES,Assistant Principal,Administrative Staff
Robert,McLaughlin,E4229,robert.mclaughlin@apsva.us,Barrett ES,Teacher,Instructional Staff
Nora,Castillo,E5035,nora.castillo@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Nora,Parra,E5044,nora.parra@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Maria,Rodriguez,E5909,maria.rodriguez@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Natalia,Dzantieva,E6150,natalia.dzantieva@apsva.us,Barrett ES,Teacher,Instructional Staff
Iliana,Gutierrez,E6169,iliana.gutierrez@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Susan,Garman,E6477,susan.garman@apsva.us,Barrett ES,Teacher,Instructional Staff
Elizabeth,Jurkevics,E7371,elizabeth.jurkevics@apsva.us,Barrett ES,Teacher,Instructional Staff
Ana,Aguirre,E8019,ana.aguirre@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Moises,Herrera,E8561,moises.herrera@apsva.us,Barrett ES,Maintenance Supervisor,Facilities Staff
Jose,Vasquez Chicas,E8855,jose.vasquez@apsva.us,Barrett ES,Custodian,Facilities Staff
Melissa,Poore,E938,melissa.poore@apsva.us,Barrett ES,Instructional Assistant,Instructional Staff
Leilap,Vega,E9908,leilap.vega@apsva.us,Barrett ES,Teacher,Instructional Staff
Mee,Kim,E10161,mee.kim@apsva.us,Campbell ES,Teacher,Instructional Staff
Beth,Terrana,E13335,beth.decker@apsva.us,Campbell ES,Teacher,Instructional Staff
Spencer,Reisinger,E13367,spencer.reisinger@apsva.us,Campbell ES,Teacher,Instructional Staff
Valerie,Reid-Smith,E13698,valerie.reidsmith@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Jessica,Berg,E15890,jessica.berg@apsva.us,Campbell ES,Teacher,Instructional Staff
Dana,Rodriguez,E15988,dana.rodriguez@apsva.us,Campbell ES,"Administrative Assistant, Principals Assistant",Staff
Erin,Watson,E1634,erin.watson@apsva.us,Campbell ES,Teacher,Instructional Staff
Ivan,Vargas,E16952,ivan.vargas@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Kim,Brewer,E16959,kim.brewer@apsva.us,Campbell ES,Teacher,Instructional Staff
Kathryn,Barker,E19236,kathryn.barker@apsva.us,Campbell ES,"Teacher, Staff",Staff
Tamara,Hill,E19297,tamara.hill@apsva.us,Campbell ES,Teacher,Instructional Staff
Tina,Ratovohery,E19796,tina.ratovohery@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Lang,Taing,E20006,lang.taing@apsva.us,Campbell ES,Instructional Assistant,Staff
Flor,Barrera-Gonzalez,E20412,flor.barrera@apsva.us,Campbell ES,Maintenance Supervisor,Facilities Staff
Crystal,Burgess,E20658,crystal.cuba@apsva.us,Campbell ES,Teacher,Instructional Staff
Rebecca,Cookson,E21487,rebecca.cookson@apsva.us,Campbell ES,Teacher,Instructional Staff
Azucena,Ventura,E21535,azucena.ventura@apsva.us,Campbell ES,Administrative Assistant,Staff
Maureen,Nesselrode,E2159,maureen.nesselrode@apsva.us,Campbell ES,Principal,Unclassified
Khadijah,Cameron,E21796,khadijah.cameron@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Patricia,Salazar,E22211,patricia.oviedo@apsva.us,Campbell ES,Teacher,Instructional Staff
Virginia,Montminy,E22227,virginia.montminy@apsva.us,Campbell ES,Teacher,Instructional Staff
Megan,Zelasko,E23162,megan.zelasko@apsva.us,Campbell ES,Teacher,Instructional Staff
Caroline,McAleer,E23220,caroline.mcaleer@apsva.us,Campbell ES,Teacher,Instructional Staff
Morgan,Burns,E23803,morgan.burns2@apsva.us,Campbell ES,Teacher,Instructional Staff
Dany,Hanbouri,E23923,dany.hanbouri@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Elaine,Belber,E24051,elaine.belber@apsva.us,Campbell ES,Teacher,Instructional Staff
Dennis,Clark,E24068,dennis.clark@apsva.us,Campbell ES,Staff,Staff
Maria,Orellana,E24262,maria.orellana@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Fitsum,Haile,E24278,fitsum.haile@apsva.us,Campbell ES,Instructional Assistant,Staff
Kristin,Drabyk-Savage,E24852,kristin.drabyk@apsva.us,Campbell ES,Teacher,Instructional Staff
Morgan,Norwood,E24867,morgan.norwood@apsva.us,Campbell ES,Teacher,Instructional Staff
Allison,Ackerman,E24901,allison.ackerman@apsva.us,Campbell ES,Teacher,Instructional Staff
Pamela,Clark,E2516,pamela.clark@apsva.us,Campbell ES,Teacher,Instructional Staff
Shannon,O'Connor,E25667,shannon.oconnor@apsva.us,Campbell ES,Teacher,Instructional Staff
Laura,Bissi,E25796,laura.bissi@apsva.us,Campbell ES,Teacher,Instructional Staff
Mary,Gerron,E25875,mary.gerron@apsva.us,Campbell ES,Teacher,Instructional Staff
Jennifer,Alsup,E26784,jennifer.alsup@apsva.us,Campbell ES,Teacher,Instructional Staff
Nicole,Croce,E26882,nicole.johnson@apsva.us,Campbell ES,Teacher,Instructional Staff
Cathy,Campbell,E26933,cathy.campbell@apsva.us,Campbell ES,Teacher,Instructional Staff
Alexandra,Bodell,E26979,alexandra.bodell@apsva.us,Campbell ES,Teacher,Instructional Staff
Nivvi,Tareen,E2720,nawazish.tareen@apsva.us,Campbell ES,Teacher,Instructional Staff
Meseret,Gessesse,E27469,meseret.gessesse2@apsva.us,Campbell ES,Staff,Staff
Margaret,Rose,E27941,margaret.rose@apsva.us,Campbell ES,Teacher,Instructional Staff
Laura,Kim,E27946,laura.kim@apsva.us,Campbell ES,Teacher,Instructional Staff
Anna,Davitt,E28128,anna.davitt@apsva.us,Campbell ES,Teacher,Instructional Staff
Kate,Sullivan,E28179,kate.sullivan@apsva.us,Campbell ES,Teacher,Instructional Staff
Rebekah,Bridges,E28454,rebekah.bridges2@apsva.us,Campbell ES,Teacher,Instructional Staff
Marta,Pineda,E28510,marta.pineda@apsva.us,Campbell ES,Custodian,Facilities Staff
Anita,Van Harten Cater,E28722,anita.vanhartencater@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Lori,Wolter,E28917,lori.wolter@apsva.us,Campbell ES,Teacher,Instructional Staff
Ofelia,Oronoz,E29065,ofelia.oronoz@apsva.us,Campbell ES,Teacher,Instructional Staff
Cristina,Quiroga Subelza,E29614,cristina.subelza@apsva.us,Campbell ES,Custodian,Facilities Staff
Gabrielle,Allen,E29895,gabrielle.allen@apsva.us,Campbell ES,Teacher,Instructional Staff
Elisabeth,Kyle,E29995,elisabeth.kyle@apsva.us,Campbell ES,Teacher,Instructional Staff
Lauren,Kleifges,E29999,lauren.kleifges@apsva.us,Campbell ES,Teacher,Instructional Staff
Aziza,Syed,E30069,aziza.syed@apsva.us,Campbell ES,Instructional Assistant,Staff
Miguel,Delgado Ruiz,E30202,miguel.delgadoruiz@apsva.us,Campbell ES,Teacher,Instructional Staff
Bishop,McGee,E30518,bishop.mcgee@apsva.us,Campbell ES,Instructional Assistant,Staff
Elisabeth,Brewer,E31119,elisabeth.brewer@apsva.us,Campbell ES,Teacher,Instructional Staff
Hanna,Davis,E31325,hanna.sokol@apsva.us,Campbell ES,Teacher,Instructional Staff
Haley,Horan,E31389,haley.horan@apsva.us,Campbell ES,Teacher,Instructional Staff
Alison,Acker,E31418,alison.acker@apsva.us,Campbell ES,Teacher,Instructional Staff
Suad,Omer,E31513,suad.omer@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Michelle,Stodel,E31686,michelle.stodel@apsva.us,Campbell ES,Teacher,Instructional Staff
Asma,Syeda,E31964,asma.syeda@apsva.us,Campbell ES,"Instructional Assistant, Staff",Staff
Amanda,Bianco Parks,E31991,amanda.biancoparks@apsva.us,Campbell ES,Teacher,Instructional Staff
Evelyn,Krippner,E31998,evelyn.krippner@apsva.us,Campbell ES,Teacher,Instructional Staff
Julianne,Maston,E32058,julianne.maston@apsva.us,Campbell ES,n/a,Unclassified
Matthew,McAndrew,E32083,matthew.mcandrew@apsva.us,Campbell ES,Teacher,Instructional Staff
Gary,Chambers,E32193,gary.chambers@apsva.us,Campbell ES,Teacher,Instructional Staff
Joyce,Navia Penaloza,E32255,joyce.naviapenaloza@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Christina,Quinn,E32297,christina.quinn2@apsva.us,Campbell ES,Teacher,Instructional Staff
Rosa,Nunez,E32410,rosa.nunez@apsva.us,Campbell ES,Custodian,Facilities Staff
Gregory,Stickeler,E32696,gregory.stickeler@apsva.us,Campbell ES,n/a,Unclassified
Kailey,Ormsby,E32727,kailey.ormsby@apsva.us,Campbell ES,Teacher,Instructional Staff
Genevieve,Merrill,E390,genevieve.merrill@apsva.us,Campbell ES,Administrative Assistant,Staff
Karen,Anselmo,E4009,karen.anselmo@apsva.us,Campbell ES,Assistant Principal,Administrative Staff
Denis,Vasquez,E4321,denis.vasquez@apsva.us,Campbell ES,Instructional Assistant,Staff
Maria,Ramirez,E4333,maria.ramirez@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Claudia,Barona,E4682,claudia.barona@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Patricia,Cornejo,E4683,patricia.cornejo@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Tonya,Dunbar,E4932,tanya.dunbar@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Regina,Richardson,E5518,regina.richardson@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Richard,Roberts,E7254,richard.roberts@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Karen,Zimmerman,E775,karen.zimmerman@apsva.us,Campbell ES,Teacher,Instructional Staff
Maria,Herrera Hernandez,E8048,maria.hernandez@apsva.us,Campbell ES,Staff,Staff
Wade,Turner,E8427,wade.turner@apsva.us,Campbell ES,Teacher,Instructional Staff
Regina,Garcia,E8572,regina.garcia@apsva.us,Campbell ES,Instructional Assistant,Instructional Staff
Christina,Kirsch,E10737,christina.kirsch@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Marcellious,Nesbitt-Gaines,E10804,marcellious.gaines@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Lisa,Hill,E10827,lisa.hill@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Javonnia,Hill,E10952,javonnia.hill@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Louis,Villafane,E1153,louis.villafane@apsva.us,Arlington Career Center,Director,Administrative Staff
Sharon,Harris,E1189,sharon.harris@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Anne,Cupero,E12506,anne.cupero@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Desiree,Alexander,E12542,desiree.alexander@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Ha,Nguyen,E13192,ha.nguyen@apsva.us,Arlington Career Center,Custodian,Facilities Staff
Joseph,Rubinstein,E1346,joseph.rubinstein@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Manny,Sanchez,E13678,manny.sanchez@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Carmen,Labrador,E14044,carmen.labrador@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Mireya,Degracia,E14159,mireya.degracia@apsva.us,Arlington Career Center,Custodian,Facilities Staff
Kenneth,Batts,E14414,kenneth.batts@apsva.us,Arlington Career Center,Maintenance Supervisor,Facilities Staff
Anthony,Lee,E14735,anthony.lee@apsva.us,Arlington Career Center,Maintenance Supervisor,Facilities Staff
Dalis,Williams,E14833,dalis.williams@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Madeline,Lasalle,E15292,madeline.lasalle@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Erin,Gagen,E15905,erin.gagen@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Lisa,Lee,E15937,lisa.lee@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Wilmer,Castro,E16029,wilmer.castro@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Geraldine,Maskelony,E16060,geraldine.maskelony@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Oumer,Jarso,E16679,oumer.jarso@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Jason,Re,E16790,jason.re@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Letecia,Poliquit,E16796,letecia.poliquit@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Michelle,Laumann,E16913,michelle.laumann@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Shane,Johnson,E1704,shane.johnson@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Ruth,Bilodeau,E1800,ruth.bilodeau@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Joshua,Patulski,E1852,joshua.patulski@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Rachel,Forbes Jackson,E19471,rachel.forbes@apsva.us,Arlington Career Center,Teacher,Instructional Staff
John,Woodhead,E19615,john.woodhead@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Monica,Lozano Caldera,E19712,monica.lozanocaldera@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Dale,Winchell,E20454,dale.winchell@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Rosa,Zamora,E20543,rosa.zamora@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Virginia,Strobach,E21379,virginia.strobach@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Rachel,Bowerman,E21471,rachel.bowerman@apsva.us,Arlington Career Center,Assistant Principal,Administrative Staff
Deborah,Weston,E21596,deborah.weston@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Frank,Derocco,E22067,frank.derocco@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Cory,Mainor,E22132,cory.mainor@apsva.us,Arlington Career Center,Assistant Principal,Administrative Staff
Nakia,Arnold,E22574,nakia.arnold@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Margaret,Chung,E23129,margaret.chung@apsva.us,Arlington Career Center,Principal,Unclassified
Yesenia,Martinez,E2321,yesenia.martinez@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Tamika,Brown,E23217,tamika.brown@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Jacqueline,Bonner,E23285,jacqueline.bonner@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Anne,Vincent,E23941,anne.vincent@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Jeffrey,Elkner,E2396,jeffrey.elkner@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Sharon,Solorzano,E23966,sharon.solorzano@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Juanita,Hooks,E24689,juanita.hooks@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Hope,Bolfek,E24902,hope.bolfek@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Mark,Dierlam,E24964,mark.dierlam@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Ana,Funes,E25492,ana.funes@apsva.us,Arlington Career Center,Custodian,Facilities Staff
Julaine,Frye,E25698,julie.frye@apsva.us,Arlington Career Center,Administrative Assistant,Staff
Bethany,Solano,E25725,bethany.solano@apsva.us,Arlington Career Center,Administrative Assistant,Staff
Christina,Eagle,E25746,christina.eagle@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Mario,Flores,E25777,mario.flores@apsva.us,Arlington Career Center,Custodian,Facilities Staff
Bradley,Rankin,E25845,bradley.rankin@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Yue,Zhen,E26073,yue.zhen@apsva.us,Arlington Career Center,n/a,Unclassified
Maria,Sonnekalb,E26634,maria.sonnekalb@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Arlene,Whitlock,E26692,arlene.whitlock@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Jamila,Robinson,E26875,jamila.robinson@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Jordan,Kivitz,E27079,jordan.kivitz@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Aaron,Herrera,E27100,aaron.herrera@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Priscella,Walker,E27484,priscella.walker@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Phu,To,E27585,phu.to@apsva.us,Arlington Career Center,Custodian,Facilities Staff
Loi,Hoang,E27609,loi.hoang@apsva.us,Arlington Career Center,Maintenance Supervisor,Facilities Staff
Miguel,Guevara,E27623,miguel.guevara@apsva.us,Arlington Career Center,Staff,Staff
Michelle,Van Lare,E27702,michelle.vanlare@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Suzanne,Richardson,E27704,suzanne.richardson@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Renee,Randolph,E27794,renee.randolph@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Tinequa,Reese,E27831,tinequa.reese@apsva.us,Arlington Career Center,Teacher,Instructional Staff
John,Theis,E28147,john.theis@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Edwin,Reyes,E28252,edwin.reyes@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Cole,Forbes,E28359,cole.forbes2@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Antares,Leask,E28490,antares.leask@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Sidra,Kalsoom,E28540,sidra.kalsoom2@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Sarah,Hartley,E28948,sarah.hartley@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Miriam,Thomas,E29006,miriam.thomas@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Timothy,Scanlon,E29019,timothy.scanlon@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Ashley,Neal,E29026,ashley.neal@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Anthony,Frazier,E29034,anthony.frazier@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Kimberly,Turner,E29083,kimberly.turner@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Stephanie,Strang,E29126,stephanie.strang@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Leonardo,Godinez Melendez,E29210,leonardo.melendez@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Ashley,Ladner,E29401,ashley.ladner@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Rokhila,Zakirova,E29680,rokhila.zakirova@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Casey,Layne,E29692,casey.layne@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Jordan,Wright,E29833,jordan.wright@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Adam,Entenberg,E29987,adam.entenberg@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Astin,Alexander,E29991,astin.alexander@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Cristin,Caparotta,E30051,cristin.caparotta@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Krystina,Lange,E30112,krystina.lange@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Henry,Hines,E30129,henry.hines@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Miguel,Delgado Ruiz,E30202,miguel.delgadoruiz@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Katelyn,Mathis,E30458,katelyn.mathis@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Milagro,Alvarez Escobar,E31165,milagro.escobar@apsva.us,Arlington Career Center,Custodian,Facilities Staff
Steve,Nystrom,E31277,steve.nystrom@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Ian,Darragh,E31320,ian.darragh@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Christina,Ascani,E31326,christina.ascani@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Olga,Hill,E31381,olga.hill@apsva.us,Arlington Career Center,Administrative Assistant,Staff
Sylvie,Degraff,E31388,sylvie.degraff@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Nathaniel,Ogle,E31408,nathaniel.ogle@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Engelbert,Kelah,E31416,engelbert.kelah@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Audrey,Reiter,E31487,audrey.reiter@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Charles,Allan,E31530,charles.allan@apsva.us,Arlington Career Center,Staff,Staff
Kelly,Antal,E31729,kelly.antal@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Mhd,Alhamoui,E31883,mhd.alhamoui@apsva.us,Arlington Career Center,n/a,Unclassified
Christina,Day,E31977,christina.day@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Lucila,Llaguno,E31988,lucila.llaguno@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Antiya,Clemons,E32016,antiya.clemons@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Alyson,Hillstrom,E32114,alyson.hillstrom@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Kristina,Brody,E32123,kristina.brody@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Thomas,Crabbe,E32278,thomas.crabbe@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Richard,Hall,E32544,richard.hall@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Kristina,Chittams-Terry,E32552,kristina.terry@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Thomas,Calderon,E32563,thomas.calderon@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Patricia,Braun,E32589,patricia.braun@apsva.us,Arlington Career Center,Coordinator,Instructional Staff
Fardousa,Wardere,E32593,fardousa.wardere@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Maxim,Khutoryan,E32604,maxim.khutoryan@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Arran,Richards,E32636,arran.richards@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Craig,Peppers,E32640,craig.peppers@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Garrett,Fabacher,E32719,garrett.fabacher@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Ryan,Miller,E32768,ryan.miller2@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Habibe,Aksoy,E32839,habibe.aksoy@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Kaung-Ti,Yung,E32870,kaungti.yung@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Claudia,Scott-Jenkins,E32918,claudia.scottjenkins@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Michele,Coffman,E32986,michele.coffman@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Ingrid,Lombardo McCoy,E33014,ingrid.lombardomccoy@apsva.us,Arlington Career Center,Staff,Staff
Ertan,Kaya,E33053,ertan.kaya@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Valerie,Shankland,E33169,valerie.shankland@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Edwin,Franklin,E33182,edwin.franklin@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Kiana,Flores,E33192,kiana.flores@apsva.us,Arlington Career Center,Staff,Staff
Emma,Billings,E33198,emma.billings@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Matthew,Cave,E33217,matthew.cave@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Maria,Kehr,E33283,maria.kehr@apsva.us,Arlington Career Center,n/a,Unclassified
Deborah,Sanchez,E33329,deborah.sanchez@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Kristen,Stevens,E33370,kristen.stevens@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Lisa,Styles,E381,lisa.styles@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Veronica,Cross,E3934,veronica.cross@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Ada,Contreras,E4112,ada.contreras@apsva.us,Arlington Career Center,Registrar,Staff
Shaniqua,Sumby,E4455,shaniqua.sumby@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Lori,Mcfail,E5722,lori.mcfail@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Leah,Graham-Mcfarlane,E5956,leah.mcfarlane@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Claudia,Vasquez,E6516,claudia.vasquez@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Scott,Lockhart,E75,scott.lockhart@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Jenny,Culver,E7608,jenny.culver@apsva.us,Arlington Career Center,Administrative Technician,Staff
Erin,Hannon,E7634,erin.hannon@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Lai,Lau,E7872,lai.lau@apsva.us,Arlington Career Center,School Finance Officer,Staff
Amanda,Trevino,E7932,amanda.trevino@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Rosenia,Peake,E8218,rosenia.peake@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Byron,Schwind,E8264,byron.schwind@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Melida,Reyes,E8318,melida.reyes@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Isaac,Zawolo,E8830,isaac.zawolo@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Tom,O'Day,E8844,tom.oday@apsva.us,Arlington Career Center,Teacher,Instructional Staff
Virginia,Barrow,E8992,virginia.barrow@apsva.us,Arlington Career Center,Administrative Assistant,Staff
Patricia,Roseboro-Shackleford,E9538,patric.roseboro@apsva.us,Arlington Career Center,Instructional Assistant,Instructional Staff
Saruhan,Hatipoglu,N9999999263,saruhan.hatipoglu@apsva.us,Arlington Career Center,n/a,Unclassified
Beronica,Salas,E10104,beronica.salas@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Maha,Abdelrahim,E1080,maha.abdelrahim@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Jennifer,Ruddon Kircher,E12011,jennifer.kircher@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Elizabeth,Aiken,E13247,elizabeth.aiken@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
David,Koppelman,E1359,david.koppelman@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Olga,Medina,E14083,olga.medina@apsva.us,Carlin Springs ES,Custodian,Facilities Staff
Lutfun,Nahar,E14143,lutfun.nahar@apsva.us,Carlin Springs ES,Staff,Staff
Courtney,Hines,E15545,courtney.hines@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Fatumo,Mose,E15594,fatumo.mose@apsva.us,Carlin Springs ES,Staff,Staff
Lorena,Beverley,E16842,lorena.beverley@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Julie,Sosa,E19004,julie.sosa@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Christine,Payack,E20849,christine.payack@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Carol,Schaedel,E21366,carol.schaedel@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Stacy,Klein,E21513,stacy.klein@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Jessica,Gilling,E21815,jessica.gilling@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Ellen,Philson,E21863,ellen.philson@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Barbara,Morrow,E22159,barbara.morrow@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Launa,Hall,E22499,launa.hall@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Maria,Baires,E22620,maria.baires@apsva.us,Carlin Springs ES,Staff,Staff
Jeremiah,Abarca,E22671,jeremiah.abarca@apsva.us,Carlin Springs ES,Maintenance Supervisor,Facilities Staff
Tajmah,Coleman,E22967,tajmah.coleman@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Katharine,Hsu,E22988,katharine.hsu@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Lauren,Elkins,E22993,lauren.elkins@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Ana,Alarcon,E23299,ana.alarcon@apsva.us,Carlin Springs ES,Staff,Staff
Kathleen,Moore,E2336,kathleen.moore@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Dennis,Clark,E24068,dennis.clark@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Ana,Quintero,E24409,ana.quintero@apsva.us,Carlin Springs ES,Staff,Staff
Saadet,Kara,E24636,saadet.kara@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Erin,Zambrana,E25016,erin.stansel@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Natani,McGinnis,E25064,tani.mcginnis@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Silvia,Martinez,E25599,silvia.martinez@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Rachida,Gdid,E25737,rachida.gdid@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Mary Ann,Hoffman,E25886,maryann.hoffman@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Katelyn,Kyser,E25948,katelyn.kyser@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Richard,Russey,E25949,richard.russey@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Holly,Nisco,E26002,holly.nisco@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Darlene,Garretson,E26102,darlene.garretson2@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Carol,Sabatino,E26158,carol.sabatino@apsva.us,Carlin Springs ES,Coordinator,Staff
Lyzbeth,Monard Eguren,E26327,lyzbeth.monardeguren@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Santiago,Quintero,E26372,santiago.quintero@apsva.us,Carlin Springs ES,Custodian,Facilities Staff
Elizabeth,O'Sullivan,E26481,elizabeth.osullivan@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Lindsay,Lovett,E26745,lindsay.lovett@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Patricia,Isaza,E26750,patricia.isaza@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Stephen,Shields,E26832,stephen.shields@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Amy,Reynolds,E26861,amy.reynolds@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Tania,Majano Benitez,E27037,tania.majanobenitez@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Jessie,Chirinos,E28673,jessie.chirinos@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Heather,Harvey,E28806,heather.harvey@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Julie,Huynh,E28971,julie.huynh@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Sarah,Sanders,E28996,sarah.sanders@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Katherine,Stephenson,E29042,katherine.stephenson@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Collins,Nitcheu,E29849,collins.nitcheu@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Michael,Smith,E29979,michael.smith2@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Megan,Grasso,E30110,megan.grasso@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Mark,Head,E30143,mark.head@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Amy,Ressing,E30159,amy.ressing@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Jessica,Juarez Reyes,E30300,jessica.juarezreyes@apsva.us,Carlin Springs ES,Custodian,Facilities Staff
Noreen,Syeda,E30345,noreen.syeda@apsva.us,Carlin Springs ES,Staff,Staff
Nancy,Williams,E30571,nancy.williams2@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Rebecca,Bickel,E31098,rebecca.bickel@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Heather,Sox,E31216,heather.sox@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Adam,Meyersieck,E31243,adam.meyersieck@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Taylor,Holman,E31349,taylor.holman@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Blake,Mason,E31375,blake.mason@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Sitlally,Orozco Vasquez,E31395,sitlally.orozco@apsva.us,Carlin Springs ES,"Administrative Assistant, Instructional Assistant",Instructional Staff
Lynn,Varshneya,E31468,lynn.varshneya@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Erin,Warin,E31612,erin.warin2@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Katherine,Olden,E31693,katherine.olden@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Judith,Kuehn,E31771,judith.kuehn@apsva.us,Carlin Springs ES,Staff,Staff
Shahnaz,Parvin,E31903,shahnaz.parvin@apsva.us,Carlin Springs ES,Staff,Staff
Megan,Szwez,E32064,megan.szwez@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Jacquelyn,Alfriend,E32065,jacquelyn.alfriend@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Nicolette,Barraza,E32091,nicolette.barraza@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Sarah,Raney,E32134,sarah.raney@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Caroline,Peterson,E32143,caroline.peterson@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Melaney,Mackin,E32209,melaney.mackin@apsva.us,Carlin Springs ES,Principal,Unclassified
Laura,Sheedy,E32395,laura.sheedy2@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Abigail,Brocato,E32648,abigail.brocato@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Kristen,Drier,E32691,kristen.drier@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Doris,Hairston,E32697,doris.hairston@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Joseph,Doherty,E32724,joseph.doherty@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Lauren,Cole,E32734,lauren.cole@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Kaiya,Fearrington,E32739,kaiya.fearrington@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Lynn,Dragity,E32772,lynn.dragity@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Africa,Brown,E32776,africa.brown@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Isabel,Gleason,E32799,isabel.gleason@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Ingris,Ordonez Munguia,E33061,ingris.ordonez@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Chloe,Ipatzi,E33459,chloe.ipatzi@apsva.us,Carlin Springs ES,Staff,Staff
Susana,Cordova,E3360,susana.cordova@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Marlena,Dasbach,E3610,marlena.dasbach@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Natividad,Roman,E3851,natividad.roman@apsva.us,Carlin Springs ES,Staff,Staff
Anthony,Battle,E4019,anthony.battle@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Candace,Mohrmann,E4750,candace.mohrmann@apsva.us,Carlin Springs ES,"Administrative Assistant, Staff",Staff
Marijoy,Cordero,E4802,marijoy.cordero@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Salvador,Moran,E5188,salvador.moran@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Lucinda,Gonzalez,E5327,lucinda.gonzales@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Claudia,Montoya,E5369,claudia.perez@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Leticia,Arellano-De-Sigala,E6137,leticia.sigala@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Julia,Downs,E6441,julia.downs@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Alvaro,Flores,E6653,alvaro.flores@apsva.us,Carlin Springs ES,Assistant Principal,Administrative Staff
Sandra,Perez,E6674,sandra.perez@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Ana,Pires,E7029,ana.pires@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Sean,Martin,E7133,sean.martin@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Leslie,Olkhovsky,E7736,leslie.olkhovsky@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Cynthia,Barrera,E7785,cynthia.barrera@apsva.us,Carlin Springs ES,Administrative Assistant,Staff
Juan,Otal,E7860,juan.otal@apsva.us,Carlin Springs ES,Teacher,Instructional Staff
Maria,Herrera Hernandez,E8048,maria.hernandez@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Mary,McNeill,E9063,mary.mcneill@apsva.us,Carlin Springs ES,Principals Assistant,Staff
Elvira,Altamirano,E9356,elvira.altamirano@apsva.us,Carlin Springs ES,Instructional Assistant,Instructional Staff
Paula,Cruz,E9599,paula.cruz@apsva.us,Carlin Springs ES,Administrative Assistant,Staff
Jose,Aleman,E9995,jose.aleman@apsva.us,Carlin Springs ES,Custodian,Facilities Staff
Cynthia,Matos,E12626,cynthia.matos@apsva.us,Claremont ES,Administrative Assistant,Staff
Mariana,Aleman,E15267,mariana.aleman@apsva.us,Claremont ES,Instructional Assistant,Instructional Staff
Haydee,Colon,E15275,haydee.colon@apsva.us,Claremont ES,Instructional Assistant,Instructional Staff
Elizabeth,Lebedeker,E15432,elizabeth.lebedeker@apsva.us,Claremont ES,Teacher,Instructional Staff
Alyssa,Westerman,E15846,alyssa.westerman@apsva.us,Claremont ES,Teacher,Instructional Staff
Holly,Clark,E16219,holly.clark@apsva.us,Claremont ES,Teacher,Instructional Staff
Laura,Larco,E16619,laura.larco@apsva.us,Claremont ES,Teacher,Instructional Staff
Matilde,Lopez De Torrico,E16864,matilde.torrico@apsva.us,Claremont ES,Staff,Staff
Kathleen,Cody,E19044,kathleen.cody@apsva.us,Claremont ES,Teacher,Instructional Staff
Ledezna,Puerto,E1945,ledezna.puerto@apsva.us,Claremont ES,Teacher,Instructional Staff
Claudia,Delgadillo,E19486,claudia.delgadillo@apsva.us,Claremont ES,Teacher,Instructional Staff
Tara,Ronzetti,E20081,tara.ronzetti@apsva.us,Claremont ES,Teacher,Instructional Staff
Carlos,Buzzi,E20372,carlos.buzzi@apsva.us,Claremont ES,Teacher,Instructional Staff
Kara,Klousia,E20535,kara.klousia@apsva.us,Claremont ES,Teacher,Instructional Staff
Cesar,Espejo,E20828,cesar.espejo@apsva.us,Claremont ES,Teacher,Instructional Staff
Jason,Cash,E21587,jason.cash@apsva.us,Claremont ES,Teacher,Instructional Staff
Amanda,Moore,E22048,amanda.moore@apsva.us,Claremont ES,Teacher,Instructional Staff
Jessica,Panfil,E22134,jessica.panfil@apsva.us,Claremont ES,Principal,Unclassified
Catarino,Jaramillo,E22219,catarino.jaramillo@apsva.us,Claremont ES,Teacher,Instructional Staff
Samantha,Kirch,E22823,samantha.kirch@apsva.us,Claremont ES,Teacher,Instructional Staff
Laura,Newbold,E23058,laura.newbold@apsva.us,Claremont ES,Teacher,Instructional Staff
Wilfredo,Padilla Melendez,E23077,wilfredo.padilla@apsva.us,Claremont ES,Teacher,Instructional Staff
Martha,Linville,E23416,martha.linville@apsva.us,Claremont ES,Instructional Assistant,Instructional Staff
Mario,Arriaza Andrade,E23428,mario.arriazaandrade@apsva.us,Claremont ES,Custodian,Facilities Staff
Laquisha,Brown,E23750,laquisha.guest@apsva.us,Claremont ES,Teacher,Instructional Staff
Amy,Cavazos,E23825,amy.cavazos@apsva.us,Claremont ES,Teacher,Instructional Staff
Miriam,Cutelis,E23953,miriam.cutelis@apsva.us,Claremont ES,Teacher,Instructional Staff
Ana,Ferber,E24061,ana.ferber@apsva.us,Claremont ES,Teacher,Instructional Staff
Maria,Telesca,E24582,maria.telesca@apsva.us,Claremont ES,Teacher,Instructional Staff
Charlene,Scott,E24927,charlene.scott@apsva.us,Claremont ES,Teacher,Instructional Staff
Aletha,Keogh,E24954,aletha.keogh@apsva.us,Claremont ES,Teacher,Instructional Staff
Denise,Santiago,E24969,denise.santiago@apsva.us,Claremont ES,Assistant Principal,Administrative Staff
Elisabeth,Lua,E24971,elisabeth.lua@apsva.us,Claremont ES,Teacher,Instructional Staff
Molly,Spooner Agnew,E24993,molly.spooneragnew@apsva.us,Claremont ES,Teacher,Instructional Staff
Carmen,Lazo,E25239,carmen.lazo@apsva.us,Claremont ES,Custodian,Facilities Staff
Sandra,Gomez,E25388,sandra.gomez@apsva.us,Claremont ES,Instructional Assistant,Instructional Staff
Carolyn,Uscinski,E25429,carolyn.uscinski@apsva.us,Claremont ES,Teacher,Instructional Staff
Daviel,Cruz Cruz,E25648,daviel.cruzcruz@apsva.us,Claremont ES,Teacher,Instructional Staff
Sheila,Majdi,E25660,sheila.majdi@apsva.us,Claremont ES,Teacher,Instructional Staff
Flora,Perez,E26011,flora.perez@apsva.us,Claremont ES,Administrative Assistant,Staff
Nadia,Vasquez,E26137,nadia.vasquez@apsva.us,Claremont ES,Administrative Assistant,Staff
Lauren,Greene,E26175,lauren.greene@apsva.us,Claremont ES,Staff,Staff
Claudia,Cante-Wambura,E26653,claudia.cantewambura@apsva.us,Claremont ES,Teacher,Instructional Staff
Gloria,Giraldo,E26761,gloria.giraldo@apsva.us,Claremont ES,Teacher,Instructional Staff
Chelsea,Marcelin,E26891,chelsea.marcelin@apsva.us,Claremont ES,Teacher,Instructional Staff
Christy,Przystawik,E26954,christy.przystawik@apsva.us,Claremont ES,Teacher,Instructional Staff
Vania,Williams,E27015,vania.williams@apsva.us,Claremont ES,Staff,Staff
Arpita,Barua,E27426,arpita.barua@apsva.us,Claremont ES,Staff,Staff
Ryan,Whitelaw,E27442,ryan.whitelaw@apsva.us,Claremont ES,Teacher,Instructional Staff
Anusorn,Khamthong,E27586,anusorn.khamthong@apsva.us,Claremont ES,n/a,Unclassified
Diana,Silva,E27633,diana.silva@apsva.us,Claremont ES,"Teacher, Staff",Staff
Patricia,Quiroga,E27663,patricia.quiroga@apsva.us,Claremont ES,Staff,Staff
Ivanelsy,Mejias-Rivera,E27915,ivanelsy.rivera@apsva.us,Claremont ES,Teacher,Instructional Staff
John,Gubser,E28115,john.gubser@apsva.us,Claremont ES,Teacher,Instructional Staff
Kamila,Gluch,E28659,kamila.gluch@apsva.us,Claremont ES,Administrative Assistant,Staff
Anna,Engelbrecht,E29032,anna.engelbrecht@apsva.us,Claremont ES,Teacher,Instructional Staff
Sandra,Valentin Garcia,E29378,sandra.garcia@apsva.us,Claremont ES,Teacher,Instructional Staff
Ana,Uria,E29419,ana.uria2@apsva.us,Claremont ES,Instructional Assistant,Instructional Staff
Kayla,Powell,E29597,kayla.powell@apsva.us,Claremont ES,Teacher,Instructional Staff
Y,Santiago-Marin,E29894,y.santiagomarin2@apsva.us,Claremont ES,Teacher,Instructional Staff
Lila,Ammar,E29910,lila.ammar@apsva.us,Claremont ES,Staff,Staff
Genet,Haileselassie,E30406,genet.haileselassie@apsva.us,Claremont ES,Staff,Staff
Ana,Aquino,E30834,ana.aquino@apsva.us,Claremont ES,Staff,Staff
Nicolas,Henderson,E30836,nicolas.henderson2@apsva.us,Claremont ES,Staff,Staff
Joaquin,Camarasa Boils,E30936,joaquin.camarasa@apsva.us,Claremont ES,Teacher,Instructional Staff
Ariel,Arias,E30971,ariel.arias@apsva.us,Claremont ES,Administrative Assistant,Staff
Syeda,Choudhury,E30994,syeda.choudhury@apsva.us,Claremont ES,Staff,Staff
Andrea,Fitch,E31002,andrea.fitch@apsva.us,Claremont ES,Teacher,Instructional Staff
Tigist,Abessa,E31077,tigist.abessa@apsva.us,Claremont ES,Staff,Staff
Zandra,Comas Betancourt,E31129,zandra.comas@apsva.us,Claremont ES,Teacher,Instructional Staff
Nancy,Davila,E31223,nancy.davila@apsva.us,Claremont ES,Teacher,Instructional Staff
Ivonnemarie,Hernandez Boneta,E31312,ivonne.hernandez@apsva.us,Claremont ES,Teacher,Instructional Staff
Muhibullah,Abdur-Rahman,E31345,m.abdurrahman@apsva.us,Claremont ES,Teacher,Instructional Staff
Mukta,Khatun,E31380,mukta.khatun@apsva.us,Claremont ES,Staff,Staff
Alejandro,Fernandez Pons,E31460,alejandro.gutierrez@apsva.us,Claremont ES,Instructional Assistant,Instructional Staff
Refath,Karim,E31553,refath.karim@apsva.us,Claremont ES,Staff,Staff
Nina,Cortina,E31758,nina.cortina@apsva.us,Claremont ES,Teacher,Instructional Staff
Michele,Rodriguez Frontera,E31818,michele.rodriguez@apsva.us,Claremont ES,Teacher,Instructional Staff
Sakina,Salehi,E31852,sakina.salehi@apsva.us,Claremont ES,Staff,Staff
Alison,Massidda,E32066,alison.massidda@apsva.us,Claremont ES,Teacher,Instructional Staff
Rebecca,Kerney,E32136,rebecca.kerney@apsva.us,Claremont ES,Teacher,Instructional Staff
Timothy,Power,E32150,timothy.power@apsva.us,Claremont ES,Teacher,Instructional Staff
Heide,Rosales,E32221,heide.rosales@apsva.us,Claremont ES,Teacher,Instructional Staff
Lillian,Meggs,E32258,lillian.meggs@apsva.us,Claremont ES,Teacher,Instructional Staff
Damaris,Colon Acevedo,E32262,damaris.colonacevedo@apsva.us,Claremont ES,Teacher,Instructional Staff
Maria,Resendiz Pineda,E32480,maria.resendizpineda@apsva.us,Claremont ES,Custodian,Facilities Staff
Valeria,Ramirez,E32555,valeria.ramirez@apsva.us,Claremont ES,Teacher,Instructional Staff
Amanda,Meyer,E32726,amanda.meyer@apsva.us,Claremont ES,Teacher,Instructional Staff
Jasmine,Azimi,E32737,jasmine.azimi@apsva.us,Claremont ES,Teacher,Instructional Staff
Sandra,Soto Colon,E32738,sandra.sotocolon@apsva.us,Claremont ES,Teacher,Instructional Staff
Ellen,Smith,E32759,ellen.smith2@apsva.us,Claremont ES,Teacher,Instructional Staff
Elizabeth,Uffelman,E32763,elizabeth.uffelman@apsva.us,Claremont ES,Teacher,Instructional Staff
Roua,Abdalla,E32823,roua.abdalla@apsva.us,Claremont ES,Staff,Staff
Sabrina,Khaloua,E32834,sabrina.khaloua@apsva.us,Claremont ES,Staff,Staff
Sanjica,Frei-Harper,E32907,sanjica.freiharper@apsva.us,Claremont ES,Teacher,Instructional Staff
Meaza,Sahlu,E33023,meaza.sahlu@apsva.us,Claremont ES,Staff,Staff
Nubia,Pena De Alvarez,E33130,nubia.penadealvarez@apsva.us,Claremont ES,Staff,Staff
Semira,Mohammed,E33272,semira.mohammed2@apsva.us,Claremont ES,Staff,Staff
Ayanna,Baccus,E3466,ayanna.baccus@apsva.us,Claremont ES,Teacher,Instructional Staff
Ava,Fitch,E4222,ava.fitch@apsva.us,Claremont ES,Instructional Assistant,Instructional Staff
Maria,Valdez,E4813,maria.valdez@apsva.us,Claremont ES,Maintenance Supervisor,Facilities Staff
Marfa,Numbela De Meneses,E4831,marfa.meneses@apsva.us,Claremont ES,Custodian,Facilities Staff
Carolyn,Parker,E6488,carolyn.parker@apsva.us,Claremont ES,Staff,Staff
Ana,Zarceno-Granados,E6682,ana.zarcenogranados@apsva.us,Claremont ES,Teacher,Instructional Staff
John,Findley,E7119,john.findley@apsva.us,Claremont ES,n/a,Unclassified
Manuela,Lozano,E9363,manuela.lozano@apsva.us,Claremont ES,Instructional Assistant,Instructional Staff
Jennifer,Altamirano,E11436,jennifer.altamirano@apsva.us,Discovery ES,Teacher,Instructional Staff
Anna,Senn,E16261,anna.senn@apsva.us,Discovery ES,Administrative Assistant,Staff
Erin,Russo,E16604,erin.russo@apsva.us,Discovery ES,Principal,Unclassified
Jennifer,Dodd,E19484,jennifer.dodd@apsva.us,Discovery ES,Teacher,Instructional Staff
Judy,Seeber,E19620,judith.seeber@apsva.us,Discovery ES,Assistant Principal,Administrative Staff
Lynne,Stein,E2025,lynne.stein@apsva.us,Discovery ES,Teacher,Instructional Staff
Sumi,Barua,E20660,sumi.barua@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
John,Duffy,E21161,john.duffy@apsva.us,Discovery ES,Teacher,Instructional Staff
Jasmin,Te Tan,E22352,jasmin.tetan@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Marcia,Williams,E22474,marcia.williams@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Heather,Clark,E22654,heather.clark@apsva.us,Discovery ES,Teacher,Instructional Staff
Kathryn,Gray,E22768,kathryn.gray@apsva.us,Discovery ES,Teacher,Instructional Staff
Tonya,Senatre,E23166,tonya.senatre@apsva.us,Discovery ES,Administrative Assistant,Staff
Kathleen,Moore Patton,E24369,kathleenmoore.patton@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Christopher,Bangs,E25030,christopher.bangs@apsva.us,Discovery ES,Teacher,Instructional Staff
Sharon,Pou,E25296,sharon.pou@apsva.us,Discovery ES,Teacher,Instructional Staff
Katherine,Stuver,E25726,katherine.stuver@apsva.us,Discovery ES,Teacher,Instructional Staff
Mallary,Hughes,E25808,mallary.hughes@apsva.us,Discovery ES,Teacher,Instructional Staff
John,Perry,E25869,john.perry@apsva.us,Discovery ES,Teacher,Instructional Staff
Katherine,Annamreddy,E25871,katherine.williams@apsva.us,Discovery ES,Teacher,Instructional Staff
Lindsay,Levy,E26380,lindsay.levy@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
John,Re,E26665,john.re@apsva.us,Discovery ES,Teacher,Instructional Staff
Robert,Timke,E26728,robert.timke@apsva.us,Discovery ES,Teacher,Instructional Staff
Jedd,Stein,E26765,jedd.stein@apsva.us,Discovery ES,Teacher,Instructional Staff
Johnsie,Friess,E26767,johnsie.friess@apsva.us,Discovery ES,Teacher,Instructional Staff
Heather,Klanderman,E26771,heather.klanderman@apsva.us,Discovery ES,Teacher,Instructional Staff
Kimberly,Cherry,E26799,kimberly.cherry@apsva.us,Discovery ES,Teacher,Instructional Staff
Angelique,Coulouris,E26800,angelique.coulouris@apsva.us,Discovery ES,Teacher,Instructional Staff
Lauren,Cevenini,E26801,lauren.cevenini@apsva.us,Discovery ES,Teacher,Instructional Staff
Zulma,Escalante,E26948,zulma.escalante@apsva.us,Discovery ES,Custodian,Facilities Staff
Rosie,Barron,E26985,rosie.barron@apsva.us,Discovery ES,Staff,Staff
Erica,McGlothlin,E27039,erica.mcglothlin2@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Wude,Mareye,E27130,wude.marye@apsva.us,Discovery ES,Staff,Staff
Jeanne,Sklar,E27228,jeannie.sklar@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Jennifer,Taylor,E27241,jennifer.taylor2@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Juliana,Spierto,E27733,juliana.spierto@apsva.us,Discovery ES,Teacher,Instructional Staff
Tamatha,Stoker,E27836,tamatha.stoker@apsva.us,Discovery ES,Teacher,Instructional Staff
Francisco,Saravia,E28052,francisco.saravia@apsva.us,Discovery ES,Staff,Staff
Rene,Escobar Arias,E28205,rene.escobararias@apsva.us,Discovery ES,Maintenance Supervisor,Facilities Staff
Khuzaima,Nisar,E28364,khuzaima.nisar2@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Allison,Hutto,E28594,allison.hutto@apsva.us,Discovery ES,Teacher,Instructional Staff
Nicol,Bermudez-Flores,E28649,nicol.bermudezflores@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Jennifer,Healy,E28910,jennifer.healy@apsva.us,Discovery ES,Teacher,Instructional Staff
Julianne,Scopellite,E29070,julianne.scopellite@apsva.us,Discovery ES,Teacher,Instructional Staff
Kathleen,McKinley,E29141,kathleen.mckinley@apsva.us,Discovery ES,Teacher,Instructional Staff
Jaime,Vincent,E29151,jaime.vincent@apsva.us,Discovery ES,Teacher,Instructional Staff
Aditya,Singh,E29448,aditya.singh@apsva.us,Discovery ES,Staff,Staff
Martha Dominguez,Dominguez Barranco,E29580,martha.barranco@apsva.us,Discovery ES,Custodian,Facilities Staff
Rebecca,Weber,E29585,rebecca.weber@apsva.us,Discovery ES,"Instructional Assistant, Staff",Staff
Claire,Nawojchik,E29947,claire.nawojchik@apsva.us,Discovery ES,Teacher,Instructional Staff
Madeline,Laux,E30188,madeline.laux@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Alexandra,Jenkins,E30920,alexandra.jenkins2@apsva.us,Discovery ES,Teacher,Instructional Staff
Virginia,Fisher,E30932,virginia.fisher@apsva.us,Discovery ES,Teacher,Instructional Staff
Rachael,Weisberg,E31000,rachael.weisberg@apsva.us,Discovery ES,Teacher,Instructional Staff
Wendy,McNally,E31132,wendy.mcnally@apsva.us,Discovery ES,Administrative Assistant,Staff
Carol,Halvorson,E31272,carol.halvorson@apsva.us,Discovery ES,Teacher,Instructional Staff
Lady,Aranibar Gallinate,E31423,lady.aranibarg@apsva.us,Discovery ES,Staff,Staff
Mekdes,Wendemu,E31432,mekdes.wendemu@apsva.us,Discovery ES,Staff,Staff
Daisy,Santiago,E31744,daisy.santiago@apsva.us,Discovery ES,Maintenance Staff,Facilities Staff
Zachary,Rush,E32113,zachary.rush@apsva.us,Discovery ES,Teacher,Instructional Staff
Leanne,Brady,E32142,leanne.brady@apsva.us,Discovery ES,Teacher,Instructional Staff
Jennifer,Caputo,E32147,jennifer.caputo@apsva.us,Discovery ES,Teacher,Instructional Staff
Daniela,DiGuido,E32162,daniela.diguido@apsva.us,Discovery ES,Teacher,Instructional Staff
Maria,Cordova Reyes,E32215,maria.cordovareyes@apsva.us,Discovery ES,Custodian,Facilities Staff
Phelan,Burns,E32515,phelan.burns@apsva.us,Discovery ES,Teacher,Instructional Staff
Annie,Del Conte,E32519,annie.delconte2@apsva.us,Discovery ES,Teacher,Instructional Staff
Molly,Lynch,E32626,molly.lynch@apsva.us,Discovery ES,Teacher,Instructional Staff
Noelle,McDonald,E32679,noelle.mcdonald@apsva.us,Discovery ES,Teacher,Instructional Staff
Daniel,Skoloda,E32953,daniel.skoloda@apsva.us,Discovery ES,Staff,Staff
Zion,Amero,E32974,zion.amero@apsva.us,Discovery ES,Staff,Staff
Caulton,Wilson,E33110,caulton.wilson@apsva.us,Discovery ES,Staff,Staff
Josue,Cisneros,E33171,josue.cisneros@apsva.us,Discovery ES,Staff,Staff
Alinah,Mphofe,E33269,alinah.mphofe@apsva.us,Discovery ES,Teacher,Instructional Staff
Rayna,Arias,E33273,rayna.arias@apsva.us,Discovery ES,Staff,Staff
Mindi,Greenberg,E33278,mindi.greenberg@apsva.us,Discovery ES,Teacher,Instructional Staff
Marcel,Ginczek,E33373,marcel.ginczek@apsva.us,Discovery ES,Staff,Staff
Wendy,Duncan,E4069,wendy.duncan@apsva.us,Discovery ES,Teacher,Instructional Staff
Lynda,Jesukiewicz,E5073,lynda.jesukiewicz@apsva.us,Discovery ES,Teacher,Instructional Staff
Maria,Terrazas,E5155,maria.terrazas@apsva.us,Discovery ES,Instructional Assistant,Instructional Staff
Delmy,Elvira,E6007,delmy.elvira@apsva.us,Discovery ES,Staff,Staff
Randy,Glasner,E692,randy.glasner@apsva.us,Discovery ES,n/a,Unclassified
Sandra,Amores,E7007,sandra.amores@apsva.us,Discovery ES,Administrative Assistant,Staff
Laura,Harmon,E8506,laura.harmon@apsva.us,Discovery ES,Teacher,Instructional Staff
Leslie,Wright,E9205,leslie.wright@apsva.us,Discovery ES,Teacher,Instructional Staff
Angela,Torpy,E9853,angela.torpy@apsva.us,Discovery ES,Teacher,Instructional Staff
Ingry,Stephens,E109,ingry.stephens@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Yoki,Ford,E14481,yoki.ford@apsva.us,Drew ES,Teacher,Instructional Staff
Sharmain,Smith,E16628,sharmain.smith@apsva.us,Drew ES,Staff,Staff
Detta,Mullen,E16650,detta.mullen@apsva.us,Drew ES,Teacher,Instructional Staff
Tracy,Gaither,E16664,tracy.gaither@apsva.us,Drew ES,Principal,Unclassified
Nida,Williams,E16993,nida.williams@apsva.us,Drew ES,Teacher,Instructional Staff
Elaina,Eliopoulos,E19155,elaina.eliopoulos@apsva.us,Drew ES,Teacher,Instructional Staff
Tracy,Smith Houston,E19704,tracy.smith@apsva.us,Drew ES,Teacher,Instructional Staff
Kimberly,Godfrey,E19908,kimberly.godfrey@apsva.us,Drew ES,Teacher,Instructional Staff
Rachel,Landry,E20340,rachel.landry@apsva.us,Drew ES,Teacher,Instructional Staff
Heidi,Drake,E20557,heidi.drake@apsva.us,Drew ES,Teacher,Instructional Staff
Adela,Cardenas Ruiz,E20777,adela.cardenasruiz@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Alexander,Hanson,E20895,alexander.hanson@apsva.us,Drew ES,Instructional Assistant,Staff
Douglas,George,E2092,douglas.george@apsva.us,Drew ES,Teacher,Instructional Staff
Ebony,Mallory,E21105,ebony.mallory@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Monica,Harvey,E21370,monica.harvey@apsva.us,Drew ES,Teacher,Instructional Staff
Jessica,Pickering,E22239,jessica.pickering@apsva.us,Drew ES,Teacher,Instructional Staff
Kacee,Grogan,E22288,kacee.grogan@apsva.us,Drew ES,Teacher,Instructional Staff
Martha,Azero,E22627,martha.azero@apsva.us,Drew ES,Teacher,Instructional Staff
Elaine,Edwards,E23078,phyllis.edwards@apsva.us,Drew ES,Teacher,Instructional Staff
Jesse,Williams,E23100,jesse.williams@apsva.us,Drew ES,Teacher,Instructional Staff
Eric,Diggs,E23191,eric.diggs@apsva.us,Drew ES,Teacher,Instructional Staff
Michael,Richard,E23267,michael.richard@apsva.us,Drew ES,Custodian,Facilities Staff
Jeff,Hurtado,E24244,jeff.hurtado@apsva.us,Drew ES,Teacher,Instructional Staff
Yvette,Quarles,E24397,yvette.quarles@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Alton,Dixon-Walker,E24608,alton.dixonwalker@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Saida,Outayeb,E24661,saida.outayeb@apsva.us,Drew ES,Staff,Staff
Nicholas,Tavenner,E24729,nicholas.tavenner@apsva.us,Drew ES,Teacher,Instructional Staff
Caitlin,McGuire,E24933,caitlin.mcguire@apsva.us,Drew ES,Teacher,Instructional Staff
Vashon,Bethea,E24941,vashon.bethea@apsva.us,Drew ES,Teacher,Instructional Staff
Amparo,Figuerola Serra,E25068,amparo.figuerola@apsva.us,Drew ES,Staff,Staff
Ja'Meeka,Lewis,E25132,jameeka.lewis@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Conor,O'Rourke,E25437,conor.orourke@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Megan,Sorrell,E25670,megan.sorrell@apsva.us,Drew ES,Teacher,Instructional Staff
Patrick,Carmack,E25753,patrick.carmack@apsva.us,Drew ES,Teacher,Instructional Staff
Melissa,Rogers,E25799,melissa.rogers@apsva.us,Drew ES,Teacher,Instructional Staff
Lauren,Mosley,E25812,lauren.brinkley@apsva.us,Drew ES,Teacher,Instructional Staff
Gwenda,Roll,E26035,gwenda.roll@apsva.us,Drew ES,Teacher,Instructional Staff
Kristina,Farrell,E26362,kristina.farrell@apsva.us,Drew ES,Teacher,Instructional Staff
Carrie,Christensen,E26797,carrie.christensen@apsva.us,Drew ES,Teacher,Instructional Staff
Deborah,Wacker,E26959,deborah.wacker@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Ashley,Shivers,E26984,ashley.shivers@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Shevina,Wilmore,E27009,shevina.wilmore@apsva.us,Drew ES,Teacher,Instructional Staff
Brittany,Boehm,E27219,brittany.boehm@apsva.us,Drew ES,Teacher,Instructional Staff
Nezha,Selloum,E27616,nezha.selloum@apsva.us,Drew ES,"Instructional Assistant, Staff",Staff
Jamie,Guidry,E27638,jamie.guidry@apsva.us,Drew ES,Teacher,Instructional Staff
Leroy,Dewitt,E27711,leroy.dewitt@apsva.us,Drew ES,"Staff, Instructional Assistant",Instructional Staff
Florencia,Morales,E27775,florencia.morales@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Nerece,Stoltz,E27789,nerece.stoltz@apsva.us,Drew ES,Teacher,Instructional Staff
Masoumeh,Sadjadekalagahe,E28240,masoumeh.sadjadekala@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Jaime,Chervin,E28328,jaime.chervin2@apsva.us,Drew ES,Teacher,Instructional Staff
Antoinette,Moseley,E28505,antoinette.moseley@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Isabel,Majewsky,E28551,isabel.majewsky@apsva.us,Drew ES,Maintenance Supervisor,Facilities Staff
Victoria,Omekam,E28576,victoria.omekam@apsva.us,Drew ES,Staff,Staff
Safwa,Mohamed,E28644,safwa.mohamed@apsva.us,Drew ES,Instructional Assistant,Staff
Angela,Stevens,E29007,angela.stevens@apsva.us,Drew ES,Teacher,Instructional Staff
Molly,Smith,E29167,molly.smith@apsva.us,Drew ES,Teacher,Instructional Staff
Sydney,Mann,E29243,sydney.mann@apsva.us,Drew ES,Teacher,Instructional Staff
Gerber,Rubio,E29325,gerber.rubio@apsva.us,Drew ES,Custodian,Facilities Staff
Aregash,Beyene,E29619,aregash.beyene@apsva.us,Drew ES,Instructional Assistant,Staff
Sabrina,Ziegler,E30046,sabrina.ziegler@apsva.us,Drew ES,Teacher,Instructional Staff
Carlette,Bethea,E30278,carlette.bethea@apsva.us,Drew ES,Teacher,Instructional Staff
Keith,Davis,E30981,keith.davis@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Alexandra,Arnold,E31001,alexandra.arnold@apsva.us,Drew ES,Teacher,Instructional Staff
Angela,Housein,E31090,angela.housein@apsva.us,Drew ES,Teacher,Instructional Staff
Autumn,Kenney,E31341,autumn.kenney@apsva.us,Drew ES,Teacher,Instructional Staff
Eboni,Banks,E31387,eboni.banks@apsva.us,Drew ES,Teacher,Instructional Staff
Courtney,St. John,E31410,courtney.stjohn@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Jean,Kodjo,E31447,jean.kodjo@apsva.us,Drew ES,Custodian,Facilities Staff
April,Webb,E31466,april.webb@apsva.us,Drew ES,Administrative Assistant,Staff
Atari,Griffin,E31505,atari.griffin@apsva.us,Drew ES,Teacher,Instructional Staff
Alize,Carrillo,E31650,alize.carrilo@apsva.us,Drew ES,Instructional Assistant,Staff
Dayren,Soto Valerio,E31656,dayren.sotovalerio@apsva.us,Drew ES,Teacher,Instructional Staff
Mary,Herron,E32011,mary.herron@apsva.us,Drew ES,Teacher,Instructional Staff
Mackenzie,Girard,E32012,mackenzie.girard@apsva.us,Drew ES,Teacher,Instructional Staff
Crystal,Moore,E32106,crystal.moore@apsva.us,Drew ES,Assistant Principal,Administrative Staff
Destinee,Freeman,E32283,destinee.freeman2@apsva.us,Drew ES,Teacher,Instructional Staff
El Tanya,Brown,E32289,eltanya.brown@apsva.us,Drew ES,Teacher,Instructional Staff
Talaya,Grimes,E32331,talaya.grimes@apsva.us,Drew ES,Staff,Staff
Davonne,Douglas,E32338,davonne.douglas@apsva.us,Drew ES,Staff,Staff
Charl,Long,E32407,charl.long@apsva.us,Drew ES,"Staff, Instructional Assistant",Staff
Denise,Green Hardy,E32409,denise.hardygreen@apsva.us,Drew ES,Staff,Staff
Jose,Diaz Vargas,E32439,jose.diazvargas@apsva.us,Drew ES,Custodian,Facilities Staff
Chera,Broadnax,E32564,chera.broadnax@apsva.us,Drew ES,Teacher,Instructional Staff
Megan,Chaney,E32587,megan.chaney@apsva.us,Drew ES,Teacher,Instructional Staff
Carla,Kelley,E32615,carla.kelley@apsva.us,Drew ES,Teacher,Instructional Staff
Melissa,Szymanski,E32621,melissa.szymanski@apsva.us,Drew ES,n/a,Unclassified
Yvonne,Kane,E32650,yvonne.kane@apsva.us,Drew ES,Teacher,Instructional Staff
Viki,Wilson,E32730,viki.wilson@apsva.us,Drew ES,Teacher,Instructional Staff
Erika,Ryan,E32773,erika.ryan@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Kirstin,Lawler,E32917,kirstin.lawler@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Kirstin,Riddick,E33001,kirstin.riddick@apsva.us,Drew ES,Teacher,Instructional Staff
Etana,Hill-Nash,E33024,etana.hillnash@apsva.us,Drew ES,Teacher,Instructional Staff
Jonahlyn,Husar,E33068,jonahlyn.husar@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Shellie,Slack,E33228,shellie.slack@apsva.us,Drew ES,Teacher,Instructional Staff
Rebecca,Begazo,E33268,rebecca.begazo@apsva.us,Drew ES,Teacher,Instructional Staff
Silvia,De Paz Orantes,E33583,silvia.depazorantes@apsva.us,Drew ES,Staff,Staff
Venesia,Edghill,E3396,venesia.edghill@apsva.us,Drew ES,Principals Assistant,Staff
Eiman,Ali,E4847,eiman.ali@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Maria,Parra,E5051,maria.parra@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Aster,Ejigu,E5489,aster.ejigu@apsva.us,Drew ES,Instructional Assistant,Staff
Evin,Rodriguez,E5799,evin.rodriguez@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Natalia,Dzantieva,E6150,natalia.dzantieva@apsva.us,Drew ES,Teacher,Instructional Staff
Maryam,Pouresmaeil,E6738,maryam.pouresmaeil@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Sayyada,Khanun,E6757,sayyada.khanun@apsva.us,Drew ES,Staff,Staff
Rose,Crittenden,E7329,rose.crittenden@apsva.us,Drew ES,Teacher,Instructional Staff
Norma,Rivera,E7332,norma.rivera@apsva.us,Drew ES,Custodian,Facilities Staff
Tatiana,Parra,E7417,tatiana.parra@apsva.us,Drew ES,Instructional Assistant,Instructional Staff
Amreen,Alvi,E826,amreen.alvi@apsva.us,Drew ES,Teacher,Instructional Staff
Sara,Costa,E8522,sara.costa@apsva.us,Drew ES,Teacher,Instructional Staff
Dora Sue,Black,E8983,dorasue.black@apsva.us,Drew ES,Teacher,Instructional Staff
Veronica,Cantor,E9367,veronica.cantor@apsva.us,Drew ES,Administrative Assistant,Staff
Tenita,Chapman,E9805,tenita.chapman@apsva.us,Drew ES,Staff,Staff
Ana,Aleman,E10002,ana.aleman@apsva.us,Glebe ES,Custodian,Facilities Staff
Beronica,Salas,E10104,beronica.salas@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Christine,Williams,E10209,christine.williams@apsva.us,Glebe ES,Teacher,Instructional Staff
Dru,Burns,E10913,dru.burns@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Jeanette,Preniczky,E11128,jeanette.preniczky@apsva.us,Glebe ES,Teacher,Instructional Staff
Jamie,Borg,E1190,jamie.borg@apsva.us,Glebe ES,Principal,Unclassified
Kate,Sydney,E12192,kate.sydney@apsva.us,Glebe ES,Teacher,Instructional Staff
Jessica,Redican,E15653,jessica.redican@apsva.us,Glebe ES,Teacher,Instructional Staff
Denise,Arce,E16701,denise.arce@apsva.us,Glebe ES,Teacher,Instructional Staff
Olukemi,Are,E20021,olukemi.are@apsva.us,Glebe ES,Teacher,Instructional Staff
Beth,Savitsky,E20373,beth.savitsky@apsva.us,Glebe ES,Teacher,Instructional Staff
Lindsey,Brizendine,E20591,lindsey.brizendine@apsva.us,Glebe ES,Teacher,Instructional Staff
Nicholas,Fernandez,E20837,nicholas.fernandez@apsva.us,Glebe ES,Teacher,Instructional Staff
Rene,Arana,E21237,rene.arana@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Julie,Ray,E21416,julie.ray@apsva.us,Glebe ES,Teacher,Instructional Staff
Diona,Fisher,E21715,diona.fisher@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Jenna,Pertl,E21793,jenna.pertl@apsva.us,Glebe ES,Teacher,Instructional Staff
Sara,Saxton,E22053,sara.saxton@apsva.us,Glebe ES,Teacher,Instructional Staff
Cassidy,Burton,E22690,cassidy.burton@apsva.us,Glebe ES,Teacher,Instructional Staff
Lindsay,Estabrooks,E23005,lindsay.estabrooks@apsva.us,Glebe ES,Teacher,Instructional Staff
Jennifer,Sebastian,E23325,jennifer.sebastian@apsva.us,Glebe ES,Teacher,Instructional Staff
Sara,Binet,E23828,sara.binet@apsva.us,Glebe ES,Teacher,Instructional Staff
Brooke,Jones,E24066,brooke.jones@apsva.us,Glebe ES,Teacher,Instructional Staff
Maria,Meraz,E24141,maria.meraz@apsva.us,Glebe ES,Administrative Assistant,Staff
Rebecca,Van Hook,E24165,rebecca.vanhook@apsva.us,Glebe ES,Teacher,Instructional Staff
Katherine,Carbo,E24935,katherine.carbo@apsva.us,Glebe ES,Teacher,Instructional Staff
Beverly,George,E25097,beverly.george@apsva.us,Glebe ES,Staff,Staff
Monique,Henderson,E25120,monique.henderson@apsva.us,Glebe ES,Staff,Staff
Melissa,Sevier,E25766,melissa.sevier@apsva.us,Glebe ES,Teacher,Instructional Staff
Richard,Halttunen,E2585,richard.halttunen@apsva.us,Glebe ES,Teacher,Instructional Staff
Emily,Nolan,E25943,emily.nolan@apsva.us,Glebe ES,Teacher,Instructional Staff
Frayne,Gantus Oller,E26015,frayne.gantusoller@apsva.us,Glebe ES,Staff,Staff
Jeanne,Terrill,E26022,jeanne.terrill@apsva.us,Glebe ES,Teacher,Instructional Staff
Poppy,Jones,E26531,poppy.jones@apsva.us,Glebe ES,Teacher,Instructional Staff
Heather,Morgan,E26639,heather.morgan@apsva.us,Glebe ES,Teacher,Instructional Staff
Erik,Kamenski,E26775,erik.kamenski@apsva.us,Glebe ES,n/a,Unclassified
Kelsey,Gongwer,E26808,kelsey.gongwer@apsva.us,Glebe ES,Teacher,Instructional Staff
Teresa,Foeckler,E26989,teresa.foeckler@apsva.us,Glebe ES,Teacher,Instructional Staff
Tdenek,Ayalew,E27332,tdenek.ayalew@apsva.us,Glebe ES,Staff,Staff
Sabina,Beg,E27614,sabina.beg@apsva.us,Glebe ES,Teacher,Instructional Staff
Tia,Laws,E27875,tia.laws@apsva.us,Glebe ES,Administrative Assistant,Staff
Kristin,Wine,E28053,kristin.wine@apsva.us,Glebe ES,Teacher,Instructional Staff
William,Cruz Cruz,E28527,william.cruzcruz@apsva.us,Glebe ES,Custodian,Facilities Staff
Sarah,Moustafa,E28601,sarah.moustafa@apsva.us,Glebe ES,Teacher,Instructional Staff
Amigh,Mariani,E2866,amigh.mariani@apsva.us,Glebe ES,Teacher,Instructional Staff
Kelly,Cannon,E28818,kelly.cannon@apsva.us,Glebe ES,Teacher,Instructional Staff
Wayne,Herninko,E2931,wayne.herninko@apsva.us,Glebe ES,Teacher,Instructional Staff
Mir,Milani,E29621,mir.milani@apsva.us,Glebe ES,Staff,Staff
Lynn,Westergren,E2981,lynn.westergren@apsva.us,Glebe ES,Teacher,Instructional Staff
Kelly,Manchisi,E29980,kelly.manchisi@apsva.us,Glebe ES,Teacher,Instructional Staff
Asmat,Pervez,E30190,asmat.pervez2@apsva.us,Glebe ES,Staff,Staff
Tara,Blankinship,E30393,tara.blankinship@apsva.us,Glebe ES,Teacher,Instructional Staff
Courtney,Markle,E30613,courtney.markle@apsva.us,Glebe ES,Teacher,Instructional Staff
Emma,Aguirre,E30745,emma.aguirre@apsva.us,Glebe ES,Custodian,Facilities Staff
Diane,Bok,E3140,diane.bok@apsva.us,Glebe ES,Teacher,Instructional Staff
Lori,West,E3167,lori.west@apsva.us,Glebe ES,Teacher,Instructional Staff
Briana,Gallagher,E32084,briana.gallagher@apsva.us,Glebe ES,Teacher,Instructional Staff
Christina,Quinn,E32297,christina.quinn2@apsva.us,Glebe ES,Teacher,Instructional Staff
Kenneth,Gray,E32343,kenneth.gray@apsva.us,Glebe ES,Staff,Staff
Hamdia,Farhan,E32357,hamdia.farhan@apsva.us,Glebe ES,Staff,Staff
Alexis,Hailey,E32470,alexis.hailey@apsva.us,Glebe ES,Staff,Staff
Angela,DiBenigno,E32861,angela.dibenigno@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Cecilia,Escobar Rivera,E32952,cecilia.escobar@apsva.us,Glebe ES,Staff,Staff
Ariane,Montero,E33060,ariane.montero@apsva.us,Glebe ES,Staff,Staff
Keisha,Wallace,E33114,keisha.wallace@apsva.us,Glebe ES,Teacher,Instructional Staff
Allyson,Greene,E3433,allyson.greene@apsva.us,Glebe ES,Teacher,Instructional Staff
Nicholas,Backer,E4190,nicholas.backer@apsva.us,Glebe ES,Teacher,Instructional Staff
Ingrid,Clarke-Marshall,E4310,i.clarkemarshall@apsva.us,Glebe ES,Assistant Principal,Administrative Staff
Laurie,Smith,E4566,laurie.smith@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Bertha,Marquina,E4672,bertha.marquina@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Angela,Gray,E5052,angela.thompson@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Stacey,Lewis,E576,stacey.lewis@apsva.us,Glebe ES,Teacher,Instructional Staff
Rosa,Galeas,E6650,rosa.galeas@apsva.us,Glebe ES,Principals Assistant,Staff
Kirsten,Gutowski,E772,kirsten.gutowski@apsva.us,Glebe ES,Teacher,Instructional Staff
Mary,Mcinerney,E8445,mary.mcinerney@apsva.us,Glebe ES,Teacher,Instructional Staff
Manuel,Hernandez,E8493,manuel.hernandez@apsva.us,Glebe ES,Maintenance Supervisor,Facilities Staff
Clarixa,Portillo,E9299,clarixa.portillo@apsva.us,Glebe ES,Instructional Assistant,Instructional Staff
Eve,Rutzen,E941,eve.rutzen@apsva.us,Glebe ES,Teacher,Instructional Staff
Kelly,Huggler,E9893,kelly.huggler@apsva.us,Glebe ES,Teacher,Instructional Staff
Robert,Dillard,E10224,robert.dillard@apsva.us,Gunston MS,Maintenance Supervisor,Facilities Staff
Josh,Merck,E10688,josh.merck@apsva.us,Gunston MS,"Teacher, Staff",Staff
Luzdary,Chamorro,E10695,luzdary.chamorro@apsva.us,Gunston MS,Teacher,Instructional Staff
James,Kolody,E10741,james.kolody@apsva.us,Gunston MS,Teacher,Instructional Staff
Joanita,Jewell,E12096,joanita.jewell@apsva.us,Gunston MS,Custodian,Facilities Staff
Sarah,Stewart,E1296,sarah.stewart@apsva.us,Gunston MS,Teacher,Instructional Staff
Gabriel,Herrera,E13978,gabriel.herrera@apsva.us,Gunston MS,Custodian,Facilities Staff
Karen,Taylor,E14453,karen.taylor@apsva.us,Gunston MS,"Instructional Assistant, Staff",Instructional Staff
Elma,Molina-Riggle,E14872,elma.molina@apsva.us,Gunston MS,Teacher,Instructional Staff
Marlene,Cordero,E14916,marlene.cordero@apsva.us,Gunston MS,Teacher,Instructional Staff
Kevin,Morrissey,E15036,kevin.morrissey@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Amaziah,Diggs,E16007,amaziah.diggs@apsva.us,Gunston MS,"Instructional Assistant, Staff",Instructional Staff
Andre,Walker,E16378,andre.walker@apsva.us,Gunston MS,Staff,Staff
Samantha,King,E16622,samantha.king@apsva.us,Gunston MS,Teacher,Instructional Staff
Elizabeth,Egbert,E19288,elizabeth.egbert@apsva.us,Gunston MS,n/a,Unclassified
Somer,Abdeljawad,E19474,somer.abdeljawad@apsva.us,Gunston MS,"Teacher, Staff",Staff
Ofelia,McKenzie,E19505,ofelia.mckenzie@apsva.us,Gunston MS,Teacher,Instructional Staff
Phillip,Mannos,E19788,phillip.mannos@apsva.us,Gunston MS,Teacher,Instructional Staff
Kathyh,Thomas,E20283,kathyh.thomas@apsva.us,Gunston MS,Manager,Staff
Jody,Gilmore,E20627,jody.gilmore@apsva.us,Gunston MS,Teacher,Instructional Staff
Elon,Gant,E20776,elon.gant@apsva.us,Gunston MS,Custodian,Facilities Staff
Harry,Costner,E2084,harry.costner@apsva.us,Gunston MS,Teacher,Instructional Staff
Daniel,Rios,E21433,daniel.rios@apsva.us,Gunston MS,Teacher,Instructional Staff
Michael,Ruck,E21557,michael.ruck@apsva.us,Gunston MS,Teacher,Instructional Staff
Wasan,Al Qaisi,E21920,wasan.alqaisi@apsva.us,Gunston MS,n/a,Unclassified
Adriano,Borruto,E22021,adriano.borruto@apsva.us,Gunston MS,Clerical Specialist,Staff
Gary,Poole,E22376,gary.poole@apsva.us,Gunston MS,Maintenance Supervisor,Facilities Staff
Rocio,Quiroga Velasquez,E22606,rocio.quiroga@apsva.us,Gunston MS,Teacher,Instructional Staff
Cherie,Phipps,E23022,cherie.phipps@apsva.us,Gunston MS,Teacher,Instructional Staff
Nikole,Joshi,E23117,nikole.joshi@apsva.us,Gunston MS,Teacher,Instructional Staff
Lina,Tenjo Tipton,E23317,lina.tenjotipton@apsva.us,Gunston MS,Teacher,Instructional Staff
Lori,Wiggins,E23851,lori.wiggins@apsva.us,Gunston MS,Principal,Unclassified
Jawana,Washington,E24094,jawana.washington@apsva.us,Gunston MS,Teacher,Instructional Staff
Farrah,Johnson,E24130,farrah.johnson@apsva.us,Gunston MS,Teacher,Instructional Staff
Manuel,Lainez,E24205,manuel.lainez@apsva.us,Gunston MS,Account Clerk,Staff
Brittney,Gilreath,E24233,brittney.gilreath@apsva.us,Gunston MS,Teacher,Instructional Staff
Brannon,Burnett,E24286,brannon.burnett@apsva.us,Gunston MS,Assistant Principal,Administrative Staff
Lamar,Sykes,E24413,lamar.sykes@apsva.us,Gunston MS,Maintenance Supervisor,Facilities Staff
Carrieann,Quinn,E24434,carrieann.quinn@apsva.us,Gunston MS,"Teacher, Staff",Staff
Megan,McCormick,E24930,megan.mccormick@apsva.us,Gunston MS,Teacher,Instructional Staff
Carolyn,Johnson,E25293,carolyn.johnson@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Clark,Petersen,E25418,clark.petersen@apsva.us,Gunston MS,n/a,Unclassified
Anthony,Hyman,E25947,anthony.hyman@apsva.us,Gunston MS,Teacher,Instructional Staff
Fallon,Keplinger,E25960,fallon.keplinger@apsva.us,Gunston MS,Teacher,Instructional Staff
Katherine,Boone,E26068,katherine.boone@apsva.us,Gunston MS,Teacher,Instructional Staff
Andrea,Ross,E26166,andrea.ross@apsva.us,Gunston MS,"Instructional Assistant, Staff",Staff
Ingrid,Kearns,E26478,ingrid.kearns@apsva.us,Gunston MS,Teacher,Instructional Staff
Kimberly,Moore,E26661,kimberly.moore@apsva.us,Gunston MS,Teacher,Instructional Staff
Khethiwe,Hudson,E26666,khethiwe.mdluli@apsva.us,Gunston MS,Assistant Principal,Administrative Staff
Nadia,Robles,E2674,nadia.robles@apsva.us,Gunston MS,Teacher,Instructional Staff
Phaedra,Long,E26754,phaedra.long@apsva.us,Gunston MS,Teacher,Instructional Staff
Mary,Kell,E26774,mary.kell@apsva.us,Gunston MS,Teacher,Instructional Staff
Maria,Cox,E26810,maria.cox@apsva.us,Gunston MS,Teacher,Instructional Staff
Wilson,Ramirez,E26868,wilson.ramirez@apsva.us,Gunston MS,Assistant Principal,Administrative Staff
Damali,Dayne,E26896,damali.dayne@apsva.us,Gunston MS,Teacher,Instructional Staff
Philippe,Millogo,E27133,philippe.millogo@apsva.us,Gunston MS,Teacher,Instructional Staff
Erlis,Ramirez,E27235,erlis.ramirez@apsva.us,Gunston MS,Custodian,Facilities Staff
Christopher,Paterno,E27249,christopher.paterno@apsva.us,Gunston MS,Teacher,Instructional Staff
Laura,O'Dea,E27353,laura.odea@apsva.us,Gunston MS,Teacher,Instructional Staff
Paxton,Helms,E27384,paxton.helms@apsva.us,Gunston MS,Teacher,Instructional Staff
Vance,Page,E27393,vance.page@apsva.us,Gunston MS,Teacher,Instructional Staff
Cynthia,MacFarlane-Picard,E27396,cynthia.picard2@apsva.us,Gunston MS,Teacher,Instructional Staff
Maria,Gomez,E27402,maria.gomez@apsva.us,Gunston MS,Teacher,Instructional Staff
Amelia,Do Rosario Fortes,E27637,amelia.fortes@apsva.us,Gunston MS,"Staff, Instructional Assistant",Instructional Staff
William,Ashby,E27665,william.ashby@apsva.us,Gunston MS,"Staff, Instructional Assistant",Staff
Xavier,Cooper,E27827,xavier.cooper@apsva.us,Gunston MS,Staff,Staff
Constanza,Rojas-Garcia,E27832,patricia.rojasgarcia@apsva.us,Gunston MS,Teacher,Instructional Staff
Almarie,Campbell,E27870,almarie.campbell@apsva.us,Gunston MS,Teacher,Instructional Staff
Joseph,Caldwell,E27931,joseph.caldwell@apsva.us,Gunston MS,Teacher,Instructional Staff
Barry,Aberdeen,E27945,barry.aberdeen@apsva.us,Gunston MS,Teacher,Instructional Staff
Daphney,Denerville-Davis,E28005,daphney.davis@apsva.us,Gunston MS,n/a,Unclassified
Danielle,Vogel,E28102,danielle.vogel@apsva.us,Gunston MS,Teacher,Instructional Staff
Yvonne,Easaw,E28103,yvonne.easaw@apsva.us,Gunston MS,Teacher,Instructional Staff
Kevin,Parks,E28133,kevin.parks@apsva.us,Gunston MS,Teacher,Instructional Staff
"Matos, Luis",Malave,E28170,matosluis.malave@apsva.us,Gunston MS,Coordinator,Staff
Dakota,Springston,E28250,dakota.springston@apsva.us,Gunston MS,Teacher,Instructional Staff
Maria,Daulon Nunez,E28708,maria.daulonnunez@apsva.us,Gunston MS,Administrative Assistant,Staff
Samia,Bellounis,E28745,samia.bellounis@apsva.us,Gunston MS,Administrative Assistant,Staff
Katie,Yu,E28769,katie.yu@apsva.us,Gunston MS,Teacher,Instructional Staff
Maren,Herzog,E28918,maren.herzog@apsva.us,Gunston MS,Teacher,Instructional Staff
Lindsay,Carr,E28920,lindsay.carr@apsva.us,Gunston MS,Teacher,Instructional Staff
Andrew,Divett,E28935,andrew.divett@apsva.us,Gunston MS,Teacher,Instructional Staff
Brandon,Clark,E29137,brandon.clark@apsva.us,Gunston MS,Teacher,Instructional Staff
Emily,Silverberg,E29152,emily.silverberg@apsva.us,Gunston MS,Teacher,Instructional Staff
Megan,Stetson,E29278,megan.stetson@apsva.us,Gunston MS,Teacher,Instructional Staff
Anita,O'Quinn,E29324,anita.oquinn@apsva.us,Gunston MS,Staff,Staff
Maria,Romero,E29343,maria.romero2@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Andrea,Villarroel,E29492,andrea.villarroel@apsva.us,Gunston MS,Administrative Assistant,Staff
Benjamin,Beatty,E30083,benjamin.beatty@apsva.us,Gunston MS,Teacher,Instructional Staff
Meredith,Kruzel,E30122,meredith.kruzel@apsva.us,Gunston MS,Teacher,Instructional Staff
Emily,Cotton,E30139,emily.cotton@apsva.us,Gunston MS,Teacher,Instructional Staff
Shanna,Ford,E30167,shanna.ford@apsva.us,Gunston MS,Administrative Assistant,Staff
Edgar,King,E30214,edgar.king@apsva.us,Gunston MS,Teacher,Instructional Staff
Westen,Muntain,E30263,westen.muntain@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Andrea,Merida Davalos,E30422,andrea.meridadavalos@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Tyler,Combs Harmon,E30588,tyler.combsharmon@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Maija,Lawler,E30654,maija.lawler@apsva.us,Gunston MS,Teacher,Instructional Staff
Yannick,Tanda,E30657,yannick.tanda@apsva.us,Gunston MS,Staff,Staff
Kimberly,Bauer,E31003,kimberly.bauer@apsva.us,Gunston MS,Teacher,Instructional Staff
Courtney,Brophy,E31094,courtney.brophy@apsva.us,Gunston MS,Teacher,Instructional Staff
Kelly,Tucker,E31145,kelly.tucker@apsva.us,Gunston MS,Teacher,Instructional Staff
Daisy,Quiroz Claros,E31177,daisy.quirozclaros@apsva.us,Gunston MS,Custodian,Facilities Staff
Kennedi,Poland,E31242,kennedi.poland@apsva.us,Gunston MS,Teacher,Instructional Staff
Julia,Taegel,E31264,julia.taegel@apsva.us,Gunston MS,Teacher,Instructional Staff
Stephanie,Giblin,E31282,stephanie.giblin@apsva.us,Gunston MS,Teacher,Instructional Staff
Rachel,Fields,E31303,rachel.fields@apsva.us,Gunston MS,Teacher,Instructional Staff
Randall,Stewart,E31314,randall.stewart@apsva.us,Gunston MS,Teacher,Instructional Staff
Lisa,Pentland,E31318,lisa.pentland@apsva.us,Gunston MS,Teacher,Instructional Staff
Donna,Lucchesi,E3138,donna.lucchesi@apsva.us,Gunston MS,Teacher,Instructional Staff
Evelyn,Benavides-Guzman,E31427,evelyn.benavides@apsva.us,Gunston MS,Staff,Staff
Nicole,Edmonds,E31441,nicole.edmonds@apsva.us,Gunston MS,n/a,Unclassified
Veronica,Stokes,E31448,veronica.stokes@apsva.us,Gunston MS,Teacher,Instructional Staff
Shepherd,Archer,E31506,shepherd.archer@apsva.us,Gunston MS,Staff,Staff
Maria,Delgado,E31552,maria.delgado@apsva.us,Gunston MS,Teacher,Instructional Staff
Robin,Roberts,E31688,robin.roberts@apsva.us,Gunston MS,Teacher,Instructional Staff
Gregory,Jones,E31973,gregory.jones@apsva.us,Gunston MS,Teacher,Instructional Staff
Jennifer,Keller,E32056,jennifer.keller@apsva.us,Gunston MS,Teacher,Instructional Staff
Nevelyne,Rodriguez Serrano,E32069,nevelyne.rodriguez@apsva.us,Gunston MS,Teacher,Instructional Staff
Kyana,Stallworth,E32140,kyana.stallworth@apsva.us,Gunston MS,Teacher,Instructional Staff
Luzmila,Huerta Romero,E32205,luzmila.huertaromero@apsva.us,Gunston MS,Custodian,Facilities Staff
Frederick,Dupree,E32210,frederick.dupree@apsva.us,Gunston MS,Teacher,Instructional Staff
Roxana,Alfaro,E3223,roxana.alfaro@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Elva,Quiroz Claros,E32231,elva.quirozclaros@apsva.us,Gunston MS,Custodian,Facilities Staff
Sarah,Kass,E32241,sarah.kass@apsva.us,Gunston MS,Teacher,Instructional Staff
Mardochee,Utshudi,E32249,mardochee.utshudi@apsva.us,Gunston MS,Teacher,Instructional Staff
Kate,Porter,E32279,kate.porter2@apsva.us,Gunston MS,Teacher,Instructional Staff
Jakyra,Sweetenburg,E32363,jakyra.sweetenburg@apsva.us,Gunston MS,Staff,Staff
Sarah,Cooper,E32364,sarah.cooper@apsva.us,Gunston MS,Staff,Staff
Alethea,Amoafo,E32403,alethea.amoafo@apsva.us,Gunston MS,Staff,Staff
Nikko,Preece,E32404,nikko.preece@apsva.us,Gunston MS,Staff,Staff
Heesook,Kang,E32406,heesook.kang@apsva.us,Gunston MS,Staff,Staff
Cheryl,Conley,E32600,cheryl.conley@apsva.us,Gunston MS,Teacher,Instructional Staff
Julio,Nunez Pagan,E32608,julio.nunezpagan@apsva.us,Gunston MS,Teacher,Instructional Staff
Devon,King,E32671,devon.king@apsva.us,Gunston MS,Teacher,Instructional Staff
Kristin,La,E32718,kristin.la@apsva.us,Gunston MS,Teacher,Instructional Staff
Karina,Curl,E32758,karina.curl@apsva.us,Gunston MS,Teacher,Instructional Staff
Danielle,Hatcher,E32762,danielle.hatcher@apsva.us,Gunston MS,Teacher,Instructional Staff
Rebecca,Lynn-Yee,E32777,rebecca.lynnyee@apsva.us,Gunston MS,Teacher,Instructional Staff
Melissa,Bridgers,E32786,melissa.bridgers@apsva.us,Gunston MS,Teacher,Instructional Staff
Shannon,Pennell,E32792,shannon.pennell@apsva.us,Gunston MS,Teacher,Instructional Staff
Giovanni,Espinoza-Montecinos,E32853,giovanni.espinoza@apsva.us,Gunston MS,Maintenance Staff,Facilities Staff
Judvernia,Baxter,E32886,judvernia.baxter@apsva.us,Gunston MS,Teacher,Instructional Staff
Fabiola,St Hilaire,E33015,fabiola.sthilaire@apsva.us,Gunston MS,Teacher,Instructional Staff
Darius,Wallace,E33054,darius.wallace@apsva.us,Gunston MS,Custodian,Facilities Staff
Cynthia,Phillips,E33248,cynthia.phillips@apsva.us,Gunston MS,Staff,Staff
Dangelo,Smith,E33568,dangelo.smith@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Charlene,Jones,E3650,charlene.wentz@apsva.us,Gunston MS,Teacher,Instructional Staff
Sharon,Kolody,E3926,sharon.kolody@apsva.us,Gunston MS,"Staff, Teacher",Staff
Tony,Roberts,E4116,tony.roberts@apsva.us,Gunston MS,Instructional Assistant,Instructional Staff
Claudia,Merida,E4825,claudia.merida@apsva.us,Gunston MS,Staff,Staff
Dena,Gollopp,E5083,dena.gollopp@apsva.us,Gunston MS,Teacher,Instructional Staff
Stephanie,Palmer-Kirkland,E5564,stephanie.blue@apsva.us,Gunston MS,Custodian,Facilities Staff
Caitlin,Wittig,E5721,caitlin.wittig@apsva.us,Gunston MS,Teacher,Instructional Staff
Karen,Mullins,E7030,karen.mullins@apsva.us,Gunston MS,"Instructional Assistant, Staff",Staff
Jenny,Saunders,E7751,jenny.saunders@apsva.us,Gunston MS,Teacher,Instructional Staff
Carmen,Thames,E8017,carmen.thames@apsva.us,Gunston MS,"Staff, Instructional Assistant",Instructional Staff
Tamika,Rector,E8558,tamika.rector@apsva.us,Gunston MS,Administrative Assistant,Staff
Janet,Luu,E9649,janet.luu@apsva.us,Gunston MS,n/a,Unclassified
Maria,Marquez,E10191,maria.marquez@apsva.us,Fleet ES,Custodian,Facilities Staff
Marshall,Cunningham,E10518,marshall.cunningham@apsva.us,Fleet ES,Maintenance Supervisor,Facilities Staff
Jeanne,Cary,E1055,jeanne.cary@apsva.us,Fleet ES,Teacher,Instructional Staff
Karen,Wolla,E10750,karen.wolla@apsva.us,Fleet ES,Teacher,Instructional Staff
Estelle,Roth,E10802,estelle.roth@apsva.us,Fleet ES,n/a,Unclassified
Keisha,Andrews-Veney,E10891,keisha.andrews@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Tammy,John,E12978,tammy.john@apsva.us,Fleet ES,Teacher,Instructional Staff
Kathy,Bovino,E13020,kathy.bovino@apsva.us,Fleet ES,Teacher,Instructional Staff
Jon,Stewart,E14069,jon.stewart@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Ingrith,Grundy,E14164,ingrith.grundy@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Sylvia,Garcia,E15893,sylvia.garcia@apsva.us,Fleet ES,Teacher,Instructional Staff
Serene,Unny,E16456,serene.unny@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Susan,Spranger,E1672,susan.spranger@apsva.us,Fleet ES,Teacher,Instructional Staff
Juana,Love,E17109,juana.love@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Nancy,Butt,E19626,nancy.buttpackard@apsva.us,Fleet ES,"Teacher, Staff",Staff
Donleigh,Zaleski,E19640,donleigh.honeywell@apsva.us,Fleet ES,Principals Assistant,Staff
Zahra,Azargui,E20127,zahra.azargui@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Erin,Upton,E20564,erin.upton@apsva.us,Fleet ES,Teacher,Instructional Staff
Rosemarie,Tenney,E20707,rosemarie.tenney@apsva.us,Fleet ES,Teacher,Instructional Staff
Jenn,Mercado,E21511,jenn.mercado@apsva.us,Fleet ES,Teacher,Instructional Staff
Danielle,Fuller,E21607,danielle.fuller@apsva.us,Fleet ES,Teacher,Instructional Staff
Robert,Te Tan,E22360,robert.tetan@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Tereza,Townsend,E22716,tereza.townsend@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Tracy,Cowden,E23043,tracy.cowden@apsva.us,Fleet ES,Teacher,Instructional Staff
Jamie,Day,E23064,jamie.day@apsva.us,Fleet ES,Teacher,Instructional Staff
Donna,English,E23160,donna.english@apsva.us,Fleet ES,Teacher,Instructional Staff
Shelly,Buxton,E23455,shelly.buxton@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Jennifer,Gildea,E23875,jennifer.gildea@apsva.us,Fleet ES,Principal,Administrative Staff
Stacey,Cooper-Porter,E23962,stacey.cooperporter@apsva.us,Fleet ES,Teacher,Instructional Staff
Rebecca,Ward,E23991,rebecca.ward@apsva.us,Fleet ES,Assistant Principal,Administrative Staff
Kelly,Jackson,E24012,kelly.mcdonald@apsva.us,Fleet ES,Teacher,Instructional Staff
Angela,Manzano,E24161,angela.manzano@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Katherine,Hernandez,E24183,katherine.hernandez@apsva.us,Fleet ES,Teacher,Instructional Staff
Julianne,Tela,E24497,julianne.tela@apsva.us,Fleet ES,Teacher,Instructional Staff
Delores,McKnight,E24506,delores.mcknight@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Mikayla,Jewell,E24515,mikayla.jewell@apsva.us,Fleet ES,Administrative Assistant,Staff
Andres,Hernandez,E24813,andres.hernandez@apsva.us,Fleet ES,Teacher,Instructional Staff
Erin,Pavolko,E24815,erin.pavolko@apsva.us,Fleet ES,Teacher,Instructional Staff
Stacy,Rinker,E25739,stacy.rinker@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Jennifer,Kim,E25762,jennifer.kim@apsva.us,Fleet ES,Teacher,Instructional Staff
Jessica,Blackburn,E25908,jessica.blackburn@apsva.us,Fleet ES,Teacher,Instructional Staff
George,Braden,E26139,george.braden@apsva.us,Fleet ES,Teacher,Instructional Staff
Brian,Coonce,E26140,brian.coonce@apsva.us,Fleet ES,Teacher,Instructional Staff
Lidia,Reyes,E26310,lidia.reyes@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Keysi,Villalobos,E26730,keysi.villalobos@apsva.us,Fleet ES,Teacher,Instructional Staff
Ashley,Snyder,E26792,ashley.snyder@apsva.us,Fleet ES,Teacher,Instructional Staff
Sharla,Hamilton,E26802,sharla.hamilton@apsva.us,Fleet ES,Teacher,Instructional Staff
Susan,Service,E2682,susan.service@apsva.us,Fleet ES,Staff,Staff
Helen,Gray,E26894,helen.gray@apsva.us,Fleet ES,Teacher,Instructional Staff
Andrea,Dell'Accio,E26967,andrea.dellaccio@apsva.us,Fleet ES,Teacher,Instructional Staff
Amanda,Murray,E27002,amanda.murray@apsva.us,Fleet ES,Teacher,Instructional Staff
Michelle,Weston,E27146,michelle.weston2@apsva.us,Fleet ES,Teacher,Instructional Staff
Christine,Valvo,E27808,christine.valvo@apsva.us,Fleet ES,Teacher,Instructional Staff
Sarah,Switaj,E27857,sarah.switaj@apsva.us,Fleet ES,Teacher,Instructional Staff
Tierra,Clayton,E28041,tierra.clayton@apsva.us,Fleet ES,Staff,Staff
Jill,Yasin,E28071,jill.yasin@apsva.us,Fleet ES,Teacher,Instructional Staff
Catherine,Maness,E28961,catherine.maness@apsva.us,Fleet ES,Teacher,Instructional Staff
Kelly,Piantedosi,E29000,kelly.worland@apsva.us,Fleet ES,Teacher,Instructional Staff
Keiry,Argueta Jarquin,E29111,keiry.arguetajarquin@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Lesli,Estrada,E29184,lesli.estrada@apsva.us,Fleet ES,Staff,Staff
Danielle,Weaver,E29233,danielle.weaver@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Paloma,Flynn,E29252,paloma.flynn@apsva.us,Fleet ES,Teacher,Instructional Staff
Shaunte,Bethea,E29339,shaunte.bethea@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Shirin,Kabir,E29426,shirin.kabir@apsva.us,Fleet ES,Staff,Staff
Lyric,Hatcher,E29829,lyric.hatcher@apsva.us,Fleet ES,Teacher,Instructional Staff
Lindsay,Ference,E30172,lindsay.ference@apsva.us,Fleet ES,Teacher,Instructional Staff
Jazmin,Lopez Soto,E30244,jazmin.lopezsoto@apsva.us,Fleet ES,Staff,Staff
R,Cespedes Villarroel,E30249,r.cespedesvillarroel@apsva.us,Fleet ES,Custodian,Facilities Staff
Grisel,Edsall,E30265,grisel.edsall@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Rawshonara,Islam,E30845,rawshonara.islam@apsva.us,Fleet ES,Staff,Staff
Emma,Urban,E30922,emma.urban2@apsva.us,Fleet ES,Teacher,Instructional Staff
Elham,Saker,E30947,elham.saker@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Kwabena,Akowuah,E30988,kwabena.akowuah@apsva.us,Fleet ES,Custodian,Facilities Staff
Troy,Savage,E31515,troy.savage@apsva.us,Fleet ES,Staff,Staff
Morgan,Lee,E31569,morgan.lee@apsva.us,Fleet ES,Teacher,Instructional Staff
Brenda,Castellanos,E31623,brenda.castellanos@apsva.us,Fleet ES,Teacher,Instructional Staff
Daniel,Merida,E31832,daniel.merida@apsva.us,Fleet ES,Staff,Staff
Regina,Montano,E31859,regina.montano@apsva.us,Fleet ES,Teacher,Instructional Staff
Anne,Womack,E32002,anne.womack@apsva.us,Fleet ES,Teacher,Instructional Staff
Anita,Hardwick,E32009,anita.hardwick@apsva.us,Fleet ES,Teacher,Instructional Staff
Joshua,Readshaw,E32032,joshua.readshaw@apsva.us,Fleet ES,Teacher,Instructional Staff
Alanah,Aquino,E32127,alanah.aquino@apsva.us,Fleet ES,Teacher,Instructional Staff
Tommi,Taylor,E32256,tommi.taylor@apsva.us,Fleet ES,Teacher,Instructional Staff
Ambra,Xhepa,E32505,ambra.xhepa@apsva.us,Fleet ES,n/a,Unclassified
William,Bates,E32516,william.bates@apsva.us,Fleet ES,Custodian,Facilities Staff
Hanna,Hensley,E32556,hanna.hensley@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Veronica,Butler,E32651,veronica.butler@apsva.us,Fleet ES,Teacher,Instructional Staff
Camryn,Groth,E32694,camryn.groth@apsva.us,Fleet ES,Teacher,Instructional Staff
Alyssa,Cox,E32704,alyssa.cox@apsva.us,Fleet ES,Teacher,Instructional Staff
Kiana,Petty,E32827,kiana.petty@apsva.us,Fleet ES,Teacher,Instructional Staff
Tabitha,Woods-Jackson,E32903,tabitha.woodsjackson@apsva.us,Fleet ES,Teacher,Instructional Staff
Jasmine,Brown,E33290,jasmine.brown@apsva.us,Fleet ES,Instructional Assistant,Instructional Staff
Gillian,Mogab,E33392,gillian.mogab@apsva.us,Fleet ES,Teacher,Instructional Staff
Mercedes,Dean,E3344,mercedes.dean@apsva.us,Fleet ES,Teacher,Instructional Staff
Ellen,Moreno,E4527,ellen.moreno@apsva.us,Fleet ES,Administrative Assistant,Staff
Keenan,Hall,E5254,keenan.hall@apsva.us,Fleet ES,Teacher,Instructional Staff
April,Sommer,E798,april.sommer@apsva.us,Fleet ES,Teacher,Instructional Staff
Luis,Guerrero,E8839,luis.guerrero@apsva.us,Fleet ES,Custodian,Facilities Staff
Valerie,Smith,E10439,valerie.jackson@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Tarica,Mason,E10446,tarica.mason@apsva.us,Hoffman-Boston ES,Staff,Staff
Maureen,Fox,E11857,maureen.fox@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Velissa,Jones,E14140,velissa.jones@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Dalila,Adams,E14304,dalila.adams@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Olivia,Funnye,E14868,olivia.funnye@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Gradiva,Jimenez,E15793,gradiva.jimenez@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Heidi,Smith,E15908,heidi.smith@apsva.us,Hoffman-Boston ES,Principal,Unclassified
Kristin,Gomez,E15912,kristin.gomez@apsva.us,Hoffman-Boston ES,n/a,Unclassified
Samuel,Wilborn,E15943,samuel.wilborn@apsva.us,Hoffman-Boston ES,Custodian,Facilities Staff
Alexandra,Tagle,E16101,alexandra.tagle@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Augusto,Wayar,E16157,augusto.wayar@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Edwin,Hernandez,E16297,edwin.hernandez@apsva.us,Hoffman-Boston ES,Manager,Staff
Christianne,Chong,E16550,christianne.chong@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Andrew,Klein,E16614,andrew.klein@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Allison,Walker,E16636,allison.walker@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Wendy,Yarborough,E16678,wendy.yarborough@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Melissa,Hinkson,E16878,melissa.hinkson@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Vonique,Bullock,E16978,vonique.bullock@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Jennifer,Walter,E1698,jennifer.walter@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Deitra,Pulliam,E1888,deitra.pulliam@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Tamara,Lamantia,E20168,tamara.lamantia@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Buoy,Kor,E20305,buoy.kor@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Brionne,Goode,E20353,brionne.goode@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Patricia,Harvey,E20423,patricia.harvey@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Lauren,Stephenson,E20479,lauren.stephenson@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Soroush,Zargarani,E21298,soroush.zargarani@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Maryruth,Findlay,E21448,maryruth.findlay@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Dalila,Cresswell,E21466,dalila.marquez@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Emily,Bye,E21529,emily.bye@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Helena,Taylor,E21577,helena.taylor@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Jennifer,Burgin,E21593,jennifer.burgin@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Elizabeth,Clavel Love,E21686,elizabeth.clavellove@apsva.us,Hoffman-Boston ES,Principals Assistant,Staff
Gregg,Siegal,E21984,gregg.siegal@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Nora,Garcia,E22400,nora.garcia@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Pricilla,Hopkins,E22449,pricilla.hopkins@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Kris,Pflaging,E22481,kris.pflaging@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Elizabeth,Houston,E22521,elizabeth.houston@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Suzanne,Paul,E22522,suzanne.paul@apsva.us,Hoffman-Boston ES,Assistant Principal,Administrative Staff
Abeba,Tesfaye,E22577,abeba.tesfaye@apsva.us,Hoffman-Boston ES,Staff,Staff
Sewasew,Belachew,E22651,sewasew.belachew@apsva.us,Hoffman-Boston ES,Staff,Staff
Carolyn,Cobb,E23098,carolyn.warren@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Lila,Vega,E23144,lila.vega2@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Belinda,Folb,E23669,belinda.folb@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Charles,Gyamfi,E23720,charles.gyamfi@apsva.us,Hoffman-Boston ES,Maintenance Supervisor,Facilities Staff
Julie,Freeman-Moore,E23754,julie.freemanmoore@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Molly,Haines,E24034,molly.haines@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Eva Marie,Fadel,E24121,evamarie.fadel@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Doaa,Elamin,E24221,doaa.elamin@apsva.us,Hoffman-Boston ES,"Instructional Assistant, Staff",Instructional Staff
Cassandra,Wilson,E24258,cassandra.wilson@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Willie,Garner,E24270,willie.garner@apsva.us,Hoffman-Boston ES,"Instructional Assistant, Staff",Instructional Staff
Asmeret,Gebremariam,E24365,asmeret.gebremariam@apsva.us,Hoffman-Boston ES,Staff,Staff
Andrew,Myers,E24455,andrew.myers@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Zachary,Martini,E24879,zachary.martini@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
John,McMahon,E25203,john.mcmahon@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Chelsea,Craddock,E25206,chelsea.craddock@apsva.us,Hoffman-Boston ES,Staff,Staff
Leyla,Plunkett,E25788,leyla.plunkett@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Emily,Nehring,E25828,emily.billmyre@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Megan,Taylor,E25840,megan.taylor@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Megan,Pavelich,E25863,megan.pavelich@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Samantha,Sklar,E25902,samantha.sklar@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Zachary,Fritz,E25911,zachary.fritz@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Mungunzaya,Coughlin,E26123,mungunzaya.coughlin@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Kiewana,Williams,E26216,kiewana.williams@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Cynthia,Columbo,E26798,cynthia.columbo@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Kelly,Barrett,E26886,kelly.barrett@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Bianca,White,E27051,bianca.white@apsva.us,Hoffman-Boston ES,Staff,Staff
Kirsten,Fitzgerald,E27078,kirsten.fitzgerald@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Liliana,Salazar,E27143,liliana.salazar@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Amira,Alhamudi,E27457,amira.alhamudi@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Caitlin,Godec,E27558,caitlin.godec@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Zaniya,Ward-Brown,E27591,zaniya.wardbrown@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Douglas,Clarke,E27963,douglas.clarke@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Erynn,O'Mahen,E28018,erynn.omahen@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Tierra,Clayton,E28041,tierra.clayton@apsva.us,Hoffman-Boston ES,Staff,Staff
Rene,Escobar,E28045,rene.escobar@apsva.us,Hoffman-Boston ES,"Staff, Instructional Assistant",Instructional Staff
Sierra,Marsicek,E28158,sierra.marsicek@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Rupa,Barua,E28270,rupa.barua2@apsva.us,Hoffman-Boston ES,Staff,Staff
Duana,Brooks,E28651,duana.brooks@apsva.us,Hoffman-Boston ES,Administrative Assistant,Staff
Emily,Hernandez,E28940,emily.hernandez@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Candy,Echague,E28962,candy.echague@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Steven,Torres,E28999,steven.torres@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Bobbi,Dailey,E29063,bobbi.walker2@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Claudia,Campos,E29139,claudia.campos@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Ngan,Tran,E29164,ngan.tran@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Julu,Kear,E29241,julu.kear@apsva.us,Hoffman-Boston ES,Custodian,Facilities Staff
Deborah,Shell-Casalengo,E29287,deborah.casalengo@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Morgan,Aylor,E29309,morgan.aylor@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Anna,Johnson,E29394,anna.johnson@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Jennifer,Fellows,E29453,jennifer.fellows@apsva.us,Hoffman-Boston ES,Administrative Assistant,Staff
Laura,Anduze Cabrero,E30254,laura.anduzecabrero@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Gwenn,Zaberer,E3059,gwenn.zaberer@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Allison,Brown,E30727,allison.brown2@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Jesus,Grados,E30877,jesus.grados@apsva.us,Hoffman-Boston ES,Custodian,Facilities Staff
Amelia,Fones,E31068,amelia.fones@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Joyce,Braxton,E31103,joyce.braxton@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Brenna,McGreevy,E31317,brenna.mcgreevy@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
John,Croddy,E31336,john.croddy@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Alejandra,Herrera,E31373,alejandra.herrera@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Daisy,Portillo-Garcia,E31518,daisy.portillogarcia@apsva.us,Hoffman-Boston ES,"Staff, Instructional Assistant",Staff
Njat,Hamed,E31716,njat.hamed@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Guadalupe,Benites,E31868,guadalupe.benites@apsva.us,Hoffman-Boston ES,"Staff, Instructional Assistant",Instructional Staff
Mhd,Alhamoui,E31883,mhd.alhamoui@apsva.us,Hoffman-Boston ES,Staff,Staff
Kimberly,Lefkowitz,E31920,kimberly.lefkowitz@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Alemayehu,Beyene,E31974,alemayehu.beyene@apsva.us,Hoffman-Boston ES,Custodian,Facilities Staff
Marilyn,Hanger,E3207,marilyn.hanger@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Ranah,Burney,E32160,ranah.ahmed@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Hailey,Spina,E32198,hailey.spina@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Steven,Hisler,E32253,steven.hisler@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Webit,Nugusu,E32293,webit.nugusu@apsva.us,Hoffman-Boston ES,Custodian,Facilities Staff
Samson,Amaniel,E32558,samson.amaniel@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Gregory,Stickeler,E32696,gregory.stickeler@apsva.us,Hoffman-Boston ES,n/a,Unclassified
Jennifer,Albro,E32700,jennifer.albro@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Michael,Fox,E32770,michael.fox2@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Teresa,Ayer,E32771,teresa.ayer@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Leah,Smith,E32802,leah.smith@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Kristen,Diaz,E32849,kristen.diaz@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Habbea,Begum,E32866,habbea.begum@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Ashley,Sanchez,E32893,ashley.sanchez@apsva.us,Hoffman-Boston ES,Staff,Staff
Ellena,Sarnoff,E33006,ellena.sarnoff@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Kelly,McKenna,E33202,kelly.mckenna@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Yuselka,Rodas Sanchez,E33226,yuselka.rodassanchez@apsva.us,Hoffman-Boston ES,Staff,Staff
Larita,Monagan,E33523,larita.monagan@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Laura,Garcia,E33579,laura.garcia@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Teresa,Fields,E3932,teresa.fields@apsva.us,Hoffman-Boston ES,Teacher,Instructional Staff
Opoku,Kwarteng,E4816,opoku.kwarteng@apsva.us,Hoffman-Boston ES,Custodian,Facilities Staff
Fozia,Maqbool,E5986,fozia.maqbool@apsva.us,Hoffman-Boston ES,Instructional Assistant,Instructional Staff
Carolyn,Parker,E6488,carolyn.parker@apsva.us,Hoffman-Boston ES,Staff,Staff
Jennifer,Lindenauer,E6905,jennifer.lindenauer@apsva.us,Hoffman-Boston ES,n/a,Unclassified
Binh,Tran,E9099,binh.tran@apsva.us,Hoffman-Boston ES,Administrative Assistant,Staff
Michael,Moore,E10411,michael.moore@apsva.us,Langston HS,Maintenance Supervisor,Facilities Staff
Laura,Guillion,E11612,laura.guillion@apsva.us,Langston HS,Teacher,Instructional Staff
Yaa,Osun,E12097,yaa.osun@apsva.us,Langston HS,Teacher,Instructional Staff
Anthony,McPhee,E15809,anthony.mcphee@apsva.us,Langston HS,Teacher,Instructional Staff
Lashawn,Graves,E16807,lashawn.graves@apsva.us,Langston HS,Teacher,Instructional Staff
Teresa,Ghiglino,E19021,teresa.ghiglino@apsva.us,Langston HS,Teacher,Instructional Staff
Rebecca,Zimmerman,E19307,rebecca.zimmerman@apsva.us,Langston HS,Teacher,Instructional Staff
Sonia,Argenal Hernandez,E20700,sonia.argenal@apsva.us,Langston HS,Administrative Assistant,Staff
Michelle,Marrero,E25554,michelle.marrero@apsva.us,Langston HS,Teacher,Instructional Staff
Erika,Drummond,E25864,erika.drummond@apsva.us,Langston HS,Teacher,Instructional Staff
Micah,Stein-Verbit,E2703,micah.stein@apsva.us,Langston HS,Teacher,Instructional Staff
Iris,Gibson,E28129,iris.gibson@apsva.us,Langston HS,Teacher,Instructional Staff
Naquan,Russell,E28619,naquan.russell@apsva.us,Langston HS,Instructional Assistant,Instructional Staff
Imran,Khaliq,E30656,imran.khaliq@apsva.us,Langston HS,Custodian,Facilities Staff
C.B.,Buchanon Smith,E30826,cb.smith@apsva.us,Langston HS,Administrative Assistant,Staff
Audrey,Vasquez-Rivera,E31131,audrey.vasquezrivera@apsva.us,Langston HS,Teacher,Instructional Staff
Nicola,Farris,E31575,nicola.farris@apsva.us,Langston HS,"Registrar, Staff",Staff
Rohini,Banerjee,E32212,rohini.banerjee@apsva.us,Langston HS,Teacher,Instructional Staff
Kathleen,Whitmeyer,E32601,kathleen.whitmeyer@apsva.us,Langston HS,Teacher,Instructional Staff
Aida,Alebachew,E33257,aida.alebachew@apsva.us,Langston HS,Staff,Staff
Chip,Bonar,E3633,chip.bonar@apsva.us,Langston HS,Administrator,Unclassified
Sun,Wilkoff,E5072,sun.wilkoff@apsva.us,Langston HS,Teacher,Instructional Staff
Eddy,Matos,E5122,eddy.matos@apsva.us,Langston HS,Teacher,Instructional Staff
Verlese,Gaither,E7432,verlese.gaither@apsva.us,Langston HS,Teacher,Instructional Staff
Nan,Holl,E9467,nan.holl@apsva.us,Langston HS,Teacher,Instructional Staff
Kelynne,Bisbee,E1098,kelynne.bisbee@apsva.us,Jamestown ES,Teacher,Instructional Staff
Monica,Roache,E14086,monica.roache@apsva.us,Jamestown ES,Assistant Principal,Administrative Staff
Raifel,Faison,E14387,raifel.faison@apsva.us,Jamestown ES,Teacher,Instructional Staff
Michelle,Mccarthy,E1520,michelle.mccarthy@apsva.us,Jamestown ES,Principal,Unclassified
Clementina,Molina,E15702,clementina.molina@apsva.us,Jamestown ES,Custodian,Facilities Staff
Mary,Gaynor,E15915,mary.gaynor@apsva.us,Jamestown ES,Teacher,Instructional Staff
Teresa,Estes,E16095,teresa.estes@apsva.us,Jamestown ES,Administrative Assistant,Staff
Valerie,Kuehn,E1617,valerie.kuehn@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Elizabeth,Scallon,E16828,elizabeth.scallon@apsva.us,Jamestown ES,Teacher,Instructional Staff
Rachna,Fraccaro,E19632,rachna.fraccaro@apsva.us,Jamestown ES,Teacher,Instructional Staff
Jason,Gawrys,E19684,jason.gawrys@apsva.us,Jamestown ES,Teacher,Instructional Staff
Sushma,Gulati,E19730,sushma.gulati@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Julie,Landefeld,E19844,julie.landefeld@apsva.us,Jamestown ES,Teacher,Instructional Staff
Meagan,Kalchbrenner,E1986,meagan.kalchbrenner@apsva.us,Jamestown ES,Teacher,Instructional Staff
Marie,Keough,E20307,marie.keough@apsva.us,Jamestown ES,Teacher,Instructional Staff
Amy,Blaine,E20505,amy.blaine@apsva.us,Jamestown ES,Teacher,Instructional Staff
Rachel,Sanders,E21571,rachel.sanders@apsva.us,Jamestown ES,Teacher,Instructional Staff
Cristobal,Flores,E21774,cristobal.flores@apsva.us,Jamestown ES,Staff,Staff
Elisa,Larounis,E22143,elisa.larounis@apsva.us,Jamestown ES,Teacher,Instructional Staff
Christine,Columbia,E22383,christine.columbia@apsva.us,Jamestown ES,Teacher,Instructional Staff
William,Donovan,E2247,william.donovan@apsva.us,Jamestown ES,Teacher,Instructional Staff
Carmen,Fallone,E22730,carmen.fallone@apsva.us,Jamestown ES,Teacher,Instructional Staff
Brian,Bersh,E22824,brian.bersh@apsva.us,Jamestown ES,n/a,Unclassified
Jose,Castro Benavides,E23056,jose.castro@apsva.us,Jamestown ES,Maintenance Supervisor,Facilities Staff
Laurie,Clark,E23380,laurie.clark@apsva.us,Jamestown ES,Teacher,Instructional Staff
Mary,Coogan,E23662,mary.coogan@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Maria,Manchester,E23973,maria.manchester@apsva.us,Jamestown ES,Teacher,Instructional Staff
James,Scarano,E23984,james.scarano@apsva.us,Jamestown ES,Teacher,Instructional Staff
Peter,Goodman,E23987,peter.goodman@apsva.us,Jamestown ES,Teacher,Instructional Staff
Gretchen,Pike,E24002,gretchen.pike@apsva.us,Jamestown ES,Teacher,Instructional Staff
Debra,Gaeta,E24013,debra.gaeta@apsva.us,Jamestown ES,Teacher,Instructional Staff
Leslie,Kasmir,E24790,leslie.kasmir@apsva.us,Jamestown ES,Staff,Staff
Brittany,Portugal,E25583,brittany.portugal@apsva.us,Jamestown ES,Teacher,Instructional Staff
Kaitlyn,Mackrell,E25821,kaitlyn.mackrell@apsva.us,Jamestown ES,Teacher,Instructional Staff
Jane,Kirchner,E26071,jane.kirchner@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Luis,Flores,E26324,luis.flores@apsva.us,Jamestown ES,Custodian,Facilities Staff
Catherine,Waszkowski,E26511,catherine.waszkowski@apsva.us,Jamestown ES,Teacher,Instructional Staff
Samia,Raki,E27273,samia.raki@apsva.us,Jamestown ES,Staff,Staff
Elizabeth,Frierson,E27430,elizabeth.frierson2@apsva.us,Jamestown ES,"Instructional Assistant, Staff",Instructional Staff
Angela,Foreman,E2763,angela.foreman@apsva.us,Jamestown ES,Teacher,Instructional Staff
Lindsey,Nathan,E27795,lindsey.nathan@apsva.us,Jamestown ES,Teacher,Instructional Staff
Amy,Richards,E27862,amy.richards@apsva.us,Jamestown ES,Teacher,Instructional Staff
Laura,Waggoner,E27867,laura.waggoner@apsva.us,Jamestown ES,Teacher,Instructional Staff
Holly,Tulley,E28718,holly.tulley2@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Claire,Ruggiero,E29080,claire.ruggiero@apsva.us,Jamestown ES,Teacher,Instructional Staff
Sheri,Sharwarko,E29091,sheri.sharwarko@apsva.us,Jamestown ES,Teacher,Instructional Staff
Adrian,Francis,E29144,adrian.francis@apsva.us,Jamestown ES,Staff,Staff
Nicole,Turner,E30036,nicole.turner@apsva.us,Jamestown ES,Teacher,Instructional Staff
Kathleen,Winston,E30094,kathleen.winston@apsva.us,Jamestown ES,Teacher,Instructional Staff
Nancy,Kauffunger,E3078,nancy.kauffunger@apsva.us,Jamestown ES,Teacher,Instructional Staff
David,Sanchez,E31244,david.sanchez@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Elizabeth,Normandin,E31307,elizabeth.normandin@apsva.us,Jamestown ES,Teacher,Instructional Staff
Kimberly,Kavanagh,E31360,kimberly.kavanagh@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Zlata,Kotliarova,E31668,zlata.kotliarova@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
John,Hopkins,E31854,john.hopkins@apsva.us,Jamestown ES,Teacher,Instructional Staff
Randi,Anthony,E31936,randi.anthony@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Monica,Warta,E32004,monica.warta@apsva.us,Jamestown ES,Teacher,Instructional Staff
Mandy,Downs,E32030,mandy.downs@apsva.us,Jamestown ES,Teacher,Instructional Staff
Sarah,Schuster,E32040,sarah.schuster@apsva.us,Jamestown ES,Teacher,Instructional Staff
Alaina,Rausa,E32100,alaina.rausa@apsva.us,Jamestown ES,Teacher,Instructional Staff
Melanie,Vu,E32263,melanie.vu@apsva.us,Jamestown ES,Administrative Assistant,Staff
Simone,Irby,E32337,simone.irby@apsva.us,Jamestown ES,Staff,Staff
Veronica,Zurita,E32387,veronica.zurita@apsva.us,Jamestown ES,Staff,Staff
Elizabeth,King,E32673,elizabeth.king@apsva.us,Jamestown ES,Teacher,Instructional Staff
Christina,Denzer,E32675,christina.denzer@apsva.us,Jamestown ES,Teacher,Instructional Staff
Heather,DeBruler,E32710,heather.debruler@apsva.us,Jamestown ES,Teacher,Instructional Staff
Elizabeth,Grodin,E32729,elizabeth.grodin@apsva.us,Jamestown ES,Teacher,Instructional Staff
Hayley,Miller,E32769,hayley.miller@apsva.us,Jamestown ES,Teacher,Instructional Staff
Roua,Abdalla,E32823,roua.abdalla@apsva.us,Jamestown ES,Instructional Assistant,Staff
Leonica,Cox,E33454,leonica.cox@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Dani,Greene,E3946,dani.shotel@apsva.us,Jamestown ES,Teacher,Instructional Staff
Mary,Rowland,E465,mary.rowland@apsva.us,Jamestown ES,Teacher,Instructional Staff
Loren,Fuentes Gil,E4655,lorenza.fuentes@apsva.us,Jamestown ES,Principals Assistant,Staff
Duc,Trieu,E6019,duc.trieu@apsva.us,Jamestown ES,Custodian,Facilities Staff
Joanne,Price,E7034,joanne.price@apsva.us,Jamestown ES,Teacher,Instructional Staff
Emilie,Rekstad,E7895,emilie.rekstad@apsva.us,Jamestown ES,Teacher,Instructional Staff
Margot,Bowers,E8489,margot.bowers@apsva.us,Jamestown ES,Teacher,Instructional Staff
Ebenezer,Owiredu,E9775,ebenezer.owiredu@apsva.us,Jamestown ES,Instructional Assistant,Instructional Staff
Martha,Byron,E10363,martha.byron@apsva.us,Jefferson MS,Teacher,Instructional Staff
Starr,Lanman,E12935,starr.lanman@apsva.us,Jefferson MS,Teacher,Instructional Staff
Matthew,Hungerford,E13073,matthew.hungerford@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Ann,Miller,E1321,ann.miller@apsva.us,Jefferson MS,Teacher,Instructional Staff
Diana,Jordan,E13656,diana.jordan@apsva.us,Jefferson MS,Assistant Principal,Administrative Staff
Ana,Mejia,E13723,ana.mejia@apsva.us,Jefferson MS,Administrative Assistant,Staff
Maureen,Nolan,E15669,maureen.nolan@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kristin,Gomez,E15912,kristin.gomez@apsva.us,Jefferson MS,n/a,Unclassified
Kerry,Fitzpatrick,E16418,kerry.fitzpatrick@apsva.us,Jefferson MS,Teacher,Instructional Staff
Aurelia,Sicha,E19269,aurelia.sicha@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Elizabeth,Egbert,E19288,elizabeth.egbert@apsva.us,Jefferson MS,Teacher,Instructional Staff
Robert,Hanson,E19472,robert.hanson@apsva.us,Jefferson MS,Assistant Principal,Administrative Staff
Heather,Boda,E19579,heather.boda@apsva.us,Jefferson MS,Teacher,Instructional Staff
Michelle,Smith,E19774,michelle.smith@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Emily,Calhoun,E20216,emily.calhoun@apsva.us,Jefferson MS,Teacher,Instructional Staff
Joanne,Mann,E20514,joanne.mann@apsva.us,Jefferson MS,Teacher,Instructional Staff
Arnold,Appanah,E20696,arnold.appanah@apsva.us,Jefferson MS,Manager,Staff
Patrick,Whelden,E20732,patrick.whelden@apsva.us,Jefferson MS,Staff,Staff
Enid,Dunbar,E21063,enid.dunbar@apsva.us,Jefferson MS,Teacher,Instructional Staff
Velma,Jones,E21066,velma.jones@apsva.us,Jefferson MS,Staff,Staff
Peter,Anderson,E21116,peter.anderson@apsva.us,Jefferson MS,Teacher,Instructional Staff
Grace,Waring,E21205,grace.waring@apsva.us,Jefferson MS,Teacher,Instructional Staff
Amy,Yosick,E21329,amy.yosick@apsva.us,Jefferson MS,Teacher,Instructional Staff
Inger,Moran,E21635,inger.moran@apsva.us,Jefferson MS,Teacher,Instructional Staff
Irma,De Leon-Veliz,E21648,irma.deleonveliz@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Jaquelina,Ferrara,E21747,jaquelina.ferrara@apsva.us,Jefferson MS,Teacher,Instructional Staff
Jennifer,Vernier,E21960,jennifer.vernier@apsva.us,Jefferson MS,Teacher,Instructional Staff
Raven,Armstrong,E22431,raven.armstrong@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Katlyn,Bennett,E22975,katlyn.bennett@apsva.us,Jefferson MS,Teacher,Instructional Staff
Mary,Foley,E23179,mary.foley@apsva.us,Jefferson MS,Teacher,Instructional Staff
Amelia,Rosegrant,E23197,amelia.rosegrant@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Sean,Curran,E23392,sean.curran@apsva.us,Jefferson MS,Teacher,Instructional Staff
Brittany,Wiltrout,E23755,brittany.wiltrout@apsva.us,Jefferson MS,Teacher,Instructional Staff
Keisha,Boggan,E23795,keisha.boggan@apsva.us,Jefferson MS,Principal,Unclassified
Lakesha,Gray,E23920,lakesha.gray@apsva.us,Jefferson MS,Administrative Assistant,Staff
Sumer,Majid,E24016,sumer.majid@apsva.us,Jefferson MS,n/a,Unclassified
Kaila,Leonberger,E24153,kaila.leonberger@apsva.us,Jefferson MS,Teacher,Instructional Staff
Lisa,Frayer,E24209,lisa.frayer@apsva.us,Jefferson MS,Staff,Staff
Stephanie,Smith,E24486,stephanie.smith@apsva.us,Jefferson MS,Teacher,Instructional Staff
Caitlin,Rotchford,E24561,caitlin.rotchford@apsva.us,Jefferson MS,Teacher,Instructional Staff
Olivia,Green,E24693,olivia.green@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Gary,Hauptman,E24717,gary.hauptman@apsva.us,Jefferson MS,Staff,Staff
Rachel,Payne,E24796,rachel.payne@apsva.us,Jefferson MS,Teacher,Instructional Staff
Christian,Banach,E24911,christian.banach@apsva.us,Jefferson MS,Teacher,Instructional Staff
Abigail,Kaster,E24960,abigail.kaster@apsva.us,Jefferson MS,Teacher,Instructional Staff
Elita,Jenks,E2550,elita.jenks@apsva.us,Jefferson MS,Teacher,Instructional Staff
Lauren,Negrete,E25564,lauren.negrete@apsva.us,Jefferson MS,Teacher,Instructional Staff
Vincent,Jarosz,E25576,vincent.jarosz@apsva.us,Jefferson MS,Assistant Principal,Administrative Staff
Amelia,Black,E25716,amelia.black@apsva.us,Jefferson MS,Teacher,Instructional Staff
Tiffini,Woody-Pope,E25823,tiffini.woodypope@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kara,McPhillips,E25842,kara.mcphillips@apsva.us,Jefferson MS,Teacher,Instructional Staff
Jessica,Kramer,E25861,jessica.kramer@apsva.us,Jefferson MS,Teacher,Instructional Staff
Dante,Hicks,E25889,dante.hicks@apsva.us,Jefferson MS,Director,Administrative Staff
Lei,Shang,E26010,lei.shang@apsva.us,Jefferson MS,Teacher,Instructional Staff
Dana,Brundidge,E26086,dana.brundidge@apsva.us,Jefferson MS,"Administrative Assistant, Staff",Staff
Antonio,Tondelli,E26439,antonio.tondellipard@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Ana,Rodriguez,E26594,ana.rodriguez2@apsva.us,Jefferson MS,Teacher,Instructional Staff
Brandon,Gladney,E26685,brandon.gladney@apsva.us,Jefferson MS,Teacher,Instructional Staff
Andrew,Bridges,E26830,andrew.bridges@apsva.us,Jefferson MS,Teacher,Instructional Staff
Devon,Riley,E26838,devon.riley@apsva.us,Jefferson MS,Teacher,Instructional Staff
Zachary,Carter,E26845,zachary.carter@apsva.us,Jefferson MS,Teacher,Instructional Staff
Traci,Holland,E26859,traci.holland@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kelly,Lopez,E26994,kelly.lopez@apsva.us,Jefferson MS,Teacher,Instructional Staff
Miyatt,Harrison,E27439,miyatt.harrison@apsva.us,Jefferson MS,Teacher,Instructional Staff
Iris,Whitehill,E27838,iris.whitehill@apsva.us,Jefferson MS,n/a,Unclassified
Paul,Perrot,E27947,paul.perrot@apsva.us,Jefferson MS,Teacher,Instructional Staff
Anna,LaVardera,E28076,anna.lavardera@apsva.us,Jefferson MS,Teacher,Instructional Staff
Camee,Beyers,E28123,camee.beyers@apsva.us,Jefferson MS,Teacher,Instructional Staff
Claudia,Martinez,E28251,claudia.martinez@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Nam,Clark,E28583,nam.clark2@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kiera,Willis,E28666,kiera.willis@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Susan,Russo,E28951,susan.russo@apsva.us,Jefferson MS,Teacher,Instructional Staff
Jill,Miller,E28978,jill.miller@apsva.us,Jefferson MS,Staff,Staff
Megan,Detweiler,E28981,megan.detweiler@apsva.us,Jefferson MS,Teacher,Instructional Staff
Susan,Wilkinson,E29107,susan.wilkinson@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kipchoge,Dow,E29129,kipchoge.dow@apsva.us,Jefferson MS,Teacher,Instructional Staff
Aisha,Madhi,E29135,aisha.madhi@apsva.us,Jefferson MS,Teacher,Instructional Staff
Chris,Ammon,E29208,chris.ammon@apsva.us,Jefferson MS,Teacher,Instructional Staff
Enkhtuul,Nyamaakhuu,E29805,enkhtuul.nyamaakhuu@apsva.us,Jefferson MS,School Finance Officer,Staff
Nigel,Somerville,E29911,nigel.somerville@apsva.us,Jefferson MS,Teacher,Instructional Staff
Michael,Sivells,E30014,michael.sivells@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kip,Malinosky,E30048,kip.malinosky@apsva.us,Jefferson MS,Teacher,Instructional Staff
Belinda,Snell,E30109,belinda.snell@apsva.us,Jefferson MS,Teacher,Instructional Staff
Celestino,Samaniego,E30154,celestino.samaniego@apsva.us,Jefferson MS,Teacher,Instructional Staff
Mathieu,Bee,E30218,mathieu.bee@apsva.us,Jefferson MS,Teacher,Instructional Staff
Martin,Heath,E3074,martin.heath@apsva.us,Jefferson MS,Teacher,Instructional Staff
Melissa,Cobbs,E31085,melissa.cobbs@apsva.us,Jefferson MS,Teacher,Instructional Staff
Shyrone,Stith,E31220,shyrone.stith@apsva.us,Jefferson MS,Teacher,Instructional Staff
Jasmine,Burton,E31263,jasmine.burton@apsva.us,Jefferson MS,Teacher,Instructional Staff
Stephanie,Washington,E31265,stephanie.washington@apsva.us,Jefferson MS,Teacher,Instructional Staff
Jeffery,Anderson,E31267,jeffery.anderson@apsva.us,Jefferson MS,Teacher,Instructional Staff
Britney,Patterson,E31295,britney.patterson@apsva.us,Jefferson MS,Teacher,Instructional Staff
Jeremy,Wintersteen,E31406,jeremy.wintersteen@apsva.us,Jefferson MS,Teacher,Instructional Staff
Maysoon,Salih,E31456,maysoon.salih@apsva.us,Jefferson MS,n/a,Unclassified
Wayne,Snider,E31527,wayne.snider@apsva.us,Jefferson MS,Teacher,Instructional Staff
Eileen,Temprosa,E32038,eileen.temprosa@apsva.us,Jefferson MS,Teacher,Instructional Staff
Robert,Griffiths,E32095,robert.griffiths@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kristina,McQuay,E32152,kristina.mcquay@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Olivia,Holloway,E32175,olivia.holloway@apsva.us,Jefferson MS,Teacher,Instructional Staff
Joshua,Chung,E32182,joshua.chung@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Miguel,Perez-Calleja,E32184,miguel.perez@apsva.us,Jefferson MS,Teacher,Instructional Staff
Andrea,Menza,E32230,andrea.menza@apsva.us,Jefferson MS,Teacher,Instructional Staff
Alicia,Cofield,E32416,alicia.cofield@apsva.us,Jefferson MS,Staff,Staff
Nicci,Luu,E32417,nicci.luu@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Edith,Benitez Velasquez,E32421,edith.benitez@apsva.us,Jefferson MS,Staff,Staff
Kelley,Perry,E32603,kelley.perry@apsva.us,Jefferson MS,Teacher,Instructional Staff
Claire,LeBovidge,E32639,claire.lebovidge@apsva.us,Jefferson MS,Teacher,Instructional Staff
Verne,Hyde,E32647,verne.hyde@apsva.us,Jefferson MS,Teacher,Instructional Staff
Tiffanie,Otto,E32674,tiffanie.otto@apsva.us,Jefferson MS,Teacher,Instructional Staff
Krystal,Wilson,E32705,krystal.wilson@apsva.us,Jefferson MS,Teacher,Instructional Staff
Donna,Seabrook,E32744,donna.seabrook@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Sabrina,Suarez,E32754,sabrina.suarez@apsva.us,Jefferson MS,Administrative Assistant,Staff
Chanai,Brewster,E32796,chanai.brewster@apsva.us,Jefferson MS,Teacher,Instructional Staff
Shaina,Womack,E32797,shaina.womack@apsva.us,Jefferson MS,Teacher,Instructional Staff
Benedicta,Tugbeh,E32830,benedicta.tugbeh@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kevin,Moore,E32841,kevin.moore@apsva.us,Jefferson MS,Teacher,Instructional Staff
Maggie,Luu,E32871,maggie.luu@apsva.us,Jefferson MS,Administrative Assistant,Staff
Garrett,Gibson,E32980,garrett.gibson@apsva.us,Jefferson MS,Staff,Staff
Saliha,Bahfid,E33065,saliha.bahfid@apsva.us,Jefferson MS,Staff,Staff
Sophia,Nowlin,E33152,sophia.nowlin@apsva.us,Jefferson MS,Staff,Staff
Kelley,Presley,E33157,kelley.presley@apsva.us,Jefferson MS,Teacher,Instructional Staff
Leeco,Gutierrez,E33314,leeco.gutierrez@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Edward,Dillard,E33512,edward.dillard@apsva.us,Jefferson MS,Maintenance Supervisor,Facilities Staff
Jeremy,Siegel,E3364,jeremy.siegel@apsva.us,Jefferson MS,Coordinator,Staff
Mary,Brown,E3713,mary.brown@apsva.us,Jefferson MS,Teacher,Instructional Staff
Nicci,Gibson Williams,E4193,nicci.williams@apsva.us,Jefferson MS,Teacher,Instructional Staff
Jason,Busby,E4554,jason.busby@apsva.us,Jefferson MS,Teacher,Instructional Staff
Emily,Shepardson,E4991,emily.shepardson@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kimberly,Miller,E5034,kimberly.miller@apsva.us,Jefferson MS,Teacher,Instructional Staff
Katarzyna,Polek-Craven,E5394,katarzyna.craven@apsva.us,Jefferson MS,Teacher,Instructional Staff
Kirsten,Wall,E5594,kirsten.wall@apsva.us,Jefferson MS,"Staff, Teacher",Staff
Jennifer,Bogdan,E6346,jennifer.bogdan@apsva.us,Jefferson MS,Teacher,Instructional Staff
Laura,Bannach,E6403,laura.bannach@apsva.us,Jefferson MS,Teacher,Instructional Staff
Alexandra,Workman,E6623,alexandra.workman@apsva.us,Jefferson MS,Teacher,Instructional Staff
Renee,Sidberry,E7008,renee.sidberry@apsva.us,Jefferson MS,Staff,Staff
Pidor,Chea,E7125,pidor.chea@apsva.us,Jefferson MS,Manager,Staff
Katherine,Zientara,E7338,katherine.zientara@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Tracey,Shepardson,E779,tracey.shepardson@apsva.us,Jefferson MS,Teacher,Instructional Staff
C,Tangchittsumran,E7898,c.tangchittsumran@apsva.us,Jefferson MS,Teacher,Instructional Staff
Nataki,Green,E7986,nataki.green@apsva.us,Jefferson MS,"Instructional Assistant, Staff",Staff
Cassidy,Nolen,E8354,cassidy.nolen@apsva.us,Jefferson MS,Teacher,Instructional Staff
Deisy,Molina Fuentes,E8576,deisy.villatoro@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Maritza,Argueta,E9346,maritza.argueta@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Cynthia,Pelham,E9600,cynthia.pelham@apsva.us,Jefferson MS,Instructional Assistant,Instructional Staff
Heidi,Neunder,E994,heidi.neunder@apsva.us,Jefferson MS,Teacher,Instructional Staff
William,Harber,E1001,william.harber@apsva.us,Kenmore MS,Teacher,Instructional Staff
Victoria,Keish,E10642,victoria.keish@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Tommy,Baugh,E10781,tommy.baugh@apsva.us,Kenmore MS,Coordinator,Staff
Bruce,Fort,E10836,bruce.fort@apsva.us,Kenmore MS,Teacher,Instructional Staff
Jeanette,Hall,E10872,jeanette.hall@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Christine,Joy,E11118,christine.joy@apsva.us,Kenmore MS,Assistant Principal,Administrative Staff
Heather,Akpata,E11201,heather.akpata@apsva.us,Kenmore MS,Staff,Staff
James,Haile,E12336,james.haile@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Che,Abdeljawad,E12515,che.abdeljawad@apsva.us,Kenmore MS,Teacher,Instructional Staff
Philip,Hayden,E1324,philip.hayden@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Joseph,Spencer,E13534,joseph.spencer@apsva.us,Kenmore MS,Staff,Staff
Allie,Weber,E14247,allie.weber@apsva.us,Kenmore MS,Teacher,Instructional Staff
Anita,Mack,E14367,anita.mack@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Ana,Guzman,E14545,ana.guzman@apsva.us,Kenmore MS,Custodian,Facilities Staff
Aurora,Chavarria,E14880,aurora.chavarria@apsva.us,Kenmore MS,Custodian,Facilities Staff
Tuesday,Williams-Wilhoit,E15132,tuesday.wilhoit@apsva.us,Kenmore MS,"Instructional Assistant, Staff",Instructional Staff
Rodney,Callands,E15156,rodney.callands@apsva.us,Kenmore MS,Maintenance Supervisor,Facilities Staff
Jeffrey,Politzer,E1554,jeffrey.politzer@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Angel,Boragay,E16557,angel.boragay@apsva.us,Kenmore MS,Maintenance Supervisor,Facilities Staff
Andrea,Paight,E17067,andrea.paight@apsva.us,Kenmore MS,Teacher,Instructional Staff
Bruce,Merrill,E1866,bruce.merrill@apsva.us,Kenmore MS,Teacher,Instructional Staff
Elizabeth,Egbert,E19288,elizabeth.egbert@apsva.us,Kenmore MS,n/a,Unclassified
Israel,Salas,E19344,israel.salas@apsva.us,Kenmore MS,Teacher,Instructional Staff
Margaret,Lindquist,E19519,margaret.lindquist@apsva.us,Kenmore MS,Teacher,Instructional Staff
Alice,Gantenbein,E19623,alice.gantenbein@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Maura,McMullen,E20100,maura.mcmullen@apsva.us,Kenmore MS,Teacher,Instructional Staff
Juanice,Jenkins,E20292,juanice.jenkins@apsva.us,Kenmore MS,Staff,Staff
Brendan,Blackburn,E20384,brendan.blackburn@apsva.us,Kenmore MS,Teacher,Instructional Staff
Jose,Sanchez Jimenez,E20455,jose.sanchez@apsva.us,Kenmore MS,Teacher,Instructional Staff
Karen,Edwards,E21373,karen.edwards@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Dawn,Scattolini,E21519,dawn.hunter@apsva.us,Kenmore MS,Teacher,Instructional Staff
Sandy,Beltran,E21564,sandy.beltran@apsva.us,Kenmore MS,Teacher,Instructional Staff
Xenia,Quintanilla,E22055,xenia.quintanilla@apsva.us,Kenmore MS,Administrative Assistant,Staff
Deidrich,Gilreath,E22576,deidrich.gilreath@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Kaitlin,Bresnahan,E22712,kaitlin.bresnahan@apsva.us,Kenmore MS,Director,Administrative Staff
Kathryn,Brown,E22936,kathryn.brown@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Gretchen,Schule,E22992,gretchen.schule@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Jennifer,Monma,E23641,jennifer.monma@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Katherine,Donnelly,E23745,katherine.donnelly@apsva.us,Kenmore MS,Teacher,Instructional Staff
Matthew,Cupples,E23773,matthew.cupples@apsva.us,Kenmore MS,Teacher,Instructional Staff
Dorothy,Lydon,E2393,dorothy.lydon@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Sumer,Majid,E24016,sumer.majid@apsva.us,Kenmore MS,n/a,Unclassified
Latesa,Giles,E24092,latesa.giles@apsva.us,Kenmore MS,Teacher,Instructional Staff
Seema,Das,E24251,seema.das@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Brannon,Burnett,E24286,brannon.burnett@apsva.us,Kenmore MS,Staff,Staff
Andy,Paparella,E2429,andy.paparella@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Neiza,Loayza Vela,E24649,neiza.loayzavela@apsva.us,Kenmore MS,"Instructional Assistant, Staff",Instructional Staff
Jennifer,Hesla,E24746,jennifer.hesla@apsva.us,Kenmore MS,Teacher,Instructional Staff
Eurith,Bowen,E24865,eurith.bowen@apsva.us,Kenmore MS,Teacher,Instructional Staff
Matthew,Price,E24974,matthew.price@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Alam,Lainez,E25060,alam.lainez@apsva.us,Kenmore MS,"Instructional Assistant, Staff",Instructional Staff
Edward,Bracken,E25144,ed.bracken@apsva.us,Kenmore MS,Teacher,Instructional Staff
Maria,Small,E25227,maria.small@apsva.us,Kenmore MS,"Instructional Assistant, Staff",Instructional Staff
Lolita,Pollard-Kezele,E25795,lolita.pollard@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Deborah,Seidenstein,E25909,deborah.seidenstein@apsva.us,Kenmore MS,Staff,Staff
Elizabeth,Cannon,E26117,elizabeth.cannon@apsva.us,Kenmore MS,Teacher,Instructional Staff
Ana,Rodriguez,E26594,ana.rodriguez2@apsva.us,Kenmore MS,Teacher,Instructional Staff
Maria,Chevez,E26606,maria.chevez@apsva.us,Kenmore MS,Custodian,Facilities Staff
Teresa,Taylor,E26725,teresa.taylor@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Danielle,Miles,E26844,danielle.miles@apsva.us,Kenmore MS,Staff,Staff
Nicole,Pradas,E27008,nicole.pradas@apsva.us,Kenmore MS,Teacher,Instructional Staff
Shaqunna,Ball,E27044,shaqunna.ball@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Johanna,Giraldo,E27269,johanna.giraldo@apsva.us,Kenmore MS,Teacher,Instructional Staff
Rosa,Gomes-Mendes,E27400,rosa.gomesmendes2@apsva.us,Kenmore MS,Teacher,Instructional Staff
Sandra,Granados Ortez,E27419,sandra.granadosortez@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Seemaab,Anjum,E27526,seemaab.anjum@apsva.us,Kenmore MS,Staff,Staff
Michelle,Van Lare,E27702,michelle.vanlare@apsva.us,Kenmore MS,Staff,Staff
Ira,Baker,E27703,ira.baker@apsva.us,Kenmore MS,Teacher,Instructional Staff
Stacey,Kennedy,E27709,stacey.kennedy@apsva.us,Kenmore MS,"Staff, Teacher",Instructional Staff
Jason,Love,E27729,jason.love@apsva.us,Kenmore MS,Assistant Principal,Administrative Staff
Chloe,Duchaj,E27736,chloe.duchaj@apsva.us,Kenmore MS,Teacher,Instructional Staff
Sair,Suarez-Tello,E27767,sair.suareztello@apsva.us,Kenmore MS,Teacher,Instructional Staff
Ronda,Cook,E27779,ronda.cook@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Victoria,Hernandez,E27812,victoria.hernandez@apsva.us,Kenmore MS,Teacher,Instructional Staff
Michele,Hubert,E27848,michele.hubert@apsva.us,Kenmore MS,Teacher,Instructional Staff
Marcella,Park,E28007,marcella.park@apsva.us,Kenmore MS,Teacher,Instructional Staff
Africa,Norris,E28064,africa.norris@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Elizabeth,Carino,E28077,elizabeth.carino@apsva.us,Kenmore MS,"Teacher, Staff",Instructional Staff
Lolita,Smith,E28183,lolita.smith@apsva.us,Kenmore MS,Instructional Assistant,Staff
Kanako,Lyons,E28216,kanako.lyons@apsva.us,Kenmore MS,Teacher,Instructional Staff
Yennyfer,Rubiano Revollo,E28245,yennyfer.rubiano@apsva.us,Kenmore MS,Teacher,Instructional Staff
Candice,Scott,E28276,candice.scott@apsva.us,Kenmore MS,Teacher,Instructional Staff
Mahama,Addai,E28405,mahama.addai@apsva.us,Kenmore MS,Custodian,Facilities Staff
Mischa,Bilanchone,E28567,mischa.bilanchone2@apsva.us,Kenmore MS,Teacher,Instructional Staff
Xiomara,Martinez Parada,E28845,xiomara.parada@apsva.us,Kenmore MS,Custodian,Facilities Staff
Tanja,Eri-Kanazir,E28965,tanja.erikanazir@apsva.us,Kenmore MS,Teacher,Instructional Staff
James,Pratte,E29030,james.pratte@apsva.us,Kenmore MS,Teacher,Instructional Staff
Nia,Valcourt,E29073,nia.valcourt2@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Elizabeth,Page,E29142,elizabeth.page@apsva.us,Kenmore MS,"Staff, Teacher",Staff
Brandee,Wade,E29192,brandee.wade@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Jose,Alvarez,E29202,jose.alvarez@apsva.us,Kenmore MS,"Teacher, Staff",Instructional Staff
Courtney,Scherting,E29368,courtney.scherting@apsva.us,Kenmore MS,Teacher,Instructional Staff
Fabricio,Gamarra,E29984,fabricio.gamarra@apsva.us,Kenmore MS,Staff,Staff
Alexandria,Schnappinger,E29998,alex.schnappinger@apsva.us,Kenmore MS,Teacher,Instructional Staff
"Galdamez, Mirna",Galdamez Guevara,E30020,galdamez.mirna@apsva.us,Kenmore MS,Staff,Staff
Sophie,Luckette,E30043,sophie.luckette@apsva.us,Kenmore MS,Staff,Staff
Leila,Matta,E30060,leila.matta@apsva.us,Kenmore MS,Teacher,Instructional Staff
Ben,Michelson,E30127,ben.michelson@apsva.us,Kenmore MS,Teacher,Instructional Staff
Kelsey,Sweat,E30185,kelsey.sweat@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Octavia,Knigge,E30317,octavia.knigge@apsva.us,Kenmore MS,Assistant Principal,Administrative Staff
Shaila,Hossain,E30407,shaila.hossain@apsva.us,Kenmore MS,Staff,Staff
Setara,Habib,E30592,setara.habib2@apsva.us,Kenmore MS,"Teacher, Staff",Instructional Staff
Robin,Stewart,E30927,robin.stewart@apsva.us,Kenmore MS,Teacher,Instructional Staff
Kay,Miller,E30966,kay.miller@apsva.us,Kenmore MS,Staff,Staff
Joy,Carey,E30997,joy.smith@apsva.us,Kenmore MS,Teacher,Instructional Staff
Nigist,Woldegebriel,E31012,nigist.woldegebriel@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Cesar,Torres,E31052,cesar.torres@apsva.us,Kenmore MS,Staff,Staff
Carolina,Kelley,E31067,carolina.kelley@apsva.us,Kenmore MS,Teacher,Instructional Staff
Ethan,Oakley,E31091,ethan.oakley@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Chris,Claure,E31164,chris.claure@apsva.us,Kenmore MS,Staff,Staff
Kaitlyn,Eret,E31217,kaitlyn.eret@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Elena,Greer,E31278,elena.greer@apsva.us,Kenmore MS,Teacher,Instructional Staff
Andrew,Jonas,E31283,andrew.jonas@apsva.us,Kenmore MS,Teacher,Instructional Staff
Terence,Ryan,E31300,terence.ryan@apsva.us,Kenmore MS,Teacher,Instructional Staff
Claire,Prisock,E31304,claire.prisock@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Timothy,Gunn,E31306,timothy.gunn@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Johnathan,Ramey,E31310,johnathan.ramey@apsva.us,Kenmore MS,Teacher,Instructional Staff
Anne,Whipp,E31322,anne.whipp@apsva.us,Kenmore MS,"Teacher, Staff",Staff
Jeremy,Wintersteen,E31406,jeremy.wintersteen@apsva.us,Kenmore MS,n/a,Unclassified
Camille,Norgbey,E31467,camille.norgbey@apsva.us,Kenmore MS,Teacher,Instructional Staff
Rachida,Hadhoumi,E31874,rachida.hadhoumi@apsva.us,Kenmore MS,Instructional Assistant,Staff
Della,Rodrigues,E3188,della.rodrigues@apsva.us,Kenmore MS,Teacher,Instructional Staff
Laura,Cusumano,E31890,laura.cusumano@apsva.us,Kenmore MS,"Teacher, Staff",Instructional Staff
Zeray,Reda,E31911,zeray.reda@apsva.us,Kenmore MS,Custodian,Facilities Staff
Willie,Figgins,E31969,willie.figgins@apsva.us,Kenmore MS,Custodian,Facilities Staff
Brandi,Barnes,E32013,brandi.barnes@apsva.us,Kenmore MS,"Staff, Teacher",Instructional Staff
Victoria,Flack,E32024,victoria.flack@apsva.us,Kenmore MS,Teacher,Instructional Staff
Julianne,Maston,E32058,julianne.maston@apsva.us,Kenmore MS,Teacher,Instructional Staff
Zeyna,McIlvain,E32088,zeyna.mcilvain@apsva.us,Kenmore MS,Teacher,Instructional Staff
Agatha,Kargbo,E32138,agatha.kargbo@apsva.us,Kenmore MS,Teacher,Instructional Staff
Jessica,Johnson,E32153,jessica.johnson@apsva.us,Kenmore MS,Teacher,Instructional Staff
Epiphany,McCant,E32211,epiphany.mccant@apsva.us,Kenmore MS,"Staff, Teacher",Staff
Aaron,Moye,E32257,aaron.moye@apsva.us,Kenmore MS,Custodian,Facilities Staff
Hamdia,Farhan,E32357,hamdia.farhan@apsva.us,Kenmore MS,Staff,Staff
Kenyata,Swann,E32369,kenyata.swann@apsva.us,Kenmore MS,n/a,Unclassified
Cassandra,Zeballos,E32393,cassandra.zeballos@apsva.us,Kenmore MS,Staff,Staff
Sarah,Halverson,E32394,sarah.halverson@apsva.us,Kenmore MS,Staff,Staff
Zachelle,Henderson,E32405,zachelle.henderson@apsva.us,Kenmore MS,Staff,Staff
Gloria,Twenebowaa,E32408,gloria.twenebowaa@apsva.us,Kenmore MS,Staff,Staff
Miranda,Carpenter,E32412,miranda.carpenter@apsva.us,Kenmore MS,Teacher,Instructional Staff
Kaytleth,Farino Alvarez,E32419,kaytleth.farino@apsva.us,Kenmore MS,"Administrative Assistant, Instructional Assistant",Instructional Staff
Jacqueline,Pagan,E32432,jacqueline.pagan@apsva.us,Kenmore MS,Staff,Staff
Rene,Esteban,E32447,rene.esteban@apsva.us,Kenmore MS,Custodian,Facilities Staff
Patrice,Gibbs,E32590,patrice.gibbs@apsva.us,Kenmore MS,Teacher,Instructional Staff
Rebecca,Gilmore Lanz,E32594,rebecca.gilmorelanz@apsva.us,Kenmore MS,Staff,Staff
Annie,Rozewski,E32725,annie.rozewski@apsva.us,Kenmore MS,Teacher,Instructional Staff
Hannah,Dunn,E32919,hannah.dunn@apsva.us,Kenmore MS,Teacher,Instructional Staff
Maria,Teran-Cabezas,E33133,maria.terancabezas@apsva.us,Kenmore MS,Staff,Staff
Darell,Grimes,E33136,darell.grimes@apsva.us,Kenmore MS,Teacher,Instructional Staff
Katelyn,Guerrero,E33372,katelyn.guerrero@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Lucretia,Glover,E33424,lucretia.glover@apsva.us,Kenmore MS,Teacher,Instructional Staff
Elizabeth,Dittamo,E33445,elizabeth.dittamo@apsva.us,Kenmore MS,Teacher,Instructional Staff
Elliot,Johnson,E3686,elliot.johnson@apsva.us,Kenmore MS,Teacher,Instructional Staff
David,McBride,E4203,david.mcbride@apsva.us,Kenmore MS,Principal,Unclassified
Helen,Parkhurst,E4651,helen.parkhurst@apsva.us,Kenmore MS,"Teacher, Staff",Instructional Staff
Robert,Maghan,E4915,robert.maghan@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Badara,Dia,E5451,badara.dia@apsva.us,Kenmore MS,"Instructional Assistant, Staff",Instructional Staff
Sue,Nuhn,E549,sue.nuhn@apsva.us,Kenmore MS,Administrative Assistant,Staff
Hada,Molina,E5786,hada.molina@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Ruby,Rivera,E5969,ruby.rivera@apsva.us,Kenmore MS,Administrative Assistant,Staff
Judyt,Herrera,E6376,judyt.herrera@apsva.us,Kenmore MS,Teacher,Instructional Staff
Shauna,Dyer,E6969,shauna.dyer@apsva.us,Kenmore MS,"Staff, Teacher",Instructional Staff
Joanne,Godwin,E707,joanne.godwin@apsva.us,Kenmore MS,"Staff, Teacher",Staff
Emily,Harvey,E7335,emily.harvey@apsva.us,Kenmore MS,Administrative Assistant,Staff
Katherine,Mcfail,E7662,katherine.mcfail@apsva.us,Kenmore MS,"Staff, Instructional Assistant",Instructional Staff
Noemi,Yerovi,E7875,noemi.yerovi@apsva.us,Kenmore MS,Instructional Assistant,Instructional Staff
Edgardo,Zambrano,E8055,edgardo.zambrano@apsva.us,Kenmore MS,Manager,Staff
Annette,Holloway,E8591,annette.holloway@apsva.us,Kenmore MS,"Instructional Assistant, Staff",Staff
Phu,Nguyen,E9364,phu.nguyen@apsva.us,Kenmore MS,Custodian,Facilities Staff
Steven,Burrow,E9702,steven.burrow@apsva.us,Kenmore MS,Teacher,Instructional Staff
Gabriela,Fernandez-Coelho,E9722,gabriela.fernandez@apsva.us,Kenmore MS,Administrative Assistant,Staff
Anthony,DI Iorio,E1045,anthony.diiorio@apsva.us,Key ES,Administrative Assistant,Staff
Heidi,Heim,E11821,heidi.heim@apsva.us,Key ES,Teacher,Instructional Staff
Frances,Lee,E11979,frances.lee@apsva.us,Key ES,Assistant Principal,Administrative Staff
Marleny,Perdomo,E12135,marleny.perdomo@apsva.us,Key ES,Principal,Unclassified
Bdeyanira,Lopez,E12161,bdeyanira.lopez@apsva.us,Key ES,Teacher,Instructional Staff
Anne,Eisman-Burns,E1232,anne.eisman@apsva.us,Key ES,Teacher,Instructional Staff
Mikey,Samayoa,E13765,mikey.samayoa@apsva.us,Key ES,Teacher,Instructional Staff
Donna,Edwards,E14102,donna.edwards@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Yolanda,Sanchez-Pena,E14347,yolanda.sanchezpena@apsva.us,Key ES,Administrative Assistant,Staff
Saundra,Rogers,E14649,saundra.rogers@apsva.us,Key ES,Teacher,Instructional Staff
Socorro,Rojas,E15295,socorro.rojas@apsva.us,Key ES,Teacher,Instructional Staff
Kerensa,McConnell,E15466,kerensa.mcconnell@apsva.us,Key ES,Teacher,Instructional Staff
Lorelie,Williams,E16149,lorelie.williams@apsva.us,Key ES,Custodian,Facilities Staff
Patricia,Jarvis,E16670,patricia.jarvis@apsva.us,Key ES,Teacher,Instructional Staff
Claudia,Samayoa,E16774,claudia.samayoa@apsva.us,Key ES,Teacher,Instructional Staff
Barbara,Molina,E1705,barbara.molina@apsva.us,Key ES,Staff,Staff
Ana,Valdes-Mosel,E19543,ana.mosel@apsva.us,Key ES,Teacher,Instructional Staff
Jessica,Castro,E19660,jessica.castro@apsva.us,Key ES,Teacher,Instructional Staff
Meg,Enriquez,E19851,meg.enriquez@apsva.us,Key ES,Teacher,Instructional Staff
Edna,Valladares,E19904,edna.valladares@apsva.us,Key ES,Staff,Staff
Johanna,Verdesoto,E20029,johanna.verdesoto@apsva.us,Key ES,Teacher,Instructional Staff
Kenyon,Spann,E20374,kenyon.spann@apsva.us,Key ES,Teacher,Instructional Staff
Evelyn,Salazar,E20966,evelyn.salazar@apsva.us,Key ES,Teacher,Instructional Staff
Jennifer,Contreras,E21309,jennifer.contreras@apsva.us,Key ES,Teacher,Instructional Staff
Debbie,Diez-Carrizo,E21442,debbie.diez@apsva.us,Key ES,Teacher,Instructional Staff
Mariana,Rosado,E21514,mariana.rosado@apsva.us,Key ES,Teacher,Instructional Staff
Miriam,Burgos,E21536,miriam.burgos@apsva.us,Key ES,"Instructional Assistant, Staff",Instructional Staff
Shirley,Andrade,E21644,shirley.andrade@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Beverly,Kilmer,E21728,beverly.kilmer@apsva.us,Key ES,Teacher,Instructional Staff
Liliana,Vedia Michel,E21816,liliana.vedia@apsva.us,Key ES,Teacher,Instructional Staff
Joseluis,Callejas,E22469,joseluis.callejas@apsva.us,Key ES,Custodian,Facilities Staff
Dana,Crepeau,E2250,dana.crepeau@apsva.us,Key ES,Teacher,Instructional Staff
Walker,Valdez,E22617,walker.valdez@apsva.us,Key ES,Teacher,Instructional Staff
Isaac,Mejia,E22665,isaac.mejia@apsva.us,Key ES,Maintenance Supervisor,Facilities Staff
Ann,Infantino,E22853,ann.infantino@apsva.us,Key ES,Teacher,Instructional Staff
Vernita,Marshall,E23115,vernita.marshall@apsva.us,Key ES,Staff,Staff
Joshua,Guzman,E23551,joshua.guzman@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Rebecca,Lehr,E23950,rebecca.lehr@apsva.us,Key ES,Teacher,Instructional Staff
Fidel,Cordoba,E246,fidel.cordoba@apsva.us,Key ES,Teacher,Instructional Staff
Laurie,Dodson,E2467,laurie.dodson@apsva.us,Key ES,Teacher,Instructional Staff
Karina,Rivera,E24710,karina.rivera@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Esther,Castillo,E24846,esther.castillo@apsva.us,Key ES,Teacher,Instructional Staff
Irma,Heidig,E25033,irma.heidig@apsva.us,Key ES,Teacher,Instructional Staff
Amilcar,Moreno-Hernandez,E26441,amilcar.hernandez@apsva.us,Key ES,Custodian,Facilities Staff
Jennifer,Hatch,E26772,jennifer.hatch@apsva.us,Key ES,Teacher,Instructional Staff
Vimaries,Santiago Vazquez,E26833,vimaries.santiago@apsva.us,Key ES,Teacher,Instructional Staff
Esmeralda,Alomia,E26860,esmeralda.alomia@apsva.us,Key ES,Teacher,Instructional Staff
Iris,Martinez Ayala,E26883,iris.martinezayala@apsva.us,Key ES,Teacher,Instructional Staff
Jeremy,Sullivan,E27234,jeremy.sullivan2@apsva.us,Key ES,Teacher,Instructional Staff
Sohany,Orellana Montiel,E27479,sohany.montiel@apsva.us,Key ES,"Instructional Assistant, Staff",Instructional Staff
Gerrika,Missouri,E27507,gerrika.missouri@apsva.us,Key ES,Staff,Staff
Heyshell,Serrano Valle,E27583,heyshell.serranovall@apsva.us,Key ES,Teacher,Instructional Staff
Nicole,Maldonado,E27892,nicole.maldonado@apsva.us,Key ES,Assistant Principal,Administrative Staff
Jodi,Maher,E27980,jodi.maher@apsva.us,Key ES,Teacher,Instructional Staff
Micaela,Pond,E28,micaela.pond@apsva.us,Key ES,Teacher,Instructional Staff
Abigail,Stengle,E28202,abigail.stengle@apsva.us,Key ES,Teacher,Instructional Staff
Michael,Fava,E2824,michael.fava@apsva.us,Key ES,Teacher,Instructional Staff
Aster,Neguse,E28472,aster.neguse@apsva.us,Key ES,Staff,Staff
Patricia,Hynes,E28895,patricia.hynes@apsva.us,Key ES,Teacher,Instructional Staff
Sandy,Quintanilla,E29149,sandy.quintanilla@apsva.us,Key ES,Staff,Staff
Ann,Lee,E29517,ann.lee2@apsva.us,Key ES,Administrative Assistant,Staff
W,Llanos Montenegro,E29773,w.llanosmontenegro@apsva.us,Key ES,Staff,Staff
Alejandra,Leonzo,E29790,alejandra.leonzo@apsva.us,Key ES,Teacher,Instructional Staff
K,Matthias Auchter,E29822,k.matthiasauchter@apsva.us,Key ES,Teacher,Instructional Staff
Marisol,Roca,E30054,marisol.roca3@apsva.us,Key ES,Teacher,Instructional Staff
Dwayne,Herrin,E30440,dwayne.herrin@apsva.us,Key ES,Staff,Staff
Heidi,Haretos,E30457,heidi.haretos@apsva.us,Key ES,Teacher,Instructional Staff
Carlos,Campos,E30526,carlos.campos2@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Jayla,Henry,E30563,jayla.henry@apsva.us,Key ES,Staff,Staff
Olivia,Crass,E30583,olivia.crass@apsva.us,Key ES,Teacher,Instructional Staff
Marlenis,Velasquez,E30735,marlenis.velasquez@apsva.us,Key ES,Custodian,Facilities Staff
Adrienne,Leach,E31076,adrienne.leach@apsva.us,Key ES,Teacher,Instructional Staff
Ashley,Granados,E31372,ashley.granados@apsva.us,Key ES,Teacher,Instructional Staff
Kathryn,Lynch,E31485,kathryn.lynch@apsva.us,Key ES,Teacher,Instructional Staff
Jennifer,Loayes Delgado,E31736,jennifer.delgado@apsva.us,Key ES,"Instructional Assistant, Staff",Instructional Staff
Katherine,Ryan,E31884,katherine.ryan@apsva.us,Key ES,Teacher,Instructional Staff
Cinthia,Paz,E32438,cinthia.paz@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Kiara,Hood,E32469,kiara.hood@apsva.us,Key ES,Staff,Staff
Gregory,Landrigan,E32612,gregory.landrigan@apsva.us,Key ES,Teacher,Instructional Staff
April,Ege,E32913,april.ege@apsva.us,Key ES,Teacher,Instructional Staff
Doris,Vicente Gomez,E33064,doris.vicentegomez@apsva.us,Key ES,Staff,Staff
Shirley,O'Donnell,E33075,shirley.odonnell@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Nelcy,Carreno Florez,E33465,nelcy.carrenoflorez@apsva.us,Key ES,Teacher,Instructional Staff
Matilde,Arciniegas,E379,matilde.arciniegas@apsva.us,Key ES,Teacher,Instructional Staff
Rosa,Navas,E400,rosa.navas@apsva.us,Key ES,Teacher,Instructional Staff
Cecilia,Rivera,E4354,cecilia.rivera@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Kristy,Bergmann,E4362,kristy.bergmann@apsva.us,Key ES,Teacher,Instructional Staff
Holly,Webb,E5091,holly.webb@apsva.us,Key ES,Teacher,Instructional Staff
Noemi,Rocabado,E6217,noemi.rocabado@apsva.us,Key ES,"Staff, Instructional Assistant",Instructional Staff
Patricia,Almeyda,E6677,patricia.almeyda@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Teresa,Montoya,E6936,teresa.montoya@apsva.us,Key ES,Administrative Assistant,Staff
John,Findley,E7119,john.findley@apsva.us,Key ES,n/a,Unclassified
Marta,Gomez,E8442,marta.gomez@apsva.us,Key ES,Instructional Assistant,Instructional Staff
Lynn,Taylor,E10210,lynn.taylor@apsva.us,Long Branch ES,Teacher,Instructional Staff
Wayne,Hogwood,E10638,wayne.hogwood@apsva.us,Long Branch ES,Teacher,Instructional Staff
Estelle,Roth,E10802,estelle.roth@apsva.us,Long Branch ES,n/a,Unclassified
Krista,Cook,E12000,krista.cook@apsva.us,Long Branch ES,Teacher,Instructional Staff
Tara,Mitchell,E12349,tara.mitchell@apsva.us,Long Branch ES,Teacher,Instructional Staff
Charron,Mckethean,E14391,charron.mckethean@apsva.us,Long Branch ES,Staff,Staff
Rosie,Gaston,E14477,rosie.gaston@apsva.us,Long Branch ES,Teacher,Instructional Staff
Monica,Sugaray,E14906,monica.sugaray@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Flor,Williams,E15519,flor.williams@apsva.us,Long Branch ES,Principals Assistant,Staff
Lisa,Chamness,E16003,lisa.chamness@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Sarah,Herlihy,E16690,sarah.herlihy@apsva.us,Long Branch ES,Teacher,Instructional Staff
William,Heim,E19621,william.heim@apsva.us,Long Branch ES,Teacher,Instructional Staff
Michelle,Smith,E19774,michelle.smith@apsva.us,Long Branch ES,Staff,Staff
Sophia,Kyerewaa,E19871,sophia.kyerewaa@apsva.us,Long Branch ES,Custodian,Facilities Staff
Lindsey,Brizendine,E20591,lindsey.brizendine@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lee,Ayoub,E20922,lee.ayoub@apsva.us,Long Branch ES,Teacher,Instructional Staff
Ashley,Hutcherson,E21229,ashley.hutcherson@apsva.us,Long Branch ES,Teacher,Instructional Staff
Susan,Loney,E21270,susan.loney@apsva.us,Long Branch ES,Teacher,Instructional Staff
Celine,Clark,E21343,celine.clark@apsva.us,Long Branch ES,Teacher,Instructional Staff
Danielle,Fuller,E21607,danielle.fuller@apsva.us,Long Branch ES,Teacher,Instructional Staff
Carla,Johnson,E21769,carla.johnson@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Martha,Paz-Espinoza,E22068,martha.espinoza@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jillian,Williams,E22305,jillian.williams@apsva.us,Long Branch ES,Teacher,Instructional Staff
Hiba,Elhag,E22515,hiba.elhag@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Alexis,Robinson,E22758,alexis.robinson@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jessica,Gray,E24064,jessica.gray@apsva.us,Long Branch ES,Teacher,Instructional Staff
Catherine,Pawlowski,E24089,catherine.pawlowski@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jennifer,Clarke,E24528,jennifer.clarke@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lisa,Greenberg,E24946,lisa.greenberg@apsva.us,Long Branch ES,Teacher,Instructional Staff
Kathryn,Oakes,E24955,kathryn.oakes@apsva.us,Long Branch ES,Teacher,Instructional Staff
Molly,Spooner Agnew,E24993,molly.spooneragnew@apsva.us,Long Branch ES,Teacher,Instructional Staff
Chelsea,Craddock,E25206,chelsea.craddock@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Kailyn,Diaz,E25270,kailyn.diaz@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jessica,Dvorak,E25541,jessica.dvorak@apsva.us,Long Branch ES,Teacher,Instructional Staff
Shirley,Doswell,E25570,shirley.doswell@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Samantha,Sklar,E25902,samantha.sklar@apsva.us,Long Branch ES,Teacher,Instructional Staff
Marissa,Allex,E26210,marissa.allex@apsva.us,Long Branch ES,Teacher,Instructional Staff
Kylah,Jackson Lott,E26246,kylah.jacksonlott@apsva.us,Long Branch ES,Teacher,Instructional Staff
Randy,Lott,E26396,randy.lott@apsva.us,Long Branch ES,Teacher,Instructional Staff
Stephanie,Martin,E26768,stephanie.martin@apsva.us,Long Branch ES,Teacher,Instructional Staff
Ghadir,Al-Khatib,E26783,ghadir.alkhatib@apsva.us,Long Branch ES,Teacher,Instructional Staff
Terisha,Fahie,E26829,terisha.fahie@apsva.us,Long Branch ES,Teacher,Instructional Staff
Romeo,Goffney,E27162,romeo.goffney@apsva.us,Long Branch ES,Staff,Staff
Johnny,De Leon,E27599,johnny.deleon@apsva.us,Long Branch ES,Maintenance Supervisor,Facilities Staff
Derrick,Clarke,E27670,derrick.clarke@apsva.us,Long Branch ES,Custodian,Facilities Staff
Monae,Blount,E28142,monae.blount@apsva.us,Long Branch ES,Staff,Staff
Tonique,Kenner,E28178,tonique.kenner@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Eden,Yilma,E28404,eden.yilma@apsva.us,Long Branch ES,Staff,Staff
Thomas,Easley,E29581,thomas.easley2@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Hilda,Vanegas,E29851,hilda.vanegas@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Emily,Beisler,E29866,emily.beisler2@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jessica,DaSilva,E31059,jessica.dasilva@apsva.us,Long Branch ES,Principal,Unclassified
Gabrielle,Wolosik,E32000,gabrielle.wolosik@apsva.us,Long Branch ES,Teacher,Instructional Staff
Samantha,McGeorge,E32008,samantha.mcgeorge@apsva.us,Long Branch ES,Teacher,Instructional Staff
Elizabeth,Persaud,E32047,elizabeth.persaud@apsva.us,Long Branch ES,Teacher,Instructional Staff
Christine,Coughlin,E32067,christine.coughlin@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lauren,Guiffre,E32163,lauren.guiffre@apsva.us,Long Branch ES,Teacher,Instructional Staff
Ivonne,Villarroel Miquilena,E32251,ivonne.villarroel@apsva.us,Long Branch ES,Administrative Assistant,Staff
Jaqueline,Garcia-Pacheco,E32336,jackie.garcia@apsva.us,Long Branch ES,Staff,Staff
Lawanda,Britt,E32415,lawanda.britt@apsva.us,Long Branch ES,Staff,Staff
Aimee,Freeman,E32529,aimee.freeman@apsva.us,Long Branch ES,Teacher,Instructional Staff
Heidi,Culbertson,E32618,heidi.culbertson@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Ahmed,Salem,E32780,ahmed.salem@apsva.us,Long Branch ES,Teacher,Instructional Staff
Litzy,Belteton-Gomez,E32988,litzy.beltetongomez@apsva.us,Long Branch ES,Staff,Staff
Aniesha,Campbell,E33040,aniesha.campbell@apsva.us,Long Branch ES,Teacher,Instructional Staff
Alyssa,Van Duyn,E33144,alyssa.vanduyn@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jessica,Tolover,E33229,jessica.tolover@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Carolyn,Jackson,E4323,carolynruth.jackson@apsva.us,Long Branch ES,Assistant Principal,Administrative Staff
Karen,Woolley,E518,karen.woolley@apsva.us,Long Branch ES,Administrative Assistant,Staff
Kate,Kaplow,E5794,kate.kaplow@apsva.us,Long Branch ES,Teacher,Instructional Staff
Evin,Rodriguez,E5799,evin.rodriguez@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Yolanda,Sotelo,E5937,yolanda.sotelo@apsva.us,Long Branch ES,Staff,Staff
Dina,Yauger,E6287,dina.yauger@apsva.us,Long Branch ES,Teacher,Instructional Staff
Ruth,Andrade,E6743,ruth.aguirre@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Ly,Trang,E7100,ly.trang@apsva.us,Long Branch ES,Custodian,Facilities Staff
Alyssa,Watkins,E7185,alyssa.watkins@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lisa,Barney,E7921,lisa.barney@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Julie,O'Dea,E8454,julie.odea@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Mary Lou,Rube,E964,marylou.rube@apsva.us,Long Branch ES,Teacher,Instructional Staff
Shelley,Roberts,E9690,shelley.roberts@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lynn,Taylor,E10210,lynn.taylor@apsva.us,Long Branch ES,Teacher,Instructional Staff
Wayne,Hogwood,E10638,wayne.hogwood@apsva.us,Long Branch ES,Teacher,Instructional Staff
Estelle,Roth,E10802,estelle.roth@apsva.us,Long Branch ES,n/a,Unclassified
Krista,Cook,E12000,krista.cook@apsva.us,Long Branch ES,Teacher,Instructional Staff
Tara,Mitchell,E12349,tara.mitchell@apsva.us,Long Branch ES,Teacher,Instructional Staff
Charron,Mckethean,E14391,charron.mckethean@apsva.us,Long Branch ES,Staff,Staff
Rosie,Gaston,E14477,rosie.gaston@apsva.us,Long Branch ES,Teacher,Instructional Staff
Monica,Sugaray,E14906,monica.sugaray@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Flor,Williams,E15519,flor.williams@apsva.us,Long Branch ES,Principals Assistant,Staff
Lisa,Chamness,E16003,lisa.chamness@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Sarah,Herlihy,E16690,sarah.herlihy@apsva.us,Long Branch ES,Teacher,Instructional Staff
William,Heim,E19621,william.heim@apsva.us,Long Branch ES,Teacher,Instructional Staff
Michelle,Smith,E19774,michelle.smith@apsva.us,Long Branch ES,Staff,Staff
Sophia,Kyerewaa,E19871,sophia.kyerewaa@apsva.us,Long Branch ES,Custodian,Facilities Staff
Lindsey,Brizendine,E20591,lindsey.brizendine@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lee,Ayoub,E20922,lee.ayoub@apsva.us,Long Branch ES,Teacher,Instructional Staff
Ashley,Hutcherson,E21229,ashley.hutcherson@apsva.us,Long Branch ES,Teacher,Instructional Staff
Susan,Loney,E21270,susan.loney@apsva.us,Long Branch ES,Teacher,Instructional Staff
Celine,Clark,E21343,celine.clark@apsva.us,Long Branch ES,Teacher,Instructional Staff
Danielle,Fuller,E21607,danielle.fuller@apsva.us,Long Branch ES,Teacher,Instructional Staff
Carla,Johnson,E21769,carla.johnson@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Martha,Paz-Espinoza,E22068,martha.espinoza@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jillian,Williams,E22305,jillian.williams@apsva.us,Long Branch ES,Teacher,Instructional Staff
Hiba,Elhag,E22515,hiba.elhag@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Alexis,Robinson,E22758,alexis.robinson@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jessica,Gray,E24064,jessica.gray@apsva.us,Long Branch ES,Teacher,Instructional Staff
Catherine,Pawlowski,E24089,catherine.pawlowski@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jennifer,Clarke,E24528,jennifer.clarke@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lisa,Greenberg,E24946,lisa.greenberg@apsva.us,Long Branch ES,Teacher,Instructional Staff
Kathryn,Oakes,E24955,kathryn.oakes@apsva.us,Long Branch ES,Teacher,Instructional Staff
Molly,Spooner Agnew,E24993,molly.spooneragnew@apsva.us,Long Branch ES,Teacher,Instructional Staff
Chelsea,Craddock,E25206,chelsea.craddock@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Kailyn,Diaz,E25270,kailyn.diaz@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jessica,Dvorak,E25541,jessica.dvorak@apsva.us,Long Branch ES,Teacher,Instructional Staff
Shirley,Doswell,E25570,shirley.doswell@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Samantha,Sklar,E25902,samantha.sklar@apsva.us,Long Branch ES,Teacher,Instructional Staff
Marissa,Allex,E26210,marissa.allex@apsva.us,Long Branch ES,Teacher,Instructional Staff
Kylah,Jackson Lott,E26246,kylah.jacksonlott@apsva.us,Long Branch ES,Teacher,Instructional Staff
Randy,Lott,E26396,randy.lott@apsva.us,Long Branch ES,Teacher,Instructional Staff
Stephanie,Martin,E26768,stephanie.martin@apsva.us,Long Branch ES,Teacher,Instructional Staff
Ghadir,Al-Khatib,E26783,ghadir.alkhatib@apsva.us,Long Branch ES,Teacher,Instructional Staff
Terisha,Fahie,E26829,terisha.fahie@apsva.us,Long Branch ES,Teacher,Instructional Staff
Romeo,Goffney,E27162,romeo.goffney@apsva.us,Long Branch ES,Staff,Staff
Johnny,De Leon,E27599,johnny.deleon@apsva.us,Long Branch ES,Maintenance Supervisor,Facilities Staff
Derrick,Clarke,E27670,derrick.clarke@apsva.us,Long Branch ES,Custodian,Facilities Staff
Monae,Blount,E28142,monae.blount@apsva.us,Long Branch ES,Staff,Staff
Tonique,Kenner,E28178,tonique.kenner@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Eden,Yilma,E28404,eden.yilma@apsva.us,Long Branch ES,Staff,Staff
Thomas,Easley,E29581,thomas.easley2@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Hilda,Vanegas,E29851,hilda.vanegas@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Emily,Beisler,E29866,emily.beisler2@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jessica,DaSilva,E31059,jessica.dasilva@apsva.us,Long Branch ES,Principal,Unclassified
Gabrielle,Wolosik,E32000,gabrielle.wolosik@apsva.us,Long Branch ES,Teacher,Instructional Staff
Samantha,McGeorge,E32008,samantha.mcgeorge@apsva.us,Long Branch ES,Teacher,Instructional Staff
Elizabeth,Persaud,E32047,elizabeth.persaud@apsva.us,Long Branch ES,Teacher,Instructional Staff
Christine,Coughlin,E32067,christine.coughlin@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lauren,Guiffre,E32163,lauren.guiffre@apsva.us,Long Branch ES,Teacher,Instructional Staff
Ivonne,Villarroel Miquilena,E32251,ivonne.villarroel@apsva.us,Long Branch ES,Administrative Assistant,Staff
Jaqueline,Garcia-Pacheco,E32336,jackie.garcia@apsva.us,Long Branch ES,Staff,Staff
Lawanda,Britt,E32415,lawanda.britt@apsva.us,Long Branch ES,Staff,Staff
Aimee,Freeman,E32529,aimee.freeman@apsva.us,Long Branch ES,Teacher,Instructional Staff
Heidi,Culbertson,E32618,heidi.culbertson@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Ahmed,Salem,E32780,ahmed.salem@apsva.us,Long Branch ES,Teacher,Instructional Staff
Litzy,Belteton-Gomez,E32988,litzy.beltetongomez@apsva.us,Long Branch ES,Staff,Staff
Aniesha,Campbell,E33040,aniesha.campbell@apsva.us,Long Branch ES,Teacher,Instructional Staff
Alyssa,Van Duyn,E33144,alyssa.vanduyn@apsva.us,Long Branch ES,Teacher,Instructional Staff
Jessica,Tolover,E33229,jessica.tolover@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Carolyn,Jackson,E4323,carolynruth.jackson@apsva.us,Long Branch ES,Assistant Principal,Administrative Staff
Karen,Woolley,E518,karen.woolley@apsva.us,Long Branch ES,Administrative Assistant,Staff
Kate,Kaplow,E5794,kate.kaplow@apsva.us,Long Branch ES,Teacher,Instructional Staff
Evin,Rodriguez,E5799,evin.rodriguez@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Yolanda,Sotelo,E5937,yolanda.sotelo@apsva.us,Long Branch ES,Staff,Staff
Dina,Yauger,E6287,dina.yauger@apsva.us,Long Branch ES,Teacher,Instructional Staff
Ruth,Andrade,E6743,ruth.aguirre@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Ly,Trang,E7100,ly.trang@apsva.us,Long Branch ES,Custodian,Facilities Staff
Alyssa,Watkins,E7185,alyssa.watkins@apsva.us,Long Branch ES,Teacher,Instructional Staff
Lisa,Barney,E7921,lisa.barney@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Julie,O'Dea,E8454,julie.odea@apsva.us,Long Branch ES,Instructional Assistant,Instructional Staff
Mary Lou,Rube,E964,marylou.rube@apsva.us,Long Branch ES,Teacher,Instructional Staff
Shelley,Roberts,E9690,shelley.roberts@apsva.us,Long Branch ES,Teacher,Instructional Staff
Anne,Eubank,E10168,anne.eubank@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Heather,Mcadam,E11911,heather.mcadam@apsva.us,McKinley ES,Teacher,Instructional Staff
Kathleen,Michael,E11956,kathleen.michael@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Caitlin,Miller,E13575,caitlin.miller@apsva.us,McKinley ES,Teacher,Instructional Staff
Ceecee,Broderick,E16384,ceecee.broderick@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Brittany,Magill,E16618,brittany.magill@apsva.us,McKinley ES,Teacher,Instructional Staff
Ziyin,Naod,E16820,ziyen.getaw@apsva.us,McKinley ES,Staff,Staff
Claudia,Santos Salmeron,E19013,claudia.salmeron@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Alice,Haserodt,E19487,alice.haserodt@apsva.us,McKinley ES,Teacher,Instructional Staff
Cassie,McBride,E20126,cassie.mcbride@apsva.us,McKinley ES,Teacher,Instructional Staff
Maren,Mapp,E20594,maren.mapp@apsva.us,McKinley ES,Teacher,Instructional Staff
Kathryn,White,E21243,kathryn.white@apsva.us,McKinley ES,Teacher,Instructional Staff
Jamie,Aubrey,E21554,jamie.aubrey@apsva.us,McKinley ES,Teacher,Instructional Staff
Sarah,Romero,E22751,sarah.romero@apsva.us,McKinley ES,Teacher,Instructional Staff
Kirsten,Walleck,E23081,kirsten.walleck@apsva.us,McKinley ES,Teacher,Instructional Staff
Jose,Morales Mena,E23852,jose.morales@apsva.us,McKinley ES,Custodian,Facilities Staff
Joyce,Agyei,E24238,joyce.agyei@apsva.us,McKinley ES,Custodian,Facilities Staff
Nicole,Lewis,E24621,nicole.lewis@apsva.us,McKinley ES,Teacher,Instructional Staff
Kevin,Trainor Jr.,E24900,kevin.trainor@apsva.us,McKinley ES,Teacher,Instructional Staff
Amy,Iger,E25718,amy.iger@apsva.us,McKinley ES,Teacher,Instructional Staff
Shannel,Hoyer,E25761,shannel.hoyer@apsva.us,McKinley ES,Teacher,Instructional Staff
Jessica,Rogers,E25772,jessica.rogers@apsva.us,McKinley ES,Teacher,Instructional Staff
Kristen,Bartholomew,E25793,kristen.bartholomew@apsva.us,McKinley ES,Teacher,Instructional Staff
Catherine,Ackleson,E26064,catherine.ackleson@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Virginia,Lopez De Flores,E26232,virginia.lopezdeflor@apsva.us,McKinley ES,Staff,Staff
Nicole,Hurren,E26315,nicole.hurren@apsva.us,McKinley ES,Teacher,Instructional Staff
Kelvin,Amaya,E26371,kelvin.amaya@apsva.us,McKinley ES,Staff,Staff
Amanda,Martin,E26470,amanda.martin@apsva.us,McKinley ES,Administrative Assistant,Staff
Stefanie,Simmerman,E26733,stefanie.simmerman@apsva.us,McKinley ES,Teacher,Instructional Staff
Amy,Kirkpatrick,E26741,amy.kirkpatrick@apsva.us,McKinley ES,Teacher,Instructional Staff
Marykate,Girardi,E26901,marykate.girardi@apsva.us,McKinley ES,Teacher,Instructional Staff
Gina,Miller,E26995,gina.miller@apsva.us,McKinley ES,Assistant Principal,Administrative Staff
Emily,Spellman,E27410,emily.spellman2@apsva.us,McKinley ES,Teacher,Instructional Staff
Erika,Paz,E27791,erika.paz@apsva.us,McKinley ES,Teacher,Instructional Staff
Meco,Guice,E27796,meco.guice@apsva.us,McKinley ES,Teacher,Instructional Staff
Rachael,Moore,E27802,rachael.moore@apsva.us,McKinley ES,Teacher,Instructional Staff
Megan,O'Brien,E27930,megan.obrien@apsva.us,McKinley ES,Teacher,Instructional Staff
Amanda,Herr,E27960,amanda.herr@apsva.us,McKinley ES,Teacher,Instructional Staff
Ernest,Basoah,E28306,ernest.basoah@apsva.us,McKinley ES,Maintenance Supervisor,Facilities Staff
Chelsea,Christ,E28393,chelsea.christ2@apsva.us,McKinley ES,Teacher,Instructional Staff
Jose Martinez,Martinez Callejas,E28406,jose.callejas@apsva.us,McKinley ES,Custodian,Facilities Staff
Nahdeya,Quarles,E28777,nahdeya.quarles@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Nicholas,Natalie,E28825,nicholas.natalie@apsva.us,McKinley ES,n/a,Unclassified
Lauren,Foreman,E28933,lauren.foreman@apsva.us,McKinley ES,Teacher,Instructional Staff
Adrienne,Glenn,E28942,adrienne.glenn@apsva.us,McKinley ES,Teacher,Instructional Staff
Adrienne,De Santis,E28979,adrienne.desantis@apsva.us,McKinley ES,Teacher,Instructional Staff
Jessica,Parnell,E28988,jessica.parnell@apsva.us,McKinley ES,Teacher,Instructional Staff
Amy,Pitaro,E28991,amy.pitaro@apsva.us,McKinley ES,Teacher,Instructional Staff
Danielle,De Lanoy,E29352,danielle.delanoy@apsva.us,McKinley ES,Teacher,Instructional Staff
Michele,Mautawalli,E2956,michele.mautawalli@apsva.us,McKinley ES,Teacher,Instructional Staff
Neim,Phe,E29874,neim.phe@apsva.us,McKinley ES,Staff,Staff
Claire,Braeuer,E29948,claire.braeuer@apsva.us,McKinley ES,Teacher,Instructional Staff
Anna,Norell,E30085,anna.norell@apsva.us,McKinley ES,Teacher,Instructional Staff
Sandy,Perez Gonzalez,E30205,sandy.perezgonzalez@apsva.us,McKinley ES,Administrative Assistant,Staff
Maria,Montas,E30223,maria.montas@apsva.us,McKinley ES,"Instructional Assistant, Administrative Assistant",Staff
Anne,Leamon,E30233,anne.leamon@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Onasis,Tanoe,E31055,onasis.tanoe@apsva.us,McKinley ES,Maintenance Staff,Facilities Staff
Claudia,Salomon Maldonado,E31294,claudia.salomon@apsva.us,McKinley ES,Teacher,Instructional Staff
Cinthya,Jaldin,E31823,cinthya.jaldin@apsva.us,McKinley ES,Staff,Staff
Natalie,Ramos,E32130,natalie.ramos@apsva.us,McKinley ES,Teacher,Instructional Staff
Stacy,Leff,E32190,stacy.leff@apsva.us,McKinley ES,Staff,Staff
Bethany,Johnson,E32616,bethany.johnson@apsva.us,McKinley ES,Teacher,Instructional Staff
Cecilia,Darpoh,E32852,cecilia.darpoh@apsva.us,McKinley ES,Custodian,Facilities Staff
Marvin,Chavez,E32865,marvin.chavez@apsva.us,McKinley ES,Custodian,Facilities Staff
Kathryn,Allen,E32935,kathryn.allen@apsva.us,McKinley ES,Teacher,Instructional Staff
Sara,Harrison,E32973,sara.harrison@apsva.us,McKinley ES,Staff,Staff
Irina,Stumpo,E3669,irina.stumpo@apsva.us,McKinley ES,Teacher,Instructional Staff
Tamla,Cantow,E4131,tammy.cantow@apsva.us,McKinley ES,Administrative Assistant,Staff
Colin,Brown,E4207,colin.brown@apsva.us,McKinley ES,Principal,Unclassified
Johanna,Boyers,E4216,johanna.boyers@apsva.us,McKinley ES,Teacher,Instructional Staff
Cheryl,Knowles,E428,cheryl.knowles@apsva.us,McKinley ES,Teacher,Instructional Staff
Maria,Sejas-Cardenas,E5207,maria.cardenas@apsva.us,McKinley ES,Teacher,Instructional Staff
Kyra,Wohlford,E6506,kyra.wohlford@apsva.us,McKinley ES,Teacher,Instructional Staff
Julie,Bolin,E69,julie.bolin@apsva.us,McKinley ES,Teacher,Instructional Staff
Gina,Rocco,E7709,gina.rocco@apsva.us,McKinley ES,Teacher,Instructional Staff
Kathy,Recinos,E8140,kathy.recinos@apsva.us,McKinley ES,Teacher,Instructional Staff
Amy,Casillas,E8631,amy.casillas@apsva.us,McKinley ES,Teacher,Instructional Staff
Regina,Owusu,E8802,regina.owusu@apsva.us,McKinley ES,Custodian,Facilities Staff
Rebekah,Kousa,E9084,rebekah.kousa@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Kathy,Villareal,E9645,kathy.villareal@apsva.us,McKinley ES,Administrative Assistant,Staff
Love,Stroble,E9731,love.stroble@apsva.us,McKinley ES,Instructional Assistant,Instructional Staff
Nicole,Derocco,E15920,nicole.derocco@apsva.us,New Directions,Teacher,Instructional Staff
Joshua,Patulski,E1852,joshua.patulski@apsva.us,New Directions,n/a,Unclassified
Teresa,Ghiglino,E19021,teresa.ghiglino@apsva.us,New Directions,Teacher,Instructional Staff
Karen,DeCarlo,E19875,karen.decarlo@apsva.us,New Directions,Teacher,Instructional Staff
Joshua,Folb,E20345,joshua.folb@apsva.us,New Directions,n/a,Unclassified
Andrew,Shreeve,E23318,andrew.shreeve@apsva.us,New Directions,n/a,Unclassified
Tracy,Santasine,E24313,tracy.santasine@apsva.us,New Directions,n/a,Unclassified
Christina,Hogan,E25727,christina.hogan@apsva.us,New Directions,Teacher,Instructional Staff
Evelyn,Matus,E27271,evelyn.matus@apsva.us,New Directions,Teacher,Instructional Staff
Chip,Bonar,E3633,chip.bonar@apsva.us,New Directions,Principal,Unclassified
Wendy,Taylor,E4806,wendy.taylor@apsva.us,New Directions,Teacher,Instructional Staff
Simon,Contreras,E4987,simon.contreras@apsva.us,New Directions,Teacher,Instructional Staff
Kim,Hale,E5212,kim.hale@apsva.us,New Directions,Teacher,Instructional Staff
Kwabena,Baffour,E6141,kwabena.baffour@apsva.us,New Directions,Custodian,Facilities Staff
Nan,Holl,E9467,nan.holl@apsva.us,New Directions,Teacher,Instructional Staff
Kelynne,Bisbee,E1098,kelynne.bisbee@apsva.us,Nottingham ES,Teacher,Instructional Staff
Sara,Devens,E1440,sara.devens@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
Carol,Sheehan,E16733,carol.sheehan@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
Dilma,Reyes,E17092,dilma.reyes@apsva.us,Nottingham ES,Maintenance Supervisor,Facilities Staff
Kim,Fortin,E190,kim.fortin@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
Ana,Mendoza,E19256,ana.mendoza@apsva.us,Nottingham ES,Custodian,Facilities Staff
Eileen,Gardner,E1943,eileen.gardner@apsva.us,Nottingham ES,Principal,Unclassified
Sarah,Zoller,E19595,sarah.zoller@apsva.us,Nottingham ES,Teacher,Instructional Staff
Katie,Biechman,E19682,katie.biechman@apsva.us,Nottingham ES,Teacher,Instructional Staff
Mardi,Moorman,E19919,mardi.moorman@apsva.us,Nottingham ES,Administrative Assistant,Staff
Nicole,Gustafson,E21456,nicole.gustafson@apsva.us,Nottingham ES,Teacher,Instructional Staff
Nora,Haley,E21598,nora.ellison@apsva.us,Nottingham ES,Administrative Assistant,Staff
Hassina,Mechelah,E21879,hassina.mechelah@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
Mischa,Joligard,E22494,mischa.joligard@apsva.us,Nottingham ES,Staff,Staff
Rolando,Posada,E22785,rolando.posada@apsva.us,Nottingham ES,Custodian,Facilities Staff
Laura,Moore,E23116,laura.moore@apsva.us,Nottingham ES,Teacher,Instructional Staff
Tamika,Brown,E23217,tamika.brown@apsva.us,Nottingham ES,Staff,Staff
Jessica,Gray,E24064,jessica.gray@apsva.us,Nottingham ES,Teacher,Instructional Staff
Maria,Meraz,E24141,maria.meraz@apsva.us,Nottingham ES,Staff,Staff
Regina,Koehler,E25329,regina.koehler@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
Sarah,Wysocki,E25784,sarah.wysocki@apsva.us,Nottingham ES,Teacher,Instructional Staff
Michael,Giusto,E25923,michael.giusto@apsva.us,Nottingham ES,Teacher,Instructional Staff
Lisa,Mathis-Barnett,E26490,lisa.mathisbarnett@apsva.us,Nottingham ES,Teacher,Instructional Staff
Megan,Lynch,E26742,megan.lynch@apsva.us,Nottingham ES,Assistant Principal,Administrative Staff
Julia,Petro,E26893,julia.petro@apsva.us,Nottingham ES,Teacher,Instructional Staff
Mark,Jones,E27669,mark.jones2@apsva.us,Nottingham ES,Teacher,Instructional Staff
Victoria,Weiss,E27739,victoria.weiss@apsva.us,Nottingham ES,Teacher,Instructional Staff
Ryan,Van Valen,E27746,ryan.vanvalen@apsva.us,Nottingham ES,Teacher,Instructional Staff
Paul,Mayer,E27852,paul.mayer@apsva.us,Nottingham ES,Teacher,Instructional Staff
Lori,Donnaruma,E27967,lori.donnaruma@apsva.us,Nottingham ES,Teacher,Instructional Staff
Noe,Corado Lemus,E28300,noe.coradolemus@apsva.us,Nottingham ES,Custodian,Facilities Staff
Laurie,Sinton,E28504,laurie.sinton2@apsva.us,Nottingham ES,Administrative Assistant,Staff
Teal,Miles,E28960,teal.miles@apsva.us,Nottingham ES,Teacher,Instructional Staff
Kristin,Muller,E29028,kristin.muller@apsva.us,Nottingham ES,Teacher,Instructional Staff
Mary,Johnson,E29090,mary.johnson@apsva.us,Nottingham ES,Teacher,Instructional Staff
Rachel,Benowitz,E29172,rachel.benowitz@apsva.us,Nottingham ES,Technical Staff,Staff
Lauren,Petro,E29963,lauren.petro@apsva.us,Nottingham ES,Teacher,Instructional Staff
Katherine,Gallagher,E30153,katherine.gallagher@apsva.us,Nottingham ES,Teacher,Instructional Staff
Sara,Joosten,E30890,sara.joosten2@apsva.us,Nottingham ES,Teacher,Instructional Staff
Alexander,Konstantin,E30921,alex.konstantin@apsva.us,Nottingham ES,Teacher,Instructional Staff
Lia,Ehrlich,E30929,lia.gargano@apsva.us,Nottingham ES,Teacher,Instructional Staff
Brandi,Arnett,E31096,brandi.arnett@apsva.us,Nottingham ES,Teacher,Instructional Staff
Dina,Lewin,E31287,dina.lewin@apsva.us,Nottingham ES,Teacher,Instructional Staff
Alexis,Aronson,E31293,alexis.aronson@apsva.us,Nottingham ES,Teacher,Instructional Staff
Lauren,Baumeister,E31298,lauren.baumeister@apsva.us,Nottingham ES,Teacher,Instructional Staff
Tricia,Zipfel,E3141,tricia.zipfel@apsva.us,Nottingham ES,Teacher,Instructional Staff
Caroline,Petro,E31502,caroline.petro@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
George,Obiero,E31772,george.obiero@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
Ann,Morrison,E31799,ann.morrison@apsva.us,Nottingham ES,Teacher,Instructional Staff
Mary,Herron,E32011,mary.herron@apsva.us,Nottingham ES,Teacher,Instructional Staff
Mackenzie,Girard,E32012,mackenzie.girard@apsva.us,Nottingham ES,Teacher,Instructional Staff
Allison,Blair,E32048,allison.blair@apsva.us,Nottingham ES,Teacher,Instructional Staff
Kayla,Greenwood,E32103,kayla.greenwood@apsva.us,Nottingham ES,Teacher,Instructional Staff
Jennifer,Caputo,E32147,jennifer.caputo@apsva.us,Nottingham ES,Teacher,Instructional Staff
Ian,Sheffey,E32347,ian.sheffey@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
Lirma,Tapia Quiroz,E32509,lirma.tapiaquiroz@apsva.us,Nottingham ES,Custodian,Facilities Staff
Nadia,Huaracha,E32803,nadia.huaracha@apsva.us,Nottingham ES,Staff,Staff
Kevin,Link,E33063,kevin.link@apsva.us,Nottingham ES,Staff,Staff
Madison,Johnson,E33249,madison.johnson@apsva.us,Nottingham ES,Teacher,Instructional Staff
Hekmat,Mohammed Salih,E33490,hekmat.mohammedsalih@apsva.us,Nottingham ES,Staff,Staff
Maurice,Katoen,E4254,maurice.katoen@apsva.us,Nottingham ES,Teacher,Instructional Staff
Maryanne,Costa,E4737,maryanne.costa@apsva.us,Nottingham ES,Teacher,Instructional Staff
Randy,Glasner,E692,randy.glasner@apsva.us,Nottingham ES,n/a,Unclassified
Patricia,Duran,E8003,patricia.duran@apsva.us,Nottingham ES,Instructional Assistant,Instructional Staff
Gail,Klein,E11005,gail.klein@apsva.us,Oakridge ES,Teacher,Instructional Staff
Greg,Chapuis,E13049,greg.chapuis@apsva.us,Oakridge ES,Teacher,Instructional Staff
Kelly,Bryant,E14497,kelly.bryant@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Glenda,Charles-Pierre,E1469,glenda.charlespierre@apsva.us,Oakridge ES,Teacher,Instructional Staff
James,Proctor,E14812,james.proctor@apsva.us,Oakridge ES,Teacher,Instructional Staff
Mirna,Lucero,E15522,mirna.lucero@apsva.us,Oakridge ES,Custodian,Facilities Staff
Eulogia,Castillo,E16188,eulogia.castillo@apsva.us,Oakridge ES,Custodian,Facilities Staff
Kristina,Stallings,E16599,kristina.stallings@apsva.us,Oakridge ES,Teacher,Instructional Staff
Beth,Scott,E16782,beth.scott@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Marian,Criswell,E16863,marian.criswell@apsva.us,Oakridge ES,Administrative Assistant,Staff
Althea,Williams,E16998,althea.williams@apsva.us,Oakridge ES,Staff,Staff
Ramon,Mejia,E19186,ramon.mejia@apsva.us,Oakridge ES,Custodian,Facilities Staff
Lynne,Wright,E2002,lynne.wright@apsva.us,Oakridge ES,Principal,Unclassified
Anne,Terwilliger,E20335,anne.terwilliger@apsva.us,Oakridge ES,Teacher,Instructional Staff
Jennifer,Crain,E20569,jennifer.crain@apsva.us,Oakridge ES,Teacher,Instructional Staff
Kelsey,Gallion,E20721,kelsey.gallion@apsva.us,Oakridge ES,Teacher,Instructional Staff
Ali,Judeh,E20889,ali.judeh@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Catherine,Gaudian,E20956,catherine.gaudian@apsva.us,Oakridge ES,Teacher,Instructional Staff
Talita,Stroh,E21304,talita.stroh@apsva.us,Oakridge ES,Teacher,Instructional Staff
Ann,Divecha,E21578,ann.divecha@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Kristen,Wolla,E22112,kristen.wolla@apsva.us,Oakridge ES,Teacher,Instructional Staff
Nicole,Thorpe,E22918,nicole.thorpe@apsva.us,Oakridge ES,Teacher,Instructional Staff
Donna,Felipe,E23612,donna.felipe@apsva.us,Oakridge ES,Teacher,Instructional Staff
Joseph,Papola,E23751,joseph.papola@apsva.us,Oakridge ES,Teacher,Instructional Staff
Shannon,Cohn,E23985,shannon.cohn@apsva.us,Oakridge ES,Teacher,Instructional Staff
Seema,Das,E24251,seema.das@apsva.us,Oakridge ES,Staff,Staff
Louis,Dorsey,E24390,louis.dorsey@apsva.us,Oakridge ES,Administrative Assistant,Staff
Denisse,Ruiz,E24423,denisse.ruiz@apsva.us,Oakridge ES,Administrative Assistant,Staff
Hayley,Dino,E24452,hayley.dino@apsva.us,Oakridge ES,Teacher,Instructional Staff
Alemnesh,Ali,E24500,alemnesh.ali@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Audrey,Nolan,E24548,audrey.nolan@apsva.us,Oakridge ES,Teacher,Instructional Staff
Kelly,Kleintank Sanguino,E25151,kelly.sanguino@apsva.us,Oakridge ES,Teacher,Instructional Staff
Hanim,Magzoub,E25259,hanim.magzoub@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Gerardo,Mendiola Mosqueda,E25320,gerardo.mosqueda@apsva.us,Oakridge ES,Maintenance Supervisor,Facilities Staff
Sumitra,Barua,E25401,sumitra.barua@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Jalice,McRae,E25405,jalice.mcrae@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Derryck,Walker,E25646,derryck.walker@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Karen,Bentall,E25756,karen.bentall@apsva.us,Oakridge ES,Teacher,Instructional Staff
Laura,Murphy,E25803,laura.murphy2@apsva.us,Oakridge ES,Teacher,Instructional Staff
Shannon,Madison,E25839,shannon.madison@apsva.us,Oakridge ES,Teacher,Instructional Staff
Rachael,Fine,E26277,rachael.fine@apsva.us,Oakridge ES,Teacher,Instructional Staff
Aquasha,Mitchell,E26456,aquasha.mitchell@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Beth,Jones,E27287,beth.jones2@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Daysi,Gonzalez,E27348,daysi.gonzalez@apsva.us,Oakridge ES,Custodian,Facilities Staff
Sarah,Minor,E27524,sarah.minor@apsva.us,Oakridge ES,Teacher,Instructional Staff
Sandra,Casco Matamoros,E27620,sandra.matamoros@apsva.us,Oakridge ES,Staff,Staff
Jennifer,Fahrenthold,E27640,jennifer.fahrenthold@apsva.us,Oakridge ES,Teacher,Instructional Staff
Christina,Fowler,E27726,christina.fowler@apsva.us,Oakridge ES,Teacher,Instructional Staff
Ngan,Ha,E27847,ngan.ha@apsva.us,Oakridge ES,Teacher,Instructional Staff
Adrion,Walker,E27942,adrion.walker@apsva.us,Oakridge ES,Teacher,Instructional Staff
Elizabeth,Lansman,E28100,elizabeth.lansman@apsva.us,Oakridge ES,Teacher,Instructional Staff
Karla,Barahona de Guerrero,E28267,karla.guerrero@apsva.us,Oakridge ES,Staff,Staff
Sagirah,Biddle,E28642,sagirah.biddle@apsva.us,Oakridge ES,Staff,Staff
Phimchanok,Chotiruk,E28741,phimchanok.chotiruk@apsva.us,Oakridge ES,Teacher,Instructional Staff
James,Welch,E29066,james.welch@apsva.us,Oakridge ES,Teacher,Instructional Staff
Alexa,Morse,E29071,alexa.morse@apsva.us,Oakridge ES,Teacher,Instructional Staff
Alberto,Quezada Campos,E29418,alberto.quezada@apsva.us,Oakridge ES,Staff,Staff
Sophie,Luckette,E30043,sophie.luckette@apsva.us,Oakridge ES,Teacher,Instructional Staff
Clayton,Sterner,E30049,clayton.sterner@apsva.us,Oakridge ES,Teacher,Instructional Staff
Kathleen,Coronado,E30115,kathleen.coronado@apsva.us,Oakridge ES,Teacher,Instructional Staff
Ma Rosario,Dayan,E30152,marosario.dayan@apsva.us,Oakridge ES,Teacher,Instructional Staff
Reem,Ali,E30299,reem.ali@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Rojin,Shojayi,E30649,rojin.shojayi@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Alexis,Johnson,E31070,alexis.johnson@apsva.us,Oakridge ES,n/a,Unclassified
Francisca,Saavedra Reyes,E31179,francisca.saavedra@apsva.us,Oakridge ES,Teacher,Instructional Staff
Lissa,Tapia Salas,E31192,lissa.tapiasalas@apsva.us,Oakridge ES,Administrative Assistant,Staff
Andrew,Jonas,E31283,andrew.jonas@apsva.us,Oakridge ES,Teacher,Instructional Staff
Jacqueline,Foley,E31316,jacqueline.foley@apsva.us,Oakridge ES,Teacher,Instructional Staff
Alice,Johnson Miller,E31346,alice.johnsonmiller@apsva.us,Oakridge ES,Teacher,Instructional Staff
Emmilu,Olson,E31464,emmilu.olson@apsva.us,Oakridge ES,Teacher,Instructional Staff
Johnny,Lazarte,E31511,johnny.lazarte@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Hannah,Geller,E32036,hannah.geller@apsva.us,Oakridge ES,Teacher,Instructional Staff
Jordan,Kerstetter,E32046,jordan.kerstetter@apsva.us,Oakridge ES,Teacher,Instructional Staff
Lauren,Guiffre,E32163,lauren.guiffre@apsva.us,Oakridge ES,Teacher,Instructional Staff
Sana,Bahjawi,E32339,sana.bahjawi@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Hannah,Steffenson,E32625,hannah.steffenson@apsva.us,Oakridge ES,Teacher,Instructional Staff
Joseph,Doherty,E32724,joseph.doherty@apsva.us,Oakridge ES,Teacher,Instructional Staff
Yanira,Aslam-Nina,E32751,yanira.aslamnina@apsva.us,Oakridge ES,n/a,Unclassified
Edith,Serrano,E32793,edith.serrano@apsva.us,Oakridge ES,Teacher,Instructional Staff
Ekaterina,Antipova,E32857,ekaterina.antipova@apsva.us,Oakridge ES,Teacher,Instructional Staff
Keshia,Henderson,E32867,keshia.henderson@apsva.us,Oakridge ES,Teacher,Instructional Staff
Francis,Panameno De Chavez,E32890,francis.panameno@apsva.us,Oakridge ES,Staff,Staff
Danielle,Rockelman,E32894,danielle.rockelman@apsva.us,Oakridge ES,Teacher,Instructional Staff
Sidnia,Marin,E32949,sidnia.marin@apsva.us,Oakridge ES,Staff,Staff
Ellena,Sarnoff,E33006,ellena.sarnoff@apsva.us,Oakridge ES,Teacher,Instructional Staff
Nicole,Beer,E33018,nicole.beer@apsva.us,Oakridge ES,Teacher,Instructional Staff
Maria,Rios,E33039,maria.rios@apsva.us,Oakridge ES,Staff,Staff
Tana,Robbins,E33069,tana.robbins@apsva.us,Oakridge ES,n/a,Unclassified
Camille,Giacomello,E33076,camille.giacomello2@apsva.us,Oakridge ES,Teacher,Instructional Staff
Emily,Chua,E33276,emily.chua@apsva.us,Oakridge ES,Teacher,Instructional Staff
Stephanie,Leslie,E33473,stephanie.leslie@apsva.us,Oakridge ES,n/a,Unclassified
Michael,Murphy,E33507,michael.murphy@apsva.us,Oakridge ES,Teacher,Instructional Staff
Salwa,Ahmed,E3581,salwa.ahmed@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Caryn,Saunders,E4118,caryn.saunders@apsva.us,Oakridge ES,Teacher,Instructional Staff
Delia,Munoz,E5730,delia.munoz@apsva.us,Oakridge ES,Teacher,Instructional Staff
Maha,Burr,E7504,maha.burr@apsva.us,Oakridge ES,Instructional Assistant,Instructional Staff
Cecilia,Cano,E8341,cecilia.cano@apsva.us,Oakridge ES,Teacher,Instructional Staff
Trudy,Walker,E913,trudy.walker@apsva.us,Oakridge ES,Administrative Assistant,Staff
Wes,Parker,E9203,wes.parker@apsva.us,Oakridge ES,Teacher,Instructional Staff
Erika,Sanchez,E9321,erika.sanchez@apsva.us,Oakridge ES,Assistant Principal,Administrative Staff
Gloria,Torrico,E10106,gloria.torrico@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Margie,Graham,E11003,margie.graham@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Jennifer,Denino,E11190,jennifer.denino@apsva.us,Randolph ES,Assistant Principal,Administrative Staff
Renee,Haire,E12761,renee.haire@apsva.us,Randolph ES,Teacher,Instructional Staff
Carolyn,Fleming,E13009,carolyn.fleming@apsva.us,Randolph ES,Teacher,Instructional Staff
Ward,Merritt,E1499,ward.merritt@apsva.us,Randolph ES,Teacher,Instructional Staff
Laine,Bowen,E16759,laine.bowen@apsva.us,Randolph ES,Teacher,Instructional Staff
Kathleen,Donnelly,E16951,kathleen.donnelly@apsva.us,Randolph ES,Teacher,Instructional Staff
Kimberly,McGuire,E1763,kimberly.mcguire@apsva.us,Randolph ES,Teacher,Instructional Staff
Christine,Chapuis,E19662,christine.chapuis@apsva.us,Randolph ES,Teacher,Instructional Staff
Ellen,Killalea,E19891,ellen.killalea@apsva.us,Randolph ES,Teacher,Instructional Staff
Tara,Ronzetti,E20081,tara.ronzetti@apsva.us,Randolph ES,Teacher,Instructional Staff
Marjorie,Dwyer,E2014,marjorie.dwyer@apsva.us,Randolph ES,Teacher,Instructional Staff
Daniel,Rodriguez,E20300,daniel.rodriguez@apsva.us,Randolph ES,Teacher,Instructional Staff
Paula,Burzio,E20745,paula.burzio@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Sisay,Habtu,E20810,sisay.habtu@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Joy,Gardner,E20830,joy.gardner@apsva.us,Randolph ES,Administrative Assistant,Staff
David,Siu,E21216,david.siu@apsva.us,Randolph ES,Teacher,Instructional Staff
Erin,Kowalevicz,E21472,erin.kowalevicz@apsva.us,Randolph ES,Teacher,Instructional Staff
Rachel,Staley,E21500,rachel.staley@apsva.us,Randolph ES,Teacher,Instructional Staff
Toby,Peabody,E21990,toby.peabody@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Terry,Kita,E22959,terry.kita@apsva.us,Randolph ES,Teacher,Instructional Staff
Lesly,Fuentes,E23200,lesly.fuentes@apsva.us,Randolph ES,Teacher,Instructional Staff
Kerri,Martin,E23429,kerri.martin@apsva.us,Randolph ES,Teacher,Instructional Staff
Maria,Hernandez,E23441,angela.hernandez@apsva.us,Randolph ES,Bilingual Family Resource Assistant,Staff
Mercedes,Oetgen,E24518,mercedes.oetgen@apsva.us,Randolph ES,Teacher,Instructional Staff
Sherri,Sewall-Easley,E24959,sherri.sewall@apsva.us,Randolph ES,Teacher,Instructional Staff
Jennifer,Baitinger,E24967,jennifer.baitinger@apsva.us,Randolph ES,Teacher,Instructional Staff
Michael,Hope,E25061,michael.hope@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Carlos,Ramirez,E25083,carlos.ramirez@apsva.us,Randolph,Principal,Unclassified
Carolina,Ranz,E25434,carolina.ranz@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Wyatt,Weber,E25732,wyatt.weber@apsva.us,Randolph ES,Teacher,Instructional Staff
Jazmyn,Beckford,E25971,jazmyn.beckford@apsva.us,Randolph ES,Teacher,Instructional Staff
Breana,Orr,E26018,breana.orr@apsva.us,Randolph ES,Teacher,Instructional Staff
Shannon,Quinn,E26796,shannon.quinn@apsva.us,Randolph ES,Teacher,Instructional Staff
Jillian,Eskenas,E26843,jillian.eskenas@apsva.us,Randolph ES,Teacher,Instructional Staff
Annie,Tshibangu,E27191,annie.tshibangu@apsva.us,Randolph ES,Staff,Staff
Aster,Goitom,E27223,aster.goitom@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Jennifer,Kaldenbach Montemayor,E27713,jennifer.montemayor@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Maria,Soto Hernandez,E27997,maria.sotohernandez@apsva.us,Randolph ES,Custodian,Facilities Staff
Jennifer,Fuentes,E28516,jennifer.fuentes@apsva.us,Randolph ES,Staff,Staff
Jali,Mutsuddi,E28517,jali.mutsuddi@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Brizeyda,Argueta Jarquin,E28537,brizeyda.jarquin@apsva.us,Randolph ES,Teacher,Instructional Staff
Angel,Lopez Soto,E28828,angel.lopezsoto@apsva.us,Randolph ES,Teacher,Instructional Staff
Dennisse,Montes Majano,E28837,dennisse.majano@apsva.us,Randolph ES,Custodian,Facilities Staff
Karina,Fasullo,E28967,karina.fasullo@apsva.us,Randolph ES,Teacher,Instructional Staff
Christine,Swift,E28986,christine.swift@apsva.us,Randolph ES,Teacher,Instructional Staff
Colette,Cupid,E28994,colette.cupid@apsva.us,Randolph ES,Teacher,Instructional Staff
Karlee,Henson,E29017,karlee.henson@apsva.us,Randolph ES,Teacher,Instructional Staff
Natalie,Prioletti,E29020,natalie.prioletti@apsva.us,Randolph ES,Teacher,Instructional Staff
Evelyn,Molina,E29242,evelyn.molina@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Pablo,Barron,E29609,pablo.barron@apsva.us,Randolph ES,Instructional Assistant,Staff
Rita,Amponsah,E29618,rita.amponsah@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Jacqui,Greene,E29930,jacqui.greene@apsva.us,Randolph ES,Teacher,Instructional Staff
Stefanie,Fleenor,E29945,stefanie.fleenor@apsva.us,Randolph ES,Teacher,Instructional Staff
Kaitlyn,Fullwood,E29992,kaitlyn.fullwood@apsva.us,Randolph ES,Teacher,Instructional Staff
Joanne,Krisko,E30045,joanne.krisko@apsva.us,Randolph ES,Teacher,Instructional Staff
Vincent,Smith,E30101,vincent.smith@apsva.us,Randolph ES,Teacher,Instructional Staff
Michael,Rosenblum,E30126,michael.rosenblum@apsva.us,Randolph ES,Teacher,Instructional Staff
Andrew,McDaniel,E30144,andrew.mcdaniel@apsva.us,Randolph ES,Teacher,Instructional Staff
Molly,Jones,E30175,molly.jones@apsva.us,Randolph ES,Teacher,Instructional Staff
Joshua,Mayer,E30199,joshua.mayer@apsva.us,Randolph ES,Teacher,Instructional Staff
Kezia,Solano,E30285,kezia.solano@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Megan,Spelman,E30925,megan.spelman@apsva.us,Randolph ES,Teacher,Instructional Staff
Bettina,Arrunategui,E30930,bettina.arrunategui@apsva.us,Randolph ES,Teacher,Instructional Staff
Brandi,Goncz,E31252,brandi.goncz@apsva.us,Randolph ES,Teacher,Instructional Staff
Alex,Baldwin,E31286,alex.baldwin@apsva.us,Randolph ES,Teacher,Instructional Staff
Rachel,Gunawardena,E31288,rachel.gunawardena@apsva.us,Randolph ES,Teacher,Instructional Staff
Emily,LeCompte,E31301,emily.lecompte@apsva.us,Randolph ES,Teacher,Instructional Staff
Maria,Castillo,E31500,maria.castillo@apsva.us,Randolph ES,Staff,Staff
Evelin,Pinkney Ayala,E31630,evelin.pinkneyayala@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Amabilia,Lopez Navarro,E31687,amabilia.lopez@apsva.us,Randolph ES,Custodian,Facilities Staff
Blanca,Rodriguez Becerra,E31700,blanca.rodriguez@apsva.us,Randolph ES,Teacher,Instructional Staff
James,Miller,E31976,james.miller@apsva.us,Randolph ES,Teacher,Instructional Staff
Julian,Mejia,E32019,julian.mejia@apsva.us,Randolph ES,Teacher,Instructional Staff
Lupita,Rodriguez-Lopez,E32124,lupita.rodriguez@apsva.us,Randolph ES,Teacher,Instructional Staff
Maria,White,E32155,maria.white2@apsva.us,Randolph ES,Teacher,Instructional Staff
Patricia,Elcan,E32250,cate.elcan2@apsva.us,Randolph ES,Teacher,Instructional Staff
Kelly,Cotter,E32565,kelly.cotter@apsva.us,Randolph ES,Teacher,Instructional Staff
Marcus,Rogers,E32668,marcus.rogers@apsva.us,Randolph ES,Teacher,Instructional Staff
Jordan,Bedford,E32703,jordan.bedford@apsva.us,Randolph ES,Teacher,Instructional Staff
Michael,Fox,E32770,michael.fox2@apsva.us,Randolph ES,Teacher,Instructional Staff
Keima,Weinberg,E32801,keima.weinberg@apsva.us,Randolph ES,Teacher,Instructional Staff
Rabeya,Afroj,E32916,rabeya.afroj@apsva.us,Randolph ES,Instructional Assistant,Staff
Gina,Quirk,E32958,gina.quirk@apsva.us,Randolph ES,Teacher,Instructional Staff
Zoraida,Rocha,E33078,zoraida.rocha@apsva.us,Randolph ES,Staff,Staff
Mina,Baamou,E33289,mina.baamou@apsva.us,Randolph ES,Staff,Staff
Fatima,Bousaid,E33430,fatima.bousaid@apsva.us,Randolph ES,Staff,Staff
Elvira,Garcia,E3379,elvira.garcia@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Maria,Perez,E3953,maria.perez@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Amani,Hassan,E5987,amani.hassan@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Carlin,Schwartz,E7690,carlin.schwartz@apsva.us,Randolph ES,Teacher,Instructional Staff
Sinia,Solano,E8054,sinia.solano@apsva.us,Randolph ES,Administrative Assistant,Staff
Yanira,Guerrero,E8706,yanira.guerrero@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Karen,Novello,E8816,karen.novello@apsva.us,Randolph ES,Instructional Assistant,Instructional Staff
Guillermo,Moran,E9403,guillermo.moran@apsva.us,Randolph ES,Maintenance Supervisor,Facilities Staff
Annette,Taylor,E10138,annette.taylor@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Bettina,McPherson,E11089,bettina.mcpherson@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Marilyn,Scholl,E13345,marilyn.scholl@apsva.us,Shriver,Teacher,Instructional Staff
Isabel,Castellanos,E14454,isabel.salazar@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Shawn,Waddell,E15801,shawn.waddell@apsva.us,Shriver,Teacher,Instructional Staff
Abdelillah,Razah,E16501,abdelillah.razah@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Aginche,Mamo,E19272,aginche.mamo@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Marissa,Graham,E19779,marissa.graham@apsva.us,Shriver,Teacher,Instructional Staff
Rosario,Quinteros Iriarte,E20237,rosario.quinteros@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Karen,Miller,E20887,karen.miller2@apsva.us,Shriver,Administrative Assistant,Staff
Bayarra,Altankhuu,E20925,bayarra.altankhuu@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Narantuya,Badarch,E22369,narantuya.badarch@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Chelsea,Spear,E28911,chelsea.spear@apsva.us,Shriver,Teacher,Instructional Staff
Scott,Shepherd,E30605,scott.shepherd@apsva.us,Shriver,Teacher,Instructional Staff
Kristen,Moretti,E31073,kristen.moretti2@apsva.us,Shriver,Teacher,Instructional Staff
Rachel,Torres,E31239,rachel.torres2@apsva.us,Shriver,Teacher,Instructional Staff
Amanda,Morales,E31292,amanda.morales@apsva.us,Shriver,Teacher,Instructional Staff
Stephanie,Bautista,E31644,stephanie.bautista@apsva.us,Shriver,Administrative Assistant,Staff
George,Hewan,E32881,george.hewan@apsva.us,Shriver,Principal,Administrative Staff
Charcel,Coleman,E33017,charcel.coleman@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Kamille,Jackson,E33244,kamille.jackson@apsva.us,Shriver,Teacher,Instructional Staff
Sunee,Claud,E3436,sunee.claud@apsva.us,Shriver,Teacher,Instructional Staff
Lucia,Hernandez,E4600,lucia.hernandez@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Betsy,Gutierrez,E6047,betsy.gutierrez@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Mary,Newlin,E6257,mary.newlin@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Elizabeth,Key,E8851,elizabeth.key@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Linda,Denney,E8863,linda.denney@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Marcus,Barnes,E8941,marcus.barnes@apsva.us,Shriver,Instructional Assistant,Instructional Staff
Joyce,Kelly,E956,joyce.kelly@apsva.us,Shriver,Teacher,Instructional Staff
Laura,Partridge,E113,laura.partridge@apsva.us,Swanson MS,Teacher,Instructional Staff
Renee,Harber,E11435,renee.harber@apsva.us,Swanson MS,Principal,Unclassified
Paul,Norris,E12699,paul.norris@apsva.us,Swanson MS,Teacher,Instructional Staff
Carolyn,Sharpe,E13703,carolyn.sharpe@apsva.us,Swanson MS,Teacher,Instructional Staff
Susan,Pacifico,E1403,susan.pacifico@apsva.us,Swanson MS,Teacher,Instructional Staff
Patricia,Parks,E14071,patricia.parks@apsva.us,Swanson MS,Custodian,Facilities Staff
Robert,Pope,E1411,robert.pope@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Maritza,Carpenter,E14171,maritza.carpenter@apsva.us,Swanson MS,Teacher,Instructional Staff
Thomas,Hartman,E14213,thomas.hartman@apsva.us,Swanson MS,Teacher,Instructional Staff
Jacqueline,Torres,E14510,jacqueline.torres@apsva.us,Swanson MS,Administrative Assistant,Staff
Patricia,Pineda,E14811,patricia.pineda@apsva.us,Swanson MS,"Instructional Assistant, Staff",Instructional Staff
Emilmari,Faria,E15239,emilmari.faria@apsva.us,Swanson MS,Teacher,Instructional Staff
Marybeth,Donnelly,E16267,marybeth.donnelly@apsva.us,Swanson MS,Teacher,Instructional Staff
Claudia,Carvallo,E16642,claudia.carvallo@apsva.us,Swanson MS,Teacher,Instructional Staff
Patty,Alcantara,E16663,patty.alcantara@apsva.us,Swanson MS,Account Clerk,Staff
Karen,Kaldahl,E17084,karen.kaldahl@apsva.us,Swanson MS,Teacher,Instructional Staff
Catherine,McMahon,E19353,catherine.mcmahon@apsva.us,Swanson MS,Teacher,Instructional Staff
Julie,Hutsell,E19406,julie.hutsell@apsva.us,Swanson MS,Teacher,Instructional Staff
Elizabeth,DeMarino,E19573,elizabeth.demarino@apsva.us,Swanson MS,Teacher,Instructional Staff
Jennifer,Stacy,E1964,jennifer.stacy@apsva.us,Swanson MS,Teacher,Instructional Staff
Ingriz,Barraza,E19865,ingriz.barraza@apsva.us,Swanson MS,Administrative Assistant,Staff
Kevin,Treakle,E20232,kevin.treakle2@apsva.us,Swanson MS,Staff,Staff
Jean,Samuel,E20453,jean.samuel@apsva.us,Swanson MS,Teacher,Instructional Staff
Anne,Brown,E20533,anne.brown@apsva.us,Swanson MS,Teacher,Instructional Staff
Emily,Williams,E20566,emily.williams@apsva.us,Swanson MS,Teacher,Instructional Staff
Melanie,Stowell,E20601,melanie.stowell@apsva.us,Swanson MS,Teacher,Instructional Staff
Melissa,Lara-Ortiz,E20659,melissa.laraortiz@apsva.us,Swanson MS,Teacher,Instructional Staff
Ken,Campbell,E20672,ken.campbell@apsva.us,Swanson MS,Teacher,Instructional Staff
Sharonda,Williams,E20800,sharonda.williams@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Kathryn,Merlene,E20838,kathryn.merlene@apsva.us,Swanson MS,Teacher,Instructional Staff
Jonas,Brown,E20869,jonas.brown@apsva.us,Swanson MS,Teacher,Instructional Staff
Magda,Knap-Tefera,E21082,magda.tefera@apsva.us,Swanson MS,Teacher,Instructional Staff
Blandine,Liguidi,E21371,blandine.liguidi@apsva.us,Swanson MS,Assistant Principal,Administrative Staff
Teresa,Canales Lopez,E21545,teresa.lopez@apsva.us,Swanson MS,Custodian,Facilities Staff
Jesse,Homburg,E21610,jesse.homburg@apsva.us,Swanson MS,Teacher,Instructional Staff
Wasan,Al Qaisi,E21920,wasan.alqaisi@apsva.us,Swanson MS,n/a,Unclassified
Bridget,Fisher,E22144,bridget.fisher@apsva.us,Swanson MS,Teacher,Instructional Staff
Rachael,Von Elmendorf,E22177,rachael.vonelmendorf@apsva.us,Swanson MS,Teacher,Instructional Staff
Josephine,Walker,E22216,josephine.walker@apsva.us,Swanson MS,Teacher,Instructional Staff
Chris,Long,E22397,chris.long@apsva.us,Swanson MS,Teacher,Instructional Staff
Michael,Ray,E22786,michael.ray@apsva.us,Swanson MS,Custodian,Facilities Staff
Mary,Martin,E22827,mary.martin@apsva.us,Swanson MS,Teacher,Instructional Staff
Hayley,Corey,E22858,hayley.corey@apsva.us,Swanson MS,Teacher,Instructional Staff
Vilma,Celis,E22919,patricia.celis@apsva.us,Swanson MS,Teacher,Instructional Staff
Kelly,Flaherty,E23934,kelly.flaherty@apsva.us,Swanson MS,Teacher,Instructional Staff
Sylvia,Guerrieri,E23944,sylvia.guerrieri@apsva.us,Swanson MS,Teacher,Instructional Staff
Caitlin,Ince,E24043,caitlin.ince@apsva.us,Swanson MS,Teacher,Instructional Staff
Weiling,Song,E24188,weiling.song@apsva.us,Swanson MS,n/a,Unclassified
Latasha,Nelson,E24191,latasha.nelson@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Delvon,Jones,E24373,delvon.jones@apsva.us,Swanson MS,Maintenance Supervisor,Facilities Staff
Eric,Berman,E24384,eric.berman@apsva.us,Swanson MS,Teacher,Instructional Staff
Jim,DeMarino,E2472,jim.demarino@apsva.us,Swanson MS,Teacher,Instructional Staff
Nely,Andrade,E24787,nely.andrade@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Rachel,Affleck,E24807,rachel.affleck@apsva.us,Swanson MS,Teacher,Instructional Staff
Whitney,Field,E25247,whitney.field@apsva.us,Swanson MS,Teacher,Instructional Staff
John,Wickline,E25502,john.wickline@apsva.us,Swanson MS,Teacher,Instructional Staff
Cynthia,Chiu,E2551,cynthia.chiu@apsva.us,Swanson MS,Teacher,Instructional Staff
Thomas,Gill,E25745,thomas.gill@apsva.us,Swanson MS,Teacher,Instructional Staff
Leigh,Rauseo,E25789,leigh.rauseo@apsva.us,Swanson MS,Teacher,Instructional Staff
Christine,Reardon,E25851,christine.reardon@apsva.us,Swanson MS,Teacher,Instructional Staff
Margaret,Pierce,E25894,margaret.pierce@apsva.us,Swanson MS,Teacher,Instructional Staff
Rachael,Behrens,E25898,rachael.behrens@apsva.us,Swanson MS,Teacher,Instructional Staff
Jillian,Nemeth,E25917,jillian.nemeth@apsva.us,Swanson MS,Teacher,Instructional Staff
Laura,Goodwyn,E25955,laura.goodwyn@apsva.us,Swanson MS,Teacher,Instructional Staff
Karla,McGhee,E25966,karla.mcghee@apsva.us,Swanson MS,Administrative Assistant,Staff
Melissa,Dyer-Dewitt,E26088,melissa.dyerdewitt@apsva.us,Swanson MS,"Teacher, Staff",Staff
Patricia,Greco,E26093,patricia.greco@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Cassandra,Graham,E26096,cassandra.graham@apsva.us,Swanson MS,Teacher,Instructional Staff
Barbara,Nicholas,E26177,barbara.nicholas2@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Malia,Rivera,E26534,malia.rivera@apsva.us,Swanson MS,Teacher,Instructional Staff
Laura,Porter,E26668,laura.porter@apsva.us,Swanson MS,Assistant Principal,Administrative Staff
Kolleena,Perry,E26874,kolleena.perry@apsva.us,Swanson MS,Teacher,Instructional Staff
Peter,Farrell,E26964,peter.farrell@apsva.us,Swanson MS,Teacher,Instructional Staff
Brooke,Beeman,E27111,brooke.beeman@apsva.us,Swanson MS,Teacher,Instructional Staff
Dibakar,Chowdhury,E27181,dibakar.chowdhury2@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Monique,Caldwell,E27303,monique.caldwell@apsva.us,Swanson MS,Teacher,Instructional Staff
Virginia,Herrera,E27364,virginia.herrera@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Eric,Tarquinio,E2741,eric.tarquinio@apsva.us,Swanson MS,Teacher,Instructional Staff
Caroline,Monek,E27522,caroline.monek2@apsva.us,Swanson MS,Teacher,Instructional Staff
Raul,Parra,E27553,raul.parra@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Daniel,Swanson,E27800,daniel.swanson@apsva.us,Swanson MS,Teacher,Instructional Staff
Ethan,Davis,E27858,ethan.davis@apsva.us,Swanson MS,Teacher,Instructional Staff
Scott,Bane,E27868,scott.bane@apsva.us,Swanson MS,Teacher,Instructional Staff
Paul,Perrot,E27947,paul.perrot@apsva.us,Swanson MS,Teacher,Instructional Staff
Richard,Ptakowski,E28244,richard.ptakowski@apsva.us,Swanson MS,Staff,Staff
Mohammed,Abdallah,E28468,mohammed.abdallah@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Samantha,Patterson,E28989,samantha.patterson@apsva.us,Swanson MS,Teacher,Instructional Staff
Yung,Nguyen,E29005,yung.nguyen@apsva.us,Swanson MS,Teacher,Instructional Staff
Maria,Cabrera,E29227,maria.cabrera@apsva.us,Swanson MS,Staff,Staff
Hannah,Albino,E29277,hannah.albino@apsva.us,Swanson MS,Teacher,Instructional Staff
Nohra,Rodriguez,E29560,nohra.rodriguez@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Shevelle,Hill,E29644,shevelle.hill@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Leroy,Ross,E30074,leroy.ross@apsva.us,Swanson MS,Custodian,Facilities Staff
Onasis,Tanoe,E31055,onasis.tanoe@apsva.us,Swanson MS,Custodian,Facilities Staff
Megan,Mckeown,E31147,megan.mckeown@apsva.us,Swanson MS,Teacher,Instructional Staff
Kaitlin,Ray,E31198,kaitlin.ray@apsva.us,Swanson MS,Teacher,Instructional Staff
Odis,Moreno,E31411,odis.moreno@apsva.us,Swanson MS,Maintenance Supervisor,Facilities Staff
Maysoon,Salih,E31456,maysoon.salih@apsva.us,Swanson MS,n/a,Unclassified
Latisha,Ellis,E31707,latisha.ellis@apsva.us,Swanson MS,Assistant Principal,Administrative Staff
Leslie,Drew,E31891,leslie.drew@apsva.us,Swanson MS,Teacher,Instructional Staff
Courtney,Seavolt,E32007,courtney.seavolt@apsva.us,Swanson MS,Teacher,Instructional Staff
Latasha,McMahan,E32107,latasha.mcmahan@apsva.us,Swanson MS,Teacher,Instructional Staff
Jomaris,Correa Mercado,E32132,jomaris.correa@apsva.us,Swanson MS,Teacher,Instructional Staff
Urmila,Dahal Ghimire,E32420,urmila.dahalghimire@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Rhonda,Dickerson,E32422,rhonda.dickerson@apsva.us,Swanson MS,Staff,Staff
Lee,Kontibon,E32426,lee.kontibon@apsva.us,Swanson MS,Staff,Staff
Loretta,Richardson,E32427,loretta.richardson@apsva.us,Swanson MS,Staff,Staff
Katie,Williams,E32637,katie.williams@apsva.us,Swanson MS,Teacher,Instructional Staff
Janet,Martin,E32656,janet.martin@apsva.us,Swanson MS,Teacher,Instructional Staff
Charisma,Council,E32699,charisma.council@apsva.us,Swanson MS,Teacher,Instructional Staff
Jacob,Chelf,E32712,jacob.chelf@apsva.us,Swanson MS,Teacher,Instructional Staff
Sheri,Weathers,E32740,sheri.weathers@apsva.us,Swanson MS,Teacher,Instructional Staff
Taisha,Estrada-Aponte,E32944,taisha.estradaaponte@apsva.us,Swanson MS,Teacher,Instructional Staff
Rachida,El Gharbaoui,E33259,rachida.elgharbaoui@apsva.us,Swanson MS,Staff,Staff
Brooke,McElroy,E33348,brooke.mcelroy@apsva.us,Swanson MS,Staff,Staff
Monalisa,Moore,E33418,monalisa.moore2@apsva.us,Swanson MS,Staff,Staff
Paula,Hughes,E33476,paula.hughes@apsva.us,Swanson MS,Administrative Assistant,Staff
Cory,Seay,E33586,cory.seay@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Sharon,Bizar,E3921,sharon.bizar@apsva.us,Swanson MS,Teacher,Instructional Staff
Wendy,Hutto,E4098,wendy.hutto@apsva.us,Swanson MS,Teacher,Instructional Staff
Rana,Luthra,E4382,rana.luthra@apsva.us,Swanson MS,Director,Administrative Staff
Tuong,Truong,E4638,tuong.truong@apsva.us,Swanson MS,Custodian,Facilities Staff
Kathleen,Campbell,E5074,kathleen.campbell@apsva.us,Swanson MS,Teacher,Instructional Staff
Cecily,Corcoran,E5174,cecily.corcoran@apsva.us,Swanson MS,Teacher,Instructional Staff
Vanessa,Coleman-Denney,E7006,vanessa.coleman@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Octavia,Harris,E7605,octavia.harris@apsva.us,Swanson MS,Administrative Assistant,Staff
Michelle,Fettig,E7922,michelle.fettig@apsva.us,Swanson MS,Instructional Assistant,Instructional Staff
Margaret (Meg),Rose,E8368,meg.rose@apsva.us,Swanson MS,Teacher,Instructional Staff
Michelle,Wilkes,E868,michelle.wilkes@apsva.us,Swanson MS,Teacher,Instructional Staff
Christopher,Kniseley,E872,christopher.kniseley@apsva.us,Swanson MS,"Teacher, Staff",Staff
Charles,Clements,E8875,charles.clements@apsva.us,Swanson MS,Teacher,Instructional Staff
Kay,Seliskar,E9023,kay.seliskar@apsva.us,Swanson MS,Teacher,Instructional Staff
Trevor,Holland,E9241,trevor.holland@apsva.us,Swanson MS,Coordinator,Staff
Richard,Hamilton,E10385,richard.hamilton@apsva.us,Taylor ES,Teacher,Instructional Staff
Nathaniel,Powers,E10706,nathaniel.powers@apsva.us,Taylor ES,Teacher,Instructional Staff
Sharon,Gaston,E10931,sharon.gaston@apsva.us,Taylor ES,Teacher,Instructional Staff
Laurie,Goss,E12823,laurie.goss@apsva.us,Taylor ES,Teacher,Instructional Staff
Marguerite,Fleming,E15157,marguerite.fleming@apsva.us,Taylor ES,"Instructional Assistant, Staff",Instructional Staff
Lemlem,Misgina,E16629,lemlem.misgina@apsva.us,Taylor ES,Staff,Staff
Niema,Yosuf,E16738,niema.yosuf@apsva.us,Taylor ES,Instructional Assistant,Staff
James,Parker,E16974,james.parker@apsva.us,Taylor ES,Administrative Assistant,Staff
Cris,Richardson,E19263,cris.richardson@apsva.us,Taylor ES,Teacher,Instructional Staff
Celia,Arnade,E193,celia.arnade@apsva.us,Taylor ES,Instructional Assistant,Instructional Staff
Michael,Evans,E19325,michael.evans@apsva.us,Taylor ES,Teacher,Instructional Staff
Lauren,Lachewitz,E19412,lauren.lachewitz@apsva.us,Taylor ES,Teacher,Instructional Staff
Sofanit,Yemiru,E19500,sofanit.yemiru@apsva.us,Taylor ES,Custodian,Facilities Staff
Katherine,Ross,E19653,katherine.ross@apsva.us,Taylor ES,Teacher,Instructional Staff
Sophie,Daniels,E19836,sophie.daniels@apsva.us,Taylor ES,Teacher,Instructional Staff
Shannon,Toole,E20711,shannon.toole@apsva.us,Taylor ES,Instructional Assistant,Instructional Staff
Rachel,Lampert,E21187,rachel.lampert@apsva.us,Taylor ES,Teacher,Instructional Staff
Molly,White,E21524,molly.white@apsva.us,Taylor ES,Teacher,Instructional Staff
Anita,O'Brien,E21956,anita.obrien@apsva.us,Taylor ES,Instructional Assistant,Instructional Staff
Yvonne,Mcneese,E22534,yvonne.mcneese@apsva.us,Taylor ES,Teacher,Instructional Staff
Ashley,Burt,E22754,ashley.burt@apsva.us,Taylor ES,Teacher,Instructional Staff
Brian,Bersh,E22824,brian.bersh@apsva.us,Taylor ES,n/a,Unclassified
Allison,Owens,E23975,allison.owens@apsva.us,Taylor ES,Teacher,Instructional Staff
Tamera,Taylor,E24143,tamera.taylor@apsva.us,Taylor ES,Teacher,Instructional Staff
Jennifer,Rachlin,E24332,jennifer.rachlin@apsva.us,Taylor ES,Teacher,Instructional Staff
Gary,Primrose,E24335,gary.primrose@apsva.us,Taylor ES,Custodian,Facilities Staff
Porscha,Adams,E24352,porscha.adams@apsva.us,Taylor ES,Instructional Assistant,Instructional Staff
Bianca,Robinson,E24375,bianca.robinson@apsva.us,Taylor ES,Teacher,Instructional Staff
Eileen,Spack,E24723,eileen.spack@apsva.us,Taylor ES,Instructional Assistant,Instructional Staff
Sima,Hadley,E24991,sima.hadley@apsva.us,Taylor ES,Teacher,Instructional Staff
Caitlin,Sherman,E25028,caitlin.sherman@apsva.us,Taylor ES,Assistant Principal,Administrative Staff
Nichole,Barrett,E25295,nichole.barrett@apsva.us,Taylor ES,Teacher,Instructional Staff
Lucy,Lauck,E25472,lucy.lauck@apsva.us,Taylor ES,Teacher,Instructional Staff
Meghan,Barlow,E25509,meghan.barlow@apsva.us,Taylor ES,Teacher,Instructional Staff
Amna,Ali,E25538,amna.ali@apsva.us,Taylor ES,Instructional Assistant,Instructional Staff
Julia,Walker,E25540,julia.walker@apsva.us,Taylor ES,Teacher,Instructional Staff
Ursula,Reilly,E25654,ursula.reilly@apsva.us,Taylor ES,Teacher,Instructional Staff
Alexandra,Ring,E25899,alexandra.ring@apsva.us,Taylor ES,Teacher,Instructional Staff
Breauna,Koger,E26195,breauna.koger@apsva.us,Taylor ES,Instructional Assistant,Staff
Ruby,Jamison Ledbetter,E26297,ruby.ledbetter@apsva.us,Taylor ES,Staff,Staff
Iliana,Gonzales,E26629,iliana.gonzales@apsva.us,Taylor ES,Principal,Unclassified
Audrey,LeVault,E26747,audrey.levault@apsva.us,Taylor ES,Teacher,Instructional Staff
Ashley,Hendrix,E26852,ashley.hendrix@apsva.us,Taylor ES,Teacher,Instructional Staff
Meredith,Melnick,E27398,meredith.melnick2@apsva.us,Taylor ES,Administrative Assistant,Staff
Darwain,Frost,E27519,darwain.frost@apsva.us,Taylor ES,"Staff, Instructional Assistant",Staff
Guy,Carcillo,E27763,guy.carcillo2@apsva.us,Taylor ES,Teacher,Instructional Staff
Dryw,Freed,E28169,dryw.freed@apsva.us,Taylor ES,Teacher,Instructional Staff
Brandi,Brittain,E28427,brandi.brittain2@apsva.us,Taylor ES,Teacher,Instructional Staff
Paola,Lopez,E28533,paola.lopez@apsva.us,Taylor ES,Staff,Staff
Kevin,Waller,E29524,kevin.waller@apsva.us,Taylor ES,Staff,Staff
Michael,Smith,E29746,michael.smith@apsva.us,Taylor ES,Teacher,Instructional Staff
Melissa,Molina,E30864,melissa.molina@apsva.us,Taylor ES,Administrative Assistant,Staff
Jazmine,Dominguez,E30913,jazmine.dominguez@apsva.us,Taylor ES,Instructional Assistant,Staff
Victoria,Cuenca Cespedes,E30924,victoria.cuenca@apsva.us,Taylor ES,Custodian,Facilities Staff
Samantha,Cordasco,E31017,samantha.cordasco@apsva.us,Taylor ES,Teacher,Instructional Staff
Salma,Cabrera,E31203,salma.cabrera@apsva.us,Taylor ES,"Instructional Assistant, Staff",Instructional Staff
Kelsey,Gamza,E31214,kelsey.gamza@apsva.us,Taylor ES,Teacher,Instructional Staff
Susan,Birnie,E31942,susan.birnie@apsva.us,Taylor ES,Teacher,Instructional Staff
Diana,Garcia Sanchez,E32062,diana.garciasanchez@apsva.us,Taylor ES,Instructional Assistant,Instructional Staff
Jennifer,Albro,E32700,jennifer.albro@apsva.us,Taylor ES,Teacher,Instructional Staff
Katherine,Edwards,E32811,katherine.edwards@apsva.us,Taylor ES,Teacher,Instructional Staff
Melissa,Marcelino Torres,E32840,melissa.marcelino@apsva.us,Taylor ES,Teacher,Instructional Staff
Kimiko,Yerick,E32887,kimiko.yerick@apsva.us,Taylor ES,Teacher,Instructional Staff
Charlbret,Polk Newton,E32992,charlbret.polknewton@apsva.us,Taylor ES,Instructional Assistant,Staff
Janis,Johnston,E33008,janis.johnston@apsva.us,Taylor ES,Administrative Assistant,Staff
Nickeda,Gravitt,E33019,nickeda.gravitt@apsva.us,Taylor ES,Instructional Assistant,Staff
Brittany,Brown,E33102,brittany.brown@apsva.us,Taylor ES,Staff,Staff
Yeshimebet,Ayela,E33126,yeshimebet.ayela@apsva.us,Taylor ES,Staff,Staff
Maria,Teran-Cabezas,E33133,maria.terancabezas@apsva.us,Taylor ES,Staff,Staff
Crystal,Haskins,E33168,crystal.haskins@apsva.us,Taylor ES,Staff,Staff
Linda,Morehead,E33180,linda.morehead@apsva.us,Taylor ES,Staff,Staff
Christyna,Haskins,E33189,christyna.haskins@apsva.us,Taylor ES,Staff,Staff
Corinna,Touwsma,E33209,corinna.touwsma@apsva.us,Taylor ES,Teacher,Instructional Staff
Mikayla,Smith,E33291,mikayla.smith@apsva.us,Taylor ES,Teacher,Instructional Staff
Meseret,W Michael,E33505,meseret.wmichael@apsva.us,Taylor ES,Staff,Staff
Christina,Castromiller,E33557,christina.castro@apsva.us,Taylor ES,Teacher,Instructional Staff
Amy,Sowa,E4375,amy.sowa@apsva.us,Taylor ES,Teacher,Instructional Staff
Akwasi Kusi,Agyemang-Kusi,E4606,akwasi.kusi@apsva.us,Taylor ES,Maintenance Supervisor,Facilities Staff
Jose,Flores,E4626,jose.flores@apsva.us,Taylor ES,Custodian,Facilities Staff
Gwen,Bellin,E4768,gwen.bellin@apsva.us,Taylor ES,Teacher,Instructional Staff
Carolyn,Washington,E4799,carolyn.washington@apsva.us,Taylor ES,Staff,Staff
Melinda,Robins,E5003,melinda.robins@apsva.us,Taylor ES,Teacher,Instructional Staff
Ann,Heidig,E5500,ann.heidig@apsva.us,Taylor ES,Teacher,Instructional Staff
Ebony,Smith,E5540,ebony.smith@apsva.us,Taylor ES,Instructional Assistant,Instructional Staff
David,McDavitt,E6455,david.mcdavitt@apsva.us,Taylor ES,Teacher,Instructional Staff
Elizabeth,Varela,E7042,elizabeth.varela@apsva.us,Taylor ES,Teacher,Instructional Staff
Annemarie,Deuel,E8329,annemarie.deuel@apsva.us,Taylor ES,Teacher,Instructional Staff
Sigrid,Vollmecke,E10075,sigrid.vollmecke@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Catrina,Moran,E10825,catrina.moran@apsva.us,Tuckahoe ES,Administrative Assistant,Staff
Robin,Andersen,E11447,robin.andersen@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Jill,Poth,E11713,jill.poth@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Jody,Birotte,E12478,jody.birotte@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Floyd,Corkins,E13661,floyd.corkins@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Margaret,Egan,E1437,margaret.egan@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Irma,Terrazas,E14714,irma.terrazas@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Melveena,Koger,E14772,melveena.koger@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Bolormaa,Jugdersuren,E16049,bolormaa.jugdersuren@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Mitch,Pascal,E1613,mitch.pascal@apsva.us,Tuckahoe ES,Principal,Unclassified
Kristine,Howard,E16158,kristine.howard@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Marianne,Sible,E19002,marianne.sible@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Kristen,Zarkowsky,E19658,kristen.zarkowsky@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Stephanie,McIntyre,E20516,stephanie.mcintyre@apsva.us,Tuckahoe ES,Assistant Principal,Administrative Staff
Nana,Akomeah,E20736,nana.akomeah@apsva.us,Tuckahoe ES,Custodian,Facilities Staff
Kate,Perkins,E21508,kate.perkins@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Latoya,Hill,E21534,latoya.hill@apsva.us,Tuckahoe ES,Administrative Assistant,Staff
Mirna,Lopez,E21612,mirna.lopez@apsva.us,Tuckahoe ES,"Administrative Assistant, Staff",Staff
Charles,Keller,E22436,charles.keller@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Aja,Campbell,E22966,aja.campbell@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Erin,Puhl,E23032,erin.puhl@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Alexandra,Dolinsky,E23128,alexandra.dolinsky@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Nghia,Vuong,E23537,nghia.vuong@apsva.us,Tuckahoe ES,Custodian,Facilities Staff
Anna,Hartwick,E23577,anna.hartwick@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Michelle,Strickland,E23960,michelle.strickland@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
James,Brent,E23994,james.brent@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Timothy,Grasso,E24096,timothy.grasso@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Bridget,Bishop,E24556,bridget.bishop@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Sarah,Moody,E25007,sarah.moody@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Nicole,Reed,E25224,nicole.reed@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Ramsay,Cogen,E25486,ramsay.cogen@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Dina,Valente,E25817,dina.valente@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Ani,Arzoomanian,E25937,ani.arzoomanian@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Andrea,Kaplowitz,E26619,andrea.kaplowitz@apsva.us,Tuckahoe ES,Staff,Staff
Sarah,Weisz,E26735,sarah.weisz@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Corinne,Reilly,E26857,corinne.reilly@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Jenna,Weinberg,E27801,jenna.weinberg@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Courtney,Chapman,E27845,courtney.chapman@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Nicholas,Natalie,E28825,nicholas.natalie@apsva.us,Tuckahoe ES,n/a,Unclassified
Lisa,Vassou,E29014,lisa.vassou@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Erin,Youngman,E29155,erin.youngman@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Bianka,Snorgrass,E29817,bianka.snorgrass2@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Lalla,Idrissi,E29909,lalla.idrissi@apsva.us,Tuckahoe ES,Instructional Assistant,Staff
Sarah,Parrish,E29929,sarah.parrish@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Kristin,Brynsvold,E30038,kristin.brynsvold@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Debra,Blackstone,E30211,debra.blackstone@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Sonia,Wald,E30325,sonia.wald2@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Dina,Nimatallah-Martyn,E30438,dina.nimatallah@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Katherine,Matthews,E30824,katherine.matthews2@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Melissa,Molina,E30864,melissa.molina@apsva.us,Tuckahoe ES,Principals Assistant,Staff
Adam,Cook,E31089,adam.cook@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Tiffaney,Knight,E31186,tiffaney.knight@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Eric,Banga,E31497,eric.banga@apsva.us,Tuckahoe ES,Maintenance Supervisor,Facilities Staff
Karen,Kaldenbach-Montemayor,E31850,karen.kaldenbach@apsva.us,Tuckahoe ES,Instructional Assistant,Staff
Detallion,McSwain,E31864,detallion.mcswain@apsva.us,Tuckahoe ES,Instructional Assistant,Staff
Thomas,Overgaag,E32034,thomas.overgaag@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Lorraine,Ryan,E32264,lorraine.ryan@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Nylah,Cooperwood,E32342,nylah.cooperwood@apsva.us,Tuckahoe ES,Staff,Staff
Jennifer,Chaitin,E32815,jennifer.chaitin@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Monalisa,Moore,E32819,monalisa.moore@apsva.us,Tuckahoe ES,Instructional Assistant,Staff
Amanda,Sandridge,E33138,amanda.sandridge@apsva.us,Tuckahoe ES,Staff,Staff
Michael,Magbanua,E33548,michael.magbanua@apsva.us,Tuckahoe ES,Staff,Staff
Theresa,Coffman,E3626,theresa.coffman@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Sonji,McNear,E5094,sonji.mcnear@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Miriam,Miglino,E5255,miriam.bernal@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Bruce,Ferratt,E6134,bruce.ferratt@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
Sonya,Robinson,E6338,sonya.robinson@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Julie,Lindeman,E6340,julie.lindeman@apsva.us,Tuckahoe ES,Teacher,Instructional Staff
June,Gardner,E7025,june.gardner@apsva.us,Tuckahoe ES,Instructional Assistant,Instructional Staff
Lisa,Labella,E12080,lisa.labella@apsva.us,Virtual High School,n/a,Unclassified
Philip,Hayden,E1324,philip.hayden@apsva.us,Virtual Middle School,n/a,Unclassified
Bridget,O'Malley,E13759,bridget.omalley@apsva.us,Virtual High School,n/a,Unclassified
Kevin,Bridwell,E16231,kevin.bridwell@apsva.us,Virtual High School,n/a,Unclassified
Greg,Cabana,E16726,greg.cabana@apsva.us,Virtual High School,n/a,Unclassified
Hunter,Benante,E20527,hunter.benante@apsva.us,Virtual High School,n/a,Unclassified
Katherine,Garcia-Larner,E21566,katherine.garcia@apsva.us,Virtual High School,n/a,Unclassified
Jake,Tan,E22774,jake.tan@apsva.us,Virtual Middle School,n/a,Unclassified
Jacqueline,Livelli,E22961,jacqueline.livelli@apsva.us,Virtual High School,n/a,Unclassified
Hyangri,Ma,E22991,hyangri.ma@apsva.us,Virtual High School,n/a,Unclassified
Susan,Tucker,E23520,susan.tucker@apsva.us,Virtual Middle School,n/a,Unclassified
Yanira,Umana Ayala,E24000,yanira.umana@apsva.us,"Virtual High School, Virtual Middle School",n/a,Unclassified
Julie,Rios,E25125,julie.rios@apsva.us,Virtual Elementary School,n/a,Unclassified
Jedediah,Bobier,E25341,jedediah.bobier@apsva.us,Virtual High School,n/a,Unclassified
Julie,Suarez,E25688,julie.suarez@apsva.us,Virtual High School,n/a,Unclassified
Haley,Wetsel,E25837,haley.wetsel@apsva.us,Virtual Middle School,n/a,Unclassified
Jelani,Walls,E25927,jelani.walls@apsva.us,Virtual High School,n/a,Unclassified
Jessica,Cash,E26732,jessica.cash@apsva.us,Virtual Elementary School,n/a,Unclassified
Kimberly,Pearson,E27017,kimberly.pearson@apsva.us,Virtual Middle School,n/a,Unclassified
Chandra,Perkins,E27749,chandra.perkins@apsva.us,Virtual Elementary School,n/a,Unclassified
Marcella,Park,E28007,marcella.park@apsva.us,Virtual Middle School,n/a,Unclassified
Jill,Miller,E28978,jill.miller@apsva.us,Virtual Middle School,n/a,Unclassified
Renee,Ott,E29081,renee.ott@apsva.us,Virtual Elementary School,n/a,Unclassified
Joshua,Brew,E29140,joshua.brew@apsva.us,Virtual Middle School,n/a,Unclassified
Michelle,Young Hasberry,E29239,michelle.hasberry@apsva.us,"Virtual Middle School, Virtual High School",n/a,Unclassified
Comfort,Mwangi,E29247,comfort.mwangi@apsva.us,Virtual Middle School,n/a,Unclassified
L,Ballester Concepcion,E29931,l.ballester@apsva.us,Virtual Elementary School,n/a,Unclassified
Laura,Cahn,E31095,laura.cahn@apsva.us,Virtual Middle School,n/a,Unclassified
Erica,O'Connor,E31140,erica.oconnor@apsva.us,Virtual Elementary School,n/a,Unclassified
Andrea,Krisko,E31183,andrea.krisko@apsva.us,Virtual High School,n/a,Unclassified
Laura,Moore,E31189,laura.moore2@apsva.us,Virtual High School,n/a,Unclassified
Sylvie,Degraff,E31388,sylvie.degraff@apsva.us,Virtual High School,n/a,Unclassified
Tanya,Bell,E31435,tanya.bell@apsva.us,Virtual High School,n/a,Unclassified
Rochelle,Proctor,E31550,rochelle.proctor@apsva.us,Virtual Elementary School,n/a,Unclassified
Kathleen,Hughes,E31564,kathleen.hughes@apsva.us,Virtual Middle School,n/a,Unclassified
Matthew,Goodman,E32093,matthew.goodman@apsva.us,Virtual Elementary School,n/a,Unclassified
Regina,Robinson,E32112,regina.robinson@apsva.us,"Virtual Middle School, Virtual High School",n/a,Unclassified
Maura,Coughlin,E32135,maura.coughlin@apsva.us,Virtual High School,n/a,Unclassified
Jessica,Johnson,E32153,jessica.johnson@apsva.us,Virtual Middle School,n/a,Unclassified
Terra,Jones,E32191,terra.jones@apsva.us,Virtual Elementary School,n/a,Unclassified
Epiphany,McCant,E32211,epiphany.mccant@apsva.us,Virtual Middle School,n/a,Unclassified
Peter,Laseau,E32267,peter.laseau@apsva.us,Virtual High School,n/a,Unclassified
Alka,Johar,E32542,alka.johar@apsva.us,Virtual High School,n/a,Unclassified
Jacklyn,Rosen,E32561,jacklyn.rosen@apsva.us,Virtual Middle School,n/a,Unclassified
Cherita,Harrod,E32742,cherita.harrod@apsva.us,Virtual Middle School,n/a,Unclassified
Amy,Jackson,E32756,amy.jackson@apsva.us,"Virtual Middle School, Virtual High School",n/a,Unclassified
Briona,Robinson,E32787,briona.robinson@apsva.us,Virtual Elementary School,n/a,Unclassified
Tia,McCoy,E32798,tia.mccoy@apsva.us,Virtual Elementary School,n/a,Unclassified
Tyi Sanna,Jones,E32808,tyisanna.jones@apsva.us,Virtual Middle School,n/a,Unclassified
Katherine,Gomez,E32812,katherine.gomez@apsva.us,Virtual Middle School,n/a,Unclassified
Donielle,Hall,E32813,donielle.hall@apsva.us,Virtual Elementary School,n/a,Unclassified
Jennifer,Chaitin,E32815,jennifer.chaitin@apsva.us,Virtual Elementary School,n/a,Unclassified
Kristin,Grant,E32826,kristin.grant@apsva.us,Virtual High School,n/a,Unclassified
Bronwyn,Smith,E32831,bronwyn.smith@apsva.us,Virtual Elementary School,n/a,Unclassified
Yolanda,Jackson,E32843,yolanda.jackson@apsva.us,Virtual Elementary School,n/a,Unclassified
David,Wilson,E32874,david.wilson2@apsva.us,Virtual High School,n/a,Unclassified
Katherine,Palmer,E32878,katherine.palmer@apsva.us,Virtual Elementary School,n/a,Unclassified
Caro,Stikeleather,E32900,caro.stikeleather@apsva.us,Virtual Elementary School,n/a,Unclassified
Javonna,Friend,E32904,javonna.friend@apsva.us,Virtual High School,n/a,Unclassified
Melissa,McCarthy,E32922,melissa.mccarthy@apsva.us,"Virtual Elementary School, Virtual Middle School, Virtual High School",n/a,Unclassified
Carrie,Verba,E32999,carrie.verba@apsva.us,Virtual Middle School,n/a,Unclassified
Fayanna,Hodge-Domingo,E33004,fayanna.hodgedomingo@apsva.us,Virtual Elementary School,n/a,Unclassified
Jacquia,Plummer,E33025,jacquia.plummer@apsva.us,Virtual Elementary School,n/a,Unclassified
Danielle,Harrell,E33055,danielle.harrell@apsva.us,"Virtual Learning Program - High, Virtual Learning Program - Elementary, Virtual Learning Program - Middle","Principal, Principal, Principal",Unclassified
Kamille,Jackson,E33244,kamille.jackson@apsva.us,Virtual Elementary School,n/a,Unclassified
Alisa,Joseph,E33261,alisa.joseph@apsva.us,Virtual Middle School,n/a,Unclassified
Debra,Cave,E3510,debra.cave@apsva.us,Virtual High School,n/a,Unclassified
Maureen,McMorrow,E4224,maureen.mcmorrow@apsva.us,Virtual Middle School,n/a,Unclassified
Matthew,Rinker,E4334,matthew.rinker@apsva.us,"Virtual Middle School, Virtual High School",n/a,Unclassified
Stacy,Brasfield,E8991,stacy.brasfield@apsva.us,Virtual High School,n/a,Unclassified
Cara,Saavedra,E9282,cara.saavedra@apsva.us,Virtual High School,n/a,Unclassified
Denis,Babichenko,E10023,denis.babichenko@apsva.us,Wakefield HS,Teacher,Instructional Staff
Holly,Jewell,E1024,holly.jewell@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michael,Lutz,E10277,michael.lutz@apsva.us,Wakefield HS,Teacher,Instructional Staff
Willow,Scott,E10372,willow.scott@apsva.us,Wakefield HS,"Administrative Assistant, Staff",Staff
Marcia,Richardson,E10435,marcia.richardson@apsva.us,Wakefield HS,Teacher,Instructional Staff
Renita,Mathis,E10481,renita.mathis@apsva.us,Wakefield HS,Teacher,Instructional Staff
Chris,Gillespie,E10769,chris.gillespie@apsva.us,Wakefield HS,Teacher,Instructional Staff
Amy,Markowitz,E11214,amy.markowitz@apsva.us,Wakefield HS,Teacher,Instructional Staff
Chris,Willmore,E11294,chris.willmore@apsva.us,Wakefield HS,Principal,Unclassified
Maggie,Hsu,E11388,maggie.hsu@apsva.us,Wakefield HS,Assistant Principal,Administrative Staff
Mikhail,Balachov,E11470,mikhail.balachov@apsva.us,Wakefield HS,Teacher,Instructional Staff
Kelly,Breedlove,E11579,kelly.breedlove@apsva.us,Wakefield HS,n/a,Unclassified
Lisa,Labella,E12080,lisa.labella@apsva.us,Wakefield HS,Teacher,Instructional Staff
Tamar,Raphaeli Rava,E12094,tamar.rava@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michelle,Harris,E12487,michelle.harris@apsva.us,Wakefield HS,Teacher,Instructional Staff
Jeaneth,Andrade,E1258,jeaneth.andrade@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Jay,Calfee,E12644,jay.calfee@apsva.us,Wakefield HS,Teacher,Instructional Staff
Jon,Jerdee,E12843,jon.jerdee@apsva.us,Wakefield HS,Teacher,Instructional Staff
Patrick,Kelly,E12881,patrick.kelly@apsva.us,Wakefield HS,Teacher,Instructional Staff
Joseph,Spencer,E13534,joseph.spencer@apsva.us,Wakefield HS,Teacher,Instructional Staff
Maureen,Naughton,E1400,maureen.naughton@apsva.us,Wakefield HS,Teacher,Instructional Staff
Analia,Almada,E14134,analia.almada@apsva.us,Wakefield HS,Teacher,Instructional Staff
Kimberly,Warschaw,E14782,kimberly.warschaw@apsva.us,Wakefield HS,Teacher,Instructional Staff
Boramy,Nginn,E15367,boramy.nginn@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Gavin,Swazya,E15387,gavin.swazya@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Mulu,Fantahun,E15619,mulu.fantahun@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Ana,Caceres,E15739,ana.caceres@apsva.us,Wakefield HS,Custodian,Facilities Staff
Nadim,Nader,E15750,nadim.nader@apsva.us,Wakefield HS,Teacher,Instructional Staff
Casey,Renner,E15798,casey.renner@apsva.us,Wakefield HS,Teacher,Instructional Staff
Wanda,McPhee,E15808,wanda.mcphee@apsva.us,Wakefield HS,Teacher,Instructional Staff
Mike,Megargee,E15852,mike.megargee@apsva.us,Wakefield HS,Teacher,Instructional Staff
Catrice,Vandross,E15932,catrice.vandross@apsva.us,Wakefield HS,Teacher,Instructional Staff
Alvin,Truesdale,E15949,alvin.truesdale@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michael,Barnett,E16069,michael.barnett@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Eddy,Guerrero,E16098,eddy.guerrero@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Kathleen,Machan,E16230,kathleen.machan@apsva.us,Wakefield HS,Teacher,Instructional Staff
Juan,Medina,E16348,juan.medina@apsva.us,Wakefield HS,Custodian,Facilities Staff
Veronica,Covarrubias,E16432,veronica.covarrubias@apsva.us,Wakefield HS,Teacher,Instructional Staff
Mike,Lenowitz,E16572,mike.lenowitz@apsva.us,Wakefield HS,Teacher,Instructional Staff
Greg,Cabana,E16726,greg.cabana@apsva.us,Wakefield HS,Teacher,Instructional Staff
Martha,Heredia,E16773,martha.heredia@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Wendy,Maitland,E16831,wendy.maitland@apsva.us,Wakefield HS,Teacher,Instructional Staff
Rosa,Montes,E16870,rosa.montes@apsva.us,Wakefield HS,Custodian,Facilities Staff
Kenneth,James,E16950,kenneth.james@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Angela,Jones,E1726,angela.jones@apsva.us,Wakefield HS,Teacher,Instructional Staff
Deborah,Pettit,E19311,deborah.pettit@apsva.us,Wakefield HS,Teacher,Instructional Staff
Sarah,Jones,E19337,sarah.jones@apsva.us,Wakefield HS,Teacher,Instructional Staff
John,Garner,E19428,john.garner@apsva.us,Wakefield HS,Staff,Staff
Devin,Shirley,E19509,devin.shirley@apsva.us,Wakefield HS,Teacher,Instructional Staff
Joann,Weiss,E19569,joann.weiss@apsva.us,Wakefield HS,Administrative Assistant,Staff
Daniel,Alpert,E19637,daniel.alpert@apsva.us,Wakefield HS,Teacher,Instructional Staff
Patricia,Hunt,E19697,patricia.hunt@apsva.us,Wakefield HS,Teacher,Instructional Staff
Giselle,Benitah,E19937,giselle.benitah@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Debbie,Strauss,E20017,debbie.strauss@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Ione,Saunders,E20110,ione.saunders@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Heather,Comeau,E20140,heather.comeau@apsva.us,Wakefield HS,Administrative Assistant,Staff
Shaheed,Patterson,E20187,shaheed.patterson@apsva.us,Wakefield HS,Teacher,Instructional Staff
Eduardo,Carrasquillo,E20224,eduardo.carrasquillo@apsva.us,Wakefield HS,Staff,Staff
Irma,Medrano,E20234,irma.medrano@apsva.us,Wakefield HS,Teacher,Instructional Staff
Gina,Glassman,E20386,gina.glassman@apsva.us,Wakefield HS,Teacher,Instructional Staff
Henry,Jessup,E20441,henry.jessup@apsva.us,Wakefield HS,Teacher,Instructional Staff
Marcus,Robinson,E20524,marcus.robinson@apsva.us,Wakefield HS,Staff,Staff
Jennifer,Shaffer,E20654,jennifer.shaffer@apsva.us,Wakefield HS,Teacher,Instructional Staff
Margot,Shteir-Dunn,E20921,margot.dunn@apsva.us,Wakefield HS,Teacher,Instructional Staff
Alicia,McPhee,E20954,alicia.mcphee@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michael,Palermo,E2096,michael.palermo@apsva.us,Wakefield HS,Teacher,Instructional Staff
Allison,Scavone,E21001,allison.scavone@apsva.us,Wakefield HS,Teacher,Instructional Staff
Nancy,Jimenez-Nunez,E21012,nancy.jimeneznunez@apsva.us,Wakefield HS,Staff,Staff
Eric,Rivera,E21062,eric.rivera@apsva.us,Wakefield HS,"Instructional Assistant, Staff",Instructional Staff
Celeste,El Hashem,E21264,celeste.elhashem@apsva.us,Wakefield HS,Teacher,Instructional Staff
Chris,Mauthe,E21325,chris.mauthe@apsva.us,Wakefield HS,Teacher,Instructional Staff
Danielle,McKenzie,E21344,danielle.mckenzie@apsva.us,Wakefield HS,Teacher,Instructional Staff
Paul,Greenfield,E21402,paul.greenfield@apsva.us,Wakefield HS,Staff,Staff
Mary,Madden,E2142,mary.madden@apsva.us,Wakefield HS,Teacher,Instructional Staff
Kathryn,Wheelock,E21525,kathryn.wheelock@apsva.us,Wakefield HS,Teacher,Instructional Staff
Maureen,Larussa,E21569,maureen.larussa@apsva.us,Wakefield HS,Teacher,Instructional Staff
Caitlin,Davies,E21570,caitlin.davies@apsva.us,Wakefield HS,Teacher,Instructional Staff
Delshaune,Jackson,E21760,delshaune.jackson@apsva.us,Wakefield HS,Administrative Assistant,Staff
Amy,Wathen,E21901,amy.wathen@apsva.us,Wakefield HS,Teacher,Instructional Staff
Wasan,Al Qaisi,E21920,wasan.alqaisi@apsva.us,Wakefield HS,n/a,Unclassified
Cristofer,Madera,E21923,cristofer.madera@apsva.us,Wakefield HS,Teacher,Instructional Staff
Arien,Heggs,E21952,arien.heggs@apsva.us,Wakefield HS,Staff,Staff
Elizabeth,Silva,E21993,elizabeth.silva@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Mark,Coleson,E22015,mark.coleson@apsva.us,Wakefield HS,Staff,Staff
Mercedes,Huffman,E22089,mercedes.huffman@apsva.us,Wakefield HS,Teacher,Instructional Staff
Aymee,Buzzi,E22174,aymee.buzzi@apsva.us,Wakefield HS,Teacher,Instructional Staff
Larenda,Bigsby,E22422,larenda.bigsby@apsva.us,Wakefield HS,Administrative Assistant,Staff
David,Sharp,E22457,david.sharp@apsva.us,Wakefield HS,Teacher,Instructional Staff
Patrick,Johnsen,E22524,patrick.johnsen@apsva.us,Wakefield HS,Assistant Principal,Administrative Staff
Jael,Himko,E22647,jael.himko@apsva.us,Wakefield HS,Administrative Assistant,Staff
Tamara,Willis,E22984,tamara.willis@apsva.us,Wakefield HS,Staff,Staff
Hyangri,Ma,E22991,hyangri.ma@apsva.us,Wakefield HS,Teacher,Instructional Staff
Susan,Haley,E23099,susan.haley@apsva.us,Wakefield HS,Teacher,Instructional Staff
Vernita,Marshall,E23115,vernita.marshall@apsva.us,Wakefield HS,Teacher,Instructional Staff
Jimmy,Vital,E23294,jimmy.vital@apsva.us,Wakefield HS,"Instructional Assistant, Staff",Instructional Staff
Teray,Bingham,E23426,teray.bingham@apsva.us,Wakefield HS,Teacher,Instructional Staff
David,Smith,E23556,david.smith@apsva.us,Wakefield HS,Teacher,Instructional Staff
Jasneen,Sahni,E23703,jasneen.sahni@apsva.us,Wakefield HS,Assistant Principal,Administrative Staff
David,Criollo,E23741,otniel.criollo@apsva.us,Wakefield HS,Administrative Assistant,Staff
Rabia,Faqih,E23743,rabia.faqih@apsva.us,Wakefield HS,Teacher,Instructional Staff
Heather,Downey,E23749,heather.downey@apsva.us,Wakefield HS,Teacher,Instructional Staff
Muktaru,Jalloh,E23760,muktaru.jalloh@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michael,Schaefer,E23925,michael.schaefer@apsva.us,Wakefield HS,Teacher,Instructional Staff
Paula,Watts,E2395,paula.watts@apsva.us,Wakefield HS,Teacher,Instructional Staff
Nick,Chauvenet,E23981,nick.chauvenet@apsva.us,Wakefield HS,Teacher,Instructional Staff
Janeth,Panta,E24065,janeth.panta@apsva.us,Wakefield HS,Administrative Assistant,Staff
Allison,Menchel,E24087,allison.menchel@apsva.us,Wakefield HS,Teacher,Instructional Staff
Reema,Chawla,E24232,reema.chawla@apsva.us,Wakefield HS,Teacher,Instructional Staff
Ivan,Lopez,E24297,ivan.lopez2@apsva.us,Wakefield HS,Teacher,Instructional Staff
Diana,Dempsey,E24327,diana.dempsey@apsva.us,Wakefield HS,Teacher,Instructional Staff
Mercedes,Long,E24420,mercedes.long@apsva.us,Wakefield HS,Teacher,Instructional Staff
Megan,Lordos,E24470,megan.lordos@apsva.us,Wakefield HS,Teacher,Instructional Staff
Zainab,Kufaishi,E24474,zainab.kufaishi@apsva.us,Wakefield HS,n/a,Unclassified
Roxana,Fiel Lazarte,E24558,roxana.fiellazarte@apsva.us,Wakefield HS,Custodian,Facilities Staff
Christopher,Rawlings,E24576,christopher.rawlings@apsva.us,Wakefield HS,Staff,Staff
Leonard,Martin,E24602,leonard.martin@apsva.us,Wakefield HS,Staff,Staff
Stephen,Lynch,E24653,stephen.lynch@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Manuel,Sanchez Hernandez,E24892,manuel.hernandez2@apsva.us,Wakefield HS,Maintenance Supervisor,Facilities Staff
Corey,Kauffman,E24944,corey.kauffman@apsva.us,Wakefield HS,Teacher,Instructional Staff
Jerome,Nelson,E25009,jerome.nelson@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Fabian,Tomala,E25055,fabian.tomala@apsva.us,Wakefield HS,Teacher,Instructional Staff
Alejandra,Garcia De Carranza,E25105,alejandra.carranza@apsva.us,Wakefield HS,Custodian,Facilities Staff
Alexander,Mercado-Reza,E25158,alexander.reza@apsva.us,Wakefield HS,Teacher,Instructional Staff
Luis,Vasquez,E25209,luis.vasquez@apsva.us,Wakefield HS,Custodian,Facilities Staff
Melvin,Pickett,E25214,melvin.pickett@apsva.us,Wakefield HS,Staff,Staff
Luis,Torres,E25248,luis.torres@apsva.us,Wakefield HS,Custodian,Facilities Staff
Sara,Donohue,E25400,sara.donohue@apsva.us,Wakefield HS,Teacher,Instructional Staff
Daniel,Redmond,E25572,daniel.redmond@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Sandra,Costa,E25594,sandra.costa@apsva.us,Wakefield HS,Teacher,Instructional Staff
Selamawit,Abed,E25721,selamawit.abed@apsva.us,Wakefield HS,Teacher,Instructional Staff
Sean,Tracy,E25800,sean.tracy@apsva.us,Wakefield HS,Teacher,Instructional Staff
Nathel,Hailey,E25831,nathel.hailey@apsva.us,Wakefield HS,Director,Administrative Staff
Kira,Atkinson,E25847,kira.atkinson@apsva.us,Wakefield HS,n/a,Unclassified
Kareen,Verneret,E25928,kareen.verneret@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Andrew,Allen,E26047,andrew.allen@apsva.us,Wakefield HS,"Instructional Assistant, Staff",Instructional Staff
Yue,Zhen,E26073,yue.zhen@apsva.us,Wakefield HS,n/a,Unclassified
Dontae,McCoy,E26131,dontae.mccoy@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Maria,Bonilla Guevara,E26373,maria.bonillaguevara@apsva.us,Wakefield HS,Custodian,Facilities Staff
Bryan,Tubbs-Herring,E26457,bryan.tubbsherring@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Angeline,Palmer,E26563,angeline.palmer@apsva.us,Wakefield HS,Teacher,Instructional Staff
Joseph,DeFranco,E26658,joseph.defranco2@apsva.us,Wakefield HS,Staff,Staff
Edward,Starr,E2673,edward.starr@apsva.us,Wakefield HS,Teacher,Instructional Staff
Phaedra,Long,E26754,phaedra.long@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michael,Welsh,E26834,michael.welsh@apsva.us,Wakefield HS,Teacher,Instructional Staff
Susi,Brittain,E26870,susi.brittain@apsva.us,Wakefield HS,Teacher,Instructional Staff
Antoinette,Waters,E26880,antoinette.waters@apsva.us,Wakefield HS,Teacher,Instructional Staff
Zoe,Anthony,E26939,zoe.anthony2@apsva.us,Wakefield HS,Staff,Staff
Hilary,Sparrell,E27055,hilary.sparrell@apsva.us,Wakefield HS,Teacher,Instructional Staff
John,Clisham,E2712,john.clisham@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michael,Grill,E2740,michael.grill@apsva.us,Wakefield HS,Teacher,Instructional Staff
Montel,Bramlett,E27482,montel.bramlett@apsva.us,Wakefield HS,Staff,Staff
Alane,Lee Mayo,E27492,alane.leemayo@apsva.us,Wakefield HS,Administrative Assistant,Staff
Josephine,Nkansah,E27535,josephine.nkansah@apsva.us,Wakefield HS,Custodian,Facilities Staff
Beth,Nalker,E27730,beth.nalker@apsva.us,Wakefield HS,Teacher,Instructional Staff
Antonio,Francis,E27760,antonio.francis2@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Catherine,Smith,E27772,catherine.smith@apsva.us,Wakefield HS,Teacher,Instructional Staff
Edson,Arango Gomez,E27840,edson.arangogomez@apsva.us,Wakefield HS,Teacher,Instructional Staff
Aimee,Puschkin,E27863,aimee.puschkin@apsva.us,Wakefield HS,Teacher,Instructional Staff
Norvell,Queen,E27864,norvell.queen@apsva.us,Wakefield HS,Teacher,Instructional Staff
Amira,Chalabi,E27922,amira.chalabi@apsva.us,Wakefield HS,Teacher,Instructional Staff
Kelly,Hopkins,E28029,kelly.hopkins@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michael,Griffin,E28093,michael.griffin@apsva.us,Wakefield HS,Teacher,Instructional Staff
Elijah,Gormes,E28152,elijah.gormes@apsva.us,Wakefield HS,"Instructional Assistant, Staff",Staff
Ana,Munoz-Gonzalez,E28157,ana.munozgonzalez@apsva.us,Wakefield HS,Teacher,Instructional Staff
Lindsay,Cowen,E28285,lindsay.cowen@apsva.us,Wakefield HS,Staff,Staff
Lisa,Ohm,E28327,lisa.ohm@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Alexandra,Economou,E28494,alexandra.economou@apsva.us,Wakefield HS,Teacher,Instructional Staff
Nelson,Fuamenya,E28513,nelson.fuamenya@apsva.us,Wakefield HS,Teacher,Instructional Staff
Celvin,Sanchez,E28536,celvin.sanchez@apsva.us,Wakefield HS,Staff,Staff
Lisa,Fowler,E28646,lisa.fowler@apsva.us,Wakefield HS,Teacher,Instructional Staff
Christine,Lively,E28657,christine.lively@apsva.us,Wakefield HS,Teacher,Instructional Staff
Nitin,Panwar,E28753,nitin.panwar2@apsva.us,Wakefield HS,Teacher,Instructional Staff
Robert,Hedderly,E28902,robert.hedderly@apsva.us,Wakefield HS,Teacher,Instructional Staff
Tarra,Austin,E28903,tarra.austin@apsva.us,Wakefield HS,Teacher,Instructional Staff
Julianna,Araujo,E28923,julianna.araujo@apsva.us,Wakefield HS,Teacher,Instructional Staff
Rachel,Dineen,E28957,rachel.dineen@apsva.us,Wakefield HS,Teacher,Instructional Staff
Nisha,Sensharma,E29041,nisha.sensharma@apsva.us,Wakefield HS,Teacher,Instructional Staff
Laurie,Artman-McIntosh,E29075,laurie.mcintosh@apsva.us,Wakefield HS,Teacher,Instructional Staff
Paola,Aguilar,E29079,paola.aguilar@apsva.us,Wakefield HS,Staff,Staff
Miruna,Stanica,E29122,miruna.stanica@apsva.us,Wakefield HS,Teacher,Instructional Staff
Paige,Irwin,E29130,paige.irwin@apsva.us,Wakefield HS,Teacher,Instructional Staff
Alejandro,Padilla Crisostomo,E29143,alejandro.crisostomo@apsva.us,Wakefield HS,Staff,Staff
William,Vincent,E29197,william.vincent@apsva.us,Wakefield HS,Teacher,Instructional Staff
Ebenezer,Darko,E29203,ebenezer.darko@apsva.us,Wakefield HS,Custodian,Facilities Staff
Toby,Thacker,E29248,toby.thacker@apsva.us,Wakefield HS,Teacher,Instructional Staff
Erika,Enright,E29251,erika.enright@apsva.us,Wakefield HS,Teacher,Instructional Staff
James,Guerra,E2935,james.guerra@apsva.us,Wakefield HS,Teacher,Instructional Staff
David,Noyes,E29436,david.noyes@apsva.us,Wakefield HS,Staff,Staff
Gregory,Campbell,E2945,gregory.campbell@apsva.us,Wakefield HS,Teacher,Instructional Staff
Kwabena,Agyemang,E29509,kwabena.agyemang@apsva.us,Wakefield HS,Custodian,Facilities Staff
Russell,Topp,E29540,russell.topp@apsva.us,Wakefield HS,Staff,Staff
Georgina,Sarpong,E29599,georgina.sarpong@apsva.us,Wakefield HS,Custodian,Facilities Staff
Kelly,Vigne,E29731,kelly.vigne@apsva.us,Wakefield HS,Teacher,Instructional Staff
Nancy,Flores,E29769,nancy.flores@apsva.us,Wakefield HS,Teacher,Instructional Staff
Scott,Hoffman,E29886,scott.hoffman@apsva.us,Wakefield HS,Staff,Staff
Nicola,Williams,E29957,nicola.williams@apsva.us,Wakefield HS,Teacher,Instructional Staff
Lelia,Troiano,E29996,lelia.troiano@apsva.us,Wakefield HS,Teacher,Instructional Staff
Opeyemi,Olufoye,E30025,opeyemi.olufoye@apsva.us,Wakefield HS,Teacher,Instructional Staff
Derell,Bell,E30104,derell.bell@apsva.us,Wakefield HS,Staff,Staff
Raimonique,Beaver,E30123,raimonique.beaver@apsva.us,Wakefield HS,Teacher,Instructional Staff
Kelly,Carruthers,E3015,kelly.carruthers@apsva.us,Wakefield HS,Teacher,Instructional Staff
Rebecca,Bullard,E30151,rebecca.bullard@apsva.us,Wakefield HS,Teacher,Instructional Staff
Daniel,Orcullo,E30200,daniel.orcullo@apsva.us,Wakefield HS,Teacher,Instructional Staff
Ashley,Rickman,E30369,ashley.rickman@apsva.us,Wakefield HS,Teacher,Instructional Staff
Hina,Aftab,E30385,hina.aftab@apsva.us,Wakefield HS,Teacher,Instructional Staff
Karima,Ibn Elfarouk,E30403,karima.ibnelfarouk@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Emilse,Guzman,E30576,emilse.guzman@apsva.us,Wakefield HS,Custodian,Facilities Staff
Joshua,Martini,E30742,joshua.martini@apsva.us,Wakefield HS,Staff,Staff
Stacey,Edington,E30799,stacey.edington@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michelle,Hess,E30830,michelle.hess@apsva.us,Wakefield HS,Administrative Assistant,Staff
Donta,Henson Garrett,E30839,donta.hensongarrett@apsva.us,Wakefield HS,Staff,Staff
Jon,Gallinger,E30888,jon.gallinger@apsva.us,Wakefield HS,Teacher,Instructional Staff
Paul,Guay,E30964,paul.guay@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Christina,Barrett-Steele,E31071,christina.barrett@apsva.us,Wakefield HS,Teacher,Instructional Staff
Brooke,Giles,E31195,brooke.giles@apsva.us,Wakefield HS,Staff,Staff
Hannah,McLinden,E31257,hannah.kvitle@apsva.us,Wakefield HS,Teacher,Instructional Staff
Rodney,Hall,E31291,rodney.hall@apsva.us,Wakefield HS,Teacher,Instructional Staff
Ernest,Bowser,E31296,ernest.bowser@apsva.us,Wakefield HS,Teacher,Instructional Staff
Michael,Rubin,E31309,michael.rubin@apsva.us,Wakefield HS,Teacher,Instructional Staff
Mark,Vincent,E31338,mark.vincent@apsva.us,Wakefield HS,Teacher,Instructional Staff
David,Trave Medina,E31399,medina.trave@apsva.us,Wakefield HS,Teacher,Instructional Staff
Craig,Broyles,E31528,craig.broyles@apsva.us,Wakefield HS,Staff,Staff
Howard,Smith,E31529,howard.smith@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Robert,Holland,E31622,robert.holland@apsva.us,Wakefield HS,"Assistant Director, Teacher",Instructional Staff
John,Leinberger,E31748,john.leinberger@apsva.us,Wakefield HS,Staff,Staff
James,Collins,E31793,james.collins@apsva.us,Wakefield HS,Staff,Staff
Christopher,Margopoulos,E31819,chris.margopoulus@apsva.us,Wakefield HS,Staff,Staff
Sameen,Awan,E31871,sameen.awan@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Cayla,Donnelly,E31946,cayla.donnelly@apsva.us,Wakefield HS,Staff,Staff
Annor,Owusu,E31960,annor.owusu@apsva.us,Wakefield HS,Custodian,Facilities Staff
Rebekah,Hoisl,E31987,rebekah.hoisl@apsva.us,Wakefield HS,Staff,Staff
Bobbie,Hanes,E32003,bobbie.hanes@apsva.us,Wakefield HS,Teacher,Instructional Staff
Nicole,Johnson,E32026,nicole.johnson2@apsva.us,Wakefield HS,Teacher,Instructional Staff
Zoe,Bliss,E32049,zoe.bliss@apsva.us,Wakefield HS,Teacher,Instructional Staff
Elliot,Merker,E32057,elliot.merker@apsva.us,Wakefield HS,Teacher,Instructional Staff
Janice,Madrecki,E32068,janice.madrecki@apsva.us,Wakefield HS,Teacher,Instructional Staff
Jeremy,Levine,E32074,jeremy.levine@apsva.us,Wakefield HS,Teacher,Instructional Staff
Craig,Fontenot,E32076,craig.fontenot@apsva.us,Wakefield HS,Teacher,Instructional Staff
Matthew,McAndrew,E32083,matthew.mcandrew@apsva.us,Wakefield HS,Staff,Staff
Renee,Dietrich,E32098,renee.dietrich@apsva.us,Wakefield HS,Teacher,Instructional Staff
Tonya,Brothers,E32111,tonya.brothers@apsva.us,Wakefield HS,Teacher,Instructional Staff
Maura,Coughlin,E32135,maura.coughlin@apsva.us,Wakefield HS,Teacher,Instructional Staff
Yvette,Lanari,E32180,yvette.lanari@apsva.us,Wakefield HS,Teacher,Instructional Staff
Marc,Ittelson,E32186,marc.ittelson@apsva.us,Wakefield HS,Teacher,Instructional Staff
Kwabena,Yiadom,E32196,kwabena.yiadom@apsva.us,Wakefield HS,Custodian,Facilities Staff
Robert,Miller,E32200,robert.miller@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Jacob,McGrail,E32216,jacob.mcgrail@apsva.us,Wakefield HS,Staff,Staff
Vincent,Crisp,E32217,vincent.crisp@apsva.us,Wakefield HS,Staff,Staff
Laura,Beimfohr,E32219,laura.beimfohr@apsva.us,Wakefield HS,Staff,Staff
Gregory,Bacon,E32222,gregory.bacon@apsva.us,Wakefield HS,Staff,Staff
Cindy,Romero,E32226,cindy.romero@apsva.us,Wakefield HS,Staff,Staff
Sherry,Kohan,E32236,sherry.kohan@apsva.us,Wakefield HS,Staff,Staff
Ashley,Hidalgo,E32243,ashley.hidalgo@apsva.us,Wakefield HS,Staff,Staff
Morgan,Kuhns,E32245,morgan.kuhns@apsva.us,Wakefield HS,Staff,Staff
Jennifer,Jones,E32254,jennifer.jones2@apsva.us,Wakefield HS,Staff,Staff
Melanie,Vu,E32263,melanie.vu@apsva.us,Wakefield HS,Administrative Assistant,Staff
Dominique,Tham,E32272,dominique.tham@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Taylor,Memon,E32284,taylor.memon@apsva.us,Wakefield HS,Staff,Staff
Yemaya,Quinn,E32292,yemaya.quinn@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Corey,Haynes,E32348,corey.haynes@apsva.us,Wakefield HS,Staff,Staff
Alisey,Corrales,E32424,alisey.corrales@apsva.us,Wakefield HS,Staff,Staff
Carlos,Farias,E32425,carlos.farias@apsva.us,Wakefield HS,Staff,Staff
Andrew,Ream,E32455,andrew.ream@apsva.us,Wakefield HS,Staff,Staff
Thomas,Rava,E32461,thomas.rava@apsva.us,Wakefield HS,Staff,Staff
Shanan,Knox,E32492,shanan.knox@apsva.us,Wakefield HS,Staff,Staff
Andrew,McPhee,E32500,andrew.mcphee@apsva.us,Wakefield HS,Staff,Staff
Jaime,Segui-Asad,E32502,jaime.seguiasad@apsva.us,Wakefield HS,Staff,Staff
Catherine,Askew-Quitto,E32545,catherine.quitto@apsva.us,Wakefield HS,Teacher,Instructional Staff
Darrell,Weeks,E32557,darrell.weeks@apsva.us,Wakefield HS,Teacher,Instructional Staff
Shane,Spellman,E32595,shane.spellman@apsva.us,Wakefield HS,Staff,Staff
Quentin,Singleton,E32602,quentin.singleton@apsva.us,Wakefield HS,Teacher,Instructional Staff
Maxim,Khutoryan,E32604,maxim.khutoryan@apsva.us,Wakefield HS,Teacher,Instructional Staff
David,Knorr,E32633,david.knorr@apsva.us,Wakefield HS,Staff,Staff
Samuel,Sharp,E32655,samuel.sharp@apsva.us,Wakefield HS,Staff,Staff
Alyssa,Swearingen,E32657,alyssa.swearingen@apsva.us,Wakefield HS,Staff,Staff
Jacqueline,Beathea,E32683,jacqueline.beathea@apsva.us,Wakefield HS,Staff,Staff
Julie,Dang,E32686,julie.dang@apsva.us,Wakefield HS,Staff,Staff
Deontae,Hargrove,E32716,deontae.hargrove@apsva.us,Wakefield HS,Staff,Staff
Jeffery,Sanford,E32781,jeffery.sanford@apsva.us,Wakefield HS,Teacher,Instructional Staff
Tara,Dixon,E32814,tara.dixon@apsva.us,Wakefield HS,Teacher,Instructional Staff
Zainab,Ajaz,E32828,zainab.ajaz@apsva.us,Wakefield HS,Teacher,Instructional Staff
Sandra,Medrano Alvarez,E32850,sandra.alvarez@apsva.us,Wakefield HS,Staff,Staff
Devin,Quarles,E32882,devin.quarles@apsva.us,Wakefield HS,Staff,Staff
Tylar,Abrams,E32895,tylar.abrams@apsva.us,Wakefield HS,Staff,Staff
Betty,Gyamfi,E32906,betty.gyamfi@apsva.us,Wakefield HS,Custodian,Facilities Staff
Magdalena,Parkhurst,E32921,magdalena.parkhurst@apsva.us,Wakefield HS,Teacher,Instructional Staff
Lorraine,Turner,E32956,lorraine.turner@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Andrew,Davis,E32985,andrew.davis@apsva.us,Wakefield HS,Staff,Staff
Marlena,Hawkins,E33032,marlena.hawkins@apsva.us,Wakefield HS,Teacher,Instructional Staff
Jesse,Delgado,E33056,jesse.delgado@apsva.us,Wakefield HS,Staff,Staff
Jabril,Adams,E33080,jabril.adams@apsva.us,Wakefield HS,Staff,Staff
Charles,Stringfellow,E33081,charles.stringfellow@apsva.us,Wakefield HS,Staff,Staff
Jeremy,Gibson,E33111,jeremy.gibson@apsva.us,Wakefield HS,Staff,Staff
Carletta,Johnson,E33125,carletta.johnson@apsva.us,Wakefield HS,Staff,Staff
Teandria,Thompson,E33150,teandria.thompson@apsva.us,Wakefield HS,Staff,Staff
Sheila,Sherrill,E33154,sheila.sherrill@apsva.us,Wakefield HS,Staff,Staff
Sarah,Vest,E33174,sarah.vest@apsva.us,Wakefield HS,Staff,Staff
Leslie,Collins,E33176,leslie.collins@apsva.us,Wakefield HS,Staff,Staff
Elleni,Tsega,E33190,elleni.tsega@apsva.us,Wakefield HS,Staff,Staff
Evan,Walker,E33235,evan.walker@apsva.us,Wakefield HS,Staff,Staff
Evelyn,Pinto,E3331,evelyn.pinto@apsva.us,Wakefield HS,Custodian,Facilities Staff
Alexandria,Hegeman,E33423,alexandria.hegeman@apsva.us,Wakefield HS,Staff,Staff
Oscar,Elinan,E33442,oscar.elinan@apsva.us,Wakefield HS,Staff,Staff
Edith,Garay Flores,E33508,edith.garayflores@apsva.us,Wakefield HS,Custodian,Facilities Staff
Francis,Fernandez De Castro Lovatelli,E33517,francis.fernandez@apsva.us,Wakefield HS,Staff,Staff
Nadia,Rendon,E33518,nadia.rendon@apsva.us,Wakefield HS,Staff,Staff
Soupar,Sovidaray,E33527,soupar.sovidaray@apsva.us,Wakefield HS,Staff,Staff
Howard,Bolden,E33537,howard.bolden@apsva.us,Wakefield HS,Staff,Staff
Shawn,Vass,E33558,shawn.vass@apsva.us,Wakefield HS,Staff,Staff
Christian,Wheeler,E33559,christian.wheeler@apsva.us,Wakefield HS,Staff,Staff
Randy,Rowles,E33560,randy.rowles@apsva.us,Wakefield HS,Staff,Staff
Joshua,Wallace,E33562,joshua.wallace@apsva.us,Wakefield HS,Staff,Staff
Daniel,Lanari,E33566,daniel.lanari@apsva.us,Wakefield HS,Staff,Staff
Jina,Davidson,E3443,jina.davidson@apsva.us,Wakefield HS,Teacher,Instructional Staff
Shaivy,Mukherji,E3691,shaivy.mukherji@apsva.us,Wakefield HS,Teacher,Instructional Staff
Clifford,Brown,E392,clifford.brown@apsva.us,Wakefield HS,Teacher,Instructional Staff
Tom,Windsor,E3998,tom.windsor@apsva.us,Wakefield HS,Staff,Staff
John,Stewart,E416,john.stewart@apsva.us,Wakefield HS,Teacher,Instructional Staff
Keith,Powell,E4163,keith.powell@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Sherwood,Jones,E4311,sherwood.jones@apsva.us,Wakefield HS,Teacher,Instructional Staff
Maria,Aguilar,E4557,maria.aguilar@apsva.us,Wakefield HS,Registrar,Staff
Peter,Fitzgerald,E4887,peter.fitzgerald@apsva.us,Wakefield HS,Teacher,Instructional Staff
Miriam,Ramirez,E4913,miriam.ramirez@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Jackson,Aleman,E5243,jackson.aleman@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Harry,Barnes,E5527,harry.barnes@apsva.us,Wakefield HS,Staff,Staff
George,Baker,E5548,george.baker2@apsva.us,Wakefield HS,Teacher,Instructional Staff
Zakia,Elgamal,E5803,zakia.elgamal@apsva.us,Wakefield HS,Teacher,Instructional Staff
Reyna,Berrios,E6139,reyna.berrios@apsva.us,Wakefield HS,Administrative Assistant,Staff
Allan,Glascock,E6360,allan.glascock@apsva.us,Wakefield HS,Teacher,Instructional Staff
Gregory,Keish,E6930,gregory.keish@apsva.us,Wakefield HS,Teacher,Instructional Staff
John,Castaneda,E7013,john.castaneda@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Lynette,Mccracken,E7036,lynette.mccracken@apsva.us,Wakefield HS,Director,Administrative Staff
Laurell,Wiersma,E706,laurell.wiersma@apsva.us,Wakefield HS,Teacher,Instructional Staff
Betty,Sanders,E7156,betty.sanders@apsva.us,Wakefield HS,Assistant Principal,Administrative Staff
Hugo,Franco,E7399,hugo.franco@apsva.us,Wakefield HS,Maintenance Supervisor,Facilities Staff
Delicia,Moton,E7599,delicia.moton@apsva.us,Wakefield HS,Administrative Assistant,Staff
Traci,Yates,E7694,traci.yates@apsva.us,Wakefield HS,Staff,Staff
Michelle,Robinson,E7702,michelle.robinson@apsva.us,Wakefield HS,Teacher,Instructional Staff
Rebecca,Kigin,E7820,rebecca.kigin@apsva.us,Wakefield HS,Teacher,Instructional Staff
Marcus,McKinney,E7880,marcus.mckinney@apsva.us,Wakefield HS,Teacher,Instructional Staff
Dan,Harris,E813,dan.harris@apsva.us,Wakefield HS,Teacher,Instructional Staff
Carlos,Murillo,E8175,carlos.murillo@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Teresa,Swihart,E8227,teresa.swihart@apsva.us,Wakefield HS,Teacher,Instructional Staff
Cynthia,Ampem,E8270,cynthia.ampem@apsva.us,Wakefield HS,Maintenance Supervisor,Facilities Staff
Elaine,Kluttz,E8380,elaine.kluttz@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Deneen,Snow,E8553,deneen.snow@apsva.us,Wakefield HS,Teacher,Instructional Staff
Timothy,Cotman,E8617,timothy.cotman@apsva.us,Wakefield HS,Teacher,Instructional Staff
Margaret,Johnson,E8672,margaret.johnson@apsva.us,Wakefield HS,Teacher,Instructional Staff
Alfred,Reid,E8872,alfred.reid@apsva.us,Wakefield HS,Teacher,Instructional Staff
Marjorie,Armstrong,E8874,marjorie.armstrong@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Tony,Bentley,E9137,tony.bentley@apsva.us,Wakefield HS,Instructional Assistant,Instructional Staff
Anita,Warner,E9558,anita.warner@apsva.us,Wakefield HS,Teacher,Instructional Staff
Luisa,Barba,E9720,luisa.barba@apsva.us,Wakefield HS,School Finance Officer,Staff
Douglas,Burns,E9762,douglas.burns@apsva.us,Wakefield HS,Teacher,Instructional Staff
William,Forbes,E10231,william.forbes@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Nicole,Turgeon-Williams,E10579,nicole.turgeonw@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jerome,Peele,E10629,jerome.peele@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Jeana,Norton,E10805,jeana.norton@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Anne,Verville,E1109,anne.verville@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Ryan,Miller,E11317,ryan.miller@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kelly,Breedlove,E11579,kelly.breedlove@apsva.us,Washington-Liberty HS,n/a,Unclassified
Carmen,Delacruz,E1160,carmen.delacruz@apsva.us,Washington-Liberty HS,Assistant Principal,Administrative Staff
Jennifer,Kelly,E12067,jennifer.kelly@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Carol,Callaway,E12767,carol.callaway@apsva.us,Washington-Liberty HS,Director,Administrative Staff
Ronald,Melkis,E1283,ronald.melkis@apsva.us,Washington-Liberty HS,Staff,Staff
Corey,Meyers,E13640,corey.meyers@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Pamela,Sanchez,E13943,pamela.sanchez@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Colleen,Auerbach,E14024,colleen.auerbach@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Dionte,Lawley,E14036,dionte.coleman@apsva.us,Washington-Liberty HS,"Instructional Assistant, Staff",Instructional Staff
Debbie,Germosen-Hill,E14120,debbie.germosen@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Carolyn,Taylor,E14355,carolyn.taylor@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Valerie,Aronson,E1450,valerie.aronson@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Clement,Rumber,E14778,clement.rumber@apsva.us,Washington-Liberty HS,Staff,Staff
Johnnie,Buxton,E15178,johnnie.buxton@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Lilliana,Maldonado-Mendez,E15259,lilliana.mendez@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Anh,Phan,E15357,anh.phan@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Nevin,Jaffer,E15533,nevin.jaffer@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Alex,Berrios,E15682,alex.berrios@apsva.us,Washington-Liberty HS,"Instructional Assistant, Staff",Instructional Staff
Steven,Brown,E15831,steven.brown@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Hiromi,Isobe,E159,hiromi.isobe@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Antonio,Hall,E15922,antonio.hall@apsva.us,Washington-Liberty HS,Principal,Unclassified
Nelvi,Lazarte,E16063,nelvi.lazarte@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Monica,Campoverde,E16108,monica.campoverde@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Kristina,Dorville,E16119,kristina.dorville@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Erica,Larsen,E16202,erica.larsen@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Matthew,Przydzial,E16235,matthew.przydzial@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Beth,Prange,E1649,beth.prange@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kenneth,McCreary,E16573,kenneth.mccreary@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jen,Dean,E16637,jen.dean@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Danielle,Karaky,E16675,danielle.karaky@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Paul,Bui,E16694,paul.bui@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Erin,Smith,E16706,erin.smith@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Paul,Jamelske,E1674,paul.jamelske@apsva.us,Washington-Liberty HS,Assistant Principal,Administrative Staff
Joel,Rockwood,E1676,joel.rockwood@apsva.us,Washington-Liberty HS,"Teacher, Staff",Staff
Meaghan,Traverse,E16780,meaghan.traverse@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Mary,Clendenning,E168,mary.clendenning@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Johanna,Narvaez,E16813,johanna.narvaez@apsva.us,Washington-Liberty HS,"Instructional Assistant, Teacher",Instructional Staff
Shayma,Al-Hanooti,E16829,shayma.alhanooti@apsva.us,Washington-Liberty HS,"Teacher, Staff",Staff
Heather,Mizell,E1683,heather.mizell@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Ronald,Valdez,E16997,ronald.valdez@apsva.us,Washington-Liberty HS,n/a,Unclassified
Maria,Veizaga,E17078,maria.veizaga@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Sandra,Baird,E17083,sandra.baird@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Latasha,Chamberlain,E17095,latasha.chamberlain@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Robert,Summers,E1834,robert.summers@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Felicia,Meier,E1862,felicia.meier@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Martin,Torres,E19028,martin.torres@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Adam,Moir,E19130,adam.moir@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Derek,Parsons,E19206,derek.parsons@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jimmy,Carrasquillo,E19219,jimmy.carrasquillo@apsva.us,Washington-Liberty HS,"Athletic Coach, Instructional Assistant",Instructional Staff
Timica,Shivers,E19453,timica.shivers@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Christina,Steury,E19507,christina.steury@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Nora,Kelley,E19655,nora.kelley@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Angie,Kelly,E19847,angie.kelly@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Charles,Shaw,E19970,charles.shaw@apsva.us,Washington-Liberty HS,Staff,Staff
Segundo,Ancalle Gutierrez,E20024,segundo.ancalle@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Jason,Brodowski,E2020,jason.brodowski@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Irma,Medrano,E20234,irma.medrano@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jacqueline,Stallworth,E20336,jacqueline.stallwort@apsva.us,Washington-Liberty HS,"Teacher, Staff",Staff
Joshua,Folb,E20345,joshua.folb@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jim,Zarro,E20450,jim.zarro@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jarrod,Hills,E20558,jarrod.hills@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Zarin,Kapadia,E20562,zarin.kapadia@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Aubrey,Mosley,E21183,aubrey.mosley@apsva.us,Washington-Liberty HS,"Athletic Coach, Teacher",Staff
Courtney,Holcombe,E2120,courtney.holcombe@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kevin,Phillips,E21340,kevin.phillips@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
David,Lunt,E21586,david.lunt@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Ana,Bentley,E21641,ana.bentley@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Lore,Rodriguez,E21645,lore.rodriguez@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Daniel,Paris,E21735,dan.paris@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Jacob,Lloyd,E21827,jacob.lloyd@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Lianne,Jaramillo,E22060,lianne.jaramillo@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Lourdes Sotomayor,Sotomayor,E22093,lourdes.sotomayor@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Nicolas,Burns,E22138,nicolas.burns@apsva.us,Washington-Liberty HS,Staff,Staff
Maria,Burke,E22142,maria.burke@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Emily,Roszkowski,E22241,emily.roszkowski@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Tisha,Ford,E22250,tisha.ford@apsva.us,Washington-Liberty HS,"Teacher, Staff",Staff
Denisha,Parker,E22536,denisha.parker@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Katherine,Hedderly,E22631,katherine.hedderly@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jessica,Ward,E22642,jessica.ward@apsva.us,Washington-Liberty HS,Staff,Staff
Ashlee,Doleno,E22645,ashlee.doleno@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Giorgina,Palomino,E22701,giorgina.palomino@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Chameka,Day,E22752,chameka.day@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Courtney,Pratt,E22832,courtney.pratt@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Andrea,Cordero,E22913,andrea.cordero@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Elysse,Catino,E22922,elysse.catino@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Keith,Ricks,E22999,keith.ricks@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Sarah,Owen,E23046,sarah.owen@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kevin,Hall,E23121,kevin.hall@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Stephanie,Sarmiento,E23125,stephanie.sarmiento@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Miles,Carey,E23172,miles.carey@apsva.us,Washington-Liberty HS,Assistant Principal,Administrative Staff
Diana,Velasquez,E23227,diana.velasquez@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Edgardo,Ventura Bustillo,E23238,edgardo.bustillo@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Beth,Romero,E23289,beth.romero@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Sarah,Becker,E23364,sarah.becker@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Darreck,Gibson,E23414,darreck.gibson@apsva.us,Washington-Liberty HS,Maintenance Supervisor,Facilities Staff
Danielle,Day,E23586,danielle.day@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Laura,Smithgall,E23928,laura.smithgall@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Patrick,Ward,E23938,patrick.ward@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Elise,Jackson,E23965,elise.jackson@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Sumer,Majid,E24016,sumer.majid@apsva.us,Washington-Liberty HS,n/a,Unclassified
Ghita,Haronni,E24088,ghita.haronni@apsva.us,Washington-Liberty HS,Staff,Staff
Kristen,Johnston,E24146,kristen.johnston@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Bidkar,Jimenez,E24239,bidkar.jimenez@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Alan,Henderson,E24364,alan.henderson@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Christopher,Daggett Rowzee,E24386,christopher.daggett@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Zainab,Kufaishi,E24474,zainab.kufaishi@apsva.us,Washington-Liberty HS,n/a,Unclassified
Lea,Bohn,E24625,lea.bohn@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Carla,Loayza,E24639,carla.loayza@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Maria,Castello Pascual,E24682,maria.castello@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Veronica,Munoz,E24887,veronica.munoz@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Jaime,Serrano Martinez,E24890,jaime.serrano@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Erin,Bruns,E24904,erin.bruns@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
William,Drake,E24947,william.drake@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Marion,Taousakis,E25112,marion.taousakis@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Michael,Harrington,E25128,michael.harrington@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Emma,Keiswetter,E25254,emma.keiswetter@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Dinora,Valdovinos,E25304,dinora.valdovinos@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Chaimae,Haronni,E25584,chaimae.haronni@apsva.us,Washington-Liberty HS,Staff,Staff
Megan,Wright,E25598,megan.wright@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kimberly,Barnes Brenneman,E25653,kimberly.barnes@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Suzanne,Stratmann,E25709,suzanne.stratmann@apsva.us,Washington-Liberty HS,"Teacher, Staff",Staff
Tracy,Bannister,E25742,tracy.bannister@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Todd,Sabatino,E25743,todd.sabatino@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Susan,Ramirez,E25778,susan.ramirez@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Michelle,MacLeod,E25804,michelle.macleod@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Michelle,Letts,E25850,michelle.letts@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Natasha,Prosise,E25867,natasha.prosise@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Yesenia,Ramirez,E25885,yesenia.ramirez@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Daniel,Morreale,E25931,daniel.morreale@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Ling-Lang,Marcus,E25942,ling.marcus@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kyle,Mortenson,E25946,kyle.petty@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Atiya,Rehman,E26150,atiya.rehman@apsva.us,Washington-Liberty HS,"Teacher, Staff",Staff
Raymond,White,E26303,raymond.white@apsva.us,Washington-Liberty HS,Staff,Staff
Elmer,Carranza,E26334,elmer.carranza@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Alexandra,Fonseca,E26339,alexandra.fonseca@apsva.us,Washington-Liberty HS,Staff,Staff
Abrianna,May,E26430,abrianna.may@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Richard,Greene,E26434,richard.greene@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Sandra,Delcid,E26455,sandra.delcid@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Danny,Issa,E26501,danny.issa2@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Leasi,Stroud,E26519,leasi.stroud@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Maria,Ascencio Sanchez,E26611,maria.ascencio@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Austin,Gladden,E26622,austin.gladden@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Connie,Williams,E26701,constance.williams@apsva.us,Washington-Liberty HS,Staff,Staff
Jessica,Gregory,E2671,jessica.gregory@apsva.us,Washington-Liberty HS,Director,Administrative Staff
Pamela,Luhowy,E26739,pamela.luhowy@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Robert,Ludwick,E26814,robert.ludwick@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Vica,Irving,E26869,vica.irving@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Sonia,Reyes,E26951,sonia.reyes@apsva.us,Washington-Liberty HS,Registrar,Staff
Jeffrey,Carpenter,E26956,jeffrey.carpenter2@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Nitza,Lord,E26988,nitza.lord@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Lillian,Bing,E27006,lillian.bing@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Traci,Garner,E27020,traci.garner@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Karen,Weaver,E27122,karen.weaver@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Tamika,George,E27176,tamika.george@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Philip,Taylor,E27196,philip.taylor2@apsva.us,Washington-Liberty HS,Staff,Staff
Behaylu,Beyene,E27367,behaylu.beyene@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Daniel,Moses,E27732,daniel.moses@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Christopher,Cook,E27766,christopher.cook2@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Nicole,McCullough,E27935,nicole.wiley@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jason,Perry,E27973,jason.perry@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Katherine,Johnson,E28003,katherine.johnson@apsva.us,Washington-Liberty HS,Staff,Staff
Gerson,Pereira,E28006,gerson.pereira@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Africa,Norris,E28064,africa.norris@apsva.us,Washington-Liberty HS,Staff,Staff
James,Sterns,E28086,james.sterns@apsva.us,Washington-Liberty HS,Manager,Staff
Sergio,Dheming,E28181,sergio.dheming@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Jeremy,Daross,E28234,jeremy.daross@apsva.us,Washington-Liberty HS,Staff,Staff
Michael,Klemencic,E28301,michael.klemencic@apsva.us,Washington-Liberty HS,Staff,Staff
Miles,Strebeck,E28307,miles.strebeck@apsva.us,Washington-Liberty HS,Staff,Staff
Timothy,McGhee,E28324,timothy.mcghee2@apsva.us,Washington-Liberty HS,n/a,Unclassified
Lester,Pertica,E28332,lester.pertica2@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Robert,Byrnes,E28547,robert.byrnes2@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Deborah,LeHardy,E28569,deborah.lehardy@apsva.us,Washington-Liberty HS,Staff,Staff
Andrew,Kim,E28733,andrew.kim@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Heather,Selig,E28813,heather.selig@apsva.us,Washington-Liberty HS,Staff,Staff
Patrice,Splan,E28830,patrice.splan@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Leonda,Archer,E28905,leonda.archer@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Bethany,Bennett,E29013,bethany.bennett@apsva.us,Washington-Liberty HS,Staff,Staff
Sukhdeep,Kaur,E29072,sukhdeep.kaur@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Theresa,Gentile,E29105,theresa.gentile@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Miriam,Watkins,E29128,miriam.watkins@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Dane,Underwood,E29280,dane.underwood@apsva.us,Washington-Liberty HS,Staff,Staff
Jose,Moreno,E29322,jose.moreno2@apsva.us,Washington-Liberty HS,Maintenance Supervisor,Facilities Staff
James,Ober,E29345,james.ober@apsva.us,Washington-Liberty HS,Staff,Staff
Mara,Quinonez,E29413,mara.quinonez@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Robert,Sheppard,E29455,robert.sheppard@apsva.us,Washington-Liberty HS,Staff,Staff
Bridgette,Wanzer-McCoy,E29484,bridgette.wanzer@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Emily,Primmer,E29514,emily.primmer@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Lauren,Burke,E29633,lauren.burke@apsva.us,Washington-Liberty HS,Staff,Staff
Hector,Campos,E29648,hector.campos@apsva.us,Washington-Liberty HS,"Instructional Assistant, Staff",Instructional Staff
Marie,Large,E29678,marie.large2@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Noelle,Smith,E29742,noelle.smith@apsva.us,Washington-Liberty HS,Staff,Staff
Huayra,Ferrufino Orellana,E29826,huayra.ferrufino@apsva.us,Washington-Liberty HS,"Instructional Assistant, Staff",Staff
Kevin,Healy,E29966,kevin.healy@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
John,Bacon,E30011,john.bacon@apsva.us,Washington-Liberty HS,Staff,Staff
Alexandra,Ferguson,E30155,alexandra.ferguson@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Melaku,Beyene,E30322,melaku.beyene@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
James,White,E30357,james.white@apsva.us,Washington-Liberty HS,Staff,Staff
James,Fowler,E30419,james.fowler@apsva.us,Washington-Liberty HS,Staff,Staff
Lauren,Griffin,E30570,lauren.griffin@apsva.us,Washington-Liberty HS,Staff,Staff
Terrence,Cox,E30764,terrence.cox@apsva.us,Washington-Liberty HS,Staff,Staff
Elizabeth,Wood,E30786,elizabeth.wood@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
William,Sorkin,E30813,william.sorkin@apsva.us,Washington-Liberty HS,Staff,Staff
Bilal,Boulali,E30820,bilal.boulali@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Susan,Carr,E3085,susan.carr@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
John,Wooldridge,E30867,john.wooldridge@apsva.us,Washington-Liberty HS,Staff,Staff
Samantha,Rittenberg,E30914,samantha.rittenberg@apsva.us,Washington-Liberty HS,Staff,Staff
Brian,Bennett,E30998,brian.bennett@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Cecelia,Larsen,E31005,cecelia.larsen@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
John,Doll,E3102,john.doll@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kyra,Walker,E31061,kyra.walker@apsva.us,Washington-Liberty HS,Coordinator,Staff
Kirby,Clark,E31108,kirby.clark@apsva.us,Washington-Liberty HS,Staff,Staff
Michael,Wilkis,E31153,michael.wilkis@apsva.us,Washington-Liberty HS,Staff,Staff
John,Moore,E31178,john.moore@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Laura,Moore,E31189,laura.moore2@apsva.us,Washington-Liberty HS,"Staff, Teacher",Staff
Theresa,Severin,E3119,theresa.severin@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Nicole,Andrade,E31219,nicole.andrade@apsva.us,Washington-Liberty HS,Staff,Staff
Gareth,Hall,E31357,gareth.hall@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kathryn,Leonard,E31522,kathryn.leonard@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Faith,Randolph,E31531,faith.randolph@apsva.us,Washington-Liberty HS,Staff,Staff
Elizabeth,Altmaier,E31559,elizabeth.altmaier@apsva.us,Washington-Liberty HS,Staff,Staff
Noah,Kaufman,E31640,noah.kaufman@apsva.us,Washington-Liberty HS,Staff,Staff
Ulysses,Smith,E31728,ulysses.smith@apsva.us,Washington-Liberty HS,Staff,Staff
Sheamus,Larkin,E31763,sheamus.larkin@apsva.us,Washington-Liberty HS,Staff,Staff
Juan,Gonzalez,E31766,juan.gonzalez@apsva.us,Washington-Liberty HS,Staff,Staff
Yan,Wen,E31791,yan.wen@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Keith,McCartney,E31792,keith.mccartney@apsva.us,Washington-Liberty HS,Staff,Staff
Karen,Walker,E31800,karen.walker@apsva.us,Washington-Liberty HS,Staff,Staff
Laurence,Milask,E31839,laurence.milask@apsva.us,Washington-Liberty HS,Staff,Staff
Clayton,Batley,E31860,clayton.batley@apsva.us,Washington-Liberty HS,Staff,Staff
Alexandra,Franck,E31882,alexandra.franck@apsva.us,Washington-Liberty HS,Staff,Staff
Charlotte,Poethke,E31958,charlotte.poethke@apsva.us,Washington-Liberty HS,Staff,Staff
Colin,Peck,E31959,colin.peck@apsva.us,Washington-Liberty HS,Staff,Staff
Lalika,Gerald,E32037,lalika.gerald@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Karen,Hatchl,E32044,karen.hatchl@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jessica,Ryan,E32060,jessica.ryan@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Elizabeth,Palmer,E32148,elizabeth.palmer@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Guillermo,Mejia Santos,E32170,guillermo.mejia@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Jackie,Barrett,E32173,jackie.barrett@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Donald,Mercy,E32223,donald.mercy@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Mackenzie,Cox,E32235,mackenzie.cox@apsva.us,Washington-Liberty HS,Staff,Staff
Magali,Fiel Lazarte,E32276,magali.fiellazarte@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
John,Allevato,E32285,john.allevato@apsva.us,Washington-Liberty HS,Staff,Staff
Karla,Aguirre,E32287,karla.aguirre@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Alexis,Miller,E32290,alexis.miller@apsva.us,Washington-Liberty HS,Staff,Staff
Alexander,Weaver,E32294,alexander.weaver@apsva.us,Washington-Liberty HS,Staff,Staff
William,Iacobucci,E32329,william.iacobucci@apsva.us,Washington-Liberty HS,Staff,Staff
Julia,Fyffe,E32344,julia.fyffe@apsva.us,Washington-Liberty HS,Staff,Staff
Nicole,Collantes,E32345,nicole.collantes@apsva.us,Washington-Liberty HS,Staff,Staff
Feir,Pace,E32435,feir.pace@apsva.us,Washington-Liberty HS,Staff,Staff
Chanel,Marshall,E32437,chanel.marshall@apsva.us,Washington-Liberty HS,Staff,Staff
Fabriana,Velasquez,E32489,fabriana.velasquez@apsva.us,Washington-Liberty HS,Staff,Staff
Hilary,Lord,E3259,hilary.lord@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Deborah,Leow,E32597,deborah.leow@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Alain,Poutre,E32617,alain.poutre@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Tiffany,Hrbacek,E32638,tiffany.hrbacek@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Candice,Barbara,E32708,candice.barbara@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Teonta,Aviles,E32721,teonta.aviles@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Brian,Hicks,E32722,brian.hicks@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jack,Luft,E32732,jack.luft@apsva.us,Washington-Liberty HS,Staff,Staff
Suhail,Thahir,E32733,suhail.thahir@apsva.us,Washington-Liberty HS,Staff,Staff
Andre,Barnes,E32736,andre.barnes@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Tyler,Spicer,E32741,tyler.spicer@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Timothy,Lynch,E32778,timothy.lynch@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Abby,Wyant,E32791,abby.wyant@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Toshiko,Parrish,E32825,toshiko.parrish@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
David,Benjamin,E32838,david.benjamin@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Lauren,Donovan,E32937,lauren.donovan@apsva.us,Washington-Liberty HS,Staff,Staff
Cody,Hutto,E32940,cody.hutto@apsva.us,Washington-Liberty HS,Staff,Staff
Mary Ellen,Moloney,E33041,maryellen.moloney@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Christa,Langley,E33101,christa.langley@apsva.us,Washington-Liberty HS,Staff,Staff
Patricia,Loverich,E33137,patricia.loverich@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Logan,Eisenbeiser,E33149,logan.eisenbeiser@apsva.us,Washington-Liberty HS,Staff,Staff
Tyreece,Byrd,E33151,tyreece.byrd@apsva.us,Washington-Liberty HS,Staff,Staff
Alexander,Bergman,E33158,alexander.bergman@apsva.us,Washington-Liberty HS,Staff,Staff
William,Holmes,E33163,william.holmes@apsva.us,Washington-Liberty HS,Staff,Staff
Christina,Potts,E33194,christina.potts@apsva.us,Washington-Liberty HS,Staff,Staff
Brian,Raciborski,E33203,brian.raciborski@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Rachel,Cotton,E33205,rachel.cotton@apsva.us,Washington-Liberty HS,Staff,Staff
James,Herring,E33275,james.herring@apsva.us,Washington-Liberty HS,Staff,Staff
Aaron,Lee,E33310,aaron.lee@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Valeriana,Colon,E33363,valeriana.colon@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Taryn,Hannam-Zatz,E33446,taryn.hannamzatz@apsva.us,Washington-Liberty HS,Staff,Staff
Kathryn,McAllister,E33478,kathryn.mcallister@apsva.us,Washington-Liberty HS,Staff,Staff
Noah,Winslow,E33479,noah.winslow@apsva.us,Washington-Liberty HS,Staff,Staff
John,Reed,E33480,john.reed@apsva.us,Washington-Liberty HS,Staff,Staff
Sarah,Chamness,E33488,sarah.chamness@apsva.us,Washington-Liberty HS,Staff,Staff
Keri,Bongo,E33513,keri.bongo@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Catherine,Manning,E33563,catherine.manning@apsva.us,Washington-Liberty HS,Staff,Staff
Deborah,Pfirrmann,E33567,deborah.pfirrmann@apsva.us,Washington-Liberty HS,Staff,Staff
David,Morrison,E33575,david.morrison@apsva.us,Washington-Liberty HS,Staff,Staff
Kenneth,Moore,E33576,kenneth.moore@apsva.us,Washington-Liberty HS,Staff,Staff
Glenroy,Williams,E3395,glenroy.williams@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Karen,Bui,E3648,karen.bui@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Ali,Akkache,E3758,ali.akkache@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Justin,Bolfek,E4172,justin.bolfek@apsva.us,Washington-Liberty HS,"Teacher, Assistant Director",Instructional Staff
Wanlace,Yates,E4281,wanlace.yates@apsva.us,Washington-Liberty HS,Staff,Staff
Kimberly,Jackson,E4322,kimberly.jackson@apsva.us,Washington-Liberty HS,Assistant Principal,Administrative Staff
Evan,Rodger,E4444,evan.rodger@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Liem,Truong,E5141,liem.truong@apsva.us,Washington-Liberty HS,Maintenance Supervisor,Facilities Staff
Julie,Cantor,E538,julie.cantor@apsva.us,Washington-Liberty HS,Coordinator,Administrative Staff
Paula,Roney,E5391,paula.roney@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jeffrey,McCarthy,E560,jeffrey.mccarthy@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Philip,Krauth,E5615,philip.krauth@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Sharon,Brahaney,E5650,sharon.brahaney@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Emily,Andrusko,E5908,emily.andrusko@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Adje,Wilson-Bahun,E6040,adje.bahun@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
James,Sample,E617,james.sample@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Gracie,Taylor,E6188,gracie.taylor@apsva.us,Washington-Liberty HS,School Finance Officer,Staff
Horace,Willis,E6234,horace.willis@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Robert,Dobson,E6413,robert.dobson@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Shirley,Ormeno,E6821,shirley.ormeno@apsva.us,Washington-Liberty HS,Administrative Assistant,Staff
Alex,Robinson,E6933,alex.robinson@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Kathleen,Claassen,E7083,kathleen.claassen@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
David,Hernandez,E7088,david.hernandez@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Dawn,McCoart,E722,dawn.mccoart@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Beth,Black,E7264,beth.black@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Sara,Fiorini,E733,sara.fiorini@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Andre,Mcfail,E7372,andre.mcfail@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Maria,Castro,E7376,maria.castro@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Jorge,Caballero,E7424,jorge.caballero@apsva.us,Washington-Liberty HS,"Instructional Assistant, Staff",Instructional Staff
Becky,Luu,E7772,becky.luu@apsva.us,Washington-Liberty HS,Custodian,Facilities Staff
Crystal,Liller,E7931,crystal.liller@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Greg,Butler,E815,greg.butler@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Joseph,Demidio,E8191,joseph.demidio@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Reginald,Brigham,E8221,reginald.brigham@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Mary,Fretts,E8343,mary.fretts@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
James,Thomas,E8436,james.thomas@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Roger,Aleman,E8761,roger.aleman@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Elizabeth,Fucella Burgos,E8936,elizabeth.burgos@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Rosa,Reyes,E9141,rosa.reyes@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Jennifer,Scher,E9153,jennifer.scher@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
David,Peters,E916,david.peters@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Florence,Dale,E9165,florence.dale@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Portia,Wilson,E9594,portia.wilson@apsva.us,Washington-Liberty HS,Instructional Assistant,Instructional Staff
Janet,Luu,E9649,janet.luu@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Josh,Shapiro,E965,josh.shapiro@apsva.us,Washington-Liberty HS,Teacher,Instructional Staff
Adam,Balutis,E9714,adam.balutis@apsva.us,Washington-Liberty HS,Athletic Coach,Staff
Diane,Tan,E1094,diane.tan@apsva.us,Williamsburg MS,"Teacher, Staff",Staff
Thomas,Feeney,E11747,thomas.feeney@apsva.us,Williamsburg MS,Teacher,Instructional Staff
William,Smolinski,E11866,william.smolinski@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Jennifer,Doll,E12266,jennifer.doll@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Latanja,Thomas,E12351,latanja.thomas@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Katherine,Willet,E13308,katherine.willet@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Ryan,Witte,E1351,ryan.witte@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Christiana,Kargbo,E14112,christiana.kargbo@apsva.us,Williamsburg MS,"Instructional Assistant, Staff",Instructional Staff
Vicky,Woodbury,E1531,vicky.woodbury@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Heather,Willis,E15925,heather.willis@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Tram,Ly,E16045,tram.ly@apsva.us,Williamsburg MS,Account Clerk,Staff
Elizabeth,Briones,E16666,elizabeth.briones@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Eric,Skrzypek,E16707,eric.skrzypek@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Felix,Luengo,E16744,felix.luengo@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Sean,Kennedy,E16761,sean.kennedy@apsva.us,Williamsburg MS,Staff,Staff
Christina,Short,E16822,christina.short@apsva.us,Williamsburg MS,Administrative Assistant,Staff
Ronald,Valdez,E16997,ronald.valdez@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Deborah,Powell,E1717,deborah.powell@apsva.us,Williamsburg MS,Instructional Assistant,Instructional Staff
Jill,Mentch,E19688,jill.mentch@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Larissa,Stewart,E20252,larissa.stewart@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Sherry,Lord,E20497,sherry.lord@apsva.us,Williamsburg MS,Staff,Staff
Engelberto,Zamora,E20522,engelberto.zamora@apsva.us,Williamsburg MS,Maintenance Supervisor,Facilities Staff
Maryann,Szwed,E21217,maryann.szwed@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Jody,Osler,E22131,jody.osler@apsva.us,Williamsburg MS,Assistant Principal,Administrative Staff
Robert,Dudek,E22154,robert.dudek@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Jesse,Klink,E22246,jesse.klink@apsva.us,Williamsburg MS,"Teacher, Staff",Staff
Elisha,Han,E22249,elisha.han@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Jeffrey,Schonfeld,E22416,jeffrey.schonfeld@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Yvonne,Mcneese,E22534,yvonne.mcneese@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Jake,Tan,E22774,jake.tan@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Bryan,Boykin,E22901,bryan.boykin@apsva.us,Williamsburg MS,Principal,Unclassified
Sara,Winter,E22957,sara.winter@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Heather,Martin,E22972,heather.martin@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Andrea,Mcferran,E23135,andrea.mcferran@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Tamika,Brown,E23217,tamika.brown@apsva.us,Williamsburg MS,Staff,Staff
Sam,Mencarini,E23497,sam.mencarini@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Susan,Tucker,E23520,susan.tucker@apsva.us,Williamsburg MS,n/a,Unclassified
Susan,Hodkin,E23772,susan.hodkin@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Colin,Sundwick,E24041,colin.sundwick@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Weiling,Song,E24188,weiling.song@apsva.us,Williamsburg MS,n/a,Unclassified
Mani,Parcham,E24476,mani.parcham@apsva.us,Williamsburg MS,"Teacher, Staff",Staff
Stephanie,Scott,E24541,stephanie.scott@apsva.us,Williamsburg MS,Instructional Assistant,Instructional Staff
Christy,Zarro,E24658,christy.zarro@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Ana,Sicer,E24804,ana.sicer@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Lisa,Blandford,E24819,lisa.blandford@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Justin,Girard,E24874,justin.girard@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Kristie,Board,E25049,kristie.board@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Alexandra,Levy,E25126,alexandra.levy@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Elise,Kenney,E25218,elise.kenney@apsva.us,Williamsburg MS,Director,Administrative Staff
Silvia,Claros Rocha,E25479,silvia.clarosrocha@apsva.us,Williamsburg MS,Custodian,Facilities Staff
Emily,Mctavish,E25816,emily.mctavish@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Amy,Wilkins,E26106,amy.wilkins@apsva.us,Williamsburg MS,Staff,Staff
Brian,Coonce,E26140,brian.coonce@apsva.us,Williamsburg MS,Staff,Staff
Daffany,Nance,E26562,daffany.nance@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Christopher,Treble,E26781,christopher.treble@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Michelle,Janney,E26877,michelle.janney@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Hayley,Post,E26902,hayley.post@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Kiare,Mays,E26971,kiare.mays@apsva.us,Williamsburg MS,Administrative Assistant,Staff
Dwayne,Eason,E26972,dwayne.eason@apsva.us,Williamsburg MS,Instructional Assistant,Instructional Staff
Lisa,Warden,E2731,lisa.warden@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Angela,Ventura Reyes,E27359,angela.venturareyes2@apsva.us,Williamsburg MS,Custodian,Facilities Staff
Julia,Davidson,E27765,julia.davidson@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Iris,Whitehill,E27838,iris.whitehill@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Heather,Canales-Williams,E27996,heather.williams2@apsva.us,Williamsburg MS,Staff,Staff
Eileen,Colihan,E2817,eileen.colihan@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Heather,Carey,E28320,heather.carey2@apsva.us,Williamsburg MS,Administrative Assistant,Staff
Silvia,Luna,E28326,silvia.luna@apsva.us,Williamsburg MS,Custodian,Facilities Staff
Holly,Vesilind,E28995,holly.vesilind@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Joseph,Keimig,E29009,joseph.keimig@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Lynne,Jackson,E29205,lynne.jackson@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Danny,La,E29574,danny.la@apsva.us,Williamsburg MS,Instructional Assistant,Instructional Staff
Carrie,Wielechowski,E30331,carrie.wielechowski@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Laura,Cahn,E31095,laura.cahn@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Amy,Flynn,E31130,amy.flynn@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Joseph,McBride,E31185,joseph.mcbride@apsva.us,Williamsburg MS,Teacher,Instructional Staff
David,Bell,E31227,david.bell@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Liliana,Martinez,E31229,liliana.martinez@apsva.us,Williamsburg MS,Administrative Assistant,Staff
Ashley,Bleakley,E31268,ashley.bleakley@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Steven,Brown,E31273,steven.brown3@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Derick,Joynes,E31363,derick.joynes@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Theresa,Andino,E31405,theresa.andino@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Nicole,Edmonds,E31441,nicole.edmonds@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Sophie,Rosenthal,E32133,sophie.rosenthal@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Paul,Gardner,E32172,paul.gardner@apsva.us,Williamsburg MS,"Teacher, Staff",Staff
Lillian,Tousley,E32274,lillian.tousley2@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Andrew,Nguyen,E32355,andrew.nguyen@apsva.us,Williamsburg MS,Staff,Staff
Ashley,Wardell,E32359,ashley.wardell@apsva.us,Williamsburg MS,Staff,Staff
Tristie,Purdie,E32399,tristie.purdie@apsva.us,Williamsburg MS,"Staff, Teacher",Instructional Staff
Carlos,Zamora Diaz,E32457,carlos.zamoradiaz@apsva.us,Williamsburg MS,Custodian,Facilities Staff
Joshua,Bruno,E32591,joshua.bruno@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Ann,Allred,E32632,ann.allred@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Melissa,Rankin,E32709,melissa.rankin@apsva.us,Williamsburg MS,Teacher,Instructional Staff
David,Sydney,E32713,david.sydney@apsva.us,Williamsburg MS,Instructional Assistant,Instructional Staff
Thomas,Williams,E33087,thomas.williams@apsva.us,Williamsburg MS,Staff,Staff
Nana,Agyekum,E33159,nana.agyekum@apsva.us,Williamsburg MS,Custodian,Facilities Staff
Lisa,Murphy,E4344,lisa.murphy@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Eric,Reiser,E488,eric.reiser@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Anne,Coia,E5057,anne.coia@apsva.us,Williamsburg MS,"Staff, Instructional Assistant",Instructional Staff
Ana,Gomez,E5296,ana.gomez@apsva.us,Williamsburg MS,Maintenance Supervisor,Facilities Staff
Ashley,Hogwood,E5770,ashley.hogwood@apsva.us,Williamsburg MS,Coordinator,Staff
Chris,Seeger,E6396,chris.seeger@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Margaret,Brown,E674,margaret.brown@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Chris,McDermott,E6993,chris.mcdermott@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Michael,Clark,E7198,michael.clark@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Sherilyn,Wall,E7236,sherilyn.wall@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Kimberly,Canonica,E743,kimberly.canonica@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Zoila,Flores,E7541,zoila.flores@apsva.us,Williamsburg MS,Custodian,Facilities Staff
Lisa,Young,E8353,lisa.young@apsva.us,Williamsburg MS,Teacher,Instructional Staff
John,Koutsouftikis,E8677,john.koutsouftikis@apsva.us,Williamsburg MS,Assistant Principal,Administrative Staff
Comfort,Nimoh,E9446,comfort.nimoh@apsva.us,Williamsburg MS,Custodian,Facilities Staff
Samantha,Teixeira,E9738,samantha.teixeira@apsva.us,Williamsburg MS,Teacher,Instructional Staff
Gretchen,Brenckle,E9834,gretchen.brenckle@apsva.us,Williamsburg MS,Teacher,Instructional Staff
William,Vanevera,E10323,william.vanevera@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Carol,Burger,E11237,carol.burger@apsva.us,HB Woodlawn,Instructional Assistant,Instructional Staff
Mignon,Kery,E1154,mignon.kery@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Kelly,Breedlove,E11579,kelly.breedlove@apsva.us,HB Woodlawn,n/a,Unclassified
George,Laumann,E12177,george.laumann@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Patti,Walsh,E12291,patti.walsh@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Eleanor,Reed,E12577,eleanor.reed@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Mark,Dickson,E13964,mark.dickson@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Marcos,Rubio,E14147,marcos.rubio@apsva.us,HB Woodlawn,Maintenance Supervisor,Facilities Staff
Daniel,Degracia,E14909,daniel.degracia@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Leigh,Buckley Altice,E15907,leigh.buckley@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Carrie,Goodfellow,E15950,carrie.goodfellow@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Kristale,Grant,E1603,kristale.grant@apsva.us,HB Woodlawn,Coordinator,Staff
Christina,Herrera,E1615,ana.herrera@apsva.us,HB Woodlawn,Administrative Assistant,Staff
Ana,Joya,E16193,ana.joya@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Reina,Malakoff,E16200,reina.malakoff@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Carl,Holmquist,E16254,carl.holmquist@apsva.us,HB Woodlawn,"Teacher, Staff",Staff
Risa,Browder,E16502,risa.browder@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Kristen,Miller,E16638,kristen.miller@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Vanessa,Piccorossi,E19190,vanessa.piccorossi@apsva.us,HB Woodlawn,Administrative Assistant,Staff
Katherine,Morrison-Taylor,E19294,k.morrisontaylor@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Jennifer,Goen,E19485,jennifer.goen@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Brian,Teixeira,E195,brian.teixeira@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Meghan,French,E19588,meghan.french@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Martha,Herrmann,E2043,martha.herrmann@apsva.us,HB Woodlawn,Instructional Assistant,Instructional Staff
Bill,Podolski,E20518,bill.podolski@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Anne,Rottinghaus,E20617,anne.rottinghaus@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Stephanie,Nichols,E21193,stephanie.nichols@apsva.us,HB Woodlawn,n/a,Unclassified
Daniel,Paris,E21735,dan.paris@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Christy,Gill,E22087,christy.gill@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Carolyn,Crumpler,E22092,carolyn.crumpler@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Rachel,Sens,E22116,rachel.sens@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Melissa,Martenak,E22203,melissa.martenak@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Steve,McKenney,E22337,steve.mckenney@apsva.us,HB Woodlawn,Teacher,Instructional Staff
John,Kauffman,E22543,john.kauffman@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Graham,McBride,E24083,graham.mcbride@apsva.us,HB Woodlawn,Assistant Principal,Administrative Staff
Kate,Seche,E24801,kate.seche@apsva.us,HB Woodlawn,Assistant Principal,Administrative Staff
Ackesha,Patrick,E24816,ackesha.patrick@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Christina,Lopez,E25274,christina.lopez@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Leslie,Keller,E25560,leslie.keller@apsva.us,HB Woodlawn,Staff,Staff
Michael,Cruse,E25787,michael.cruse@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Yue,Zhen,E26073,yue.zhen@apsva.us,HB Woodlawn,n/a,Unclassified
Leann,Smuthkochorn,E26171,leann.smuthkochorn2@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Connie,Williams,E26701,constance.williams@apsva.us,HB Woodlawn,"Instructional Assistant, Teacher",Instructional Staff
Margaret,Carpenter,E26719,margaret.carpenter@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Kristin,Kappmeyer,E2683,kristin.kappmeyer@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Travis,Reyes,E26847,travis.reyes@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Deidre,Reimers,E26910,deidre.reimers@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Randolph,Latimer,E27019,randolph.latimer@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Gabrielle,Frohock,E27043,gabrielle.frohock@apsva.us,HB Woodlawn,"Instructional Assistant, Teacher, Staff",Instructional Staff
Chontel,Walker,E27227,chontel.walker2@apsva.us,HB Woodlawn,Instructional Assistant,Instructional Staff
Deborah,Seto,E27483,deborah.seto@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Peggy,Gaines,E28264,peggy.gaines2@apsva.us,HB Woodlawn,Staff,Staff
Richard,Boyle,E28274,richard.boyle@apsva.us,HB Woodlawn,"Instructional Assistant, Teacher",Instructional Staff
Deborah,Patterson,E28345,debi.patterson@apsva.us,HB Woodlawn,Coordinator,Instructional Staff
Samantha,Taggart,E28608,samantha.taggart@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Eric,Young,E29001,eric.young@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Elizabeth,Hill,E29302,elizabeth.hill@apsva.us,HB Woodlawn,Staff,Staff
Alexandra,Bieniek,E29516,alexandra.bieniek2@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Tyler,Witman,E29961,tyler.witman@apsva.us,HB Woodlawn,n/a,Unclassified
Emily,Marter,E30136,emily.marter@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Rosario,Valencia Martinez,E30819,rosario.valencia@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Ana,Mendez,E30993,ana.mendez@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Kirsten,Albert,E31200,kirsten.albert@apsva.us,HB Woodlawn,Teacher,Instructional Staff
John,Brunschwyler,E31201,john.brunschwyler@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Kathleen,Akerley,E31228,kathleen.akerley@apsva.us,HB Woodlawn,"Teacher, Staff",Staff
John,Barendt,E31297,john.barendt@apsva.us,HB Woodlawn,"Teacher, Instructional Assistant",Instructional Staff
Hope,Lambert,E31404,hope.lambert@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Nelson,Rivera,E31440,nelson.rivera@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Yisehak,Woldemichael,E31561,yisehak.woldemichael@apsva.us,HB Woodlawn,Instructional Assistant,Instructional Staff
Ana,Salamanca,E31764,ana.salamanca@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Iris,Guzman Benitez,E31953,iris.guzmanbenitez@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Hang,Owen,E32108,hang.owen@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Taylor,Lawrence,E32391,taylor.lawrence2@apsva.us,HB Woodlawn,Staff,Staff
Kayla,Boston,E32720,kayla.boston@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Michael,Stowers,E32745,michael.stowers@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Nora,LeValley,E33037,nora.levalley@apsva.us,HB Woodlawn,Instructional Assistant,Instructional Staff
Olivia,Arellano,E33038,olivia.arellano@apsva.us,HB Woodlawn,Staff,Staff
Khin,Kyaw,E33077,khin.kyaw@apsva.us,HB Woodlawn,Staff,Staff
Kateri,Young,E33166,kateri.young@apsva.us,HB Woodlawn,n/a,Unclassified
Aysia,Moten,E33307,aysia.moten@apsva.us,HB Woodlawn,Instructional Assistant,Instructional Staff
Hala,Somo,E33364,hala.somo@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Xavier,Maxstadt,E33426,xavier.maxstadt@apsva.us,HB Woodlawn,Staff,Staff
Avery,Hiskey,E33460,avery.hiskey@apsva.us,HB Woodlawn,Instructional Assistant,Instructional Staff
Fatima,Posada-Bellaz,E3724,fatima.posadabellaz@apsva.us,HB Woodlawn,Administrative Assistant,Staff
Judith,Pendergast,E418,judith.pendergast@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Paul,Weiss,E4778,paul.weiss@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Sally,Moss,E4856,sally.moss@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Rebecca,Bieniek,E5175,rebecca.bieniek@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Nekya,Ball,E6385,nekya.ball@apsva.us,HB Woodlawn,Teacher,Instructional Staff
David,Contessa,E6935,david.contessa@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Daysi,Palomeque,E7136,daysi.palomeque@apsva.us,HB Woodlawn,Instructional Assistant,Instructional Staff
Faylinda,Kodis,E73,faylinda.kodis@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Catherine,Frum,E7827,catherine.frum@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Casey,Robinson,E7969,casey.robinson@apsva.us,HB Woodlawn,Principal,Unclassified
Kathy,Funes,E818,kathy.funes@apsva.us,HB Woodlawn,School Finance Officer,Staff
Liz,Waters,E8319,liz.waters@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Earnest,Gant,E8513,earnest.gant@apsva.us,HB Woodlawn,Custodian,Facilities Staff
Deneen,Snow,E8553,deneen.snow@apsva.us,HB Woodlawn,n/a,Unclassified
Ana,Castillo,E8718,ana.castillo@apsva.us,HB Woodlawn,Administrative Assistant,Staff
Michael,Coughlin,E880,michael.coughlin@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Stefan,Green,E9189,stefan.green@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Cecilia,Allen,E9765,cecilia.allen@apsva.us,HB Woodlawn,Teacher,Instructional Staff
Carlos,Ore Jimenez,E10178,carlos.orejimenez@apsva.us,Yorktown HS,Custodian,Facilities Staff
Robin,Nugent,E10940,robin.nugent@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tracy,Maguire,E1108,tracy.maguire@apsva.us,Yorktown HS,Teacher,Instructional Staff
Stephanie,Meadows,E11146,stephanie.meadows@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Heather,Akpata,E11201,heather.akpata@apsva.us,Yorktown HS,Teacher,Instructional Staff
Deborah,Waldron,E11292,deborah.waldron@apsva.us,Yorktown HS,Teacher,Instructional Staff
Cheryl,Stotler,E11517,cheryl.stotler@apsva.us,Yorktown HS,"Administrative Assistant, Assistant Director",Staff
Kelly,Breedlove,E11579,kelly.breedlove@apsva.us,Yorktown HS,n/a,Unclassified
Rachel,Tarr,E11845,rachel.tarr@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Sadauskas,E12020,rachel.sadauskas@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Daniel,Carroll,E1252,daniel.carroll@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Chikes,E1256,sarah.chikes@apsva.us,Yorktown HS,Teacher,Instructional Staff
Evan,Glasier,E12750,evan.glasier@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christina,Smith,E13293,christina.smith2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Suzanne,Evans,E13400,suzanne.evans@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Curt,Dorman,E13509,curt.dorman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Dolores,Rubalcava,E13684,dolores.rubalcava@apsva.us,Yorktown HS,Administrative Assistant,Staff
Marlon,Telleria,E13915,marlon.telleria@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thomas,Hartman,E14213,thomas.hartman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rafael,Espinoza,E14398,rafael.espinoza@apsva.us,Yorktown HS,Teacher,Instructional Staff
Enrique,Solorzano,E14417,enrique.solorzano@apsva.us,Yorktown HS,Teacher,Instructional Staff
William,Lomax,E15164,william.lomax@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Maria,O'Connell,E15724,maria.oconnell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Aaron,Schuetz,E15797,aaron.schuetz@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anthony,McPhee,E15809,anthony.mcphee@apsva.us,Yorktown HS,Staff,Staff
Jeffrey,Stahl,E1594,jeffrey.stahl@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Kevin,Bridwell,E16231,kevin.bridwell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thor,Young,E16266,thor.young@apsva.us,Yorktown HS,Teacher,Instructional Staff
Audrey,Chan,E16449,audrey.chan@apsva.us,Yorktown HS,Administrative Assistant,Staff
Caryn,Rahawi,E1654,caryn.rahawi@apsva.us,Yorktown HS,"Instructional Assistant, Staff",Instructional Staff
Kimberly,Cordell,E16587,kimberly.cordell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anne,Stewart,E16602,anner.stewart@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sara,Espinoza,E16644,sara.espinoza@apsva.us,Yorktown HS,Custodian,Facilities Staff
Kevin,Clark,E16676,kevin.clark@apsva.us,Yorktown HS,Principal,Unclassified
Paige,Hamrick,E16717,paige.hamrick@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrea,Rosegrant,E16841,andrea.rosegrant@apsva.us,Yorktown HS,Administrative Assistant,Staff
William,Marlow,E17026,william.marlow@apsva.us,Yorktown HS,Staff,Staff
Adrienne,Wright,E17040,adrienne.wright@apsva.us,Yorktown HS,Teacher,Instructional Staff
Md,Calabro,E1896,md.calabro@apsva.us,Yorktown HS,Teacher,Instructional Staff
Claire,Shreeve,E19162,claire.shreeve@apsva.us,Yorktown HS,Teacher,Instructional Staff
Gregory,Beer,E19221,gregory.beer@apsva.us,Yorktown HS,Athletic Coach,Staff
Wilson,Desousa,E19222,wilson.desousa@apsva.us,Yorktown HS,Athletic Coach,Staff
Natalie,Roy,E19225,natalie.roy@apsva.us,Yorktown HS,Athletic Coach,Staff
Allyson,McKowen,E1934,allyson.mckowen@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Murphy,E19351,amy.murphy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Peter,Ketcham-Colwill,E19492,peter.ketchumcolwill@apsva.us,Yorktown HS,Staff,Staff
Cristina,Rivera,E19518,cristina.rivera@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christopher,Roland,E19591,christopher.roland@apsva.us,Yorktown HS,Staff,Staff
Laura,Albright,E19652,laura.albright@apsva.us,Yorktown HS,Teacher,Instructional Staff
Allison,Gilbert,E19701,allison.gilbert@apsva.us,Yorktown HS,Teacher,Instructional Staff
Maria,Suaznabar,E19729,maria.suaznabar@apsva.us,Yorktown HS,Custodian,Facilities Staff
Robert,Shaw,E19926,robert.shaw@apsva.us,Yorktown HS,Athletic Coach,Staff
James,Slaughter,E19960,james.slaughter@apsva.us,Yorktown HS,Athletic Coach,Staff
Juliet,Boakye,E20064,juliet.boakye@apsva.us,Yorktown HS,Custodian,Facilities Staff
Lottie,Mack,E2019,lottie.mack@apsva.us,Yorktown HS,School Finance Officer,Staff
Juanice,Jenkins,E20292,juanice.jenkins@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Sherry,Lord,E20497,sherry.lord@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tony,Price,E20510,tony.price@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lisa,Troiano,E20614,lisa.troiano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alesandra,Bakaj,E20739,alesandra.bakaj@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tamara,Molina,E20965,tamara.molina@apsva.us,Yorktown HS,Teacher,Instructional Staff
Richard,Gibson,E21109,richard.gibson@apsva.us,Yorktown HS,"Teacher, Staff",Staff
David,Mower,E21119,david.mower@apsva.us,Yorktown HS,Teacher,Instructional Staff
Emmet,Conroy,E21275,emmet.conroy@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Tom,Norton,E21295,tom.norton@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nancie,Graf-Sidiropoulos,E21339,nancie.sidiropoulos@apsva.us,Yorktown HS,Teacher,Instructional Staff
Roseline,Berger,E21369,roseline.berger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Paul,Hessler,E21430,paul.hessler@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Evan,Ruffner,E21802,evan.ruffner@apsva.us,Yorktown HS,"Athletic Coach, Teacher",Staff
Susan,Harrison,E21916,susan.harrison@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kenneth,Mandel,E22066,kenneth.mandel@apsva.us,Yorktown HS,Teacher,Instructional Staff
Mark,Rooks,E2222,mark.rooks@apsva.us,Yorktown HS,Director,Administrative Staff
Heather,Sutphin,E22225,heather.sutphin@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Sean,Kinnard,E22255,sean.kinnard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Justine,Springberg,E22450,justine.springberg@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amali,Amarasinghe,E22641,amali.amarasinghe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lindsay,Kennedy,E22838,lindsay.kennedy@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Mary Ann,Mahan,E22880,maryann.mahan@apsva.us,Yorktown HS,Administrative Assistant,Staff
Jessica,Reeve,E22923,jessica.reeve@apsva.us,Yorktown HS,Teacher,Instructional Staff
Wendy,Boone,E23063,wendy.boone@apsva.us,Yorktown HS,Coordinator,Instructional Staff
Morgan,Froy,E23169,morgan.froy@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Jennifer,Falbo,E23284,jennifer.falbo@apsva.us,Yorktown HS,Administrative Assistant,Staff
Janet,Frimpong,E23536,janet.frimpong@apsva.us,Yorktown HS,Custodian,Facilities Staff
Brianna McHugh,McArthur,E23752,brianna.mcarthur@apsva.us,Yorktown HS,Teacher,Instructional Staff
Victoria,Walchak,E23781,victoria.walchak@apsva.us,Yorktown HS,Teacher,Instructional Staff
Felix,Carcamo,E23924,felix.carcamo@apsva.us,Yorktown HS,Custodian,Facilities Staff
Brittany,Garner,E23927,brittany.garner@apsva.us,Yorktown HS,Athletic Coach,Staff
Gwendolyn,Nixon,E23992,gwendolyn.nixon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sumer,Majid,E24016,sumer.majid@apsva.us,Yorktown HS,n/a,Unclassified
Barbara,Marks,E24029,barbara.marks@apsva.us,Yorktown HS,Teacher,Instructional Staff
Colin,Sundwick,E24041,colin.sundwick@apsva.us,Yorktown HS,Staff,Staff
Zahra,Castellano,E24053,zahra.castellano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Daniel,Barkan,E24081,daniel.barkan@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Brannon,Burnett,E24286,brannon.burnett@apsva.us,Yorktown HS,Staff,Staff
Steven,McArthur,E24314,steven.mcarthur@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kelly,Dillon,E24405,kelly.dillon@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Zainab,Kufaishi,E24474,zainab.kufaishi@apsva.us,Yorktown HS,n/a,Unclassified
Sheena,Eldred,E24842,sheena.eldred@apsva.us,Yorktown HS,Athletic Coach,Staff
Kelley,Parent,E24862,kelley.parent@apsva.us,Yorktown HS,Teacher,Instructional Staff
Olivia,Shipley,E24877,olivia.shipley@apsva.us,Yorktown HS,Staff,Staff
Veronica,Todd,E24885,veronica.bailey@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Lanay,Burke,E25003,lanay.burke@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nancy,Hooper,E25221,nancy.hooper@apsva.us,Yorktown HS,Athletic Coach,Staff
Jonathan,Daniel,E25271,jonathan.daniel@apsva.us,Yorktown HS,Teacher,Instructional Staff
Misila,Ceasar,E25283,misila.ceasar@apsva.us,Yorktown HS,Custodian,Facilities Staff
Beau,Obetts,E2547,beau.obetts@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jennifer,Webster,E25470,jennifer.webster@apsva.us,Yorktown HS,Teacher,Instructional Staff
Susan,Rochard,E25544,susan.rochard@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Diane,Holland,E25559,diane.holland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Benjamin,Luong,E25575,benjamin.luong@apsva.us,Yorktown HS,Custodian,Facilities Staff
Christine,Bonnefil,E25723,christine.bonnefil@apsva.us,Yorktown HS,Teacher,Instructional Staff
Virginia,Maloney,E25758,virginia.maloney@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christine,Bolon,E25774,christine.bolon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Adam,Sheppard,E25819,adam.sheppard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kevin,Thompson,E25879,kevin.thompson@apsva.us,Yorktown HS,Athletic Coach,Staff
Kimberly,Graver,E25901,kimberly.graver@apsva.us,Yorktown HS,Teacher,Instructional Staff
Deborah,Seidenstein,E25909,deborah.seidenstein@apsva.us,Yorktown HS,Teacher,Instructional Staff
John,Skaggs,E26008,john.skaggs@apsva.us,Yorktown HS,Athletic Coach,Staff
Yue,Zhen,E26073,yue.zhen@apsva.us,Yorktown HS,n/a,Unclassified
Kevin,Robertson,E26078,kevin.robertson@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Taliah,Graves,E26258,taliah.graves2@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jenny,Polio,E26345,jenny.polio@apsva.us,Yorktown HS,Custodian,Facilities Staff
Shari,Benites,E2657,shari.benites@apsva.us,Yorktown HS,Teacher,Instructional Staff
Joseph,Crawford,E26602,joseph.crawford@apsva.us,Yorktown HS,Staff,Staff
Andrew,Holland,E26617,andrew.holland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Cohen,E26657,amy.cohen@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Juan,Garay,E26712,juan.garay2@apsva.us,Yorktown HS,Custodian,Facilities Staff
Meg,Hunter,E26854,meg.hunter@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jessica,Paz-Soldan,E26889,jessica.pazsoldan@apsva.us,Yorktown HS,Teacher,Instructional Staff
Teresa,Cordova,E26937,teresa.cordova@apsva.us,Yorktown HS,Teacher,Instructional Staff
Marlyn,Bakalli,E26952,marlyn.bakalli@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Rebecca,Chervin,E26990,rebecca.chervin@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danielle,Jones,E26993,danielle.jones@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Ebenezer,Oware,E27033,ebenezer.oware@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Jhaquelin,Loayza Vela,E27166,jhaquelin.loayzavela@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Nicholas,Haring,E27408,nicholas.haring2@apsva.us,Yorktown HS,Staff,Staff
Juwahn,Nash,E27448,juwahna.nash@apsva.us,Yorktown HS,Custodian,Facilities Staff
Carlos,Aranda,E27470,carlos.aranda2@apsva.us,Yorktown HS,Staff,Staff
Sonja,Schulken,E27505,sonja.schulken2@apsva.us,Yorktown HS,Administrative Assistant,Staff
Steven,White,E27556,steven.white@apsva.us,Yorktown HS,Staff,Staff
Jocelyn,Mullins,E27627,jocelyn.mullins2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Hannah,Davis,E27701,hannah.davis@apsva.us,Yorktown HS,Staff,Staff
Corey,Klein,E27727,corey.klein@apsva.us,Yorktown HS,Teacher,Instructional Staff
Erika,Lucas,E27738,erika.lucas@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christopher,McIntosh,E27744,christopher.mcintosh@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Amy,Snyder,E27748,amy.snyder@apsva.us,Yorktown HS,Teacher,Instructional Staff
Clare,Davis,E27793,clare.davis@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Emili,Serghie,E27804,emili.serghie@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danilo,Loor,E27860,danilo.loor@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ryan,Larcamp,E27944,ryan.larcamp@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anai,Perez,E28032,anai.perez@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Natalie,Pulsifer,E28056,natalie.pulsifer@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Mindy,Straley,E28126,mindy.straley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Julia,Hawkins,E28165,julia.hawkins@apsva.us,Yorktown HS,Teacher,Instructional Staff
Molly,Gormley,E28206,molly.gormley@apsva.us,Yorktown HS,Staff,Staff
Kala,Craddock Mcintosh,E28261,kala.mcintosh@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
William,Lane,E28305,william.lane@apsva.us,Yorktown HS,Staff,Staff
Stephanie,Moreno,E28355,stephanie.moreno@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Marwa,Badran,E28395,marwa.badran2@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Vasilakos,E28436,andrew.vasilakos2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Emily Miranda,Ward,E28491,emily.ward@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Andrea,Pfiester,E28502,andrea.pfiester@apsva.us,Yorktown HS,Staff,Staff
Sandy,Dane,E28514,sandy.dane@apsva.us,Yorktown HS,Administrative Assistant,Staff
Ali,Samey,E28518,ali.samey@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Adams,E28554,andrew.adams@apsva.us,Yorktown HS,Staff,Staff
Freweini,Berhane,E28691,freweini.berhane@apsva.us,Yorktown HS,Administrative Assistant,Staff
Anita,Van Harten Cater,E28722,anita.vanhartencater@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Dani,Seltzer,E28762,dani.seltzer@apsva.us,Yorktown HS,Staff,Staff
Sally,Englander,E28772,sally.englander@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nyla,Walker,E28850,nyla.walker@apsva.us,Yorktown HS,Staff,Staff
Surender,Raut,E28913,surender.raut@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ruth,Mohr,E28970,ruth.mohr@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ryan,Zito,E28975,ryan.zito@apsva.us,Yorktown HS,Teacher,Instructional Staff
Scott,Painter,E29023,scott.painter@apsva.us,Yorktown HS,Teacher,Instructional Staff
Troy,Olsen,E29031,troy.olsen@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Jeffrey,Klein,E29056,jeffrey.klein@apsva.us,Yorktown HS,Teacher,Instructional Staff
Colissa,Huggins,E29165,colissa.huggins@apsva.us,Yorktown HS,Staff,Staff
Austin,Hamill,E29196,austin.hamill@apsva.us,Yorktown HS,Teacher,Instructional Staff
Michael,Lovrencic,E2920,michael.lovrencic@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christine,Murphy,E29219,christine.murphy@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jenny,Tran,E29235,jenny.tran@apsva.us,Yorktown HS,Staff,Staff
Tyler,Monroe,E29265,tyler.monroe@apsva.us,Yorktown HS,Staff,Staff
Torin,Ortmayer,E29384,torin.ortmayer@apsva.us,Yorktown HS,Staff,Staff
Amy,Sutton,E29486,amy.sutton@apsva.us,Yorktown HS,Staff,Staff
Ashley,Moore,E29582,ashley.moore@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lan,Lai,E29589,lan.lai@apsva.us,Yorktown HS,Custodian,Facilities Staff
Victoria,Soesbee,E29607,victoria.soesbee@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jonathan,Shears,E29655,jonathan.shears@apsva.us,Yorktown HS,Staff,Staff
Christopher,Crowe,E29669,christopher.crowe@apsva.us,Yorktown HS,Staff,Staff
Madleine,Brennan,E29683,madleine.brennan@apsva.us,Yorktown HS,Staff,Staff
Carolyn,Kroeger,E29916,carolyn.kroeger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Steven,Tinter,E29928,steven.tinter@apsva.us,Yorktown HS,Staff,Staff
Meghan,Smeenk,E29989,meghan.smeenk@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jamie,Cooke,E30001,jamie.cooke@apsva.us,Yorktown HS,Teacher,Instructional Staff
Brian,Dean,E30015,brian.dean@apsva.us,Yorktown HS,Teacher,Instructional Staff
Oana,Newcombe,E30100,oana.newcombe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danielle,McCarthy,E30107,danielle.mccarthy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Wilson,E30116,sarah.wilson@apsva.us,Yorktown HS,Teacher,Instructional Staff
Erin,Orourke,E30137,erin.orourke@apsva.us,Yorktown HS,Staff,Staff
Jacob,Dumford,E30266,jacob.dumford@apsva.us,Yorktown HS,Staff,Staff
Carson,Kramer,E30303,carson.kramer2@apsva.us,Yorktown HS,"Instructional Assistant, Teacher",Instructional Staff
Elizabeth,Herrington,E30421,elizabeth.herrington@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Edward,Lauber,E30447,edward.lauber@apsva.us,Yorktown HS,Teacher,Instructional Staff
Robert,Terry,E30452,robert.terry@apsva.us,Yorktown HS,Staff,Staff
Alani,Kravitz,E30468,alani.kravitz@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Covington,E30469,rachel.covington@apsva.us,Yorktown HS,Teacher,Instructional Staff
Britnee,Wade,E30485,britnee.wade@apsva.us,Yorktown HS,Administrative Assistant,Staff
Rachel,Dickenson,E30520,rachel.dickenson@apsva.us,Yorktown HS,Staff,Staff
Diana,Gamez Maravilla,E30714,diana.gamezmaravilla@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Dane,E30730,andrew.dane@apsva.us,Yorktown HS,Staff,Staff
Shannon,Aikens,E30741,shannon.aikens@apsva.us,Yorktown HS,Staff,Staff
Glorinda,Perez Mendez,E30746,glorinda.perezmendez@apsva.us,Yorktown HS,Custodian,Facilities Staff
Colin,Evans,E30951,colin.evans@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Audrey,Vasquez-Rivera,E31131,audrey.vasquezrivera@apsva.us,Yorktown HS,Teacher,Instructional Staff
Blake,Edwards,E31168,blake.edwards@apsva.us,Yorktown HS,Staff,Staff
Joseph,McBride,E31185,joseph.mcbride@apsva.us,Yorktown HS,Staff,Staff
Danielle,Dessaso,E31222,danielle.dessaso@apsva.us,Yorktown HS,Teacher,Instructional Staff
Brian,Wiltshire,E31236,brian.wiltshire@apsva.us,Yorktown HS,Teacher,Instructional Staff
Elizabeth,Hall,E31249,elizabeth.hall@apsva.us,Yorktown HS,Teacher,Instructional Staff
Daniela,Sava,E31266,daniela.sava@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Morgan,E31361,sarah.morgan@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Blaine,Kaltman,E31398,blaine.kaltman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Theresa,Andino,E31405,theresa.andino@apsva.us,Yorktown HS,Teacher,Instructional Staff
Shelby,McDavid,E31412,shelby.mcdavid@apsva.us,Yorktown HS,Staff,Staff
Paul,Fordjour,E31469,paul.fordjour@apsva.us,Yorktown HS,Custodian,Facilities Staff
Sang,Yoon,E31491,sang.yoon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Shepherd,E31548,amy.shepherd@apsva.us,Yorktown HS,Staff,Staff
Gabriel,Colmenarez Fuentes,E31616,gabriel.colmenarez@apsva.us,Yorktown HS,Staff,Staff
Cory,Allgood,E31694,cory.allgood@apsva.us,Yorktown HS,Staff,Staff
Erik,Thomas,E31698,erik.thomas@apsva.us,Yorktown HS,Staff,Staff
Ashley,Candeletti,E31765,ashley.candeletti@apsva.us,Yorktown HS,Staff,Staff
Elizabeth,Benedetto,E31829,elizabeth.benedetto@apsva.us,Yorktown HS,Staff,Staff
Shannon,Lewandowski,E31830,shannon.lewandowski@apsva.us,Yorktown HS,Staff,Staff
Lucretia,Kebreau,E31876,lucretia.kebreau@apsva.us,Yorktown HS,Teacher,Instructional Staff
Laney,Placido,E31947,laney.placido@apsva.us,Yorktown HS,Staff,Staff
Anthony,Dayse,E32085,anthony.dayse@apsva.us,Yorktown HS,Teacher,Instructional Staff
Neil,Kelly,E32087,neil.kelly@apsva.us,Yorktown HS,Teacher,Instructional Staff
Doria,Smith,E32201,doria.smith@apsva.us,Yorktown HS,Staff,Staff
Sophia,Zavada,E32213,sophia.zavada@apsva.us,Yorktown HS,Staff,Staff
Kaitlyn,Bryan,E32218,kaitlyn.bryan@apsva.us,Yorktown HS,Staff,Staff
Brandon,Glass,E32237,brandon.glass@apsva.us,Yorktown HS,Staff,Staff
Ashley,Gardner,E32248,ashley.gardner@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Amy,Marlow,E32252,amy.marlow@apsva.us,Yorktown HS,Staff,Staff
Kaitlin,Luncher,E32259,kaitlin.luncher@apsva.us,Yorktown HS,Staff,Staff
Madeline,Carzon,E32271,madeline.carzon@apsva.us,Yorktown HS,Staff,Staff
Graeme,Fineman,E32275,graeme.fineman@apsva.us,Yorktown HS,"Instructional Assistant, Staff",Staff
Marissa,Shaw,E32281,marissa.shaw@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Linda,Kyei,E32282,linda.kyei@apsva.us,Yorktown HS,Custodian,Facilities Staff
Collin,Johnston,E32295,collin.johnston@apsva.us,Yorktown HS,Staff,Staff
Emily,Stewart,E32299,emily.stewart@apsva.us,Yorktown HS,Staff,Staff
Chloe,Bergere,E32356,chloe.bergere@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Ronald,Avila,E32372,ronald.avila@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jeffries,Lhee,E32386,jeffries.lhee@apsva.us,Yorktown HS,Staff,Staff
Eric,Del Rosso,E32401,eric.delrosso@apsva.us,Yorktown HS,Staff,Staff
Elaf,Al Rushdawi,E32428,elaf.alrushdawi@apsva.us,Yorktown HS,Staff,Staff
Erin,Boyle,E32462,erin.boyle@apsva.us,Yorktown HS,Staff,Staff
Margot,Edinger,E32464,margot.edinger@apsva.us,Yorktown HS,Staff,Staff
Isabel,Rogers,E32506,isabel.rogers@apsva.us,Yorktown HS,Staff,Staff
Shelby,Thurgood,E32562,shelby.thurgood@apsva.us,Yorktown HS,Teacher,Instructional Staff
Joseph,Witkowski,E32573,joseph.witkowski@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jay,Conley,E32579,jay.conley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alexis,Andre,E32588,alexis.andre@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Reed,E32592,rachel.reed@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrew,Dabney,E32598,andrew.dabney@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jonia,Holley,E32599,jonia.holley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Dane,Huling,E32678,dane.huling@apsva.us,Yorktown HS,Teacher,Instructional Staff
Max,Adamo,E32680,max.adamo2@apsva.us,Yorktown HS,Staff,Staff
Charlotte,Reilly,E32684,charlotte.reilly@apsva.us,Yorktown HS,Staff,Staff
Allison,Loranger,E32717,allison.loranger@apsva.us,Yorktown HS,Staff,Staff
Hannah,Repke,E32731,hannah.repke@apsva.us,Yorktown HS,Staff,Staff
Aidan,Leddy,E32735,aidan.leddy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ricardo,Huaman,E32790,ricardo.huaman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Toshiko,Parrish,E32825,toshiko.parrish@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thuan,Nguyen-Lakrik,E32835,thuan.nguyenlakrik@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jake,Oster,E32842,jake.oster@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alexsis,Grate,E32892,alexsis.grate@apsva.us,Yorktown HS,Staff,Staff
Rama,Najib,E32938,rama.najib@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tayvon,Brown,E32955,tayvon.brown@apsva.us,Yorktown HS,Staff,Staff
Albert,Owusu,E32959,albert.owusu@apsva.us,Yorktown HS,Custodian,Facilities Staff
Setareh,Farzinfard,E32963,setareh.farzinfard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Shannon,Wood Graham,E32972,shannon.woodgraham@apsva.us,Yorktown HS,Staff,Staff
Benjamin,St. Pierre,E33067,benjamin.stpierre@apsva.us,Yorktown HS,Staff,Staff
Morgan,Stahl,E33104,morgan.stahl@apsva.us,Yorktown HS,Staff,Staff
Lucy,Ayers,E33153,lucy.ayers@apsva.us,Yorktown HS,Staff,Staff
Daniel,Chovil,E33191,daniel.chovil@apsva.us,Yorktown HS,Staff,Staff
Miguel,Gonzalez,E33197,miguel.gonzalez@apsva.us,Yorktown HS,Staff,Staff
Rianne,Connelly,E33234,rianne.connelly@apsva.us,Yorktown HS,Staff,Staff
Meghan,Hamby,E33236,meghan.hamby@apsva.us,Yorktown HS,Staff,Staff
Sophia,Ebanks,E33253,sophia.ebanks@apsva.us,Yorktown HS,Staff,Staff
Riley,Sophy,E33254,riley.sophy@apsva.us,Yorktown HS,Staff,Staff
Hala,Somo,E33364,hala.somo@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nicholas,Beadles,E33371,nicholas.beadles@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Holden,Gunster,E33393,holden.gunster@apsva.us,Yorktown HS,Staff,Staff
Stachia,Reuwsaat,E33411,stachia.reuwsaat@apsva.us,Yorktown HS,Staff,Staff
Ashley,Sanchez Garcia,E33441,ashley.sanchezgarcia@apsva.us,Yorktown HS,Staff,Staff
Eli,Lorenzi,E33444,eli.lorenzi@apsva.us,Yorktown HS,Staff,Staff
Charles,Molloy,E33485,charles.molloy@apsva.us,Yorktown HS,Staff,Staff
Harrison,Engoren,E33532,harrison.engoren@apsva.us,Yorktown HS,Staff,Staff
Christopher,Monahan,E33580,christopher.monahan@apsva.us,Yorktown HS,Teacher,Instructional Staff
Bruce,Hanson,E3781,bruce.hanson@apsva.us,Yorktown HS,Teacher,Instructional Staff
Meagan,Cummings,E3835,meagan.cummings@apsva.us,Yorktown HS,Teacher,Instructional Staff
Carol,Thompson,E4171,carol.thompson@apsva.us,Yorktown HS,Registrar,Staff
Matthew,Rinker,E4334,matthew.rinker@apsva.us,Yorktown HS,Teacher,Instructional Staff
Monica,Stroik,E4370,monica.stroik@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jon,Schildknecht,E4510,jon.schildknecht@apsva.us,Yorktown HS,Teacher,Instructional Staff
Michael,Krulfeld,E5119,michael.krulfeld@apsva.us,Yorktown HS,Director,Administrative Staff
Jennifer,Shearin,E5426,jennifer.shearin@apsva.us,Yorktown HS,Teacher,Instructional Staff
Richard,Avila,E5431,richard.avila@apsva.us,Yorktown HS,Teacher,Instructional Staff
Constance,Campana,E5462,constance.campana@apsva.us,Yorktown HS,Administrative Assistant,Staff
Maria,Pena,E5476,maria.pena@apsva.us,Yorktown HS,Custodian,Facilities Staff
Kathryn,Speakman,E5591,kathryn.speakman@apsva.us,Yorktown HS,"Staff, Teacher",Instructional Staff
Harold,Andersen,E568,harold.andersen@apsva.us,Yorktown HS,Teacher,Instructional Staff
Allen,Beland,E5713,allen.beland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rebecca,Gibbs Jones,E5783,rebecca.jones@apsva.us,Yorktown HS,Teacher,Instructional Staff
Scott,McKeown,E5839,scott.mckeown@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Christine,Brown,E6233,christine.brown@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Chris,Seeger,E6396,chris.seeger@apsva.us,Yorktown HS,Staff,Staff
Hatice,Danisir,E6635,hatice.danisir@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Linda,Hudson-O'Neill,E6913,linda.oneill@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrew,Prantner,E7295,andrew.prantner@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Cathy,Day,E7533,cathy.day@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Michael,Smalley,E7838,michael.smalley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Chrissy,Wiedemann,E7977,chrissy.wiedemann@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Eileen,Wagner,E8304,eileen.wagner@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ana,Ratcliffe,E8401,ana.ratcliffe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Devaughn,Drayton,E8414,devaughn.drayton@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Rebecca,Bonzano,E8600,rebecca.bonzano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Svetlana,Marchenko,E8840,svetlana.marchenko@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Patricia,Rojas,E9078,patricia.rojas@apsva.us,Yorktown HS,Administrative Assistant,Staff
Laurie,Vena,E9131,laurie.vena@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alex,Hicks,E9246,alex.hicks@apsva.us,Yorktown HS,Teacher,Instructional Staff
Robin,Grunberger,E9248,robin.grunberger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lucy,Middelthon,E9657,lucy.middelthon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Chris,Kaldahl,E9833,chris.kaldahl@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jose,Moreno,E9991,jose.moreno@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Carlos,Ore Jimenez,E10178,carlos.orejimenez@apsva.us,Yorktown HS,Custodian,Facilities Staff
Robin,Nugent,E10940,robin.nugent@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tracy,Maguire,E1108,tracy.maguire@apsva.us,Yorktown HS,Teacher,Instructional Staff
Stephanie,Meadows,E11146,stephanie.meadows@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Heather,Akpata,E11201,heather.akpata@apsva.us,Yorktown HS,Teacher,Instructional Staff
Deborah,Waldron,E11292,deborah.waldron@apsva.us,Yorktown HS,Teacher,Instructional Staff
Cheryl,Stotler,E11517,cheryl.stotler@apsva.us,Yorktown HS,"Administrative Assistant, Assistant Director",Staff
Kelly,Breedlove,E11579,kelly.breedlove@apsva.us,Yorktown HS,n/a,Unclassified
Rachel,Tarr,E11845,rachel.tarr@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Sadauskas,E12020,rachel.sadauskas@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Daniel,Carroll,E1252,daniel.carroll@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Chikes,E1256,sarah.chikes@apsva.us,Yorktown HS,Teacher,Instructional Staff
Evan,Glasier,E12750,evan.glasier@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christina,Smith,E13293,christina.smith2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Suzanne,Evans,E13400,suzanne.evans@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Curt,Dorman,E13509,curt.dorman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Dolores,Rubalcava,E13684,dolores.rubalcava@apsva.us,Yorktown HS,Administrative Assistant,Staff
Marlon,Telleria,E13915,marlon.telleria@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thomas,Hartman,E14213,thomas.hartman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rafael,Espinoza,E14398,rafael.espinoza@apsva.us,Yorktown HS,Teacher,Instructional Staff
Enrique,Solorzano,E14417,enrique.solorzano@apsva.us,Yorktown HS,Teacher,Instructional Staff
William,Lomax,E15164,william.lomax@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Maria,O'Connell,E15724,maria.oconnell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Aaron,Schuetz,E15797,aaron.schuetz@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anthony,McPhee,E15809,anthony.mcphee@apsva.us,Yorktown HS,Staff,Staff
Jeffrey,Stahl,E1594,jeffrey.stahl@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Kevin,Bridwell,E16231,kevin.bridwell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thor,Young,E16266,thor.young@apsva.us,Yorktown HS,Teacher,Instructional Staff
Audrey,Chan,E16449,audrey.chan@apsva.us,Yorktown HS,Administrative Assistant,Staff
Caryn,Rahawi,E1654,caryn.rahawi@apsva.us,Yorktown HS,"Instructional Assistant, Staff",Instructional Staff
Kimberly,Cordell,E16587,kimberly.cordell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anne,Stewart,E16602,anner.stewart@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sara,Espinoza,E16644,sara.espinoza@apsva.us,Yorktown HS,Custodian,Facilities Staff
Kevin,Clark,E16676,kevin.clark@apsva.us,Yorktown HS,Principal,Unclassified
Paige,Hamrick,E16717,paige.hamrick@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrea,Rosegrant,E16841,andrea.rosegrant@apsva.us,Yorktown HS,Administrative Assistant,Staff
William,Marlow,E17026,william.marlow@apsva.us,Yorktown HS,Staff,Staff
Adrienne,Wright,E17040,adrienne.wright@apsva.us,Yorktown HS,Teacher,Instructional Staff
Md,Calabro,E1896,md.calabro@apsva.us,Yorktown HS,Teacher,Instructional Staff
Claire,Shreeve,E19162,claire.shreeve@apsva.us,Yorktown HS,Teacher,Instructional Staff
Gregory,Beer,E19221,gregory.beer@apsva.us,Yorktown HS,Athletic Coach,Staff
Wilson,Desousa,E19222,wilson.desousa@apsva.us,Yorktown HS,Athletic Coach,Staff
Natalie,Roy,E19225,natalie.roy@apsva.us,Yorktown HS,Athletic Coach,Staff
Allyson,McKowen,E1934,allyson.mckowen@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Murphy,E19351,amy.murphy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Peter,Ketcham-Colwill,E19492,peter.ketchumcolwill@apsva.us,Yorktown HS,Staff,Staff
Cristina,Rivera,E19518,cristina.rivera@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christopher,Roland,E19591,christopher.roland@apsva.us,Yorktown HS,Staff,Staff
Laura,Albright,E19652,laura.albright@apsva.us,Yorktown HS,Teacher,Instructional Staff
Allison,Gilbert,E19701,allison.gilbert@apsva.us,Yorktown HS,Teacher,Instructional Staff
Maria,Suaznabar,E19729,maria.suaznabar@apsva.us,Yorktown HS,Custodian,Facilities Staff
Robert,Shaw,E19926,robert.shaw@apsva.us,Yorktown HS,Athletic Coach,Staff
James,Slaughter,E19960,james.slaughter@apsva.us,Yorktown HS,Athletic Coach,Staff
Juliet,Boakye,E20064,juliet.boakye@apsva.us,Yorktown HS,Custodian,Facilities Staff
Lottie,Mack,E2019,lottie.mack@apsva.us,Yorktown HS,School Finance Officer,Staff
Juanice,Jenkins,E20292,juanice.jenkins@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Sherry,Lord,E20497,sherry.lord@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tony,Price,E20510,tony.price@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lisa,Troiano,E20614,lisa.troiano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alesandra,Bakaj,E20739,alesandra.bakaj@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tamara,Molina,E20965,tamara.molina@apsva.us,Yorktown HS,Teacher,Instructional Staff
Richard,Gibson,E21109,richard.gibson@apsva.us,Yorktown HS,"Teacher, Staff",Staff
David,Mower,E21119,david.mower@apsva.us,Yorktown HS,Teacher,Instructional Staff
Emmet,Conroy,E21275,emmet.conroy@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Tom,Norton,E21295,tom.norton@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nancie,Graf-Sidiropoulos,E21339,nancie.sidiropoulos@apsva.us,Yorktown HS,Teacher,Instructional Staff
Roseline,Berger,E21369,roseline.berger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Paul,Hessler,E21430,paul.hessler@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Evan,Ruffner,E21802,evan.ruffner@apsva.us,Yorktown HS,"Athletic Coach, Teacher",Staff
Susan,Harrison,E21916,susan.harrison@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kenneth,Mandel,E22066,kenneth.mandel@apsva.us,Yorktown HS,Teacher,Instructional Staff
Mark,Rooks,E2222,mark.rooks@apsva.us,Yorktown HS,Director,Administrative Staff
Heather,Sutphin,E22225,heather.sutphin@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Sean,Kinnard,E22255,sean.kinnard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Justine,Springberg,E22450,justine.springberg@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amali,Amarasinghe,E22641,amali.amarasinghe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lindsay,Kennedy,E22838,lindsay.kennedy@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Mary Ann,Mahan,E22880,maryann.mahan@apsva.us,Yorktown HS,Administrative Assistant,Staff
Jessica,Reeve,E22923,jessica.reeve@apsva.us,Yorktown HS,Teacher,Instructional Staff
Wendy,Boone,E23063,wendy.boone@apsva.us,Yorktown HS,Coordinator,Instructional Staff
Morgan,Froy,E23169,morgan.froy@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Jennifer,Falbo,E23284,jennifer.falbo@apsva.us,Yorktown HS,Administrative Assistant,Staff
Janet,Frimpong,E23536,janet.frimpong@apsva.us,Yorktown HS,Custodian,Facilities Staff
Brianna McHugh,McArthur,E23752,brianna.mcarthur@apsva.us,Yorktown HS,Teacher,Instructional Staff
Victoria,Walchak,E23781,victoria.walchak@apsva.us,Yorktown HS,Teacher,Instructional Staff
Felix,Carcamo,E23924,felix.carcamo@apsva.us,Yorktown HS,Custodian,Facilities Staff
Brittany,Garner,E23927,brittany.garner@apsva.us,Yorktown HS,Athletic Coach,Staff
Gwendolyn,Nixon,E23992,gwendolyn.nixon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sumer,Majid,E24016,sumer.majid@apsva.us,Yorktown HS,n/a,Unclassified
Barbara,Marks,E24029,barbara.marks@apsva.us,Yorktown HS,Teacher,Instructional Staff
Colin,Sundwick,E24041,colin.sundwick@apsva.us,Yorktown HS,Staff,Staff
Zahra,Castellano,E24053,zahra.castellano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Daniel,Barkan,E24081,daniel.barkan@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Brannon,Burnett,E24286,brannon.burnett@apsva.us,Yorktown HS,Staff,Staff
Steven,McArthur,E24314,steven.mcarthur@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kelly,Dillon,E24405,kelly.dillon@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Zainab,Kufaishi,E24474,zainab.kufaishi@apsva.us,Yorktown HS,n/a,Unclassified
Sheena,Eldred,E24842,sheena.eldred@apsva.us,Yorktown HS,Athletic Coach,Staff
Kelley,Parent,E24862,kelley.parent@apsva.us,Yorktown HS,Teacher,Instructional Staff
Olivia,Shipley,E24877,olivia.shipley@apsva.us,Yorktown HS,Staff,Staff
Veronica,Todd,E24885,veronica.bailey@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Lanay,Burke,E25003,lanay.burke@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nancy,Hooper,E25221,nancy.hooper@apsva.us,Yorktown HS,Athletic Coach,Staff
Jonathan,Daniel,E25271,jonathan.daniel@apsva.us,Yorktown HS,Teacher,Instructional Staff
Misila,Ceasar,E25283,misila.ceasar@apsva.us,Yorktown HS,Custodian,Facilities Staff
Beau,Obetts,E2547,beau.obetts@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jennifer,Webster,E25470,jennifer.webster@apsva.us,Yorktown HS,Teacher,Instructional Staff
Susan,Rochard,E25544,susan.rochard@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Diane,Holland,E25559,diane.holland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Benjamin,Luong,E25575,benjamin.luong@apsva.us,Yorktown HS,Custodian,Facilities Staff
Christine,Bonnefil,E25723,christine.bonnefil@apsva.us,Yorktown HS,Teacher,Instructional Staff
Virginia,Maloney,E25758,virginia.maloney@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christine,Bolon,E25774,christine.bolon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Adam,Sheppard,E25819,adam.sheppard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kevin,Thompson,E25879,kevin.thompson@apsva.us,Yorktown HS,Athletic Coach,Staff
Kimberly,Graver,E25901,kimberly.graver@apsva.us,Yorktown HS,Teacher,Instructional Staff
Deborah,Seidenstein,E25909,deborah.seidenstein@apsva.us,Yorktown HS,Teacher,Instructional Staff
John,Skaggs,E26008,john.skaggs@apsva.us,Yorktown HS,Athletic Coach,Staff
Yue,Zhen,E26073,yue.zhen@apsva.us,Yorktown HS,n/a,Unclassified
Kevin,Robertson,E26078,kevin.robertson@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Taliah,Graves,E26258,taliah.graves2@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jenny,Polio,E26345,jenny.polio@apsva.us,Yorktown HS,Custodian,Facilities Staff
Shari,Benites,E2657,shari.benites@apsva.us,Yorktown HS,Teacher,Instructional Staff
Joseph,Crawford,E26602,joseph.crawford@apsva.us,Yorktown HS,Staff,Staff
Andrew,Holland,E26617,andrew.holland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Cohen,E26657,amy.cohen@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Juan,Garay,E26712,juan.garay2@apsva.us,Yorktown HS,Custodian,Facilities Staff
Meg,Hunter,E26854,meg.hunter@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jessica,Paz-Soldan,E26889,jessica.pazsoldan@apsva.us,Yorktown HS,Teacher,Instructional Staff
Teresa,Cordova,E26937,teresa.cordova@apsva.us,Yorktown HS,Teacher,Instructional Staff
Marlyn,Bakalli,E26952,marlyn.bakalli@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Rebecca,Chervin,E26990,rebecca.chervin@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danielle,Jones,E26993,danielle.jones@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Ebenezer,Oware,E27033,ebenezer.oware@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Jhaquelin,Loayza Vela,E27166,jhaquelin.loayzavela@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Nicholas,Haring,E27408,nicholas.haring2@apsva.us,Yorktown HS,Staff,Staff
Juwahn,Nash,E27448,juwahna.nash@apsva.us,Yorktown HS,Custodian,Facilities Staff
Carlos,Aranda,E27470,carlos.aranda2@apsva.us,Yorktown HS,Staff,Staff
Sonja,Schulken,E27505,sonja.schulken2@apsva.us,Yorktown HS,Administrative Assistant,Staff
Steven,White,E27556,steven.white@apsva.us,Yorktown HS,Staff,Staff
Jocelyn,Mullins,E27627,jocelyn.mullins2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Hannah,Davis,E27701,hannah.davis@apsva.us,Yorktown HS,Staff,Staff
Corey,Klein,E27727,corey.klein@apsva.us,Yorktown HS,Teacher,Instructional Staff
Erika,Lucas,E27738,erika.lucas@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christopher,McIntosh,E27744,christopher.mcintosh@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Amy,Snyder,E27748,amy.snyder@apsva.us,Yorktown HS,Teacher,Instructional Staff
Clare,Davis,E27793,clare.davis@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Emili,Serghie,E27804,emili.serghie@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danilo,Loor,E27860,danilo.loor@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ryan,Larcamp,E27944,ryan.larcamp@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anai,Perez,E28032,anai.perez@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Natalie,Pulsifer,E28056,natalie.pulsifer@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Mindy,Straley,E28126,mindy.straley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Julia,Hawkins,E28165,julia.hawkins@apsva.us,Yorktown HS,Teacher,Instructional Staff
Molly,Gormley,E28206,molly.gormley@apsva.us,Yorktown HS,Staff,Staff
Kala,Craddock Mcintosh,E28261,kala.mcintosh@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
William,Lane,E28305,william.lane@apsva.us,Yorktown HS,Staff,Staff
Stephanie,Moreno,E28355,stephanie.moreno@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Marwa,Badran,E28395,marwa.badran2@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Vasilakos,E28436,andrew.vasilakos2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Emily Miranda,Ward,E28491,emily.ward@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Andrea,Pfiester,E28502,andrea.pfiester@apsva.us,Yorktown HS,Staff,Staff
Sandy,Dane,E28514,sandy.dane@apsva.us,Yorktown HS,Administrative Assistant,Staff
Ali,Samey,E28518,ali.samey@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Adams,E28554,andrew.adams@apsva.us,Yorktown HS,Staff,Staff
Freweini,Berhane,E28691,freweini.berhane@apsva.us,Yorktown HS,Administrative Assistant,Staff
Anita,Van Harten Cater,E28722,anita.vanhartencater@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Dani,Seltzer,E28762,dani.seltzer@apsva.us,Yorktown HS,Staff,Staff
Sally,Englander,E28772,sally.englander@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nyla,Walker,E28850,nyla.walker@apsva.us,Yorktown HS,Staff,Staff
Surender,Raut,E28913,surender.raut@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ruth,Mohr,E28970,ruth.mohr@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ryan,Zito,E28975,ryan.zito@apsva.us,Yorktown HS,Teacher,Instructional Staff
Scott,Painter,E29023,scott.painter@apsva.us,Yorktown HS,Teacher,Instructional Staff
Troy,Olsen,E29031,troy.olsen@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Jeffrey,Klein,E29056,jeffrey.klein@apsva.us,Yorktown HS,Teacher,Instructional Staff
Colissa,Huggins,E29165,colissa.huggins@apsva.us,Yorktown HS,Staff,Staff
Austin,Hamill,E29196,austin.hamill@apsva.us,Yorktown HS,Teacher,Instructional Staff
Michael,Lovrencic,E2920,michael.lovrencic@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christine,Murphy,E29219,christine.murphy@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jenny,Tran,E29235,jenny.tran@apsva.us,Yorktown HS,Staff,Staff
Tyler,Monroe,E29265,tyler.monroe@apsva.us,Yorktown HS,Staff,Staff
Torin,Ortmayer,E29384,torin.ortmayer@apsva.us,Yorktown HS,Staff,Staff
Amy,Sutton,E29486,amy.sutton@apsva.us,Yorktown HS,Staff,Staff
Ashley,Moore,E29582,ashley.moore@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lan,Lai,E29589,lan.lai@apsva.us,Yorktown HS,Custodian,Facilities Staff
Victoria,Soesbee,E29607,victoria.soesbee@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jonathan,Shears,E29655,jonathan.shears@apsva.us,Yorktown HS,Staff,Staff
Christopher,Crowe,E29669,christopher.crowe@apsva.us,Yorktown HS,Staff,Staff
Madleine,Brennan,E29683,madleine.brennan@apsva.us,Yorktown HS,Staff,Staff
Carolyn,Kroeger,E29916,carolyn.kroeger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Steven,Tinter,E29928,steven.tinter@apsva.us,Yorktown HS,Staff,Staff
Meghan,Smeenk,E29989,meghan.smeenk@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jamie,Cooke,E30001,jamie.cooke@apsva.us,Yorktown HS,Teacher,Instructional Staff
Brian,Dean,E30015,brian.dean@apsva.us,Yorktown HS,Teacher,Instructional Staff
Oana,Newcombe,E30100,oana.newcombe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danielle,McCarthy,E30107,danielle.mccarthy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Wilson,E30116,sarah.wilson@apsva.us,Yorktown HS,Teacher,Instructional Staff
Erin,Orourke,E30137,erin.orourke@apsva.us,Yorktown HS,Staff,Staff
Jacob,Dumford,E30266,jacob.dumford@apsva.us,Yorktown HS,Staff,Staff
Carson,Kramer,E30303,carson.kramer2@apsva.us,Yorktown HS,"Instructional Assistant, Teacher",Instructional Staff
Elizabeth,Herrington,E30421,elizabeth.herrington@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Edward,Lauber,E30447,edward.lauber@apsva.us,Yorktown HS,Teacher,Instructional Staff
Robert,Terry,E30452,robert.terry@apsva.us,Yorktown HS,Staff,Staff
Alani,Kravitz,E30468,alani.kravitz@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Covington,E30469,rachel.covington@apsva.us,Yorktown HS,Teacher,Instructional Staff
Britnee,Wade,E30485,britnee.wade@apsva.us,Yorktown HS,Administrative Assistant,Staff
Rachel,Dickenson,E30520,rachel.dickenson@apsva.us,Yorktown HS,Staff,Staff
Diana,Gamez Maravilla,E30714,diana.gamezmaravilla@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Dane,E30730,andrew.dane@apsva.us,Yorktown HS,Staff,Staff
Shannon,Aikens,E30741,shannon.aikens@apsva.us,Yorktown HS,Staff,Staff
Glorinda,Perez Mendez,E30746,glorinda.perezmendez@apsva.us,Yorktown HS,Custodian,Facilities Staff
Colin,Evans,E30951,colin.evans@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Audrey,Vasquez-Rivera,E31131,audrey.vasquezrivera@apsva.us,Yorktown HS,Teacher,Instructional Staff
Blake,Edwards,E31168,blake.edwards@apsva.us,Yorktown HS,Staff,Staff
Joseph,McBride,E31185,joseph.mcbride@apsva.us,Yorktown HS,Staff,Staff
Danielle,Dessaso,E31222,danielle.dessaso@apsva.us,Yorktown HS,Teacher,Instructional Staff
Brian,Wiltshire,E31236,brian.wiltshire@apsva.us,Yorktown HS,Teacher,Instructional Staff
Elizabeth,Hall,E31249,elizabeth.hall@apsva.us,Yorktown HS,Teacher,Instructional Staff
Daniela,Sava,E31266,daniela.sava@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Morgan,E31361,sarah.morgan@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Blaine,Kaltman,E31398,blaine.kaltman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Theresa,Andino,E31405,theresa.andino@apsva.us,Yorktown HS,Teacher,Instructional Staff
Shelby,McDavid,E31412,shelby.mcdavid@apsva.us,Yorktown HS,Staff,Staff
Paul,Fordjour,E31469,paul.fordjour@apsva.us,Yorktown HS,Custodian,Facilities Staff
Sang,Yoon,E31491,sang.yoon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Shepherd,E31548,amy.shepherd@apsva.us,Yorktown HS,Staff,Staff
Gabriel,Colmenarez Fuentes,E31616,gabriel.colmenarez@apsva.us,Yorktown HS,Staff,Staff
Cory,Allgood,E31694,cory.allgood@apsva.us,Yorktown HS,Staff,Staff
Erik,Thomas,E31698,erik.thomas@apsva.us,Yorktown HS,Staff,Staff
Ashley,Candeletti,E31765,ashley.candeletti@apsva.us,Yorktown HS,Staff,Staff
Elizabeth,Benedetto,E31829,elizabeth.benedetto@apsva.us,Yorktown HS,Staff,Staff
Shannon,Lewandowski,E31830,shannon.lewandowski@apsva.us,Yorktown HS,Staff,Staff
Lucretia,Kebreau,E31876,lucretia.kebreau@apsva.us,Yorktown HS,Teacher,Instructional Staff
Laney,Placido,E31947,laney.placido@apsva.us,Yorktown HS,Staff,Staff
Anthony,Dayse,E32085,anthony.dayse@apsva.us,Yorktown HS,Teacher,Instructional Staff
Neil,Kelly,E32087,neil.kelly@apsva.us,Yorktown HS,Teacher,Instructional Staff
Doria,Smith,E32201,doria.smith@apsva.us,Yorktown HS,Staff,Staff
Sophia,Zavada,E32213,sophia.zavada@apsva.us,Yorktown HS,Staff,Staff
Kaitlyn,Bryan,E32218,kaitlyn.bryan@apsva.us,Yorktown HS,Staff,Staff
Brandon,Glass,E32237,brandon.glass@apsva.us,Yorktown HS,Staff,Staff
Ashley,Gardner,E32248,ashley.gardner@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Amy,Marlow,E32252,amy.marlow@apsva.us,Yorktown HS,Staff,Staff
Kaitlin,Luncher,E32259,kaitlin.luncher@apsva.us,Yorktown HS,Staff,Staff
Madeline,Carzon,E32271,madeline.carzon@apsva.us,Yorktown HS,Staff,Staff
Graeme,Fineman,E32275,graeme.fineman@apsva.us,Yorktown HS,"Instructional Assistant, Staff",Staff
Marissa,Shaw,E32281,marissa.shaw@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Linda,Kyei,E32282,linda.kyei@apsva.us,Yorktown HS,Custodian,Facilities Staff
Collin,Johnston,E32295,collin.johnston@apsva.us,Yorktown HS,Staff,Staff
Emily,Stewart,E32299,emily.stewart@apsva.us,Yorktown HS,Staff,Staff
Chloe,Bergere,E32356,chloe.bergere@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Ronald,Avila,E32372,ronald.avila@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jeffries,Lhee,E32386,jeffries.lhee@apsva.us,Yorktown HS,Staff,Staff
Eric,Del Rosso,E32401,eric.delrosso@apsva.us,Yorktown HS,Staff,Staff
Elaf,Al Rushdawi,E32428,elaf.alrushdawi@apsva.us,Yorktown HS,Staff,Staff
Erin,Boyle,E32462,erin.boyle@apsva.us,Yorktown HS,Staff,Staff
Margot,Edinger,E32464,margot.edinger@apsva.us,Yorktown HS,Staff,Staff
Isabel,Rogers,E32506,isabel.rogers@apsva.us,Yorktown HS,Staff,Staff
Shelby,Thurgood,E32562,shelby.thurgood@apsva.us,Yorktown HS,Teacher,Instructional Staff
Joseph,Witkowski,E32573,joseph.witkowski@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jay,Conley,E32579,jay.conley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alexis,Andre,E32588,alexis.andre@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Reed,E32592,rachel.reed@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrew,Dabney,E32598,andrew.dabney@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jonia,Holley,E32599,jonia.holley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Dane,Huling,E32678,dane.huling@apsva.us,Yorktown HS,Teacher,Instructional Staff
Max,Adamo,E32680,max.adamo2@apsva.us,Yorktown HS,Staff,Staff
Charlotte,Reilly,E32684,charlotte.reilly@apsva.us,Yorktown HS,Staff,Staff
Allison,Loranger,E32717,allison.loranger@apsva.us,Yorktown HS,Staff,Staff
Hannah,Repke,E32731,hannah.repke@apsva.us,Yorktown HS,Staff,Staff
Aidan,Leddy,E32735,aidan.leddy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ricardo,Huaman,E32790,ricardo.huaman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Toshiko,Parrish,E32825,toshiko.parrish@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thuan,Nguyen-Lakrik,E32835,thuan.nguyenlakrik@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jake,Oster,E32842,jake.oster@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alexsis,Grate,E32892,alexsis.grate@apsva.us,Yorktown HS,Staff,Staff
Rama,Najib,E32938,rama.najib@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tayvon,Brown,E32955,tayvon.brown@apsva.us,Yorktown HS,Staff,Staff
Albert,Owusu,E32959,albert.owusu@apsva.us,Yorktown HS,Custodian,Facilities Staff
Setareh,Farzinfard,E32963,setareh.farzinfard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Shannon,Wood Graham,E32972,shannon.woodgraham@apsva.us,Yorktown HS,Staff,Staff
Benjamin,St. Pierre,E33067,benjamin.stpierre@apsva.us,Yorktown HS,Staff,Staff
Morgan,Stahl,E33104,morgan.stahl@apsva.us,Yorktown HS,Staff,Staff
Lucy,Ayers,E33153,lucy.ayers@apsva.us,Yorktown HS,Staff,Staff
Daniel,Chovil,E33191,daniel.chovil@apsva.us,Yorktown HS,Staff,Staff
Miguel,Gonzalez,E33197,miguel.gonzalez@apsva.us,Yorktown HS,Staff,Staff
Rianne,Connelly,E33234,rianne.connelly@apsva.us,Yorktown HS,Staff,Staff
Meghan,Hamby,E33236,meghan.hamby@apsva.us,Yorktown HS,Staff,Staff
Sophia,Ebanks,E33253,sophia.ebanks@apsva.us,Yorktown HS,Staff,Staff
Riley,Sophy,E33254,riley.sophy@apsva.us,Yorktown HS,Staff,Staff
Hala,Somo,E33364,hala.somo@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nicholas,Beadles,E33371,nicholas.beadles@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Holden,Gunster,E33393,holden.gunster@apsva.us,Yorktown HS,Staff,Staff
Stachia,Reuwsaat,E33411,stachia.reuwsaat@apsva.us,Yorktown HS,Staff,Staff
Ashley,Sanchez Garcia,E33441,ashley.sanchezgarcia@apsva.us,Yorktown HS,Staff,Staff
Eli,Lorenzi,E33444,eli.lorenzi@apsva.us,Yorktown HS,Staff,Staff
Charles,Molloy,E33485,charles.molloy@apsva.us,Yorktown HS,Staff,Staff
Harrison,Engoren,E33532,harrison.engoren@apsva.us,Yorktown HS,Staff,Staff
Christopher,Monahan,E33580,christopher.monahan@apsva.us,Yorktown HS,Teacher,Instructional Staff
Bruce,Hanson,E3781,bruce.hanson@apsva.us,Yorktown HS,Teacher,Instructional Staff
Meagan,Cummings,E3835,meagan.cummings@apsva.us,Yorktown HS,Teacher,Instructional Staff
Carol,Thompson,E4171,carol.thompson@apsva.us,Yorktown HS,Registrar,Staff
Matthew,Rinker,E4334,matthew.rinker@apsva.us,Yorktown HS,Teacher,Instructional Staff
Monica,Stroik,E4370,monica.stroik@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jon,Schildknecht,E4510,jon.schildknecht@apsva.us,Yorktown HS,Teacher,Instructional Staff
Michael,Krulfeld,E5119,michael.krulfeld@apsva.us,Yorktown HS,Director,Administrative Staff
Jennifer,Shearin,E5426,jennifer.shearin@apsva.us,Yorktown HS,Teacher,Instructional Staff
Richard,Avila,E5431,richard.avila@apsva.us,Yorktown HS,Teacher,Instructional Staff
Constance,Campana,E5462,constance.campana@apsva.us,Yorktown HS,Administrative Assistant,Staff
Maria,Pena,E5476,maria.pena@apsva.us,Yorktown HS,Custodian,Facilities Staff
Kathryn,Speakman,E5591,kathryn.speakman@apsva.us,Yorktown HS,"Staff, Teacher",Instructional Staff
Harold,Andersen,E568,harold.andersen@apsva.us,Yorktown HS,Teacher,Instructional Staff
Allen,Beland,E5713,allen.beland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rebecca,Gibbs Jones,E5783,rebecca.jones@apsva.us,Yorktown HS,Teacher,Instructional Staff
Scott,McKeown,E5839,scott.mckeown@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Christine,Brown,E6233,christine.brown@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Chris,Seeger,E6396,chris.seeger@apsva.us,Yorktown HS,Staff,Staff
Hatice,Danisir,E6635,hatice.danisir@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Linda,Hudson-O'Neill,E6913,linda.oneill@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrew,Prantner,E7295,andrew.prantner@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Cathy,Day,E7533,cathy.day@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Michael,Smalley,E7838,michael.smalley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Chrissy,Wiedemann,E7977,chrissy.wiedemann@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Eileen,Wagner,E8304,eileen.wagner@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ana,Ratcliffe,E8401,ana.ratcliffe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Devaughn,Drayton,E8414,devaughn.drayton@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Rebecca,Bonzano,E8600,rebecca.bonzano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Svetlana,Marchenko,E8840,svetlana.marchenko@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Patricia,Rojas,E9078,patricia.rojas@apsva.us,Yorktown HS,Administrative Assistant,Staff
Laurie,Vena,E9131,laurie.vena@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alex,Hicks,E9246,alex.hicks@apsva.us,Yorktown HS,Teacher,Instructional Staff
Robin,Grunberger,E9248,robin.grunberger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lucy,Middelthon,E9657,lucy.middelthon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Chris,Kaldahl,E9833,chris.kaldahl@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jose,Moreno,E9991,jose.moreno@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Carlos,Ore Jimenez,E10178,carlos.orejimenez@apsva.us,Yorktown HS,Custodian,Facilities Staff
Robin,Nugent,E10940,robin.nugent@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tracy,Maguire,E1108,tracy.maguire@apsva.us,Yorktown HS,Teacher,Instructional Staff
Stephanie,Meadows,E11146,stephanie.meadows@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Heather,Akpata,E11201,heather.akpata@apsva.us,Yorktown HS,Teacher,Instructional Staff
Deborah,Waldron,E11292,deborah.waldron@apsva.us,Yorktown HS,Teacher,Instructional Staff
Cheryl,Stotler,E11517,cheryl.stotler@apsva.us,Yorktown HS,"Administrative Assistant, Assistant Director",Staff
Kelly,Breedlove,E11579,kelly.breedlove@apsva.us,Yorktown HS,n/a,Unclassified
Rachel,Tarr,E11845,rachel.tarr@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Sadauskas,E12020,rachel.sadauskas@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Daniel,Carroll,E1252,daniel.carroll@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Chikes,E1256,sarah.chikes@apsva.us,Yorktown HS,Teacher,Instructional Staff
Evan,Glasier,E12750,evan.glasier@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christina,Smith,E13293,christina.smith2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Suzanne,Evans,E13400,suzanne.evans@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Curt,Dorman,E13509,curt.dorman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Dolores,Rubalcava,E13684,dolores.rubalcava@apsva.us,Yorktown HS,Administrative Assistant,Staff
Marlon,Telleria,E13915,marlon.telleria@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thomas,Hartman,E14213,thomas.hartman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rafael,Espinoza,E14398,rafael.espinoza@apsva.us,Yorktown HS,Teacher,Instructional Staff
Enrique,Solorzano,E14417,enrique.solorzano@apsva.us,Yorktown HS,Teacher,Instructional Staff
William,Lomax,E15164,william.lomax@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Maria,O'Connell,E15724,maria.oconnell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Aaron,Schuetz,E15797,aaron.schuetz@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anthony,McPhee,E15809,anthony.mcphee@apsva.us,Yorktown HS,Staff,Staff
Jeffrey,Stahl,E1594,jeffrey.stahl@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Kevin,Bridwell,E16231,kevin.bridwell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thor,Young,E16266,thor.young@apsva.us,Yorktown HS,Teacher,Instructional Staff
Audrey,Chan,E16449,audrey.chan@apsva.us,Yorktown HS,Administrative Assistant,Staff
Caryn,Rahawi,E1654,caryn.rahawi@apsva.us,Yorktown HS,"Instructional Assistant, Staff",Instructional Staff
Kimberly,Cordell,E16587,kimberly.cordell@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anne,Stewart,E16602,anner.stewart@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sara,Espinoza,E16644,sara.espinoza@apsva.us,Yorktown HS,Custodian,Facilities Staff
Kevin,Clark,E16676,kevin.clark@apsva.us,Yorktown HS,Principal,Unclassified
Paige,Hamrick,E16717,paige.hamrick@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrea,Rosegrant,E16841,andrea.rosegrant@apsva.us,Yorktown HS,Administrative Assistant,Staff
William,Marlow,E17026,william.marlow@apsva.us,Yorktown HS,Staff,Staff
Adrienne,Wright,E17040,adrienne.wright@apsva.us,Yorktown HS,Teacher,Instructional Staff
Md,Calabro,E1896,md.calabro@apsva.us,Yorktown HS,Teacher,Instructional Staff
Claire,Shreeve,E19162,claire.shreeve@apsva.us,Yorktown HS,Teacher,Instructional Staff
Gregory,Beer,E19221,gregory.beer@apsva.us,Yorktown HS,Athletic Coach,Staff
Wilson,Desousa,E19222,wilson.desousa@apsva.us,Yorktown HS,Athletic Coach,Staff
Natalie,Roy,E19225,natalie.roy@apsva.us,Yorktown HS,Athletic Coach,Staff
Allyson,McKowen,E1934,allyson.mckowen@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Murphy,E19351,amy.murphy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Peter,Ketcham-Colwill,E19492,peter.ketchumcolwill@apsva.us,Yorktown HS,Staff,Staff
Cristina,Rivera,E19518,cristina.rivera@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christopher,Roland,E19591,christopher.roland@apsva.us,Yorktown HS,Staff,Staff
Laura,Albright,E19652,laura.albright@apsva.us,Yorktown HS,Teacher,Instructional Staff
Allison,Gilbert,E19701,allison.gilbert@apsva.us,Yorktown HS,Teacher,Instructional Staff
Maria,Suaznabar,E19729,maria.suaznabar@apsva.us,Yorktown HS,Custodian,Facilities Staff
Robert,Shaw,E19926,robert.shaw@apsva.us,Yorktown HS,Athletic Coach,Staff
James,Slaughter,E19960,james.slaughter@apsva.us,Yorktown HS,Athletic Coach,Staff
Juliet,Boakye,E20064,juliet.boakye@apsva.us,Yorktown HS,Custodian,Facilities Staff
Lottie,Mack,E2019,lottie.mack@apsva.us,Yorktown HS,School Finance Officer,Staff
Juanice,Jenkins,E20292,juanice.jenkins@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Sherry,Lord,E20497,sherry.lord@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tony,Price,E20510,tony.price@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lisa,Troiano,E20614,lisa.troiano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alesandra,Bakaj,E20739,alesandra.bakaj@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tamara,Molina,E20965,tamara.molina@apsva.us,Yorktown HS,Teacher,Instructional Staff
Richard,Gibson,E21109,richard.gibson@apsva.us,Yorktown HS,"Teacher, Staff",Staff
David,Mower,E21119,david.mower@apsva.us,Yorktown HS,Teacher,Instructional Staff
Emmet,Conroy,E21275,emmet.conroy@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Tom,Norton,E21295,tom.norton@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nancie,Graf-Sidiropoulos,E21339,nancie.sidiropoulos@apsva.us,Yorktown HS,Teacher,Instructional Staff
Roseline,Berger,E21369,roseline.berger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Paul,Hessler,E21430,paul.hessler@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Evan,Ruffner,E21802,evan.ruffner@apsva.us,Yorktown HS,"Athletic Coach, Teacher",Staff
Susan,Harrison,E21916,susan.harrison@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kenneth,Mandel,E22066,kenneth.mandel@apsva.us,Yorktown HS,Teacher,Instructional Staff
Mark,Rooks,E2222,mark.rooks@apsva.us,Yorktown HS,Director,Administrative Staff
Heather,Sutphin,E22225,heather.sutphin@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Sean,Kinnard,E22255,sean.kinnard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Justine,Springberg,E22450,justine.springberg@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amali,Amarasinghe,E22641,amali.amarasinghe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lindsay,Kennedy,E22838,lindsay.kennedy@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Mary Ann,Mahan,E22880,maryann.mahan@apsva.us,Yorktown HS,Administrative Assistant,Staff
Jessica,Reeve,E22923,jessica.reeve@apsva.us,Yorktown HS,Teacher,Instructional Staff
Wendy,Boone,E23063,wendy.boone@apsva.us,Yorktown HS,Coordinator,Instructional Staff
Morgan,Froy,E23169,morgan.froy@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Jennifer,Falbo,E23284,jennifer.falbo@apsva.us,Yorktown HS,Administrative Assistant,Staff
Janet,Frimpong,E23536,janet.frimpong@apsva.us,Yorktown HS,Custodian,Facilities Staff
Brianna McHugh,McArthur,E23752,brianna.mcarthur@apsva.us,Yorktown HS,Teacher,Instructional Staff
Victoria,Walchak,E23781,victoria.walchak@apsva.us,Yorktown HS,Teacher,Instructional Staff
Felix,Carcamo,E23924,felix.carcamo@apsva.us,Yorktown HS,Custodian,Facilities Staff
Brittany,Garner,E23927,brittany.garner@apsva.us,Yorktown HS,Athletic Coach,Staff
Gwendolyn,Nixon,E23992,gwendolyn.nixon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sumer,Majid,E24016,sumer.majid@apsva.us,Yorktown HS,n/a,Unclassified
Barbara,Marks,E24029,barbara.marks@apsva.us,Yorktown HS,Teacher,Instructional Staff
Colin,Sundwick,E24041,colin.sundwick@apsva.us,Yorktown HS,Staff,Staff
Zahra,Castellano,E24053,zahra.castellano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Daniel,Barkan,E24081,daniel.barkan@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Brannon,Burnett,E24286,brannon.burnett@apsva.us,Yorktown HS,Staff,Staff
Steven,McArthur,E24314,steven.mcarthur@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kelly,Dillon,E24405,kelly.dillon@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Zainab,Kufaishi,E24474,zainab.kufaishi@apsva.us,Yorktown HS,n/a,Unclassified
Sheena,Eldred,E24842,sheena.eldred@apsva.us,Yorktown HS,Athletic Coach,Staff
Kelley,Parent,E24862,kelley.parent@apsva.us,Yorktown HS,Teacher,Instructional Staff
Olivia,Shipley,E24877,olivia.shipley@apsva.us,Yorktown HS,Staff,Staff
Veronica,Todd,E24885,veronica.bailey@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Lanay,Burke,E25003,lanay.burke@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nancy,Hooper,E25221,nancy.hooper@apsva.us,Yorktown HS,Athletic Coach,Staff
Jonathan,Daniel,E25271,jonathan.daniel@apsva.us,Yorktown HS,Teacher,Instructional Staff
Misila,Ceasar,E25283,misila.ceasar@apsva.us,Yorktown HS,Custodian,Facilities Staff
Beau,Obetts,E2547,beau.obetts@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jennifer,Webster,E25470,jennifer.webster@apsva.us,Yorktown HS,Teacher,Instructional Staff
Susan,Rochard,E25544,susan.rochard@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Diane,Holland,E25559,diane.holland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Benjamin,Luong,E25575,benjamin.luong@apsva.us,Yorktown HS,Custodian,Facilities Staff
Christine,Bonnefil,E25723,christine.bonnefil@apsva.us,Yorktown HS,Teacher,Instructional Staff
Virginia,Maloney,E25758,virginia.maloney@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christine,Bolon,E25774,christine.bolon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Adam,Sheppard,E25819,adam.sheppard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Kevin,Thompson,E25879,kevin.thompson@apsva.us,Yorktown HS,Athletic Coach,Staff
Kimberly,Graver,E25901,kimberly.graver@apsva.us,Yorktown HS,Teacher,Instructional Staff
Deborah,Seidenstein,E25909,deborah.seidenstein@apsva.us,Yorktown HS,Teacher,Instructional Staff
John,Skaggs,E26008,john.skaggs@apsva.us,Yorktown HS,Athletic Coach,Staff
Yue,Zhen,E26073,yue.zhen@apsva.us,Yorktown HS,n/a,Unclassified
Kevin,Robertson,E26078,kevin.robertson@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Taliah,Graves,E26258,taliah.graves2@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jenny,Polio,E26345,jenny.polio@apsva.us,Yorktown HS,Custodian,Facilities Staff
Shari,Benites,E2657,shari.benites@apsva.us,Yorktown HS,Teacher,Instructional Staff
Joseph,Crawford,E26602,joseph.crawford@apsva.us,Yorktown HS,Staff,Staff
Andrew,Holland,E26617,andrew.holland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Cohen,E26657,amy.cohen@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Juan,Garay,E26712,juan.garay2@apsva.us,Yorktown HS,Custodian,Facilities Staff
Meg,Hunter,E26854,meg.hunter@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jessica,Paz-Soldan,E26889,jessica.pazsoldan@apsva.us,Yorktown HS,Teacher,Instructional Staff
Teresa,Cordova,E26937,teresa.cordova@apsva.us,Yorktown HS,Teacher,Instructional Staff
Marlyn,Bakalli,E26952,marlyn.bakalli@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Rebecca,Chervin,E26990,rebecca.chervin@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danielle,Jones,E26993,danielle.jones@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Ebenezer,Oware,E27033,ebenezer.oware@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Jhaquelin,Loayza Vela,E27166,jhaquelin.loayzavela@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Nicholas,Haring,E27408,nicholas.haring2@apsva.us,Yorktown HS,Staff,Staff
Juwahn,Nash,E27448,juwahna.nash@apsva.us,Yorktown HS,Custodian,Facilities Staff
Carlos,Aranda,E27470,carlos.aranda2@apsva.us,Yorktown HS,Staff,Staff
Sonja,Schulken,E27505,sonja.schulken2@apsva.us,Yorktown HS,Administrative Assistant,Staff
Steven,White,E27556,steven.white@apsva.us,Yorktown HS,Staff,Staff
Jocelyn,Mullins,E27627,jocelyn.mullins2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Hannah,Davis,E27701,hannah.davis@apsva.us,Yorktown HS,Staff,Staff
Corey,Klein,E27727,corey.klein@apsva.us,Yorktown HS,Teacher,Instructional Staff
Erika,Lucas,E27738,erika.lucas@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christopher,McIntosh,E27744,christopher.mcintosh@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Amy,Snyder,E27748,amy.snyder@apsva.us,Yorktown HS,Teacher,Instructional Staff
Clare,Davis,E27793,clare.davis@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Emili,Serghie,E27804,emili.serghie@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danilo,Loor,E27860,danilo.loor@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ryan,Larcamp,E27944,ryan.larcamp@apsva.us,Yorktown HS,Teacher,Instructional Staff
Anai,Perez,E28032,anai.perez@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Natalie,Pulsifer,E28056,natalie.pulsifer@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Mindy,Straley,E28126,mindy.straley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Julia,Hawkins,E28165,julia.hawkins@apsva.us,Yorktown HS,Teacher,Instructional Staff
Molly,Gormley,E28206,molly.gormley@apsva.us,Yorktown HS,Staff,Staff
Kala,Craddock Mcintosh,E28261,kala.mcintosh@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
William,Lane,E28305,william.lane@apsva.us,Yorktown HS,Staff,Staff
Stephanie,Moreno,E28355,stephanie.moreno@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Marwa,Badran,E28395,marwa.badran2@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Vasilakos,E28436,andrew.vasilakos2@apsva.us,Yorktown HS,Teacher,Instructional Staff
Emily Miranda,Ward,E28491,emily.ward@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Andrea,Pfiester,E28502,andrea.pfiester@apsva.us,Yorktown HS,Staff,Staff
Sandy,Dane,E28514,sandy.dane@apsva.us,Yorktown HS,Administrative Assistant,Staff
Ali,Samey,E28518,ali.samey@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Adams,E28554,andrew.adams@apsva.us,Yorktown HS,Staff,Staff
Freweini,Berhane,E28691,freweini.berhane@apsva.us,Yorktown HS,Administrative Assistant,Staff
Anita,Van Harten Cater,E28722,anita.vanhartencater@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Dani,Seltzer,E28762,dani.seltzer@apsva.us,Yorktown HS,Staff,Staff
Sally,Englander,E28772,sally.englander@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nyla,Walker,E28850,nyla.walker@apsva.us,Yorktown HS,Staff,Staff
Surender,Raut,E28913,surender.raut@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ruth,Mohr,E28970,ruth.mohr@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ryan,Zito,E28975,ryan.zito@apsva.us,Yorktown HS,Teacher,Instructional Staff
Scott,Painter,E29023,scott.painter@apsva.us,Yorktown HS,Teacher,Instructional Staff
Troy,Olsen,E29031,troy.olsen@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Jeffrey,Klein,E29056,jeffrey.klein@apsva.us,Yorktown HS,Teacher,Instructional Staff
Colissa,Huggins,E29165,colissa.huggins@apsva.us,Yorktown HS,Staff,Staff
Austin,Hamill,E29196,austin.hamill@apsva.us,Yorktown HS,Teacher,Instructional Staff
Michael,Lovrencic,E2920,michael.lovrencic@apsva.us,Yorktown HS,Teacher,Instructional Staff
Christine,Murphy,E29219,christine.murphy@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jenny,Tran,E29235,jenny.tran@apsva.us,Yorktown HS,Staff,Staff
Tyler,Monroe,E29265,tyler.monroe@apsva.us,Yorktown HS,Staff,Staff
Torin,Ortmayer,E29384,torin.ortmayer@apsva.us,Yorktown HS,Staff,Staff
Amy,Sutton,E29486,amy.sutton@apsva.us,Yorktown HS,Staff,Staff
Ashley,Moore,E29582,ashley.moore@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lan,Lai,E29589,lan.lai@apsva.us,Yorktown HS,Custodian,Facilities Staff
Victoria,Soesbee,E29607,victoria.soesbee@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Jonathan,Shears,E29655,jonathan.shears@apsva.us,Yorktown HS,Staff,Staff
Christopher,Crowe,E29669,christopher.crowe@apsva.us,Yorktown HS,Staff,Staff
Madleine,Brennan,E29683,madleine.brennan@apsva.us,Yorktown HS,Staff,Staff
Carolyn,Kroeger,E29916,carolyn.kroeger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Steven,Tinter,E29928,steven.tinter@apsva.us,Yorktown HS,Staff,Staff
Meghan,Smeenk,E29989,meghan.smeenk@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jamie,Cooke,E30001,jamie.cooke@apsva.us,Yorktown HS,Teacher,Instructional Staff
Brian,Dean,E30015,brian.dean@apsva.us,Yorktown HS,Teacher,Instructional Staff
Oana,Newcombe,E30100,oana.newcombe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Danielle,McCarthy,E30107,danielle.mccarthy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Wilson,E30116,sarah.wilson@apsva.us,Yorktown HS,Teacher,Instructional Staff
Erin,Orourke,E30137,erin.orourke@apsva.us,Yorktown HS,Staff,Staff
Jacob,Dumford,E30266,jacob.dumford@apsva.us,Yorktown HS,Staff,Staff
Carson,Kramer,E30303,carson.kramer2@apsva.us,Yorktown HS,"Instructional Assistant, Teacher",Instructional Staff
Elizabeth,Herrington,E30421,elizabeth.herrington@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Edward,Lauber,E30447,edward.lauber@apsva.us,Yorktown HS,Teacher,Instructional Staff
Robert,Terry,E30452,robert.terry@apsva.us,Yorktown HS,Staff,Staff
Alani,Kravitz,E30468,alani.kravitz@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Covington,E30469,rachel.covington@apsva.us,Yorktown HS,Teacher,Instructional Staff
Britnee,Wade,E30485,britnee.wade@apsva.us,Yorktown HS,Administrative Assistant,Staff
Rachel,Dickenson,E30520,rachel.dickenson@apsva.us,Yorktown HS,Staff,Staff
Diana,Gamez Maravilla,E30714,diana.gamezmaravilla@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Andrew,Dane,E30730,andrew.dane@apsva.us,Yorktown HS,Staff,Staff
Shannon,Aikens,E30741,shannon.aikens@apsva.us,Yorktown HS,Staff,Staff
Glorinda,Perez Mendez,E30746,glorinda.perezmendez@apsva.us,Yorktown HS,Custodian,Facilities Staff
Colin,Evans,E30951,colin.evans@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Audrey,Vasquez-Rivera,E31131,audrey.vasquezrivera@apsva.us,Yorktown HS,Teacher,Instructional Staff
Blake,Edwards,E31168,blake.edwards@apsva.us,Yorktown HS,Staff,Staff
Joseph,McBride,E31185,joseph.mcbride@apsva.us,Yorktown HS,Staff,Staff
Danielle,Dessaso,E31222,danielle.dessaso@apsva.us,Yorktown HS,Teacher,Instructional Staff
Brian,Wiltshire,E31236,brian.wiltshire@apsva.us,Yorktown HS,Teacher,Instructional Staff
Elizabeth,Hall,E31249,elizabeth.hall@apsva.us,Yorktown HS,Teacher,Instructional Staff
Daniela,Sava,E31266,daniela.sava@apsva.us,Yorktown HS,Teacher,Instructional Staff
Sarah,Morgan,E31361,sarah.morgan@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Blaine,Kaltman,E31398,blaine.kaltman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Theresa,Andino,E31405,theresa.andino@apsva.us,Yorktown HS,Teacher,Instructional Staff
Shelby,McDavid,E31412,shelby.mcdavid@apsva.us,Yorktown HS,Staff,Staff
Paul,Fordjour,E31469,paul.fordjour@apsva.us,Yorktown HS,Custodian,Facilities Staff
Sang,Yoon,E31491,sang.yoon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Amy,Shepherd,E31548,amy.shepherd@apsva.us,Yorktown HS,Staff,Staff
Gabriel,Colmenarez Fuentes,E31616,gabriel.colmenarez@apsva.us,Yorktown HS,Staff,Staff
Cory,Allgood,E31694,cory.allgood@apsva.us,Yorktown HS,Staff,Staff
Erik,Thomas,E31698,erik.thomas@apsva.us,Yorktown HS,Staff,Staff
Ashley,Candeletti,E31765,ashley.candeletti@apsva.us,Yorktown HS,Staff,Staff
Elizabeth,Benedetto,E31829,elizabeth.benedetto@apsva.us,Yorktown HS,Staff,Staff
Shannon,Lewandowski,E31830,shannon.lewandowski@apsva.us,Yorktown HS,Staff,Staff
Lucretia,Kebreau,E31876,lucretia.kebreau@apsva.us,Yorktown HS,Teacher,Instructional Staff
Laney,Placido,E31947,laney.placido@apsva.us,Yorktown HS,Staff,Staff
Anthony,Dayse,E32085,anthony.dayse@apsva.us,Yorktown HS,Teacher,Instructional Staff
Neil,Kelly,E32087,neil.kelly@apsva.us,Yorktown HS,Teacher,Instructional Staff
Doria,Smith,E32201,doria.smith@apsva.us,Yorktown HS,Staff,Staff
Sophia,Zavada,E32213,sophia.zavada@apsva.us,Yorktown HS,Staff,Staff
Kaitlyn,Bryan,E32218,kaitlyn.bryan@apsva.us,Yorktown HS,Staff,Staff
Brandon,Glass,E32237,brandon.glass@apsva.us,Yorktown HS,Staff,Staff
Ashley,Gardner,E32248,ashley.gardner@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Amy,Marlow,E32252,amy.marlow@apsva.us,Yorktown HS,Staff,Staff
Kaitlin,Luncher,E32259,kaitlin.luncher@apsva.us,Yorktown HS,Staff,Staff
Madeline,Carzon,E32271,madeline.carzon@apsva.us,Yorktown HS,Staff,Staff
Graeme,Fineman,E32275,graeme.fineman@apsva.us,Yorktown HS,"Instructional Assistant, Staff",Staff
Marissa,Shaw,E32281,marissa.shaw@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Linda,Kyei,E32282,linda.kyei@apsva.us,Yorktown HS,Custodian,Facilities Staff
Collin,Johnston,E32295,collin.johnston@apsva.us,Yorktown HS,Staff,Staff
Emily,Stewart,E32299,emily.stewart@apsva.us,Yorktown HS,Staff,Staff
Chloe,Bergere,E32356,chloe.bergere@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Ronald,Avila,E32372,ronald.avila@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jeffries,Lhee,E32386,jeffries.lhee@apsva.us,Yorktown HS,Staff,Staff
Eric,Del Rosso,E32401,eric.delrosso@apsva.us,Yorktown HS,Staff,Staff
Elaf,Al Rushdawi,E32428,elaf.alrushdawi@apsva.us,Yorktown HS,Staff,Staff
Erin,Boyle,E32462,erin.boyle@apsva.us,Yorktown HS,Staff,Staff
Margot,Edinger,E32464,margot.edinger@apsva.us,Yorktown HS,Staff,Staff
Isabel,Rogers,E32506,isabel.rogers@apsva.us,Yorktown HS,Staff,Staff
Shelby,Thurgood,E32562,shelby.thurgood@apsva.us,Yorktown HS,Teacher,Instructional Staff
Joseph,Witkowski,E32573,joseph.witkowski@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jay,Conley,E32579,jay.conley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alexis,Andre,E32588,alexis.andre@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rachel,Reed,E32592,rachel.reed@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrew,Dabney,E32598,andrew.dabney@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jonia,Holley,E32599,jonia.holley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Dane,Huling,E32678,dane.huling@apsva.us,Yorktown HS,Teacher,Instructional Staff
Max,Adamo,E32680,max.adamo2@apsva.us,Yorktown HS,Staff,Staff
Charlotte,Reilly,E32684,charlotte.reilly@apsva.us,Yorktown HS,Staff,Staff
Allison,Loranger,E32717,allison.loranger@apsva.us,Yorktown HS,Staff,Staff
Hannah,Repke,E32731,hannah.repke@apsva.us,Yorktown HS,Staff,Staff
Aidan,Leddy,E32735,aidan.leddy@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ricardo,Huaman,E32790,ricardo.huaman@apsva.us,Yorktown HS,Teacher,Instructional Staff
Toshiko,Parrish,E32825,toshiko.parrish@apsva.us,Yorktown HS,Teacher,Instructional Staff
Thuan,Nguyen-Lakrik,E32835,thuan.nguyenlakrik@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jake,Oster,E32842,jake.oster@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alexsis,Grate,E32892,alexsis.grate@apsva.us,Yorktown HS,Staff,Staff
Rama,Najib,E32938,rama.najib@apsva.us,Yorktown HS,Teacher,Instructional Staff
Tayvon,Brown,E32955,tayvon.brown@apsva.us,Yorktown HS,Staff,Staff
Albert,Owusu,E32959,albert.owusu@apsva.us,Yorktown HS,Custodian,Facilities Staff
Setareh,Farzinfard,E32963,setareh.farzinfard@apsva.us,Yorktown HS,Teacher,Instructional Staff
Shannon,Wood Graham,E32972,shannon.woodgraham@apsva.us,Yorktown HS,Staff,Staff
Benjamin,St. Pierre,E33067,benjamin.stpierre@apsva.us,Yorktown HS,Staff,Staff
Morgan,Stahl,E33104,morgan.stahl@apsva.us,Yorktown HS,Staff,Staff
Lucy,Ayers,E33153,lucy.ayers@apsva.us,Yorktown HS,Staff,Staff
Daniel,Chovil,E33191,daniel.chovil@apsva.us,Yorktown HS,Staff,Staff
Miguel,Gonzalez,E33197,miguel.gonzalez@apsva.us,Yorktown HS,Staff,Staff
Rianne,Connelly,E33234,rianne.connelly@apsva.us,Yorktown HS,Staff,Staff
Meghan,Hamby,E33236,meghan.hamby@apsva.us,Yorktown HS,Staff,Staff
Sophia,Ebanks,E33253,sophia.ebanks@apsva.us,Yorktown HS,Staff,Staff
Riley,Sophy,E33254,riley.sophy@apsva.us,Yorktown HS,Staff,Staff
Hala,Somo,E33364,hala.somo@apsva.us,Yorktown HS,Teacher,Instructional Staff
Nicholas,Beadles,E33371,nicholas.beadles@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Holden,Gunster,E33393,holden.gunster@apsva.us,Yorktown HS,Staff,Staff
Stachia,Reuwsaat,E33411,stachia.reuwsaat@apsva.us,Yorktown HS,Staff,Staff
Ashley,Sanchez Garcia,E33441,ashley.sanchezgarcia@apsva.us,Yorktown HS,Staff,Staff
Eli,Lorenzi,E33444,eli.lorenzi@apsva.us,Yorktown HS,Staff,Staff
Charles,Molloy,E33485,charles.molloy@apsva.us,Yorktown HS,Staff,Staff
Harrison,Engoren,E33532,harrison.engoren@apsva.us,Yorktown HS,Staff,Staff
Christopher,Monahan,E33580,christopher.monahan@apsva.us,Yorktown HS,Teacher,Instructional Staff
Bruce,Hanson,E3781,bruce.hanson@apsva.us,Yorktown HS,Teacher,Instructional Staff
Meagan,Cummings,E3835,meagan.cummings@apsva.us,Yorktown HS,Teacher,Instructional Staff
Carol,Thompson,E4171,carol.thompson@apsva.us,Yorktown HS,Registrar,Staff
Matthew,Rinker,E4334,matthew.rinker@apsva.us,Yorktown HS,Teacher,Instructional Staff
Monica,Stroik,E4370,monica.stroik@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jon,Schildknecht,E4510,jon.schildknecht@apsva.us,Yorktown HS,Teacher,Instructional Staff
Michael,Krulfeld,E5119,michael.krulfeld@apsva.us,Yorktown HS,Director,Administrative Staff
Jennifer,Shearin,E5426,jennifer.shearin@apsva.us,Yorktown HS,Teacher,Instructional Staff
Richard,Avila,E5431,richard.avila@apsva.us,Yorktown HS,Teacher,Instructional Staff
Constance,Campana,E5462,constance.campana@apsva.us,Yorktown HS,Administrative Assistant,Staff
Maria,Pena,E5476,maria.pena@apsva.us,Yorktown HS,Custodian,Facilities Staff
Kathryn,Speakman,E5591,kathryn.speakman@apsva.us,Yorktown HS,"Staff, Teacher",Instructional Staff
Harold,Andersen,E568,harold.andersen@apsva.us,Yorktown HS,Teacher,Instructional Staff
Allen,Beland,E5713,allen.beland@apsva.us,Yorktown HS,Teacher,Instructional Staff
Rebecca,Gibbs Jones,E5783,rebecca.jones@apsva.us,Yorktown HS,Teacher,Instructional Staff
Scott,McKeown,E5839,scott.mckeown@apsva.us,Yorktown HS,Assistant Principal,Administrative Staff
Christine,Brown,E6233,christine.brown@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Chris,Seeger,E6396,chris.seeger@apsva.us,Yorktown HS,Staff,Staff
Hatice,Danisir,E6635,hatice.danisir@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Linda,Hudson-O'Neill,E6913,linda.oneill@apsva.us,Yorktown HS,Teacher,Instructional Staff
Andrew,Prantner,E7295,andrew.prantner@apsva.us,Yorktown HS,"Teacher, Staff",Instructional Staff
Cathy,Day,E7533,cathy.day@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Michael,Smalley,E7838,michael.smalley@apsva.us,Yorktown HS,Teacher,Instructional Staff
Chrissy,Wiedemann,E7977,chrissy.wiedemann@apsva.us,Yorktown HS,"Teacher, Staff",Staff
Eileen,Wagner,E8304,eileen.wagner@apsva.us,Yorktown HS,Teacher,Instructional Staff
Ana,Ratcliffe,E8401,ana.ratcliffe@apsva.us,Yorktown HS,Teacher,Instructional Staff
Devaughn,Drayton,E8414,devaughn.drayton@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Rebecca,Bonzano,E8600,rebecca.bonzano@apsva.us,Yorktown HS,Teacher,Instructional Staff
Svetlana,Marchenko,E8840,svetlana.marchenko@apsva.us,Yorktown HS,Instructional Assistant,Instructional Staff
Patricia,Rojas,E9078,patricia.rojas@apsva.us,Yorktown HS,Administrative Assistant,Staff
Laurie,Vena,E9131,laurie.vena@apsva.us,Yorktown HS,Teacher,Instructional Staff
Alex,Hicks,E9246,alex.hicks@apsva.us,Yorktown HS,Teacher,Instructional Staff
Robin,Grunberger,E9248,robin.grunberger@apsva.us,Yorktown HS,Teacher,Instructional Staff
Lucy,Middelthon,E9657,lucy.middelthon@apsva.us,Yorktown HS,Teacher,Instructional Staff
Chris,Kaldahl,E9833,chris.kaldahl@apsva.us,Yorktown HS,Teacher,Instructional Staff
Jose,Moreno,E9991,jose.moreno@apsva.us,Yorktown HS,Maintenance Supervisor,Facilities Staff
Liz,Gephardt,E11328,liz.gephardt@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Juanita,Gibbons,E14838,juanita.gibbons@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Ellen,Smith,E1604,ellen.smith@apsva.us,Dorothy Hamm MS,Principal,Unclassified
Lisa,Welde Moore,E16413,lisa.welde@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Diane,Myers,E16634,diane.myers@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Matthew,Redican,E16702,matthew.redican@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Joycelyn,Clarke,E16737,joycelyn.clarke@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Marc,Zeballos,E16921,marc.zeballos@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Karen,Bastow,E19128,karen.bastow@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Patricia,Carlson,E1929,patricia.carlson@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Carolyn,Riley,E19576,carolyn.riley@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Tiffany,Ratliff,E19781,tiffany.ratliff@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Ingriz,Barraza,E19865,ingriz.barraza@apsva.us,Dorothy Hamm MS,Staff,Staff
Crystal,Richardson,E20416,crystal.richardson@apsva.us,Dorothy Hamm MS,Account Clerk,Staff
Maria,Araya Chanon,E21152,maria.araya@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Kathy,Ascenzi,E2141,kathy.ascenzi@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
David,Lunt,E21586,david.lunt@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Callie,Wright,E22304,callie.wright@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Jenny,Shanker,E22568,jenny.shanker@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Amy,Juengst,E22623,amy.juengst@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Janae,Rittenhouse,E23023,janae.rittenhouse@apsva.us,Dorothy Hamm MS,Director,Administrative Staff
Todd,Orange,E23141,todd.orange@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Poorvi,Shah,E23403,poorvi.shah@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Nicole,Holohan,E23591,nicole.holohan@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Elizabeth,Malks,E23731,elizabeth.malks@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Katherine,Leon,E23753,katherine.leon@apsva.us,Dorothy Hamm MS,Coordinator,Staff
Beth,Sanderson,E24116,beth.sanderson@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Weiling,Song,E24188,weiling.song@apsva.us,Dorothy Hamm MS,n/a,Unclassified
Audrey,Tsai,E24341,audrey.tsai@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Lisa,Nasr,E24404,lisa.nasr@apsva.us,Dorothy Hamm MS,n/a,Unclassified
Aden,Negasi,E25012,aden.negasi@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Carla,Bran,E25700,carla.bran@apsva.us,Dorothy Hamm MS,Administrative Assistant,Staff
Julie,Westcott,E25730,julie.westcott@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Heather,Donaldson,E25757,heather.donaldson@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Victoria,Metzger,E25802,victoria.metzger@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Erin,Pennington,E25826,erin.pennington@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Ellen,Brennan,E25892,ellen.brennan@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Angelica,Elder,E26347,angelica.elder@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Jennifer,Palermo,E26820,jennifer.palermo@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Sally,Donnelly,E26826,sally.donnelly@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Autumn,Rodgers,E26934,autumn.rodgers@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Courtney,Schlottman,E26940,courtney.schlottman@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Megan,Kroger,E27159,megan.kroger2@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Christopher,Mann,E27445,christopher.mann@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Seemaab,Anjum,E27526,seemaab.anjum@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Kimberly,Marshall,E27759,kimberly.clayton@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Carrie,Schaefer,E27925,carrie.schaefer@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Emma,Foley,E28109,emma.foley@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Juan,Moya Rojas,E28176,juan.moya@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Jayda,Parker,E28196,jayda.parker@apsva.us,Dorothy Hamm MS,Administrative Assistant,Staff
Brenare,Williams,E28214,brenare.williams@apsva.us,Dorothy Hamm MS,Administrative Assistant,Staff
Andrew,Vasilakos,E28436,andrew.vasilakos2@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Danielle,Cortes,E28618,danielle.cortes@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Susan,Kennedy,E29441,susan.kennedy@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Jamallah,Drayton,E29507,jamallah.drayton@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Shannon,Hill,E29600,shannon.hill@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Kandace,Connor,E29729,kandace.connor@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Megan,Smith,E30186,megan.smith@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Heather,Schoenebeck,E30275,heather.schoenebeck@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Angela,Nunez Zarceno,E30365,angela.nunezzarceno@apsva.us,Dorothy Hamm MS,Custodian,Facilities Staff
Summer,Paris,E30424,summer.paris@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Pedro,Perez,E30523,pedro.perez@apsva.us,Dorothy Hamm MS,Maintenance Supervisor,Facilities Staff
Elaine,Kennedy,E31101,elaine.kennedy@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Erin,Causey,E31207,erin.causey@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Regina,Boyd,E31208,regina.boyd@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Madison,Azzara,E31225,madison.azzara2@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Brian,Weiser,E31235,brian.weiser@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Payton,Worth,E31259,payton.worth@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Kathleen,Fitzpatrick,E31284,kathleen.fitzpatrick@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Katherine,Hanckel,E31299,katherine.hanckel@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Robert,Loutsios,E31329,robert.loutsios@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Christopher,Barulic,E31333,christopher.barulic@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Ilsa,Boteo Figueroa,E31403,ilsa.boteofigueroa@apsva.us,Dorothy Hamm MS,Custodian,Facilities Staff
Jeremy,Wintersteen,E31406,jeremy.wintersteen@apsva.us,Dorothy Hamm MS,n/a,Unclassified
Maysoon,Salih,E31456,maysoon.salih@apsva.us,Dorothy Hamm MS,n/a,Unclassified
Herman,Hines,E31601,herman.hines@apsva.us,Dorothy Hamm MS,Custodian,Facilities Staff
Euvelys,Lizardo Castillo,E31646,euvelys.castillo@apsva.us,Dorothy Hamm MS,Maintenance Staff,Facilities Staff
Agnes,Agyare,E31680,agnes.agyare@apsva.us,Dorothy Hamm MS,Custodian,Facilities Staff
Carmen,Cuellar Apuri,E31708,carmen.cuellarapuri@apsva.us,Dorothy Hamm MS,Custodian,Facilities Staff
Tiffanie,Hegerty,E31745,tiffanie.hegerty2@apsva.us,Dorothy Hamm MS,Staff,Staff
Eric,Wagner,E31965,eric.wagner@apsva.us,Dorothy Hamm MS,Staff,Staff
Madison,Loszynski,E32059,madison.loszynski@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Craig,Rampersad,E32071,craig.rampersad@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Molly,Norrbom,E32075,molly.norrbom@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Donald,Marszalek,E32089,donald.marszalek@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Abigail,Gwydir,E32104,abigail.gwydir@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Laportia,Banks,E32105,laportia.banks@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Sabrina,McManus,E32110,sabrina.mcmanus@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Savannah,Lunger,E32171,savannah.lunger@apsva.us,Dorothy Hamm MS,"Instructional Assistant, Teacher",Instructional Staff
Candice,Barnes,E32244,candice.barnes@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Andreia,Martins De Farias,E32362,andreia.farias@apsva.us,Dorothy Hamm MS,Staff,Staff
Finn,Moran,E32388,finn.moran@apsva.us,Dorothy Hamm MS,Staff,Staff
Dianna,Collins,E32392,dianna.collins@apsva.us,Dorothy Hamm MS,Staff,Staff
Halit,Rosario,E32707,omar.rosario@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Dominique,Williams,E32788,dominique.williams@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Patrice,Allen,E32789,patrice.allen@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Thuan,Nguyen-Lakrik,E32835,thuan.nguyenlakrik@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Zulma,Flores Pineda,E32845,zulma.florespineda@apsva.us,Dorothy Hamm MS,Custodian,Facilities Staff
Dawn,Daly,E32899,dawn.daly@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Sunita,Tamang,E32927,sunita.tamang@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
KaMyka,Glenn,E32954,kamyka.glenn@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Gabriella,Richey,E32964,gabriella.richey@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Alfred,Reid,E33026,alfred.reid2@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Ruth,Hailu,E33170,ruth.hailu@apsva.us,Dorothy Hamm MS,Staff,Staff
Fanaye,Gessesse,E33206,fanaye.gessesse@apsva.us,Dorothy Hamm MS,n/a,Unclassified
Nicholas,Kean,E33309,nicholas.kean@apsva.us,Dorothy Hamm MS,"Instructional Assistant, Teacher",Instructional Staff
Sabiha,Sungur,E33569,sabiha.sungur@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Patty,Tuttle-Newby,E4053,patty.tuttle@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Robert,Tuttle,E4075,robert.tuttle@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Laurel,Cerrud,E414,laurel.cerrud@apsva.us,Dorothy Hamm MS,Assistant Principal,Administrative Staff
Elise,Krueger,E4303,elise.krueger@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Lisa,Moore,E6378,lisa.moore@apsva.us,Dorothy Hamm MS,Assistant Principal,Administrative Staff
Renee,Sidberry,E7008,renee.sidberry@apsva.us,Dorothy Hamm MS,Staff,Staff
Gisela,Caballero,E7425,gisela.caballero@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Rosa,Torres,E7563,rosa.torres@apsva.us,Dorothy Hamm MS,Maintenance Supervisor,Facilities Staff
Elizabeth,Meikle,E8100,elizabeth.meikle@apsva.us,Dorothy Hamm MS,Administrative Assistant,Staff
Henry,Cardenas,E8470,henry.cardenas@apsva.us,Dorothy Hamm MS,Instructional Assistant,Instructional Staff
Unika,Dabney,E8725,unika.dabney@apsva.us,Dorothy Hamm MS,Coordinator,Staff
Jane,Scruggs,E9124,jane.scruggs@apsva.us,Dorothy Hamm MS,Teacher,Instructional Staff
Lidia,Ordonez,E10027,lidia.ordonez@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Suzanne,Stork,E12133,suzanne.stork@apsva.us,Montessori,Teacher,Instructional Staff
Maryjean,Bruno,E15909,maryjean.bruno@apsva.us,Montessori,Teacher,Instructional Staff
Adelia,Penaloza De Navia,E16004,adelia.navia@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Jaspreet,Choudhary,E16047,jaspreet.choudhary@apsva.us,Montessori,Teacher,Instructional Staff
Katina,Wells,E1679,katina.wells@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Rigoberto,Pacheco,E17130,rigoberto.pacheco@apsva.us,Montessori,Custodian,Facilities Staff
Catherine,Kalkus,E19476,catherine.kalkus@apsva.us,Montessori,Teacher,Instructional Staff
Eileen,Lopatkiewicz,E19479,eileen.lopatkiewicz@apsva.us,Montessori,Teacher,Instructional Staff
Shanika,Peart-Payne,E20201,shanika.payne@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Heather,Barbour,E20405,heather.barbour@apsva.us,Montessori,Teacher,Instructional Staff
Martha,Marquez,E21947,martha.marquez@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Yolanda,Nashid,E22152,yolanda.nashid@apsva.us,Montessori,Assistant Principal,Administrative Staff
Reina,Martinez,E22351,reina.martinez@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Shakina,Hardy,E22661,shakina.hardy@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Sean,McGee,E22952,sean.mcgee@apsva.us,Montessori,Teacher,Instructional Staff
Emily,Hall,E22994,emily.hall@apsva.us,Montessori,Teacher,Instructional Staff
Lila,Ross,E23008,lila.ross@apsva.us,Montessori,Teacher,Instructional Staff
Ebonee,Fleming,E23256,ebonee.fleming@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Amie,Flowers Shakespeare,E23709,amie.shakespeare@apsva.us,Montessori,Teacher,Instructional Staff
Catharina,Genove,E23963,catharina.genove@apsva.us,Montessori,Principal,Unclassified
Kathleen,O'Donnell,E23999,kathleen.odonnell@apsva.us,Montessori,Teacher,Instructional Staff
Eve,Mendolia,E24030,eve.mendolia@apsva.us,Montessori,Teacher,Instructional Staff
Shanta,Joshi,E24227,shanta.joshi@apsva.us,Montessori,Teacher,Instructional Staff
Kiarra,Parker,E24578,kiarra.parker@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Freweini,Brhane,E24715,freweini.brhane@apsva.us,Montessori,Staff,Staff
Lisa,Hernandez,E24905,lisa.hernandez@apsva.us,Montessori,Teacher,Instructional Staff
Kirsten,Fletcher,E24949,kirsten.fletcher@apsva.us,Montessori,Teacher,Instructional Staff
Isis,Peake,E24979,isis.peake@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Shawntia,Martin,E25142,shawntia.martin@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Monet,Evans,E25389,monet.evans@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Ikea,Gunn,E25453,ikea.gunn@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Trinidad,Coopman,E25536,trinidad.coopman@apsva.us,Montessori,Teacher,Instructional Staff
Richard,Russey,E25949,richard.russey@apsva.us,Montessori,Teacher,Instructional Staff
Nicole,Hambleton,E25970,nicole.hambleton@apsva.us,Montessori,Teacher,Instructional Staff
Andrea,Hernandez,E26276,andrea.hernandez@apsva.us,Montessori,Teacher,Instructional Staff
Diane,Reeser,E2664,diane.reeser@apsva.us,Montessori,Teacher,Instructional Staff
Erik,Kamenski,E26775,erik.kamenski@apsva.us,Montessori,n/a,Unclassified
Barbara,Murphy,E27268,barbara.murphy2@apsva.us,Montessori,Teacher,Instructional Staff
Jennifer,Pappafotopoulos,E27737,jennifer.pirro@apsva.us,Montessori,Teacher,Instructional Staff
Joanna,Yamashita,E28153,joanna.yamashita@apsva.us,Montessori,Teacher,Instructional Staff
Yanelys,Barett,E28635,yanelys.barett@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Tanjuara,Taslim,E28872,tanjuara.taslim@apsva.us,Montessori,Staff,Staff
Kathleen,Habuki,E29086,kathleen.habuki@apsva.us,Montessori,Teacher,Instructional Staff
Adiya,Enkhtaivan,E29116,adiya.enkhtaivan@apsva.us,Montessori,Staff,Staff
Karina,Valdez,E29134,karina.valdez@apsva.us,Montessori,Administrative Assistant,Staff
Ingrid,Basurto,E29201,ingrid.basurto2@apsva.us,Montessori,Administrative Assistant,Staff
Tequila,Howard,E29231,tequila.howard@apsva.us,Montessori,Teacher,Instructional Staff
Shaunte,Bethea,E29339,shaunte.bethea@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Victoria,Hardesty-Allen,E29770,victoria.hardesty@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Dashdorj,Danzannyam,E29795,dashdorj.danzannyam@apsva.us,Montessori,Staff,Staff
Rebecca,Rubilotta,E30138,rebecca.rubilotta@apsva.us,Montessori,Teacher,Instructional Staff
Kia,Stevenson-Haynes,E30173,kia.stevensonhaynes@apsva.us,Montessori,Administrative Assistant,Staff
Micah,Williams,E30612,micah.williams@apsva.us,Montessori,Teacher,Instructional Staff
Kamala,Alatmeh,E30881,kamala.alatmeh@apsva.us,Montessori,Staff,Staff
Kay,Miller,E30966,kay.miller@apsva.us,Montessori,Teacher,Instructional Staff
Elisabeth,Brewer,E31119,elisabeth.brewer@apsva.us,Montessori,Teacher,Instructional Staff
Martha,Darif,E31258,martha.darif@apsva.us,Montessori,Teacher,Instructional Staff
Carol,Halvorson,E31272,carol.halvorson@apsva.us,Montessori,Teacher,Instructional Staff
Fawzia,Fazily,E31313,fawzia.fazily@apsva.us,Montessori,Teacher,Instructional Staff
Julie,Carson,E31514,julie.carson@apsva.us,Montessori,Teacher,Instructional Staff
Jennifer,Gonzaga,E31636,jennifer.gonzaga@apsva.us,Montessori,Administrative Assistant,Staff
Nadia,Cruceta Arias,E31756,nadia.crucetaarias@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Tamara,Freeman,E31826,tamara.freeman@apsva.us,Montessori,Teacher,Instructional Staff
Yancarla,Delgadillo Herrera,E31853,yancarla.delgadillo@apsva.us,Montessori,Staff,Staff
Megan,Kain,E32001,megan.kain@apsva.us,Montessori,Teacher,Instructional Staff
Victoria,Stephens,E32063,victoria.stephens@apsva.us,Montessori,Teacher,Instructional Staff
Heidi,Novak,E32125,heidi.novak@apsva.us,Montessori,Teacher,Instructional Staff
Kristine,Henderson,E32335,kristine.henderson@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Dora,Raymundo,E3242,dora.cruz@apsva.us,Montessori,Custodian,Facilities Staff
Katherine,Gomez,E32812,katherine.gomez@apsva.us,Montessori,Teacher,Instructional Staff
Rebecca,Trytten,E33279,rebecca.trytten@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Guido,Mercado,E4556,guido.mercado@apsva.us,Montessori,Maintenance Supervisor,Facilities Staff
Rachel,Kipperman,E68,rachel.kipperman@apsva.us,Montessori,Teacher,Instructional Staff
Jennifer,Lindenauer,E6905,jennifer.lindenauer@apsva.us,Montessori,Teacher,Instructional Staff
Randy,Glasner,E692,randy.glasner@apsva.us,Montessori,n/a,Unclassified
Silvia,Yzquieta,E7253,silvia.yzquieta@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Richard,Roberts,E7254,richard.roberts@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Maria,Diaz,E8234,maria.diaz@apsva.us,Montessori,Custodian,Facilities Staff
Amreen,Alvi,E826,amreen.alvi@apsva.us,Montessori,Teacher,Instructional Staff
Henry,Cardenas,E8470,henry.cardenas@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Laverne,Gant,E8911,laverne.gant@apsva.us,Montessori,Instructional Assistant,Instructional Staff
Suneeta,Maheshwari,E9724,suneeta.maheshwari@apsva.us,Montessori,Teacher,Instructional Staff
Nazia,Bano,E14542,nazia.bano@apsva.us,Arlington Traditional Summer Program,Staff,Staff
Shyara,Cherubim,E15888,shyara.cherubim@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Molly,Poth,E19149,molly.poth@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Emma,Hernandez,E20241,emma.hernandez@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Rebecca,Ferree,E20691,rebecca.ferree@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Katherine,Kerley,E20875,katherine.kerley@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Rebecca,Kallem,E20918,rebecca.kallem@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Capri,Kilby,E22517,capri.kilby@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Carol,Strohl,E23359,carol.strohl@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Keenan,Hunt,E23911,keenan.hunt@apsva.us,Arlington Traditional Summer Program,Maintenance Supervisor,Facilities Staff
Kate,Neal,E24060,kate.neal@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Sara,Mehrnama,E24732,sara.mehrnama@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Christine,A'Hearn,E24908,christine.ahearn@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Dana,Probasco,E26589,dana.probasco@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Lauren,Worley,E26736,lauren.worley@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Melinda,Phillips,E26895,melinda.phillips@apsva.us,Arlington Traditional Summer Program,Assistant Principal,Administrative Staff
Emily,Maniace,E26980,emily.maniace@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Naomi,Moir,E27016,naomi.moir@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Jennifer,Hernandez,E27425,jennifer.hernandez@apsva.us,Arlington Traditional Summer Program,Administrative Assistant,Staff
Megan,Moran,E27741,megan.moran@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Maya,Ward,E27940,maya.ward@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Mary,Ettelt,E28563,mary.ettelt2@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Adiya,Enkhtaivan,E29116,adiya.enkhtaivan@apsva.us,Arlington Traditional Summer Program,Staff,Staff
Claudia,Tavera,E29380,claudia.tavera@apsva.us,Arlington Traditional Summer Program,Administrative Assistant,Staff
Jessica,Poveda Reyes,E29483,jessica.povedareyes2@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Drake,Anderson,E29513,drake.anderson@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Deanna,Beckham,E29610,deanna.beckham@apsva.us,Arlington Traditional Summer Program,Staff,Staff
Carrie,Cleary,E29968,carrie.cleary@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Julienne,Hawald,E30042,julienne.hawald@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Emily,Garcia,E30886,emily.garcia@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Yazmin,Murray,E30897,yazmin.murray@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Kathryn,Petroskey,E30903,kathryn.petroskey2@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Lakia,Dozier,E30910,lakia.dozier@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Kaitlyn,Mejia,E31064,kaitlyn.mejia@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Jill,Kester,E31253,jill.kester@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Marissa,Ezrow,E31261,marissa.ezrow@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Lori,Wilsher,E31279,lori.wilsher@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Bouria,Hallato,E31425,bouria.hallato@apsva.us,Arlington Traditional Summer Program,Staff,Staff
Macarena,Aristegui,E31446,macarena.aristegui@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Ebert,Velasquez Blanco,E31547,ebert.velasquez@apsva.us,Arlington Traditional Summer Program,Custodian,Facilities Staff
Samira,Kaidi,E31738,samira.kaidi@apsva.us,Arlington Traditional Summer Program,Staff,Staff
Timothy,Robinson,E32005,timothy.robinson@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Erica,Kulas,E32072,erica.kulas@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Anna,Sellers,E32082,anna.sellers@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Erin,Rudzewicz,E32094,erin.rudzewicz@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Johnna,McGreevy,E32102,johnna.mcgreevy@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Gretchen,Mills,E32117,gretchen.mills@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Rachel,Hunter,E32156,rachel.hunter@apsva.us,Arlington Traditional Summer Program,Administrative Assistant,Staff
Lizzie,Rubio,E32269,lizzie.rubio@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Isai,Reyes,E32349,isai.reyes@apsva.us,Arlington Traditional Summer Program,Custodian,Facilities Staff
Johanna,Tobar Navas,E32449,johanna.tobarnavas@apsva.us,Arlington Traditional Summer Program,Custodian,Facilities Staff
Katherine,Holman,E32575,katherine.holman@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Sophia,Ritt,E32576,sophia.ritt@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Tara,Bokum,E32578,tara.bokum@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Perry,Meade,E32629,perry.meade@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Maria,Garrett,E32653,maria.garrett@apsva.us,Arlington Traditional Summer Program,Custodian,Facilities Staff
Paula,Grueninger,E32695,paula.grueninger@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Margaret,Connelly,E32702,margaret.connelly@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Jonah,Klixbull,E32706,jonah.klixbull@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Cameron,Tefft,E33374,cameron.tefft@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Linda,Butcher,E33425,linda.butcher@apsva.us,Arlington Traditional Summer Program,Administrative Assistant,Staff
Sarah,Congable,E4964,sarah.congable@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Vanessa,Vasquez-Guzman,E5069,vanessa.vasquez@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Shahida,Rafique,E5696,shahida.rafique@apsva.us,Arlington Traditional Summer Program,Staff,Staff
Lesly,Pineda,E6733,lesly.lopez@apsva.us,Arlington Traditional Summer Program,Instructional Assistant,Instructional Staff
Claire,Peters,E7358,claire.peters@apsva.us,Innovation Elementary School,Principal,Unclassified
Ellen,Vicens,E7616,ellen.vicens@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Keith,Knott,E8396,keith.knott@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Julie,Bato,E968,julie.bato@apsva.us,Arlington Traditional Summer Program,Teacher,Instructional Staff
Nancy,Neubert,E100,nancy.neubert@apsva.us,Substitutes,Staff,Unclassified
Susan,Wulf,E10012,susan.wulf@apsva.us,Administrative Services,Staff,Staff
Angela,Hillman,E10030,angela.hillman@apsva.us,Food Services,Food Services Staff,Food Services Staff
Jane,Coonce,E10058,jane.coonce@apsva.us,Adult Education,Staff,Staff
Mary,Weinberg,E10066,mary.weinberg@apsva.us,Office of the Superintendent,Staff,Staff
Desalegne,Eshite,E10102,desalegne.eshite@apsva.us,Transportation,Bus Driver,Transportation Staff
Beronica,Salas,E10104,beronica.salas@apsva.us,Intake Center,Staff,Staff
Elizabeth,Rojas,E10108,elizabeth.rojas@apsva.us,Transportation,Bus Driver,Transportation Staff
Shirley,Monroe,E10113,shirley.monroe@apsva.us,Transportation,Staff,Staff
William,White,E10187,william.white@apsva.us,Facilities and Operations,Staff,Staff
David,Schloe,E10215,david.schloe@apsva.us,Information Services,Technical Staff,Staff
Bobby,Lassiter,E10382,bobby.lassiter@apsva.us,Adult Education,Staff,Staff
Cynthia,Keels,E10436,cynthia.keels@apsva.us,"Food Services, Transportation","Staff, Bus Attendant",Staff
Tarica,Mason,E10446,tarica.mason@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Mary,Sweeney,E10517,mary.sweeney@apsva.us,Substitutes,Staff,Staff
Marilyn,Uveges,E1064,marilyn.uveges@apsva.us,Administrative Services,Staff,Staff
Mahi,Savage,E10725,mahi.savage@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Patricia,Apfelbaum,E10782,patricia.apfelbaum@apsva.us,Substitutes,Staff,Staff
Estelle,Roth,E10802,estelle.roth@apsva.us,APS,Teacher,Instructional Staff
Catrina,Moran,E10825,catrina.moran@apsva.us,Substitutes,Staff,Staff
Patricia,Myles,E1085,patricia.myles@apsva.us,Substitutes,Staff,Staff
Lisa,Powell,E10888,lisa.powell@apsva.us,Food Services,Food Services Staff,Food Services Staff
Keisha,Andrews-Veney,E10891,keisha.andrews@apsva.us,Extended Day Substitutes,Staff,Staff
Rosa,Ewell,E11030,rosa.ewell@apsva.us,Division of Instruction,Administrative Assistant,Staff
Wendy,Carria,E11040,wendy.carria@apsva.us,Student Services,Supervisor,Administrative Staff
Jeanette,Preniczky,E11128,jeanette.preniczky@apsva.us,Division of Instruction,Staff,Staff
Melissa,Leupp,E11155,melissa.leupp@apsva.us,Special Education,Teacher,Instructional Staff
Mary,Begley,E11187,mary.begley@apsva.us,Administrative Services,Principal,Administrative Staff
Elena,Vidrascu,E11217,elena.vidrascu@apsva.us,REEP Program,Staff,Staff
Steven,Stricker,E11239,steven.stricker@apsva.us,Facilities and Operations,Manager,Staff
Chris,Willmore,E11294,chris.willmore@apsva.us,Administrative Services,Principal,Administrative Staff
Susan,Randall,E11321,susan.randall@apsva.us,"Administrative Services, Substitutes, Division of Instruction","Staff, Staff, Staff",Staff
Barbara,Thompson,E11335,barbara.thompson@apsva.us,Administrative Services,Principal,Administrative Staff
Tonique,Mason,E11372,tonique.mason@apsva.us,Extended Day,Staff,Staff
Renee,Harber,E11435,renee.harber@apsva.us,Facilities and Operations,Assistant Superintendent,Administrative Staff
Ruijuan,Xia,E11563,ruijuan.xia@apsva.us,Information Services,Analyst,Staff
Kelly,Breedlove,E11579,kelly.breedlove@apsva.us,Division of Instruction,Teacher,Instructional Staff
Kerm,Towler,E11621,kerm.towler@apsva.us,Facilities and Operations,Specialist,Staff
Clark,Jones,E11686,clark.jones@apsva.us,Special Education,Teacher,Instructional Staff
Karen,Scheer,E1170,karen.scheer@apsva.us,Division of Instruction,Staff,Staff
Joanne,Uyeda,E11722,joanne.uyeda@apsva.us,"Administrative Services, Office of Personnel","Staff, Staff",Staff
Brian,Stapleton,E11786,brian.stapleton@apsva.us,Special Education,Teacher,Instructional Staff
Meg,Tuccillo,E1185,meg.tuccillo@apsva.us,"Administrative Services, Facilities and Operations","Staff, Staff",Staff
Julie,Henry,E11859,julie.henry@apsva.us,Special Education,Teacher,Instructional Staff
Cynthia,Evans,E11860,cynthia.evans@apsva.us,Special Education,Teacher,Instructional Staff
Jamie,Borg,E1190,jamie.borg@apsva.us,Administrative Services,Principal,Administrative Staff
Shannan,Ellis,E11910,shannan.ellis@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Erin,Wales-Smith,E1193,erin.walessmith@apsva.us,Office of Personnel,Director,Administrative Staff
Robin,Gardner,E11937,robin.gardner@apsva.us,Division of Instruction,Coordinator,Staff
Maria,Kaplan,E1197,maria.kaplan@apsva.us,Intake Center,Staff,Staff
Susan,Palmore,E1198,susan.palmore@apsva.us,Family Center,Staff,Staff
Renee,Shaw,E12049,renee.shaw@apsva.us,Division of Instruction,Coordinator,Staff
Katherine,Carey,E1208,katherine.carey@apsva.us,Division of Instruction,Staff,Staff
Tamar,Raphaeli Rava,E12094,tamar.rava@apsva.us,Student Services,Staff,Staff
Marleny,Perdomo,E12135,marleny.perdomo@apsva.us,Administrative Services,Principal,Administrative Staff
Meg,Davis,E12182,meg.davis@apsva.us,Special Education,Teacher,Instructional Staff
Sara,Witherow,E122,sara.witherow@apsva.us,Information Services,Analyst,Staff
Web,Grandish,E12205,weber.grandish@apsva.us,Information Services,Technology Architect,Staff
Lori,Ockerman,E12214,lori.ockerman@apsva.us,Student Services,Teacher,Instructional Staff
Jennifer,Doll,E12266,jennifer.doll@apsva.us,Special Education,Teacher,Instructional Staff
Susan,Senn,E12335,susan.senn@apsva.us,"Administrative Services, Substitutes, Division of Instruction","Staff, Staff, Staff",Staff
Mary,McLean,E12386,mary.mclean@apsva.us,Substitutes,Staff,Staff
Chris,Brown,E12389,chris.brown@apsva.us,Information Services,Technology Architect,Staff
Stacy,Clark,E12406,stacy.clark@apsva.us,REEP Program,Staff,Staff
Carolyn,Thiell,E12621,carolyn.thiell@apsva.us,Special Education,Teacher,Instructional Staff
Ratree,Bracey,E12698,ratree.bracey@apsva.us,Office of Personnel,Specialist,Staff
Bridgid,Hendrixson,E12743,bridgid.hendrixson@apsva.us,Special Education,Teacher,Instructional Staff
An,Ong,E12771,an.ong@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Laura,Winfrey,E12776,laura.winfrey@apsva.us,Substitutes,Staff,Staff
Sandra,O'Connor,E12781,sandra.oconnor@apsva.us,Food Services,Specialist,Staff
Amber,Siegel,E12864,amber.siegel@apsva.us,Special Education,Teacher,Instructional Staff
Lynne,Kozma,E12885,lynne.kozma@apsva.us,Administrative Services,Staff,Staff
Matthew,Kennedy,E12919,matthew.kennedy@apsva.us,REEP Program,Staff,Staff
Maritza,Ergueta,E12991,maritza.ergueta@apsva.us,Intake Center,Staff,Staff
Ruth,Drucker,E12992,ruth.drucker@apsva.us,Intake Center,Staff,Staff
Gebreselassie,Tekle Tedla,E13035,gebreselassie.tedla@apsva.us,Transportation,Bus Driver,Transportation Staff
Margaret,Jones,E13161,margaret.jones@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Patricia,Hammond,E13206,patricia.hammond@apsva.us,Division of Instruction,Staff,Staff
Hattie,Harmon,E13265,hattie.harmon@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Ricia,Weiner,E13377,ricia.weiner@apsva.us,Student Services,Teacher,Instructional Staff
Dagmar,Waters,E13421,dagmar.waters@apsva.us,Adult Education,Staff,Staff
Samrach,Nguon,E13497,samrach.nguon@apsva.us,Transportation,Bus Driver,Transportation Staff
Marta,Romero,E13531,marta.romero@apsva.us,Intake Center,Staff,Staff
Lilyan,Williams,E13607,lilyan.williams@apsva.us,Special Education,Teacher,Instructional Staff
Toni,Washington,E1365,toni.washington@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Anna,Samayoa,E13669,anna.samayoa@apsva.us,Office of Finance,Account Clerk,Staff
Amy,Davis,E13747,amy.davis@apsva.us,Special Education,Teacher,Instructional Staff
Charles,Lihn,E13775,charles.lihn@apsva.us,Division of Instruction,Staff,Staff
Betty,Reid,E13801,betty.reid@apsva.us,Substitutes,Staff,Staff
Marichu,Bucelli,E13836,marichu.bucelli@apsva.us,Adult Education,Staff,Staff
Matt,Smith,E13839,matt.smith@apsva.us,Information Services,Coordinator,Staff
Marvin,Flores,E13871,marvin.flores@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Elizabeth,Allen,E13954,elizabeth.allen@apsva.us,Substitutes,Staff,Staff
Marjorie,Myers,E13987,marjorie.myers@apsva.us,Administrative Services,Staff,Staff
Raphaella,Morard-Vogel,E14018,raphaella.morard@apsva.us,Division of Instruction,Clerical Specialist,Staff
Terance,Proctor,E14046,terance.proctor@apsva.us,Information Services,Director,Administrative Staff
Hung,Do,E14054,hung.do@apsva.us,Information Services,Engineer,Staff
Sharmane,Franklin,E14061,sharmane.franklin@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Lutfun,Nahar,E14143,lutfun.nahar@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Abdelaziz,Abbas,E14170,abdelaziz.abbas@apsva.us,Transportation,Bus Driver,Transportation Staff
Joseph,Pope,E1420,joseph.pope@apsva.us,Student Services,Staff,Staff
Joan,Bickelhaupt,E1423,joan.bickelhaupt@apsva.us,Administrative Services,Staff,Staff
Carmelita,Montesa,E14258,carmelita.montesa@apsva.us,Adult Education,Staff,Staff
Josefa,Sarmiento,E14271,josefa.sarmiento@apsva.us,Adult Education,Staff,Staff
Pamela,Persaud,E14276,pamela.persaud@apsva.us,Food Services,Staff,Staff
Kris,Martini,E14322,kris.martini@apsva.us,Adult Education,Director,Administrative Staff
Johanna,Kehoe,E14349,johanna.kehoe@apsva.us,Adult Education,Staff,Staff
Charron,Mckethean,E14391,charron.mckethean@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Deborah,DeFranco,E144,deborah.defranco@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Ashey,Deljo,E14419,ashey.deljo@apsva.us,Special Education,Specialist,Staff
Sopha,Touch,E14442,sopha.touch@apsva.us,Transportation,Bus Attendant,Transportation Staff
Noorsama,Ahmadzai,E14447,noorsama.ahmadzai@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Isabel,Castellanos,E14454,isabel.salazar@apsva.us,Extended Day Substitutes,Staff,Staff
Corina,Coronel,E14463,corina.coronel@apsva.us,Intake Center,Coordinator,Administrative Staff
Michael,Goodman,E14472,michael.goodman@apsva.us,Division of Instruction,Coordinator,Staff
Glorismel,Chavez,E14476,glorismel.chavez@apsva.us,Transportation,Bus Driver,Transportation Staff
Brigido,Aguilar,E14480,brigido.aguilar@apsva.us,Relief Custodians,Custodian,Facilities Staff
Magna,Hawkins,E14533,magna.hawkins@apsva.us,REEP Program,Clerical Specialist,Staff
Nazia,Bano,E14542,nazia.bano@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Martha,Valdez,E14647,martha.valdez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Hildi,Pardo,E14693,hildi.pardo@apsva.us,Information Services,Administrator,Staff
Christine,Katcher,E14711,christine.katcher@apsva.us,Student Services,Teacher,Instructional Staff
Irma,Terrazas,E14714,irma.terrazas@apsva.us,Intake Center,Staff,Staff
Donna,Townsend,E14724,donna.townsend@apsva.us,Special Education,Teacher,Instructional Staff
Sharon,Hopkins,E14758,sharon.hopkins@apsva.us,Food Services,Food Services Staff,Food Services Staff
Charlene,Giles,E14770,charlene.giles@apsva.us,Student Services,Teacher,Instructional Staff
Carla,Williams,E14775,carla.williams@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Maria,Abarca,E14798,maria.abarca@apsva.us,"Substitutes, Intake Center","Staff, Staff",Staff
Deborah,Gilman,E14818,deborah.gilman@apsva.us,Special Education,Teacher,Instructional Staff
Jonathan,Velsey,E14850,jonathan.velsey@apsva.us,APS,Teacher,Instructional Staff
Elaine,Perkins,E1487,elaine.perkins@apsva.us,Special Education,Coordinator,Staff
Shrita,Squire,E14876,shrita.squire@apsva.us,Special Education,Teacher,Instructional Staff
Sohana,Siddique,E1489,sohana.siddique@apsva.us,"Substitutes, Intake Center, Adult Education","Substitute, Staff, Staff",Staff
Sara,Tekle,E14902,sara.tekle@apsva.us,Transportation,Bus Driver,Transportation Staff
Monica,Sugaray,E14906,monica.sugaray@apsva.us,Intake Center,Staff,Staff
Yvonne,Pettiford,E15046,yvonne.pettiford@apsva.us,Family Center,"Teacher, Staff",Staff
Karen,Key,E15057,karen.key@apsva.us,"Food Services, Extended Day","Food Services Staff, Staff",Food Services Staff
Hilda,Andrade,E15073,hilda.andrade@apsva.us,"Substitutes, Intake Center, Division of Instruction","Staff, Staff, Staff",Staff
Everton,Anderson,E15123,everton.anderson@apsva.us,Information Services,Specialist,Staff
James,Jefferson,E15126,james.jefferson@apsva.us,Transportation,Bus Driver,Transportation Staff
Marguerite,Fleming,E15157,marguerite.fleming@apsva.us,Extended Day,Staff,Staff
Michelle,Mccarthy,E1520,michelle.mccarthy@apsva.us,Administrative Services,Principal,Administrative Staff
Cara,Jones,E15207,cara.jones@apsva.us,Student Services,Teacher,Instructional Staff
Digna,Alejandro,E15285,digna.alejandro@apsva.us,Intake Center,Clerical Specialist,Staff
Thanh,Thai,E15342,thanh.thai@apsva.us,Office of Finance,Administrative Technician,Staff
Tung,Mullen,E15350,tung.mullen@apsva.us,Substitutes,Staff,Staff
Peter,Kinberg,E1536,peter.kinberg@apsva.us,Substitutes,Staff,Staff
Dat,Le,E15368,dat.le@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Thanh,Au,E15372,thanh.au@apsva.us,Facilities and Operations,Carpenter,Facilities Staff
Ethel,Edmond,E15385,ethel.edmond@apsva.us,Extended Day,Staff,Staff
Ramona,Harris,E15396,ramona.harris@apsva.us,Office of Personnel,Administrative Assistant,Staff
Pamela,Nagurka,E15413,pamela.nagurka@apsva.us,Adult Education,Teacher,Instructional Staff
Renee,Gorsky,E15460,renee.gorsky@apsva.us,Substitutes,Staff,Staff
Mengistu,Haile,E15476,mengistu.haile@apsva.us,Transportation,Bus Driver,Transportation Staff
Jeannette,Hunter,E15483,jeannette.hunter@apsva.us,Food Services,Food Services Staff,Food Services Staff
Jeffrey,Humphries,E15486,jeffrey.humphries@apsva.us,Aquatics,Manager,Staff
Francisco,Martinez,E15550,francisco.martinez@apsva.us,Student Services,Teacher,Instructional Staff
Fatumo,Mose,E15594,fatumo.mose@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Mamit,Gebreegziabher,E15632,mamit.gebreegziabher@apsva.us,"Intake Center, Extended Day Substitutes","Staff, Staff",Staff
Shubhda,Fajfar,E15681,shubhda.fajfar@apsva.us,"Substitutes, Special Education","Staff, Staff",Staff
Joelle,Banza,E15687,joelle.ikete@apsva.us,"Food Services, Transportation","Staff, Bus Attendant",Staff
Juancarlos,Trejo,E15707,juancarlos.trejo@apsva.us,Facilities and Operations,Electrician,Facilities Staff
Tiffany,Anderson,E15733,tiffany.anderson@apsva.us,Information Services,Specialist,Staff
Holline,Bramlett,E1575,holline.bramlett@apsva.us,Food Services,Food Services Staff,Food Services Staff
William,Breslin,E15779,william.breslin@apsva.us,Information Services,Analyst,Staff
Gradiva,Jimenez,E15793,gradiva.jimenez@apsva.us,Intake Center,Staff,Staff
Carmela,Del Vecchio,E15828,carmela.delvecchio@apsva.us,Special Education,Teacher,Instructional Staff
Mary,Dolby-Mcdonald,E15837,mary.dolbymcdonald@apsva.us,Student Services,Teacher,Instructional Staff
Asmeret,Tesfai,E15880,asmeret.tesfai@apsva.us,Transportation,Bus Driver,Transportation Staff
Susan,Bhalla,E15904,susan.bhalla@apsva.us,Special Education,Teacher,Instructional Staff
Heidi,Smith,E15908,heidi.smith@apsva.us,Administrative Services,Principal,Administrative Staff
Kristin,Gomez,E15912,kristin.gomez@apsva.us,APS,Teacher,Instructional Staff
Antonio,Hall,E15922,antonio.hall@apsva.us,Administrative Services,Principal,Administrative Staff
Maria,Flores,E15945,maria.flores2@apsva.us,Food Services,Food Services Staff,Food Services Staff
Jennie,Wallace,E15968,jennie.wallace@apsva.us,Special Education,Administrative Assistant,Staff
Margaret,Staeben,E15978,margaret.staeben@apsva.us,Division of Instruction,Staff,Staff
Den,So,E15982,den.so@apsva.us,Transportation,Bus Driver,Transportation Staff
Deriba,Reba,E15984,deriba.reba@apsva.us,Transportation,Coordinator,Staff
Michele,Karnbach,E15989,michele.karnbach@apsva.us,Division of Instruction,Teacher,Instructional Staff
Kathleen,Kane,E1599,kathleen.coyne@apsva.us,Division of Instruction,Teacher,Instructional Staff
Jose,Turcios,E15992,jose.turcios@apsva.us,Relief Custodians,Custodian,Facilities Staff
Aschalech,Siyoum,E15993,aschalech.siyoum@apsva.us,Transportation,Staff,Staff
Mercedes,Larrain,E16016,mercedes.larrain@apsva.us,Food Services,Food Services Staff,Food Services Staff
Shelly,Saunders,E16020,shelly.saunders@apsva.us,Aquatics,Manager,Staff
Ellen,Smith,E1604,ellen.smith@apsva.us,Administrative Services,Principal,Administrative Staff
Jodi,Cohen,E16044,jodi.cohen@apsva.us,"Substitutes, Student Services","Substitute, Staff",Staff
Bolormaa,Jugdersuren,E16049,bolormaa.jugdersuren@apsva.us,Adult Education,Staff,Staff
Alice,Blair,E16056,alice.blair@apsva.us,Special Education,Teacher,Instructional Staff
Clifford,Tripp,E16057,clifford.tripp@apsva.us,Food Services,Food Services Staff,Food Services Staff
Jacqueline,Gibbs,E16061,jacqueline.gibbs@apsva.us,Facilities and Operations,Electrician,Facilities Staff
Mitch,Pascal,E1613,mitch.pascal@apsva.us,Administrative Services,Principal,Administrative Staff
Ashley,Leftwich,E16139,ashley.leftwich@apsva.us,Transportation,Bus Attendant,Transportation Staff
Lauren,Bonnet,E16148,lauren.bonnet@apsva.us,Special Education,Teacher,Instructional Staff
Neeru,Tandon,E16152,neeru.tandon@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Celia,Galdo,E16153,celia.galdo@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kathleen,Bragaw,E1616,kathleen.bragaw@apsva.us,Division of Instruction,Teacher,Instructional Staff
Bryan,Eckerson,E16195,bryan.eckerson@apsva.us,School and Community Relations,Producer,Staff
Chris,Reid,E16207,chris.reid@apsva.us,Division of Instruction,Clerical Specialist,Staff
Cathy,Wague,E16243,cathy.wague@apsva.us,Division of Instruction,Coordinator,Staff
Tamiru,Mekuria,E16246,tamiru.mekuria@apsva.us,Transportation,Bus Driver,Transportation Staff
Charles,Bell,E16247,charles.bell@apsva.us,Transportation,Bus Driver,Transportation Staff
Troy,Moore,E16259,troy.moore@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Luz,Garcia De Solares,E16260,luz.garciadesolares@apsva.us,Extended Day,Staff,Staff
Samuel,Zewede,E16268,samuel.zewede@apsva.us,Transportation,Bus Driver,Transportation Staff
Idris,Abdu,E16269,idris.abdu@apsva.us,Transportation,Bus Driver,Transportation Staff
Milagros,Martinez,E1627,milagros.martinez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Mitiku,Denta,E16270,mitiku.denta@apsva.us,Transportation,Bus Driver,Transportation Staff
Alison,Sheahan,E16276,alison.sheahan@apsva.us,Substitutes,Staff,Staff
Maria,Mendoza,E16277,maria.mendoza2@apsva.us,Food Services,Food Services Staff,Food Services Staff
Maria,Maza,E16369,maria.maza@apsva.us,REEP Program,Staff,Staff
Rosio,Gonzales,E16372,rosio.gonzales@apsva.us,Transportation,Bus Driver,Transportation Staff
Elizabeth,Dempsey,E16381,elizabeth.dempsey@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Salaheldin,Mohamed,E16386,salaheldin.mohamed@apsva.us,"Food Services, Extended Day Substitutes","Food Services Staff, Staff",Food Services Staff
Tsege,Selassie,E16392,tsege.selassie@apsva.us,Substitutes,Staff,Staff
Maribel,Melendez,E16400,maribel.melendez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Alphonso,Medley Jr,E16404,alphonso.medleyjr@apsva.us,Transportation,Bus Driver,Transportation Staff
Veronica,Valdes,E16405,veronica.valdes@apsva.us,Student Services,Teacher,Instructional Staff
Veronica,Covarrubias,E16432,veronica.covarrubias@apsva.us,Division of Instruction,Staff,Staff
Blanca,Guevara,E16438,blanca.guevara@apsva.us,Food Services,Food Services Staff,Food Services Staff
Marilyn,Mcallister,E16444,marilyn.mcallister@apsva.us,Substitutes,Substitute,Staff
Ram,Thing,E16461,ram.thing@apsva.us,Relief Custodians,Custodian,Facilities Staff
Abdelillah,Razah,E16501,abdelillah.razah@apsva.us,Extended Day,Staff,Staff
Janeth,Sanguinetti,E16571,janeth.sanguinetti@apsva.us,Division of Instruction,Analyst,Staff
Erin,Russo,E16604,erin.russo@apsva.us,Administrative Services,Principal,Administrative Staff
Sharmain,Smith,E16628,sharmain.smith@apsva.us,"Substitutes, Extended Day","Staff, Staff",Staff
Lemlem,Misgina,E16629,lemlem.misgina@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Allison,Walker,E16636,allison.walker@apsva.us,Substitutes,Staff,Staff
Tracy,Gaither,E16664,tracy.gaither@apsva.us,Administrative Services,Principal,Administrative Staff
Mary,Smith,E16669,mary.smith@apsva.us,Division of Instruction,Staff,Staff
Kevin,Clark,E16676,kevin.clark@apsva.us,Administrative Services,Principal,Administrative Staff
Frank,Bellavia,E16736,frank.bellavia@apsva.us,School and Community Relations,Director,Staff
Niema,Yosuf,E16738,niema.yosuf@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Shari,Brown,E16775,shari.brown@apsva.us,Adult Education,Teacher,Instructional Staff
Sayonda,Yen,E16786,sayonda.yen@apsva.us,Transportation,Bus Attendant,Transportation Staff
Grace,Santarelli,E16789,grace.santarelli@apsva.us,Student Services,Teacher,Instructional Staff
Mary,Clendenning,E168,mary.clendenning@apsva.us,Division of Instruction,Staff,Staff
Gina,Boller,E16803,gina.boller@apsva.us,Substitutes,Staff,Staff
Ziyin,Naod,E16820,ziyen.getaw@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Tamir,Belachew,E16832,tamir.belachew@apsva.us,Transportation,Bus Attendant,Transportation Staff
Mary,Stump,E1685,mary.stump@apsva.us,Substitutes,Staff,Staff
Gina,Gomez,E16859,gina.gomez@apsva.us,Special Education,Teacher,Instructional Staff
Matilde,Lopez De Torrico,E16864,matilde.torrico@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Angela,Greene,E16868,angela.greene@apsva.us,REEP Program,Staff,Staff
Bharti,Temkin,E16877,bharti.temkin@apsva.us,Substitutes,Staff,Staff
Jonathan,Martinez,E16898,jonathan.martinez@apsva.us,Special Education,Administrative Assistant,Staff
Martha,Galeano,E16904,martha.galeano@apsva.us,Intake Center,Staff,Staff
Marc,Zeballos,E16921,marc.zeballos@apsva.us,Intake Center,Staff,Staff
Russell,Norfleet,E16936,russell.norfleet@apsva.us,Substitutes,Staff,Staff
Sandra,Stoppel,E16937,sandra.stoppel@apsva.us,Special Education,Teacher,Instructional Staff
Ralph,Billings,E16962,ralph.billings@apsva.us,Facilities and Operations,Maintenance Technician,Facilities Staff
Michael,Freda,E16963,michael.freda@apsva.us,Office of Finance,Analyst,Staff
Deiry,Trejo,E16969,deiry.trejo@apsva.us,Food Services,Clerical Specialist,Staff
Althea,Williams,E16998,althea.williams@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Phyllis,Thompson,E17012,phyllis.thompson@apsva.us,Student Services,Teacher,Instructional Staff
Bijayashree,Paudyal,E17022,bijayashree.paudyal@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Amy,Jones,E17031,amy.jones@apsva.us,Facilities and Operations,Administrative Specialist,Staff
Paul,Velit,E17041,paul.velit@apsva.us,Information Services,Analyst,Staff
Barbara,Molina,E1705,barbara.molina@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Karen,Reich,E17055,karen.reich@apsva.us,Student Services,Teacher,Instructional Staff
Amy,Maclosky,E17056,amy.maclosky@apsva.us,Office of Finance,Director,Staff
Tsega,Woldu,E17081,tsega.woldu@apsva.us,Food Services,Food Services Staff,Food Services Staff
Tsega,Mekonnen,E17089,tsega.mekonnen@apsva.us,"Substitutes, Intake Center","Staff, Staff",Staff
Teshale,Mengesha,E17101,teshale.mengesha@apsva.us,Transportation,Bus Driver,Transportation Staff
Frank,Howell,E17117,frank.howell@apsva.us,Facilities and Operations,Carpenter,Facilities Staff
James,Meikle,E17123,james.meikle@apsva.us,Facilities and Operations,Director,Staff
Ana,Soto,E17124,ana.soto@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Judith,Knight,E1722,judith.knight@apsva.us,Administrative Services,Staff,Staff
Angela,Jones,E1726,angela.jones@apsva.us,Special Education,Staff,Staff
Kristin,Devaney,E1751,kristin.devaney@apsva.us,Student Services,Supervisor,Administrative Staff
Michael,Zito,E1798,michael.zito@apsva.us,Substitutes,Staff,Staff
Kelly,Krug,E1857,kelly.krug@apsva.us,Division of Instruction,Director,Administrative Staff
Muhammad,Mughal,E1858,muhammad.mughal@apsva.us,Transportation,Bus Driver,Transportation Staff
Georgette,Miller,E1873,georgette.miller@apsva.us,"Substitutes, Adult Education","Staff, Staff",Staff
Dawn,Smith,E19008,dawn.smith@apsva.us,School and Community Relations,Coordinator,Staff
Angel,Gonzales,E19010,angel.gonzales@apsva.us,Transportation,Bus Driver,Transportation Staff
Tsigereda,Senbete,E19015,tsigereda.senbete@apsva.us,Transportation,Bus Driver,Transportation Staff
Haileslassie,Abreha,E19016,haileslassie.abreha@apsva.us,Transportation,Bus Driver,Transportation Staff
Ruth,Aregai,E19019,ruth.aregai@apsva.us,Transportation,Bus Driver,Transportation Staff
Julio,Hijar-Silva,E19030,julio.hijarsilva@apsva.us,Transportation,Bus Driver,Transportation Staff
Gladis,Cruz Zyoud,E19031,gladis.zyoud@apsva.us,Transportation,Bus Driver,Transportation Staff
Jeancarroll,Browder,E19036,jeancarroll.browder@apsva.us,Substitutes,Staff,Staff
Leticia,Macias,E19136,leticia.macias@apsva.us,Office of Personnel,Specialist,Staff
Heng,Chea,E19139,heng.chea@apsva.us,Transportation,Bus Attendant,Transportation Staff
Elaina,Eliopoulos,E19155,elaina.eliopoulos@apsva.us,Student Services,Staff,Staff
Jessica,Leslie,E19172,jessica.leslie@apsva.us,Division of Instruction,Instructional Assistant,Instructional Staff
Nicole,Boatwright,E19185,nicole.boatwright@apsva.us,Family Center,"Instructional Assistant, Staff",Instructional Staff
Wilson,Desousa,E19222,wilson.desousa@apsva.us,Substitutes,Substitute,Staff
Charles,Harvey,E1925,charles.harvey@apsva.us,Substitutes,Substitute,Staff
Asma,Parveen,E19276,asma.parveen@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Veronica,Terry,E19299,veronica.terry@apsva.us,"Food Services, Extended Day Substitutes","Staff, Staff",Staff
Celia,Arnade,E193,celia.arnade@apsva.us,Intake Center,Staff,Staff
Lipin,Kutup,E19303,lipin.kutup@apsva.us,Substitutes,Substitute,Staff
Daniel,Lelong,E19345,daniel.lelong@apsva.us,Substitutes,Staff,Staff
Paul,McCabe,E1936,paul.mccabe@apsva.us,REEP Program,Supervisor,Administrative Staff
Daniel,Estes,E19364,daniel.estes@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Rudy,Angulo,E19378,rudy.angulo@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Anita,Panwar,E1940,anita.panwar@apsva.us,Substitutes,Staff,Staff
Yeni,Ruiz,E19405,yeni.ruiz@apsva.us,Office of Personnel,Administrative Specialist,Staff
Eileen,Gardner,E1943,eileen.gardner@apsva.us,Administrative Services,Principal,Administrative Staff
Larry,Fishtahler,E19437,larry.fishtahler@apsva.us,Substitutes,Staff,Staff
Yolanda,Chaale,E19448,yolanda.chaale@apsva.us,Special Education,Staff,Staff
Erica,Allgood,E19456,erica.allgood@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Erin,Creek,E19497,erin.creek@apsva.us,Family Center,Instructional Assistant,Instructional Staff
Minta,Garcia,E19528,minta.garcia@apsva.us,Transportation,Bus Driver,Transportation Staff
Liska,Friedman,E19534,liska.friedman@apsva.us,Student Services,Teacher,Instructional Staff
Peggy Cole-Sabouni,Sabouni,E19539,peggy.cole@apsva.us,Special Education,Teacher,Instructional Staff
Melissa,Evans,E19562,melissa.deichmann@apsva.us,Special Education,Teacher,Instructional Staff
Maria,Slattery,E19572,maria.slattery@apsva.us,Division of Instruction,Teacher,Instructional Staff
Jennifer,Lambdin,E19628,jennifer.lambdin@apsva.us,Student Services,Teacher,Instructional Staff
Yomara,Gregg,E1965,yomara.gregg@apsva.us,Transportation,Bus Driver,Transportation Staff
Mary,Wilcox,E19656,mary.wilcox@apsva.us,Student Services,Teacher,Instructional Staff
Shu,Chen,E19719,shu.chen@apsva.us,Extended Day,Staff,Staff
Jamara,Miller,E19727,jamara.miller@apsva.us,Office of Personnel,Administrative Specialist,Staff
Hajjah,Muhammad,E19731,hajjah.muhammad@apsva.us,Substitutes,Substitute,Staff
Diego,Del Carpio,E19758,diego.delcarpio@apsva.us,Substitutes,Substitute,Staff
Stella,Gonzalez Hijar,E19760,stella.gonzalez@apsva.us,Transportation,Bus Attendant,Transportation Staff
Dominique,Gore,E19761,dominique.gore@apsva.us,"Food Services, Transportation","Staff, Bus Driver",Staff
Louy,Kuy,E19762,louy.kuy@apsva.us,Transportation,Bus Attendant,Transportation Staff
Rashida,Begum,E19763,rashida.begum@apsva.us,Transportation,Bus Driver,Transportation Staff
Cuc,Sheets,E19773,cuc.sheets@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Marissa,Graham,E19779,marissa.graham@apsva.us,Student Services,Staff,Staff
Tiffany,Ratliff,E19781,tiffany.ratliff@apsva.us,Student Services,Staff,Staff
Haile,Yohannes,E19783,haile.yohannes@apsva.us,Transportation,Bus Attendant,Transportation Staff
Anne,Johannessen,E19804,anne.johannessen@apsva.us,Substitutes,Staff,Staff
Letina,Okubamicael,E19806,letina.okubamicael@apsva.us,"Food Services, Extended Day","Food Services Staff, Extended Day Staff",Food Services Staff
Faduma,Kabey,E19807,faduma.kabey@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Staff
Frances,Doyon,E19827,frances.doyon@apsva.us,Substitutes,Staff,Staff
Roxanna,Hernandez Torres,E19828,roxanna.hernandez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Freddie,Lee,E19832,freddie.lee@apsva.us,Food Services,Staff,Staff
Remell,Segovia,E19857,remell.segovia@apsva.us,Aquatics,Staff,Staff
Karen,DeCarlo,E19875,karen.decarlo@apsva.us,Division of Instruction,Staff,Staff
Kim,Chisolm,E19885,kim.chisolm@apsva.us,Student Services,Teacher,Instructional Staff
Padraig,Mcloughlin,E19903,padraig.mcloughlin@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Edna,Valladares,E19904,edna.valladares@apsva.us,Substitutes,Staff,Staff
Zufan,Gidey,E19916,zufan.gidey@apsva.us,Transportation,Bus Driver,Transportation Staff
Stephan,Ayers,E19917,stephan.ayers@apsva.us,Transportation,Bus Driver,Transportation Staff
Melkam,Assafe,E19920,melkam.assafe@apsva.us,"Food Services, Transportation","Staff, Bus Driver",Staff
Tsehayn,Hailemariam,E19931,tsehayn.hailemariam@apsva.us,Transportation,Bus Driver,Transportation Staff
Mulugeta,Feyissa,E19959,mulugeta.feyissa@apsva.us,Transportation,Bus Driver,Transportation Staff
Donna,Sanders,E19962,donna.sanders@apsva.us,Substitutes,Substitute,Staff
Odalys,Perez,E19964,odalys.perez@apsva.us,Family Center,Instructional Assistant,Instructional Staff
Alganash,Feleke,E19973,alganash.feleke@apsva.us,Transportation,Bus Attendant,Transportation Staff
Lang,Taing,E20006,lang.taing@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Lynne,Wright,E2002,lynne.wright@apsva.us,Administrative Services,Principal,Administrative Staff
Christopher,Monroy,E20030,christopher.monroy@apsva.us,Division of Instruction,Coordinator,Staff
Jeannette,Allen,E20033,jeannette.allen@apsva.us,Administrative Services,Director,Administrative Staff
Michele,Cona,E20034,michele.cona@apsva.us,REEP Program,Specialist,Staff
Logan,Wiley,E20049,logan.wiley@apsva.us,Substitutes,Staff,Staff
Nargis,Mughal,E20050,nargis.mughal@apsva.us,Substitutes,Staff,Staff
Farhana,Shafi,E20052,farhana.shafi@apsva.us,Substitutes,Substitute,Staff
Zoila,Suria,E20073,zoila.suria@apsva.us,Extended Day Substitutes,Staff,Staff
Antonelly,Bradiel,E20116,antonelly.bradiel@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
John,Schelble,E20134,john.schelble@apsva.us,"Substitutes, Adult Education","Staff, Staff",Staff
Marina,Mitchell,E20149,marina.mitchell@apsva.us,Food Services,Staff,Staff
Tameka,Wilds,E20151,tameka.wilds@apsva.us,Facilities and Operations,Clerical Coordinator,Staff
Ruth,Sysak,E20207,ruth.sysak@apsva.us,REEP Program,Staff,Staff
Laura,Bobeczko,E20215,laura.bobeczko@apsva.us,Substitutes,Staff,Staff
Girish,Rajput,E20223,girish.rajput@apsva.us,Information Services,Director,Administrative Staff
Zhen,Liu,E20227,zhen.liu@apsva.us,Food Services,Food Services Staff,Food Services Staff
Rosario,Quinteros Iriarte,E20237,rosario.quinteros@apsva.us,Extended Day Substitutes,Staff,Staff
Brenda,Guevara,E20240,brenda.guevara@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Berhe,Tesfai,E20251,berhe.tesfai@apsva.us,Intake Center,Staff,Staff
Lisseth,Mosquera,E20255,lisseth.mosquera@apsva.us,Administrative Services,Administrative Assistant,Staff
Lisa,Fortune,E20260,lisa.fortune@apsva.us,REEP Program,Staff,Staff
Matthew,Gavin,E20277,matthew.gavin@apsva.us,Special Education,Teacher,Instructional Staff
Peter,Ramming,E20288,peter.ramming@apsva.us,Student Services,Staff,Staff
David,Webb,E20325,david.webb@apsva.us,Office of Finance,Director,Staff
Richmond,Kelly,E2039,richmond.kelly@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Martha,Herrmann,E2043,martha.herrmann@apsva.us,Special Education,Staff,Staff
Dale,Winchell,E20454,dale.winchell@apsva.us,Adult Education,Staff,Staff
Wily,Rivas,E20474,wily.rivas@apsva.us,Facilities and Operations,Electrician,Facilities Staff
Musa,Conteh,E20476,musa.conteh@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Bridget,Mancke,E20478,bridget.mancke@apsva.us,Special Education,Teacher,Instructional Staff
David,Lough,E20486,david.lough@apsva.us,Substitutes,Staff,Staff
Corey,Levenberry,E20487,corey.levenberry@apsva.us,Substitutes,Staff,Staff
Adrienne,Day,E20509,adrienne.day@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Elizabeth,McKenzie,E20523,elizabeth.mckenzie@apsva.us,Special Education,Teacher,Instructional Staff
Sri,Nallamala,E20531,sri.nallamala@apsva.us,Information Services,Engineer,Staff
Jennifer,Crain,E20569,jennifer.crain@apsva.us,Special Education,Teacher,Instructional Staff
Alvera,Wilson,E206,alvera.wilson@apsva.us,Office of Finance,Analyst,Staff
Rahel,Aregai,E20609,rahel.aregai@apsva.us,Transportation,Bus Driver,Transportation Staff
Kathy,Gust,E20615,kathy.gust@apsva.us,Division of Instruction,Coordinator,Staff
Katherine,Mosquera,E20621,katherine.patton@apsva.us,Administrative Services,Administrative Specialist,Staff
Kim,Dinardo,E2063,kim.dinardo@apsva.us,Substitutes,Staff,Staff
Raul,Mendy,E20634,raul.mendy@apsva.us,Transportation,Bus Driver,Transportation Staff
Richard,Bernal,E20643,richard.bernal@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Sumi,Barua,E20660,sumi.barua@apsva.us,Food Services,Staff,Staff
Eileen,Warren,E20664,eileen.warren@apsva.us,Special Education,Teacher,Instructional Staff
Bobby,Kaplow,E20668,bobby.kaplow@apsva.us,Office of Finance,Director,Staff
Jacqueline,Bouknight,E20687,jacqueline.bouknight@apsva.us,Substitutes,Staff,Staff
Nazre,Abbass,E20708,nazre.abbass@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Abida,Adhi,E20715,abida.adhi@apsva.us,Intake Center,Staff,Staff
Tsige,Wolde,E20752,tsige.wolde@apsva.us,Transportation,Bus Attendant,Transportation Staff
Michelle,Scott,E20753,michelle.scott@apsva.us,Division of Instruction,Administrative Specialist,Staff
Aster,Seyoum,E20770,aster.seyoum@apsva.us,Transportation,Bus Attendant,Transportation Staff
Helen,King,E20771,helen.king@apsva.us,Substitutes,Staff,Staff
Adela,Cardenas Ruiz,E20777,adela.cardenasruiz@apsva.us,Extended Day Substitutes,Staff,Staff
Sharonda,Williams,E20800,sharonda.williams@apsva.us,Division of Instruction,Staff,Staff
Daniel,Mariam,E20855,daniel.mariam@apsva.us,"Substitutes, Intake Center","Substitute, Staff",Staff
Suman,Dilawri,E20858,suman.dilawri@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Soraya,Strobach,E20859,soraya.strobach@apsva.us,Intake Center,Staff,Staff
Bob,Weaver,E20870,bob.weaver@apsva.us,Information Services,Specialist,Staff
Erin,Donohue,E20879,erin.donohue@apsva.us,Special Education,Teacher,Instructional Staff
Alexander,Hanson,E20895,alexander.hanson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Christopher,Manaois,E20908,christopher.manaois@apsva.us,Division of Instruction,Instructional Assistant,Instructional Staff
Jean,Waterbury,E20910,jean.waterbury@apsva.us,Student Services,Teacher,Instructional Staff
Janet,Quantrille,E2093,janet.quantrille@apsva.us,Special Education,Staff,Staff
Marguerite,Scully,E20945,marguerite.scully@apsva.us,Substitutes,Staff,Staff
Lisa,Koch,E20948,lisa.koch@apsva.us,Substitutes,Staff,Staff
Richard,Blome,E20955,richard.blome@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Catherine,Gaudian,E20956,catherine.gaudian@apsva.us,Division of Instruction,Staff,Staff
Jay,Hatfield,E20972,jay.hatfield@apsva.us,Substitutes,Staff,Staff
Charlotte,Yakovleff,E20995,charlotte.yakovleff@apsva.us,Substitutes,Staff,Staff
Josecarlos,Downs,E20999,josecarlos.downs@apsva.us,Adult Education,Staff,Staff
Allison,Scavone,E21001,allison.scavone@apsva.us,Division of Instruction,Staff,Staff
Rasha,Barri,E21007,rasha.barri@apsva.us,Food Services,Staff,Staff
Melissa,Nuwaysir,E21034,melissa.nuwaysir@apsva.us,Substitutes,Staff,Staff
Roberto,Jimenez,E21043,roberto.jimenez@apsva.us,"Adult Education, Adult Education","Staff, Staff",Staff
Velma,Jones,E21066,velma.jones@apsva.us,Extended Day Substitutes,Staff,Staff
Hector,Ventura,E21159,hector.ventura@apsva.us,Information Services,Technical Staff,Staff
Brittane,Neely,E21163,brittane.neely@apsva.us,Substitutes,Staff,Staff
Stephanie,Nichols,E21193,stephanie.nichols@apsva.us,Division of Instruction,Teacher,Instructional Staff
Erica,Midboe,E21247,erica.midboe@apsva.us,Special Education,Teacher,Instructional Staff
Valerie,Carroll,E21280,valerie.carroll@apsva.us,Special Education,Teacher,Instructional Staff
Kevin,Cronin,E21314,kevin.cronin@apsva.us,Aquatics,Manager,Staff
Adriana,Ferro,E21360,adriana.ferro@apsva.us,"Substitutes, Intake Center","Staff, Staff",Staff
Julian,Nevares,E21389,julian.nevares@apsva.us,Aquatics,Staff,Staff
Glen,Denning,E21390,glen.denning@apsva.us,Aquatics,Manager,Staff
Nathan,Prange,E21393,nathan.prange@apsva.us,Aquatics,Staff,Staff
Katherine,Partington,E21412,katherine.partington@apsva.us,Division of Instruction,Teacher,Instructional Staff
Marie,Hone,E21449,marie.hone@apsva.us,Division of Instruction,Coordinator,Staff
Caroline,Levy,E21451,caroline.levy@apsva.us,Substitutes,Staff,Staff
Haelee,Solomon,E21454,haelee.solomon@apsva.us,Division of Instruction,Coordinator,Staff
Erik,Endo,E21457,erik.endo@apsva.us,Adult Education,"Teacher, Staff",Staff
Thai,Nguyen,E21458,thai.nguyen@apsva.us,Food Services,Specialist,Staff
Jessica,Kingsley,E21489,jessica.kingsley@apsva.us,Student Services,Teacher,Instructional Staff
Elizabeth,Rowden,E21505,elizabeth.rowden@apsva.us,Office of Personnel,Teacher,Instructional Staff
Gina,Piccolini,E21517,gina.piccolini@apsva.us,Special Education,Teacher,Instructional Staff
Jodi,Nicholson,E21547,jodi.nicholson@apsva.us,Special Education,Teacher,Instructional Staff
Sirena,Butler,E21551,sirena.butler@apsva.us,"Substitutes, Extended Day","Staff, Extended Day Staff",Staff
Shannon,Simms,E21552,shannon.simms@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Allison,Ransom,E21560,allison.ransom@apsva.us,Extended Day Substitutes,Staff,Staff
Sara,Daidy,E21573,sara.daidy@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Cameron,Childs,E21576,cameron.childs@apsva.us,Division of Instruction,Teacher,Instructional Staff
Marnie,Lewis,E2158,marnie.lewis@apsva.us,Division of Instruction,Coordinator,Staff
Maureen,Nesselrode,E2159,maureen.nesselrode@apsva.us,Administrative Services,Principal,Administrative Staff
Shitaye,Yoseph,E21618,shitu.yoseph@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Laly,Kea,E21619,laly.kea@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Staff
Ellen,Law,E21626,ellen.law@apsva.us,Adult Education,Staff,Staff
Rocio,Rivero,E21658,rocio.rivero@apsva.us,Intake Center,Staff,Staff
Hoang,Nguyen,E21676,hoang.nguyen@apsva.us,Division of Instruction,Custodian,Facilities Staff
Maria,Flores,E21677,maria.flores@apsva.us,Transportation,Bus Attendant,Transportation Staff
Shauna,Min,E21687,shauna.min@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Genet,Jemberie,E21699,genet.jemberie@apsva.us,Transportation,Specialist,Staff
Dilia,Salazar Ortiz,E21700,dilia.salazarortiz@apsva.us,"Food Services, Transportation","Staff, Bus Attendant",Staff
Ebony,Giles,E21701,ebony.giles@apsva.us,Food Services,Food Services Staff,Food Services Staff
Haregu,Berhane,E21706,hargu.asfahaberhane@apsva.us,Transportation,Bus Attendant,Transportation Staff
Crystal,Laws,E21731,crystal.laws@apsva.us,Transportation,Bus Attendant,Transportation Staff
Tsehay,Amde,E21744,tsehay.amde@apsva.us,Transportation,Bus Attendant,Transportation Staff
Ho,Yoo,E21746,ho.yoo@apsva.us,Transportation,Bus Driver,Transportation Staff
Soo,Lee,E21749,soo.lee@apsva.us,Transportation,Bus Driver,Transportation Staff
Bhupinder,Kaur,E21750,bhupinder.kaur@apsva.us,Substitutes,Staff,Staff
Adelaide,Ruble,E21752,adelaide.ruble@apsva.us,Adult Education,Staff,Staff
Cristobal,Flores,E21774,cristobal.flores@apsva.us,Transportation,Bus Attendant,Transportation Staff
Robert,Zambrano,E21837,robert.zambrano@apsva.us,"Office of Personnel, Division of Instruction, Adult Education","Staff, Staff, Staff",Staff
Kenneth,Oaks,E21843,kenneth.oaks@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Mextli,Guerrero,E21881,mextli.guerrero@apsva.us,Office of Finance,Analyst,Staff
Nancy,Routson,E21895,nancy.routson@apsva.us,Office of Personnel,Teacher,Instructional Staff
Blanca,Chicas,E21907,blanca.chicas@apsva.us,Adult Education,"Instructional Assistant, Staff",Instructional Staff
Charles,Conrad,E21921,charles.conrad@apsva.us,Substitutes,Staff,Staff
Destaye,Yoseph,E21926,destaye.yoseph@apsva.us,"Food Services, Extended Day Substitutes","Food Services Staff, Staff",Food Services Staff
Deborah,Hammer,E21933,deborah.hammer@apsva.us,Special Education,Teacher,Instructional Staff
Ernest,Schneider,E21945,ernest.schneider@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Mark,Coleson,E22015,mark.coleson@apsva.us,Aquatics,Staff,Staff
Carmen,Aguilar,E22027,carmen.aguilar@apsva.us,Office of Personnel,Manager,Staff
Nina,Sheppard,E22056,nina.sheppard@apsva.us,REEP Program,Staff,Staff
Shirtona,Horton,E22061,shirtona.horton@apsva.us,Division of Instruction,Coordinator,Staff
Martha,Paz-Espinoza,E22068,martha.espinoza@apsva.us,Student Services,Staff,Staff
Lauren,Johnson,E22090,lauren.johnson@apsva.us,Division of Instruction,Teacher,Instructional Staff
Kerri,Hirsch,E22094,kerri.hirsch@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Lynette,Yoakum,E22130,lynette.yoakum@apsva.us,Special Education,Teacher,Instructional Staff
Jessica,Panfil,E22134,jessica.panfil@apsva.us,Administrative Services,Principal,Administrative Staff
Sara,Daniel,E22147,sara.daniel@apsva.us,School and Community Relations,Supervisor,Staff
Lorin,Youngdahl,E22156,lorin.youngdahl@apsva.us,Special Education,Teacher,Instructional Staff
Brian,Fisher,E22212,brian.fisher@apsva.us,Adult Education,Staff,Staff
Alyssa,D'Amore-Yarnall,E22240,alyssa.damoreyarnall@apsva.us,Special Education,Teacher,Instructional Staff
Megan,Henry,E22257,megan.henry@apsva.us,Substitutes,Staff,Staff
Jillian,Williams,E22305,jillian.williams@apsva.us,Student Services,Staff,Staff
James,Kirchenbauer,E22346,james.kirchenbauer@apsva.us,Adult Education,Staff,Staff
Gloria,Alvarez,E22356,gloria.alvarez@apsva.us,"Food Services, Substitutes, Extended Day","Staff, Staff, Staff",Staff
Rosalba,Guerrero Gomez,E22363,rosalba.gomez@apsva.us,Food Services,Food Services Staff,Food Services Staff
Sonia,Delgado,E22367,sonia.delgado@apsva.us,Division of Instruction,Administrative Assistant,Staff
Cynthia,Young,E22372,cynthia.young@apsva.us,Adult Education,Staff,Staff
Susan,West,E22378,susan.west@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Theresa,Agyare,E22381,theresa.agyare@apsva.us,"Substitutes, Extended Day","Staff, Staff",Staff
Sair,Molina,E22393,sair.molina@apsva.us,Office of Finance,Account Clerk,Staff
Anuska,Willson,E22394,anuska.willson@apsva.us,Substitutes,Staff,Staff
Catherine,Lin,E22402,cathy.lin@apsva.us,Facilities and Operations,Director,Administrative Staff
Juan,Terrazas,E22428,juan.terrazas@apsva.us,Food Services,Food Services Staff,Food Services Staff
Karen,Caldwell,E22441,karen.caldwell@apsva.us,Substitutes,Substitute,Staff
Daniel,Hauser,E22443,daniel.hauser@apsva.us,Division of Instruction,Coordinator,Staff
Tuguldur,Batmunkh,E22468,tuguldur.batmunkh@apsva.us,Intake Center,Registrar,Staff
Mischa,Joligard,E22494,mischa.joligard@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Clare,Mclean,E22497,clare.mclean@apsva.us,Division of Instruction,Staff,Staff
Larry,Robertson,E22509,larry.robertson@apsva.us,Information Services,Operator,Staff
John,Kauffman,E22543,john.kauffman@apsva.us,Adult Education,Staff,Staff
Theresia,Hernisanti,E22562,theresia.hernisanti@apsva.us,Food Services,Food Services Staff,Food Services Staff
Abeba,Tesfaye,E22577,abeba.tesfaye@apsva.us,Transportation,Bus Attendant,Transportation Staff
Meggie,Scogna,E22595,meggie.scogna@apsva.us,Special Education,Teacher,Instructional Staff
Sara,Tsegay,E22636,sara.tsegay@apsva.us,Transportation,Bus Attendant,Transportation Staff
Yeshi,Mekonen,E22637,yeshi.mekonen@apsva.us,Transportation,Bus Driver,Transportation Staff
Sewasew,Belachew,E22651,sewasew.belachew@apsva.us,"Food Services, Transportation","Staff, Bus Attendant",Staff
Getachew,Abdi,E22656,getachew.abdi@apsva.us,Transportation,Bus Driver,Transportation Staff
Yeshefana,Ambaye,E22659,yeshefana.ambaye@apsva.us,Transportation,Bus Attendant,Transportation Staff
Bezunesh,Bekele,E22683,bezunesh.bekele@apsva.us,Transportation,Bus Attendant,Transportation Staff
Dagmawet,Haile,E22687,dagmawet.haile@apsva.us,Transportation,Specialist,Staff
Azeb,Estifanos,E22695,azeb.estifanos@apsva.us,Transportation,Bus Attendant,Transportation Staff
Genet,Bisrat,E22696,genet.bisrat@apsva.us,Transportation,Bus Attendant,Transportation Staff
Nighat,Khan,E22703,nighat.khan@apsva.us,Substitutes,Staff,Staff
Brian,York,E22704,brian.york@apsva.us,Facilities and Operations,Carpenter,Facilities Staff
Latanja,Bryant,E22718,latanja.bryant@apsva.us,Transportation,Bus Attendant,Transportation Staff
Silvia,O'Hara,E22733,silvia.ohara@apsva.us,"Substitutes, Intake Center","Staff, Staff",Staff
Dharam,Kaur,E22743,dharam.kaur@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Valerie,Register,E22757,valerie.register@apsva.us,Substitutes,Staff,Staff
Catherine,Dowling,E2276,catherine.dowling@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Irma,Sierra,E22763,irma.sierra@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Staff
Kristin,Shymoniak,E22766,kristin.shymoniak@apsva.us,Special Education,Teacher,Instructional Staff
Ajibola,Robinson,E22771,ajibola.robinson@apsva.us,Facilities and Operations,Manager,Staff
Charlotte,Gregory,E22776,charlotte.gregory@apsva.us,Substitutes,Staff,Staff
Jorge,Velazquez,E22787,jorge.velazquez@apsva.us,Office of Finance,Analyst,Staff
Anh,Nguyen,E22805,anh.nguyen@apsva.us,Information Services,Staff,Staff
Shiraz,Bensalem,E22814,shiraz.bensalem@apsva.us,Intake Center,Registrar,Staff
Teclai,Simon,E22816,teclai.simon@apsva.us,Transportation,Bus Attendant,Transportation Staff
Brian,Bersh,E22824,brian.bersh@apsva.us,APS,Teacher,Instructional Staff
Mary,Martin,E22827,mary.martin@apsva.us,Special Education,Staff,Staff
Antwonn,Johnson,E22833,antwonn.johnson@apsva.us,Transportation,Bus Attendant,Transportation Staff
Caroline,Damren,E22848,caroline.damren@apsva.us,Special Education,Teacher,Instructional Staff
Rosalita,Santiago,E22872,rosalita.santiago@apsva.us,Adult Education,Teacher,Instructional Staff
Keith,Reeves,E22873,keith.reeves@apsva.us,Division of Instruction,Coordinator,Staff
Ricardo,Sorto,E22889,ricardo.sorto@apsva.us,Office of Personnel,Administrator,Staff
Douglas,Zebrak,E22898,douglas.zebrak@apsva.us,Substitutes,Staff,Staff
Bryan,Boykin,E22901,bryan.boykin@apsva.us,Administrative Services,Principal,Administrative Staff
Valaria,Soroko,E22902,valaria.soroko@apsva.us,Media Processing,Administrative Assistant,Staff
Valerie,Budney,E22945,valerie.budney@apsva.us,Special Education,Teacher,Instructional Staff
Sheila,Jimenez,E22946,sheila.jimenez@apsva.us,Substitutes,Staff,Staff
Janet,Needham,E22954,janet.needham@apsva.us,Special Education,Teacher,Instructional Staff
Gloria,Alvarez,E22955,gloria.alvarez2@apsva.us,Special Education,Teacher,Instructional Staff
Tomika,Robinson,E22970,tomika.robinson@apsva.us,Office of Finance,Analyst,Staff
Heather,Martin,E22972,heather.martin@apsva.us,Division of Instruction,Staff,Staff
Sandy,Prum,E22980,sandy.prum@apsva.us,Transportation,Bus Driver,Transportation Staff
Yewondwossen,Tekle,E22982,yewondwossen.tekle@apsva.us,Transportation,Bus Driver,Transportation Staff
Jill,Meyer,E23,jill.meyer@apsva.us,Substitutes,Staff,Staff
Lindsay,Estabrooks,E23005,lindsay.estabrooks@apsva.us,Substitutes,Staff,Staff
Marit,Simenson,E23020,marit.simenson@apsva.us,Special Education,Teacher,Instructional Staff
Rene,Lopez-Veizaga,E23055,rene.lopezveizaga@apsva.us,Food Services,Food Services Staff,Food Services Staff
Wendy,Boone,E23063,wendy.boone@apsva.us,Division of Instruction,Staff,Staff
Daniel,Capacho,E23065,daniel.capacho@apsva.us,Facilities and Operations,Glazier/Roofer,Facilities Staff
Dana,Dougherty,E23104,dana.dougherty@apsva.us,Substitutes,Staff,Staff
Margaret,Chung,E23129,margaret.chung@apsva.us,Administrative Services,Principal,Administrative Staff
Shannon,Odom,E23168,shannon.odom@apsva.us,Student Services,Teacher,Instructional Staff
Bethany,Belcher,E23170,bethany.belcher@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Michael,Parks,E23181,michael.parks@apsva.us,Extended Day,Supervisor,Extended Day Staff
Ryan,Taylor,E23185,ryan.taylor@apsva.us,Information Services,Technical Staff,Staff
Magda,Ochoa,E23202,magda.ochoa@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Filamor,Pangan,E23239,filamor.pangan@apsva.us,Food Services,Food Services Staff,Food Services Staff
Ebonee,Fleming,E23256,ebonee.fleming@apsva.us,Extended Day Substitutes,Staff,Staff
Donato,Soranno,E23268,donato.soranno@apsva.us,Adult Education,Staff,Staff
Liya,Tesfamichael,E23281,liya.tesfamichael@apsva.us,Transportation,Bus Driver,Transportation Staff
Worku,Bedane,E23291,worku.bedane@apsva.us,Transportation,Bus Driver,Transportation Staff
Juan,Garay,E23304,juan.garay@apsva.us,Facilities and Operations,Plumber,Facilities Staff
Andrew,Shreeve,E23318,andrew.shreeve@apsva.us,Special Education,Teacher,Instructional Staff
Jessie,Togan,E23333,jessie.togan@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Rigat,Tesfay,E23340,rigat.tesfay@apsva.us,Food Services,Staff,Staff
Judy,Carter,E23343,judy.carter@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Pam,Farrell,E2335,pam.farrell@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Guadalupe,Mitchell,E23375,guadalupe.mitchell@apsva.us,Food Services,Staff,Staff
Breauna,Cleggett-Askew,E23377,breauna.cleggett@apsva.us,Substitutes,Staff,Staff
Alexander,Eisenberg,E23383,alexander.eisenberg@apsva.us,Substitutes,Staff,Staff
Poorvi,Shah,E23403,poorvi.shah@apsva.us,Substitutes,Staff,Staff
Eduardo,Robielos,E23413,eduardo.robielos@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Kerri,Martin,E23429,kerri.martin@apsva.us,Division of Instruction,Staff,Staff
Andrew,Livingston,E23445,andrew.livingston@apsva.us,Adult Education,Staff,Staff
Laura,Dooley,E23460,laura.dooley@apsva.us,Adult Education,Staff,Staff
Touria,Oulabas,E23464,tourias.oulabas@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Staff
Coreen,Endo-Davis,E23466,coreen.endodavis@apsva.us,"Special Education, Adult Education","Staff, Staff",Staff
Fernando,Rivas,E23491,fernando.rivas@apsva.us,Transportation,Bus Driver,Transportation Staff
Kyndra,Fenwick,E23493,kyndra.fenwick@apsva.us,Transportation,Bus Driver,Transportation Staff
Tedd,Williams,E23494,tedd.williams@apsva.us,Transportation,Bus Driver,Transportation Staff
Elizabeth,Walsh,E23503,elizabeth.walsh@apsva.us,Special Education,Coordinator,Instructional Staff
Andrew,Moser,E23528,andrew.moser@apsva.us,Substitutes,Staff,Staff
James,Cassatt,E23532,james.cassatt@apsva.us,Adult Education,Staff,Staff
Thomas,Taylor,E23534,thomas.taylor@apsva.us,Extended Day,Staff,Staff
Judith,Sandoval Gonzalez,E23548,judith.sandoval@apsva.us,Intake Center,Staff,Staff
Bikram,Rana,E23568,bikram.rana@apsva.us,Intake Center,Staff,Staff
Rhianna,Abdeljawad,E23573,rhianna.abdeljawad@apsva.us,Substitutes,Staff,Staff
Anna,Hartwick,E23577,anna.hartwick@apsva.us,Extended Day,Staff,Staff
Diana,Basurto,E23581,diana.basurto@apsva.us,"Substitutes, Intake Center","Staff, Staff",Staff
Vanessa,Adragna,E23584,vanessa.adragna@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Carmen,Saphos,E23608,carmen.saphos@apsva.us,Substitutes,Staff,Staff
Ann,Garrison,E23626,ann.garrison@apsva.us,Substitutes,Staff,Staff
Carlos,Barragan,E23638,carlos.barragan@apsva.us,Adult Education,Staff,Staff
Kevin,Blair,E23674,kevin.blair@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Jia,Zuo,E23698,jia.zuo@apsva.us,Intake Center,Staff,Staff
Keisha,Boggan,E23795,keisha.boggan@apsva.us,Administrative Services,Principal,Administrative Staff
Kimberley,Graves,E23796,kimberley.graves@apsva.us,Office of the Superintendent,CHIEF OF SCHOOL SUPPORT,Administrative Staff
Charles,Resnick,E23798,charles.resnick@apsva.us,Division of Instruction,"Instructional Assistant, Staff",Instructional Staff
Meredith,Allen,E23799,meredith.allen@apsva.us,Division of Instruction,Coordinator,Staff
Morgan,Burns,E23803,morgan.burns2@apsva.us,Student Services,Staff,Staff
Joel,Gildea,E23833,joel.gildea@apsva.us,Division of Instruction,Coordinator,Staff
Yuan,Zhai,E23838,yuan.zhai@apsva.us,Adult Education,Staff,Staff
Cheryl,McCullough,E23846,cheryl.mccullough@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Lori,Wiggins,E23851,lori.wiggins@apsva.us,Administrative Services,Principal,Administrative Staff
Atleacia,Gibson,E23897,atleacia.gibson@apsva.us,Division of Instruction,Coordinator,Staff
Patricia,Donahoo,E23916,patricia.donahoo@apsva.us,Office of Personnel,Administrative Specialist,Staff
Catharina,Genove,E23963,catharina.genove@apsva.us,Administrative Services,Principal,Administrative Staff
Elizabeth,Sinclair,E23967,elizabeth.sinclair@apsva.us,Special Education,Staff,Staff
Marian,Klymkowsky,E24015,marian.klymkowsky@apsva.us,Substitutes,Staff,Staff
Ann,Irby,E24025,ann.irby@apsva.us,Office of Personnel,Specialist,Staff
Rahmo,Wardere,E24028,rahmo.wardere@apsva.us,Administrative Services,Specialist,Staff
Eileen,O'Casey,E24031,eileen.ocasey@apsva.us,Special Education,Teacher,Instructional Staff
Elizabeth,Auten,E24050,elizabeth.auten@apsva.us,Student Services,Teacher,Instructional Staff
Angela,McMahan,E24075,angela.mcmahan@apsva.us,Food Services,Staff,Staff
Andrea,Brunk,E24098,andrea.brunk@apsva.us,Special Education,"Teacher, Staff",Staff
Sarah,Peters,E24100,sarah.peters@apsva.us,Special Education,Teacher,Instructional Staff
Eva Marie,Fadel,E24121,evamarie.fadel@apsva.us,Substitutes,Staff,Staff
Cynthia,Margeson,E2414,cynthia.margeson@apsva.us,Substitutes,Staff,Staff
Carlos,Mora,E24142,carlos.mora@apsva.us,Information Services,Staff,Staff
Dina,Becerra,E24159,dina.becerra@apsva.us,Transportation,Bus Driver,Transportation Staff
Justino,Jaca,E24179,justino.jaca@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Renee,Clough,E24180,renee.clough@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Chris,Scheer,E24193,chris.scheer@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Jacquelyn,Cubero,E24197,jacquelyn.cubero@apsva.us,"Substitutes, Division of Instruction","Substitute, Staff",Staff
Kerry,Carlsen,E24235,kerry.carlsen@apsva.us,REEP Program,Staff,Staff
Gerson,Paniagua,E24241,gerson.paniagua@apsva.us,School and Community Relations,Coordinator,Staff
Jeff,Hurtado,E24244,jeff.hurtado@apsva.us,Extended Day Substitutes,Staff,Staff
Ashley,Brewton,E24254,ashley.brewton@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Niva,Barua,E24259,niva.barua@apsva.us,"Food Services, Extended Day Substitutes","Staff, Staff",Staff
Joanne,Peyton,E24261,joanne.peyton@apsva.us,Special Education,Staff,Staff
Qiu,Chen,E24265,qiu.chen@apsva.us,Food Services,Staff,Staff
Willie,Garner,E24270,willie.garner@apsva.us,Special Education,Staff,Staff
Benjamin,Harris,E24276,ben.harris@apsva.us,Extended Day,Assistant Director,Staff
Fitsum,Haile,E24278,fitsum.haile@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Catherine,Ventura-Merkel,E24289,catherine.merkel@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Bibi,Sayera,E24294,bibi.sayera@apsva.us,Food Services,Staff,Staff
Phillip,Haughton,E24298,phillip.haughton@apsva.us,Substitutes,Staff,Staff
Clara,Jaramillo,E24310,clara.jaramillo@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Tracy,Santasine,E24313,tracy.santasine@apsva.us,Substitutes,Staff,Staff
Mary,Dowell,E24317,mary.dowell@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Ivannia,Escobar Centeno,E24325,ivannia.escobar@apsva.us,Intake Center,Staff,Staff
Jennifer,Rachlin,E24332,jennifer.rachlin@apsva.us,Student Services,Staff,Staff
Selamawit,Tesfai,E24343,selamawit.tesfai@apsva.us,"Food Services, Transportation","Staff, Bus Driver",Staff
Umeka,Pulliam,E24345,umeka.pulliam@apsva.us,Transportation,Bus Driver,Transportation Staff
Sotha,Chin,E24346,sotha.chin@apsva.us,Transportation,Bus Driver,Transportation Staff
Demoz,Hailu,E24357,demoz.hailu@apsva.us,Transportation,Bus Driver,Transportation Staff
Asmeret,Gebremariam,E24365,asmeret.gebremariam@apsva.us,Transportation,Bus Attendant,Transportation Staff
Johana,Tovar,E24368,johana.tovar@apsva.us,Transportation,Bus Driver,Transportation Staff
Mark,Stripe,E2437,mark.stripe@apsva.us,Substitutes,Staff,Staff
Debora,Wren,E2439,debora.wren@apsva.us,"Administrative Services, Division of Instruction","Staff, Staff",Staff
Colleen,Koval,E24395,colleen.koval@apsva.us,Special Education,Coordinator,Staff
Arthur,Bell,E2440,arthur.bell@apsva.us,Facilities and Operations,Director,Staff
Lisa,Nasr,E24404,lisa.nasr@apsva.us,Substitutes,Staff,Staff
Ana,Quintero,E24409,ana.quintero@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Minnie,Quinlan,E24454,minnie.quinlan@apsva.us,Special Education,Teacher,Instructional Staff
Tammy,Bewitz,E24466,tammy.bewitz@apsva.us,"Administrative Services, Substitutes, Student Services","Staff, Staff, Staff",Staff
Kathleen,Costar,E2449,kathleen.costar@apsva.us,Division of Instruction,Teacher,Instructional Staff
Hayat,Zahraoui,E24494,hayat.zahraoui@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Mary,Brocking,E24496,elisabeth.brocking@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Wanda,Fountain,E24501,wanda.fountain@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Adam,Howard,E24504,adam.howard@apsva.us,Substitutes,Staff,Staff
Girmaye,Serbessa,E24512,girmaye.serbessa@apsva.us,Transportation,Bus Driver,Transportation Staff
Breonna,Zeballos,E24529,breonna.mcclain@apsva.us,Administrative Services,Principal,Administrative Staff
Audrey,Nolan,E24548,audrey.nolan@apsva.us,Administrative Services,Staff,Staff
Glenn,Swanson,E24567,glenn.swanson@apsva.us,Substitutes,Substitute,Staff
Stephanie,Simmons,E24581,stephanie.simmons@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Solomon,Altaye,E24583,solomon.altaye@apsva.us,Transportation,Bus Driver,Transportation Staff
Quentin,Basil,E24592,quentin.basil@apsva.us,Information Services,Technical Staff,Staff
Alton,Dixon-Walker,E24608,alton.dixonwalker@apsva.us,Extended Day Substitutes,Staff,Staff
Emily,Dozier,E24609,emily.dozier@apsva.us,Substitutes,Staff,Staff
Victoria,Metz,E2462,victoria.metz@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Saida,Outayeb,E24661,saida.outayeb@apsva.us,Substitutes,Staff,Staff
Grant,Hutton,E24667,grant.hutton@apsva.us,Aquatics,Staff,Staff
Yassin,Bensalem,E24688,yassin.bensalem@apsva.us,Information Services,Technical Staff,Staff
Aleta,Myers,E2470,aleta.myers@apsva.us,Special Education,Teacher,Instructional Staff
Freweini,Brhane,E24715,freweini.brhane@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Amy,Hailey,E24762,amy.hailey@apsva.us,Media Processing,Supervisor,Administrative Staff
Lamine,Hamdad,E24791,lamine.hamdad@apsva.us,Adult Education,Staff,Staff
Grace,Bonilla,E24811,grace.bonilla@apsva.us,Special Education,Teacher,Instructional Staff
Bernadette,Meny-Plunkett,E24812,bernadette.plunkett@apsva.us,Special Education,Teacher,Instructional Staff
Laura,Yellin,E24814,laura.yellin@apsva.us,Special Education,Teacher,Instructional Staff
Sandra,Hill,E24829,sandra.hill@apsva.us,Adult Education,Staff,Staff
Elissa,Norton,E24889,elissa.norton@apsva.us,Special Education,Teacher,Instructional Staff
Catherine,Thompson,E24897,catherine.thompson@apsva.us,Special Education,"Teacher, Staff",Staff
Christine,Ray,E24923,christine.ray@apsva.us,Division of Instruction,Teacher,Instructional Staff
Laura,Smith,E24936,laura.smith2@apsva.us,Division of Instruction,Administrative Assistant,Staff
Kimberly,Traw,E24961,kimberly.traw@apsva.us,Special Education,Teacher,Instructional Staff
Bolorchimeg,Tsevegjav,E24987,bolor.tsevegjav@apsva.us,Intake Center,Staff,Staff
Wendy,Bermudez,E24992,wendy.bermudez@apsva.us,Division of Instruction,Teacher,Instructional Staff
Aden,Negasi,E25012,aden.negasi@apsva.us,Extended Day,Staff,Staff
Brittany,Dennis,E25035,brittany.dennis@apsva.us,Office of Personnel,Account Clerk,Staff
Xiomara,Escobar,E25043,xiomara.escobar@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Collin,Creedon,E25069,collin.creedon@apsva.us,Aquatics,Staff,Staff
Fatima,Ayala,E25070,fatima.ayala@apsva.us,Food Services,Staff,Staff
Linda,Mock,E25071,linda.mock@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Rahkia,Legrand,E25072,rahkia.legrand@apsva.us,"Food Services, Transportation","Bus Attendant, Staff",Transportation Staff
Javier,Cruz Aguilar,E25079,javier.cruzaguilar@apsva.us,Transportation,Bus Driver,Transportation Staff
Tracy,Thompson,E25082,tracy.thompson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Carlos,Ramirez,E25083,carlos.ramirez@apsva.us,Administrative Services,Principal,Administrative Staff
Sonia,Zorrilla Zuniga,E25084,sonia.zorrillazuniga@apsva.us,Student Services,Teacher,Instructional Staff
Maria,De La Cerda Rohde,E25093,maria.delacerdarohde@apsva.us,Adult Education,Staff,Staff
Beverly,George,E25097,beverly.george@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Anette,Rivas,E25103,anette.rivas@apsva.us,Division of Instruction,Coordinator,Staff
Monique,Henderson,E25120,monique.henderson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Ja'Meeka,Lewis,E25132,jameeka.lewis@apsva.us,Extended Day,Staff,Staff
Rebecca,Oteng,E25134,rebecca.oteng@apsva.us,Food Services,Food Services Staff,Food Services Staff
Deborah,Higgins,E25136,deborah.higgins@apsva.us,Substitutes,Staff,Staff
Maria,Ceballos,E25137,maria.ceballos@apsva.us,Student Services,Teacher,Instructional Staff
Anastasia,Wingard,E25141,anastasia.wingard@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Leslie,Jones,E25149,leslie.jones@apsva.us,Substitutes,Staff,Staff
Henry,Bendon,E25171,henry.bendon@apsva.us,Substitutes,Staff,Staff
Corey,Dotson,E25174,corey.dotson@apsva.us,Office of Personnel,Director,Administrative Staff
Kate,Hines,E2518,kate.hines@apsva.us,Special Education,Teacher,Instructional Staff
Gerald,Mendizabal,E25182,gerald.mendizabal@apsva.us,"Food Services, Transportation, Facilities and Operations","Staff, Bus Driver, Staff",Staff
Tammeron,Warren,E25183,tammeron.warren@apsva.us,Transportation,Bus Driver,Transportation Staff
Pablo,Santana,E25194,pablo.santana@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Elizabeth,Lambert,E25199,lisa.lambert@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Brandon,Withers,E25205,brandon.withers@apsva.us,Information Services,Technical Staff,Staff
Chelsea,Craddock,E25206,chelsea.craddock@apsva.us,Extended Day Substitutes,Staff,Staff
Danielle,Vingelis,E25213,danielle.vingelis@apsva.us,Special Education,Specialist,Staff
Evelyn,Jordan,E25225,evelyn.jordan@apsva.us,Food Services,Staff,Staff
Diane,Murphy,E2523,diane.murphy@apsva.us,Adult Education,Staff,Staff
Parvin,Aktr,E25242,parvin.aktr@apsva.us,Food Services,Staff,Staff
David,Walden,E25258,david.walden@apsva.us,Substitutes,Staff,Staff
Hanim,Magzoub,E25259,hanim.magzoub@apsva.us,School and Community Relations,Staff,Staff
Iman,Aziz,E25264,iman.aziz@apsva.us,Substitutes,Staff,Staff
Elois,Jackson,E25286,elois.jackson@apsva.us,Transportation,Bus Driver,Transportation Staff
Margo,Watters,E25294,margo.watters@apsva.us,Extended Day Substitutes,Staff,Staff
Ashebir,Gebre,E25298,ashebir.gebre@apsva.us,Transportation,Bus Driver,Transportation Staff
Marcia,Carter,E2530,marcia.carter@apsva.us,Special Education,Teacher,Instructional Staff
Ariel,Valdivia Fernandez,E25303,ariel.fernandez@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Marianne,Ellis,E2531,marianne.ellis@apsva.us,Student Services,Teacher,Instructional Staff
Dilia,Alvarez,E25317,dilia.alvarez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Sandra,Pena,E25318,sandra.pena@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Arash,Shayestehpour,E25334,arash.shayestehpour@apsva.us,Substitutes,Substitute,Staff
Salma,Mahmood,E25358,salma.mahmood@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Robinette,Nero,E25406,robinette.nero@apsva.us,Transportation,Bus Driver,Transportation Staff
Craig,Hirai,E25407,craig.hirai@apsva.us,Adult Education,Staff,Staff
John,Mickevice,E25414,john.mickevice@apsva.us,Administrative Services,Director,Staff
Clark,Petersen,E25418,clark.petersen@apsva.us,Substitutes,Staff,Staff
Russell,Hunter,E25421,russell.hunter@apsva.us,Transportation,Bus Driver,Transportation Staff
Kirubel,Belineh,E25423,kirubel.belineh@apsva.us,Transportation,Bus Driver,Transportation Staff
Carolina,Ranz,E25434,carolina.ranz@apsva.us,Intake Center,Staff,Staff
Sylvia,Gardner,E25452,sylvia.gardner@apsva.us,Substitutes,Staff,Staff
Lorraine,Kozich,E25454,lorraine.kozich@apsva.us,Substitutes,Staff,Staff
Patricia,Ingoldsby,E25455,patricia.ingoldsby@apsva.us,Substitutes,Staff,Staff
Ana,White,E25462,ana.white@apsva.us,Substitutes,Staff,Staff
Michael,Polaski,E25475,michael.polaski@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Manuel,Medina,E25480,manuel.medina@apsva.us,Information Services,Technical Staff,Staff
Wendy,Rios,E25491,wendy.rios@apsva.us,"Food Services, Extended Day Substitutes","Staff, Food Services Staff",Staff
Leslie,Morgado,E25500,leslie.morgado@apsva.us,Special Education,Teacher,Instructional Staff
Chandra,Vemuri,E25518,chandra.vemuri@apsva.us,Information Services,Engineer,Staff
Lyudmyla,Scheuchenzuber,E25533,ly.scheuchenzuber@apsva.us,"Intake Center, Adult Education","Staff, Staff",Staff
Marco,Jimenez Albarracin,E25551,marco.jimenez@apsva.us,Food Services,Food Services Staff,Food Services Staff
Leslie,Keller,E25560,leslie.keller@apsva.us,Substitutes,Staff,Staff
Shirley,Doswell,E25570,shirley.doswell@apsva.us,Intake Center,Staff,Staff
Daniel,Redmond,E25572,daniel.redmond@apsva.us,Adult Education,Staff,Staff
Barbara,Lavelle,E2558,barbara.lavelle@apsva.us,Division of Instruction,Administrative Assistant,Staff
Yesly,Gonzalez Benavidez,E25602,yesly.gonzalez@apsva.us,Substitutes,Staff,Staff
Maria,Quihuango,E25620,maria.quihuango@apsva.us,"Extended Day Substitutes, Special Education","Staff, Instructional Assistant",Staff
Devin,Donnell,E25627,devin.donnell@apsva.us,Information Services,Analyst,Staff
Kathryn,Hawkins,E25642,kathryn.hawkins@apsva.us,Special Education,Coordinator,Instructional Staff
Joseph,Clark,E25650,joseph.clark@apsva.us,Information Services,Technical Staff,Staff
Dereje,Getachew,E25694,dereje.getachew@apsva.us,Transportation,Bus Driver,Transportation Staff
Eyerusalem,Sissay,E25695,eyerusalem.sissay@apsva.us,Transportation,Bus Driver,Transportation Staff
Susan,Clark,E25707,susan.clark@apsva.us,Special Education,Teacher,Instructional Staff
Christina,Hogan,E25727,christina.hogan@apsva.us,Special Education,Staff,Staff
Jeni,Merino,E25729,jeni.merino@apsva.us,School and Community Relations,Administrative Assistant,Staff
Adora,Aldana,E25734,adora.aldana@apsva.us,Office of Personnel,Coordinator,Administrative Staff
Nate,Giles,E25736,nate.giles@apsva.us,Substitutes,Staff,Staff
Megan,Starke,E25752,megan.starke@apsva.us,Special Education,Teacher,Instructional Staff
Virginia,Maloney,E25758,virginia.maloney@apsva.us,Division of Instruction,Staff,Staff
Rebecca,Firehock,E25759,rebecca.firehock@apsva.us,Special Education,Teacher,Instructional Staff
Kristin,Wallace,E25790,kristin.wallace@apsva.us,Special Education,Teacher,Instructional Staff
Diran,Cowell,E25792,diran.cowell@apsva.us,Special Education,Teacher,Instructional Staff
Victoria,Metzger,E25802,victoria.metzger@apsva.us,Division of Instruction,Staff,Staff
Christine,Stokes-Beverley,E25813,christine.stokes@apsva.us,Division of Instruction,Coordinator,Staff
Stephen,Ross,E25824,stephen.ross@apsva.us,Division of Instruction,Coordinator,Staff
Corryn,Schonberg,E25827,corryn.schonberg@apsva.us,Special Education,Teacher,Instructional Staff
Melissa,Meck,E25846,melissa.meck@apsva.us,Special Education,Teacher,Instructional Staff
Kira,Atkinson,E25847,kira.atkinson@apsva.us,Special Education,Teacher,Instructional Staff
Megan,Pavelich,E25863,megan.pavelich@apsva.us,Substitutes,Staff,Staff
Erika,Drummond,E25864,erika.drummond@apsva.us,Student Services,Staff,Staff
Megan,Khaerisman,E25870,megan.khaerisman@apsva.us,Student Services,Teacher,Instructional Staff
Sean,Jones,E25916,sean.jones@apsva.us,Division of Instruction,Coordinator,Staff
Brandon,Christian,E25926,brandon.christian@apsva.us,Office of Finance,Administrator,Staff
Leslie,Whiteside Elgendi,E25983,leslie.elgendi@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Courtney,Jenkins,E26000,courtney.jenkins@apsva.us,Special Education,Teacher,Instructional Staff
Brook,Yimer,E26007,brook.yimer@apsva.us,Aquatics,Staff,Staff
Trina,Alcorn,E26009,trina.alcorn@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Dayana,Zyoud-Cruz,E26012,dayana.zyoudcruz@apsva.us,Transportation,Bus Driver,Transportation Staff
Frayne,Gantus Oller,E26015,frayne.gantusoller@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Shaquera,Rome,E26016,shaquera.rome@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Breana,Orr,E26018,breana.orr@apsva.us,"Student Services, Special Education","Staff, Staff",Staff
Leah,Frazzano,E26027,leah.frazzano@apsva.us,Substitutes,Substitute,Staff
Nesta,Joseph,E26048,nesta.joseph@apsva.us,Adult Education,Staff,Staff
Dana,Brundidge,E26086,dana.brundidge@apsva.us,Division of Instruction,Staff,Staff
Ira,Allen,E26095,ira.allen2@apsva.us,Substitutes,Staff,Staff
David,Pickens,E26099,david.pickens@apsva.us,"Substitutes, Information Services","Staff, Staff",Staff
Arthur,Branch,E26109,arthur.branch@apsva.us,Information Services,Technical Staff,Staff
Juan,Salgado,E26118,juan.salgado@apsva.us,Transportation,Bus Driver,Transportation Staff
Mungunzaya,Coughlin,E26123,mungunzaya.coughlin@apsva.us,School and Community Relations,Staff,Staff
Mohammed,Jammeh,E26126,mohammed.jammeh@apsva.us,Substitutes,Staff,Staff
Ronnice,Hull,E26132,ronnice.hull@apsva.us,Extended Day Substitutes,Staff,Staff
Tesfay,Maasho,E26133,tesfay.maasho@apsva.us,Transportation,Bus Driver,Transportation Staff
Merrie,Craig-Wood,E26138,merrie.craigwood@apsva.us,Substitutes,Staff,Staff
Evelene,Adams,E26161,evelene.adams@apsva.us,Transportation,Bus Attendant,Transportation Staff
Eric,Smith,E26180,eric.smith@apsva.us,Information Services,Technical Staff,Staff
Charles,Feinson,E26183,charles.feinson2@apsva.us,Substitutes,Staff,Staff
Tarina,Ewell,E26192,tarina.ewell@apsva.us,"Food Services, Extended Day Substitutes, Substitutes","Staff, Staff, Staff",Staff
Breauna,Koger,E26195,breauna.koger@apsva.us,"Substitutes, Extended Day Substitutes, Food Services","Staff, Substitute, Staff",Staff
Susan,Murphy,E26207,susan.murphy2@apsva.us,Substitutes,Staff,Staff
Lesly,Caceres,E26211,lesly.caceres@apsva.us,Transportation,Specialist,Staff
Cherinet,Habtegiorgis,E26212,cherinet.habtegiorgi@apsva.us,Transportation,Bus Driver,Transportation Staff
Dilcia,Hernandez,E26217,dilcia.hernandez@apsva.us,Transportation,Clerical Coordinator,Staff
Virginia,Lopez De Flores,E26232,virginia.lopezdeflor@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Estelle,Bowers,E26263,estelle.bowers@apsva.us,"Substitutes, Special Education","Staff, Staff",Staff
Abera,Hieyi,E26268,abera.hieyi@apsva.us,Transportation,Bus Driver,Transportation Staff
Pat,Anderson,E2627,pat.anderson@apsva.us,Information Services,Staff,Staff
Patrick,Wallace,E26271,patrick.wallace@apsva.us,"Adult Education, Adult Education","Staff, Coordinator",Staff
Marithalia,Caton,E26281,marithalia.caton@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Lada,Mnatsakanova,E26285,lada.mnatsakanova@apsva.us,Adult Education,Staff,Staff
Ruby,Jamison Ledbetter,E26297,ruby.ledbetter@apsva.us,"Substitutes, Extended Day Substitutes","Staff, Staff",Staff
Tecla,Albarracin,E26307,tecla.albarracin@apsva.us,Transportation,Bus Driver,Transportation Staff
Lidia,Reyes,E26310,lidia.reyes@apsva.us,Intake Center,Staff,Staff
Barbara,Kanninen,E26314,barbara.kanninen@apsva.us,Office of the School Board,School Board Member,Staff
Elisabeth,Harrington,E26325,elisabeth.harrington@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Bushra,Al-Sadi,E26335,bushra.alsadi@apsva.us,"Intake Center, Adult Education","Staff, Staff",Staff
Adela,Claros,E26367,adela.claros@apsva.us,Transportation,Bus Driver,Transportation Staff
Kelvin,Amaya,E26371,kelvin.amaya@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Yanneth,Pedraza-Torres,E26374,yanneth.pedrazatorre@apsva.us,Aquatics,Staff,Staff
Grace,Chang,E26378,grace.chang@apsva.us,Substitutes,Staff,Staff
Lindsay,Levy,E26380,lindsay.levy@apsva.us,Division of Instruction,Staff,Staff
Deborah,Baker,E26385,deborah.baker2@apsva.us,Substitutes,Staff,Staff
Julie,Vu,E26398,julie.vu@apsva.us,Adult Education,Staff,Staff
Benjamin,Burgin,E26403,benjamin.burgin@apsva.us,Facilities and Operations,Assistant Director,Staff
Tarana,Aktar,E26404,tarana.aktar@apsva.us,Food Services,Food Services Staff,Food Services Staff
Daysi,Martinez,E26407,daysi.martinez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Linda,Saiidifar,E2641,linda.saiidifar@apsva.us,Division of Instruction,Teacher,Instructional Staff
Marieme,Barry,E26420,marieme.barry@apsva.us,Food Services,Food Services Staff,Food Services Staff
Richard,Greene,E26434,richard.greene@apsva.us,Division of Instruction,Staff,Staff
Kevin,Talavera Depaz,E26437,kevin.talaveradepaz@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Antonio,Tondelli,E26439,antonio.tondellipard@apsva.us,Extended Day Substitutes,Staff,Staff
Henriette,Azimi,E26443,henriette.weeserik@apsva.us,Adult Education,Staff,Staff
Tara,Gray,E26448,tara.gray@apsva.us,Transportation,Bus Attendant,Transportation Staff
Silvana,Pinkney,E26450,silvana.pinkney@apsva.us,Food Services,Food Services Staff,Food Services Staff
Aquasha,Mitchell,E26456,aquasha.mitchell@apsva.us,Extended Day,Staff,Staff
Cristina,Pereira,E26459,cristina.pereira@apsva.us,Substitutes,Staff,Staff
Christopher,Palmiero,E26460,christopher.palmiero@apsva.us,Substitutes,Staff,Staff
Erika,Escariz,E26467,erika.escariz@apsva.us,Substitutes,Substitute,Staff
Amanda,Martin,E26470,amanda.martin@apsva.us,Substitutes,Staff,Staff
Alison,Raffaldt,E26488,alison.raffaldt2@apsva.us,Substitutes,Substitute,Staff
Martina,Sabo,E26489,martina.sabo@apsva.us,Substitutes,Staff,Staff
George,Paul,E26505,george.paul@apsva.us,Food Services,Food Services Staff,Food Services Staff
Robert,Lewis,E26514,robert.lewis@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Maria,Del Villar,E26516,maria.delvillar2@apsva.us,Special Education,Administrative Assistant,Staff
Endris,Getahun,E26522,endris.getahun@apsva.us,Transportation,Bus Driver,Transportation Staff
Lauren,Hassel,E26523,lauren.hassel@apsva.us,Facilities and Operations,"Coordinator, Staff",Staff
Malia,Rivera,E26534,malia.rivera@apsva.us,Division of Instruction,Staff,Staff
Lizette,Torres,E26542,lizette.torres@apsva.us,Office of the Superintendent,Executive Assistant,Staff
Julio,Basurto,E26557,julio.basurto@apsva.us,Intake Center,Staff,Staff
Paquita,Sabrera Bardales,E26571,paquita.sabrera@apsva.us,Intake Center,Staff,Staff
Liliana,Lara Ajo,E26574,liliana.laraajo@apsva.us,Food Services,Food Services Staff,Food Services Staff
Danielle,Guerere,E26598,danielle.guerere@apsva.us,Special Education,Teacher,Instructional Staff
Kelsey,Coia,E26610,kelsey.coia@apsva.us,Substitutes,Staff,Staff
Andrea,Kaplowitz,E26619,andrea.kaplowitz@apsva.us,Substitutes,Staff,Staff
Samuel,Hoover,E26621,samuel.hoover@apsva.us,Information Services,Technical Staff,Staff
Iliana,Gonzales,E26629,iliana.gonzales@apsva.us,Administrative Services,Principal,Administrative Staff
Heather,Morgan,E26639,heather.morgan@apsva.us,Special Education,Staff,Staff
Raymond,Palmer-Rainey,E26651,raymond.palmer@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Michelle,Roy,E26678,michelle.roy@apsva.us,Transportation,Bus Driver,Transportation Staff
William,Leggett,E26705,william.leggett@apsva.us,Aquatics,Staff,Staff
Sabrina,Ashby,E26714,sabrina.ashby@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Peter,Gulick,E26715,peter.gulick@apsva.us,Transportation,Bus Driver,Transportation Staff
Robert,Crowe,E26717,robert.crowe2@apsva.us,Aquatics,Staff,Staff
Michele,Mort,E26729,michele.mort@apsva.us,Special Education,Teacher,Instructional Staff
Tameka,Lovett-Miller,E26737,tameka.lovettmiller@apsva.us,Office of Finance,Director,Staff
Kristin,Emery,E26753,kristin.emery@apsva.us,"Substitutes, Special Education","Staff, Teacher",Staff
Joanne,Manning,E26769,joanne.manning2@apsva.us,Food Services,Food Services Staff,Food Services Staff
Erik,Kamenski,E26775,erik.kamenski@apsva.us,"Division of Instruction, APS","Teacher, Teacher",Instructional Staff
Danielle,Stetz,E26790,danielle.stetz@apsva.us,Student Services,Teacher,Instructional Staff
Naghmeh,Merck,E26813,naghmeh.merck@apsva.us,Student Services,Teacher,Instructional Staff
Laura,Jacobs,E26842,laura.jacobs@apsva.us,Special Education,Teacher,Instructional Staff
Danielle,Miles,E26844,danielle.miles@apsva.us,Special Education,Teacher,Instructional Staff
Kimberly,Boyle,E2685,kimberly.boyle@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Tahirih,Lawot,E26853,tahirih.lawot@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Sharon,Sterling,E2690,sharon.sterling@apsva.us,Special Education,Teacher,Instructional Staff
Emily,Gillespie,E26900,emily.gillespie@apsva.us,Special Education,Teacher,Instructional Staff
Helana,Bales,E26913,catie.bales@apsva.us,Division of Instruction,Coordinator,Staff
Katelyn,Gurgiolo,E26915,katelyn.gurgiolo@apsva.us,Special Education,Teacher,Instructional Staff
Elizabeth,Goodman,E26929,elizabeth.goodman@apsva.us,Student Services,Teacher,Instructional Staff
Cathy,Campbell,E26933,cathy.campbell@apsva.us,Division of Instruction,Staff,Staff
Shunda,Johnson,E26935,shunda.johnson@apsva.us,Special Education,Teacher,Instructional Staff
Courtney,Schlottman,E26940,courtney.schlottman@apsva.us,Student Services,Staff,Staff
Suly,Gramajo,E26943,suly.gramajo@apsva.us,Food Services,Staff,Staff
Yaa,Oduro,E26945,yaa.oduro@apsva.us,Food Services,Staff,Staff
Julie,Jones,E26969,julie.jones@apsva.us,Office of the Superintendent,Administrative Assistant,Staff
Mariflor,Ventura,E26978,mariflor.ventura2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Rosie,Barron,E26985,rosie.barron@apsva.us,"Food Services, Extended Day","Extended Day Staff, Staff",Staff
Yeshimebet,Bekele,E27014,yeshimebet.bekele2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Vania,Williams,E27015,vania.williams@apsva.us,"Extended Day, Substitutes","Staff, Staff",Staff
Nichole,Anderson,E27024,nichole.anderson@apsva.us,Office of Personnel,Administrative Specialist,Staff
Jeanell,Pearson,E27027,jeanell.pearson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Charles,Smith,E27032,charles.smith3@apsva.us,Transportation,Bus Driver,Transportation Staff
Madhat,Choudhury,E27034,madhat.choudhury2@apsva.us,"Information Services, Intake Center","Technical Staff, Staff",Staff
Betelehem,Abera,E27048,betelehem.abera2@apsva.us,"Food Services, Transportation","Bus Attendant, Staff",Staff
Nanette,Steele,E27049,nanette.steele2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Bianca,White,E27051,bianca.white@apsva.us,"Food Services, Substitutes, Special Education","Staff, Staff, Staff",Staff
Hilary,Sparrell,E27055,hilary.sparrell@apsva.us,Student Services,Staff,Staff
Maria,Torres,E27058,maria.torres2@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Rebekah,Williams,E27059,rebekah.williams2@apsva.us,Substitutes,Staff,Staff
Wesley,Palmer Bullock,E27070,wesley.palmerbullock@apsva.us,"Special Education, Extended Day","Instructional Assistant, Staff",Instructional Staff
Carolina,Gauto,E27081,carolina.gauto@apsva.us,Substitutes,Staff,Staff
Gulsah,Selvi,E27083,gulsah.selvi@apsva.us,REEP Program,Staff,Staff
Luisa,Echevarria,E27088,luisa.echevarria@apsva.us,Adult Education,Staff,Staff
Catina,Claytor-Frye,E27094,catina.claytorfrye@apsva.us,Division of Instruction,Coordinator,Staff
DeLorenzo,Keels,E27096,delorenzo.keels@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Teneasha,Keels,E27098,teneasha.keels@apsva.us,Extended Day Substitutes,Staff,Staff
Regina,Aguilar Reyes,E27101,regina.aguilarreyes@apsva.us,Food Services,Staff,Staff
Steven,Siekkinen,E27109,steven.siekkinen@apsva.us,Adult Education,Staff,Staff
Lane,Peters,E27115,lane.peters@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Veronica,Jackson,E27126,veronica.jackson@apsva.us,Adult Education,Staff,Staff
Wude,Mareye,E27130,wude.marye@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
John,Schwarz,E27142,john.schwarz@apsva.us,Substitutes,Staff,Staff
Theresa,Hardenburgh,E27154,theresa.hardenburgh2@apsva.us,Substitutes,Staff,Staff
Donald,Riggs,E27157,donald.riggs@apsva.us,Substitutes,Staff,Staff
Romeo,Goffney,E27162,romeo.goffney@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Abdellatif,Rahman,E27169,abdellatif.rahman@apsva.us,Substitutes,Staff,Staff
Diana,So,E27175,diana.so2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Ismael,Gonzalez Ortiz,E27177,ismael.gonzalezortiz@apsva.us,Transportation,Bus Attendant,Transportation Staff
Michelle,Barbour,E27184,michelle.barbour2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Phetsamay,Sayboun,E27187,phetsamay.sayboun@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Annie,Tshibangu,E27191,annie.tshibangu@apsva.us,Extended Day,Staff,Staff
Paul,Dibenedetto,E272,paul.dibenedetto@apsva.us,Aquatics,Staff,Staff
Samira,El Fettahi,E27200,samira.elfettahi2@apsva.us,"Transportation, Food Services","Staff, Bus Attendant",Staff
Sakeal,Touch,E27201,sakeal.touch2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Shiuli,Barua,E27210,shiuli.barua@apsva.us,Food Services,Staff,Staff
Omnia,El-Hakim,E27212,omina.elhakim@apsva.us,Adult Education,Staff,Staff
Maki,Takamatsu,E27213,maki.takamatsu@apsva.us,Adult Education,Staff,Staff
Meskerem,Bekele,E27214,meskerem.bekele@apsva.us,"Substitutes, Extended Day Substitutes","Staff, Staff",Staff
Zuriashwerk,Beyene,E27221,zuriashwerk.beyene@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Staff
Aster,Goitom,E27223,aster.goitom@apsva.us,Extended Day,Staff,Staff
Barhasbadi,Gombojav,E27224,barhasbadi.gombojav@apsva.us,"Intake Center, Adult Education","Staff, Staff",Staff
Jane,Posner,E27245,jane.posner@apsva.us,Substitutes,Staff,Staff
Suman,Barua,E27250,suman.barua@apsva.us,Food Services,Staff,Staff
Amelva,Spaine,E27251,amelva.spaine@apsva.us,"Extended Day, Substitutes","Extended Day Staff, Staff",Staff
Mariam,Abdelgadir,E27255,mariam.abdelgadir@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Amare,Adane,E27257,amare.adane2@apsva.us,Transportation,Bus Driver,Transportation Staff
Evelyn,Matus,E27271,evelyn.matus@apsva.us,"Adult Education, Special Education","Staff, Staff",Staff
William,Pratt,E27272,william.pratt@apsva.us,Relief Custodians,Custodian,Facilities Staff
Beth,Jones,E27287,beth.jones2@apsva.us,Division of Instruction,Staff,Staff
Fatima,Laamiri,E27294,fatima.laamiri@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Extended Day Staff
Teboho,Motaboli,E27307,teboho.motaboli2@apsva.us,Substitutes,Staff,Staff
Rawaa,Al-Dulaimi,E27317,rawaa.aldulaimi@apsva.us,Intake Center,Staff,Staff
Danny,Canales,E27331,jose.canales@apsva.us,Substitutes,Staff,Staff
Tdenek,Ayalew,E27332,tdenek.ayalew@apsva.us,Food Services,Staff,Staff
Omar,Al-Dulaimi,E27333,omar.aldulaimi@apsva.us,Intake Center,Staff,Staff
Jamie,Marin,E27338,jamie.marin@apsva.us,Special Education,Teacher,Instructional Staff
Joe,Tran,E27339,joe.tran@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Alicia,Morris,E27351,alicia.mroczyk@apsva.us,Substitutes,Staff,Staff
Christine,Schofield,E27365,christine.schofield@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Reid,Goldstein,E27366,reid.goldstein@apsva.us,Office of the School Board,School Board Member,Staff
Samuel,Jones,E27368,samuel.jones2@apsva.us,Transportation,Bus Driver,Transportation Staff
Yuly,Paredes,E27369,yuly.paredes2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Jack,Magill,E27383,jack.magill2@apsva.us,Substitutes,Staff,Staff
Vinita,Chawla,E27387,vinita.chawla@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Hilarie,Hoting,E27392,hilarie.hoting@apsva.us,Substitutes,Staff,Staff
Cynthia,MacFarlane-Picard,E27396,cynthia.picard2@apsva.us,Division of Instruction,Staff,Staff
James,Canales,E27417,james.canales@apsva.us,Substitutes,Staff,Staff
Arpita,Barua,E27426,arpita.barua@apsva.us,Extended Day Substitutes,Staff,Staff
Nadia,Girgirah,E27428,nadia.girgirah2@apsva.us,Substitutes,Staff,Staff
Cherie-Ann,Wright,E27434,cherieann.wright@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Margaret,Barbour,E27435,margaret.barbour2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Gail,Benkert,E27436,gail.benkert@apsva.us,Adult Education,Staff,Staff
Janis,James-Northover,E27443,janis.jamesnorthover@apsva.us,"Substitutes, Facilities and Operations","Staff, Staff",Staff
Hanane,Belassal,E27455,hanane.belassal@apsva.us,Substitutes,Staff,Staff
Carmen,Santos Moros,E27465,carmen.maros@apsva.us,Adult Education,Staff,Staff
Alexis,Carrillo,E27466,alexis.carrillo@apsva.us,Substitutes,Staff,Staff
Meseret,Gessesse,E27469,meseret.gessesse2@apsva.us,Transportation,Bus Driver,Transportation Staff
Montel,Bramlett,E27482,montel.bramlett@apsva.us,Facilities and Operations,Staff,Staff
June,Hall,E27496,june.hall2@apsva.us,"Extended Day, Adult Education","Extended Day Staff, Instructional Assistant",Extended Day Staff
Maria,Rodriguez,E27516,maria.rodriguez2@apsva.us,Intake Center,Staff,Staff
Seemaab,Anjum,E27526,seemaab.anjum@apsva.us,Division of Instruction,Staff,Staff
Massimo,De Bernardinis,E27531,massimo.debernardini@apsva.us,"Food Services, Adult Education","Staff, Staff",Staff
Alexander,Hilten,E27533,alexander.hilten@apsva.us,Aquatics,Staff,Staff
Cressida,McKean,E27548,cressida.mckean@apsva.us,Adult Education,Staff,Staff
Earlene,Cassells,E27561,earlene.cassells2@apsva.us,Transportation,Bus Driver,Transportation Staff
James,Mignerey,E27579,james.mignerney@apsva.us,Administrative Services,Staff,Staff
Anusorn,Khamthong,E27586,anusorn.khamthong@apsva.us,Substitutes,Staff,Staff
Shirley,Adoboli,E27588,shirley.adoboli@apsva.us,Relief Custodians,Custodian,Facilities Staff
Ann,Wroth,E27589,ann.wroth@apsva.us,REEP Program,Staff,Staff
Zeinab,Afifi,E27593,zeinab.afifi2@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Keren,Sheffield,E27607,keren.sheffield@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Julio,Basurto,E27611,julio.basurto2@apsva.us,Intake Center,Staff,Staff
Sandra,Casco Matamoros,E27620,sandra.matamoros@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Omayma,Ghabosh,E27621,omayma.ghabosh@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kidest,Tefera,E27639,kidest.tefera@apsva.us,Transportation,Bus Driver,Transportation Staff
Kelli,Andrews,E27651,kelli.andrews@apsva.us,"Extended Day, Food Services","Staff, Extended Day Staff",Staff
Andrea,Padilla Crisostomo,E27655,andrea.crisostomo@apsva.us,Aquatics,Staff,Staff
Yoanna,Zavora,E27661,yoanna.zavora@apsva.us,Intake Center,Staff,Staff
Patricia,Quiroga,E27663,patricia.quiroga@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Ronald,Galloway,E27668,ronald.galloway2@apsva.us,Transportation,Bus Driver,Transportation Staff
Christian,Cheshire,E27682,christian.cheshire@apsva.us,Substitutes,Substitute,Staff
Stephen,Martin,E27687,stephen.martin2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Leroy,Dewitt,E27711,leroy.dewitt@apsva.us,Extended Day,Staff,Staff
Anthony,Luu,E27715,anthony.luu2@apsva.us,Facilities and Operations,Carpenter,Facilities Staff
Robin,Hodges,E27718,robin.hodges@apsva.us,Facilities and Operations,Manager,Staff
In Yeol,Choi,E27725,inyeol.choi@apsva.us,Substitutes,Staff,Staff
David,Stash,E2773,david.stash@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Alexis,Rowland,E27757,alexis.rowland@apsva.us,Substitutes,Staff,Staff
Mary,Borda,E27762,mary.borda@apsva.us,Special Education,Teacher,Instructional Staff
Jesse,Ramirez,E27773,jesse.ramirez2@apsva.us,Information Services,Technical Staff,Staff
Florencia,Morales,E27775,florencia.morales@apsva.us,Extended Day,Staff,Staff
Mikayla,Butler,E27777,mikayla.butler@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kelsey,Fay,E27811,kelsey.fay@apsva.us,Division of Instruction,Coordinator,Staff
Carter,Stucki,E27822,carter.stucki2@apsva.us,Division of Instruction,Staff,Staff
Xavier,Cooper,E27827,xavier.cooper@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Verity,Brown,E27841,verity.brown@apsva.us,Student Services,Staff,Staff
Michele,Hubert,E27848,michele.hubert@apsva.us,Student Services,Staff,Staff
Brooke,Zeller,E27853,brooke.zeller@apsva.us,Student Services,Teacher,Instructional Staff
Tia,Laws,E27875,tia.laws@apsva.us,Information Services,Staff,Staff
Senait,Meskel,E27877,senait.meskel@apsva.us,"Substitutes, Extended Day Substitutes, Intake Center","Staff, Staff, Staff",Staff
William,Thai,E27880,william.thai2@apsva.us,Information Services,Staff,Staff
Erik,Tyson,E27881,erik.tyson2@apsva.us,Aquatics,Staff,Staff
Sam,Klein,E27887,samuel.klein@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Jennifer,Sexton,E27928,jennifer.sexton@apsva.us,Student Services,Teacher,Instructional Staff
Amanda,Dempsey,E27934,amanda.dempsey@apsva.us,Special Education,Teacher,Instructional Staff
Cynthia,Swarts,E27937,cynthia.swarts@apsva.us,Special Education,Teacher,Instructional Staff
Filisha,Manancero,E27950,filisha.manancero@apsva.us,Special Education,Teacher,Instructional Staff
Jessica,Davis,E27970,jessica.davis@apsva.us,Special Education,Teacher,Instructional Staff
Gregory,Myers,E27979,gregory.myers@apsva.us,Student Services,Teacher,Instructional Staff
James,Siddall,E27984,james.siddall@apsva.us,Student Services,Teacher,Instructional Staff
Amanda,Kenney,E27985,amanda.kenney@apsva.us,Special Education,Teacher,Instructional Staff
Kevin,Carey,E27989,kevin.carey2@apsva.us,Transportation,Bus Driver,Transportation Staff
Getahun,Kefyalew,E27994,getahun.kefyalew@apsva.us,Transportation,Bus Driver,Transportation Staff
Ana,Canales-Williams,E27995,ana.canaleswilliams2@apsva.us,Aquatics,Staff,Staff
Heather,Canales-Williams,E27996,heather.williams2@apsva.us,Aquatics,Staff,Staff
Michael DePalma,DePalma,E27999,michael.depalma@apsva.us,Facilities and Operations,Specialist,Staff
Laura,Newton,E28001,laura.newton@apsva.us,Administrative Services,Director,Administrative Staff
Daphney,Denerville-Davis,E28005,daphney.davis@apsva.us,Special Education,Teacher,Instructional Staff
Margaret,Michel,E28015,margaret.michel@apsva.us,Special Education,Teacher,Instructional Staff
Shawniquawn,Jackson,E28026,shawniquawn.jackson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Tanya,Moncrieffe-Heath,E28034,tanya.moncrieffeheat@apsva.us,Student Services,Teacher,Instructional Staff
Marnie,Rudolph,E28036,marnie.rudolph@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Michele,Maurer,E28037,michele.maurer@apsva.us,Division of Instruction,Coordinator,Staff
Tierra,Clayton,E28041,tierra.clayton@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Jeffrey,Chambers,E28047,jeffrey.chambers@apsva.us,Facilities and Operations,Director,Staff
Francisco,Saravia,E28052,francisco.saravia@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Natalie,Pulsifer,E28056,natalie.pulsifer@apsva.us,Student Services,Staff,Staff
Celeste,Schultz,E28066,celeste.schultz@apsva.us,Substitutes,Staff,Staff
Melesse,Demissay,E28067,melesse.demissay@apsva.us,Special Education,Teacher,Instructional Staff
Lacey,Meenaghan,E28069,lacey.meenaghan@apsva.us,Special Education,Teacher,Instructional Staff
Kidist,Feseha,E28081,kidist.feseha@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Conor,O'Connor,E28098,conor.oconnor@apsva.us,Aquatics,Staff,Staff
Lori,Silver,E28104,lori.silver@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Emma,Foley,E28109,emma.foley@apsva.us,Division of Instruction,Staff,Staff
Musse,Yimer,E28110,musse.yimer@apsva.us,Aquatics,Staff,Staff
Kenwyn,Schaffner,E2812,kenwyn.schaffner@apsva.us,Administrative Services,Staff,Staff
Iris,Gibson,E28129,iris.gibson@apsva.us,Student Services,Staff,Staff
Bernardine,Reed,E28137,bernardine.reed@apsva.us,Special Education,Teacher,Instructional Staff
Shannel,Shorter,E28140,shannel.shorter@apsva.us,Substitutes,Staff,Staff
Monae,Blount,E28142,monae.blount@apsva.us,Extended Day Substitutes,Staff,Staff
Christian,Jones,E28151,christian.jones@apsva.us,Adult Education,Staff,Staff
Zoila,Gonzalez Guillen,E28160,zoila.guillen@apsva.us,Food Services,Staff,Staff
Beth,Miller,E28162,beth.miller@apsva.us,Special Education,Teacher,Instructional Staff
Samuel,Wightman,E28171,samuel.wightman@apsva.us,Division of Instruction,Coordinator,Staff
Lolita,Smith,E28183,lolita.smith@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Sicili,McCoy,E28184,sicili.mccoy@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Nancy,Hart,E28186,nancy.hart2@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Brielle,Matthews,E28191,brielle.matthews@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Cora,Lai,E28192,cora.lai@apsva.us,Family Center,Staff,Staff
Phuc,Nguyen,E28199,phuc.nguyen@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Terreka,King,E28200,terreka.king@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Abigail,Stengle,E28202,abigail.stengle@apsva.us,"Division of Instruction, Information Services","Staff, Staff",Staff
Romana,Haque,E28204,romana.haque@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Diane,Carsten-Pelak,E28210,diane.carstenpelak@apsva.us,Adult Education,Staff,Staff
Jeffrey,Lash,E28213,jeffrey.lash@apsva.us,Intake Center,Teacher,Instructional Staff
Tamika,Saunders,E28222,tamika.saunders@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Micayla,Burrows,E28224,micayla.burrows@apsva.us,REEP Program,Specialist,Staff
Tanya,Norman,E28226,tanya.norman@apsva.us,Transportation,Bus Attendant,Transportation Staff
Tesmurun,Gungaadorj,E28227,tesmurun.gungaadorj@apsva.us,"Intake Center, Division of Instruction","Staff, Staff",Staff
Jeanna,Jackson,E28230,jeanna.jackson@apsva.us,Special Education,Teacher,Instructional Staff
Masoumeh,Sadjadekalagahe,E28240,masoumeh.sadjadekala@apsva.us,Extended Day Substitutes,Staff,Staff
Darlene,Lee,E28243,darlene.lee@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Rhonda,Lawhorne,E28247,rhonda.lawhorne2@apsva.us,Transportation,Bus Driver,Transportation Staff
Devin,Ryan,E28248,devin.ryan@apsva.us,Aquatics,Staff,Staff
Kala,Craddock Mcintosh,E28261,kala.mcintosh@apsva.us,Extended Day Substitutes,Staff,Staff
Karla,Barahona de Guerrero,E28267,karla.guerrero@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Regina,Addo,E28268,regina.addo@apsva.us,Food Services,Staff,Staff
Elmer,Premo,E28269,elmer.premo@apsva.us,"Substitutes, Special Education","Staff, Staff",Staff
Rupa,Barua,E28270,rupa.barua2@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Extended Day Staff
Blanca,Saravia,E28282,blanca.saravia2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Jaimie,Davis,E28283,jaimie.davis2@apsva.us,Transportation,Bus Driver,Transportation Staff
Lindsay,Cowen,E28285,lindsay.cowen@apsva.us,Substitutes,Staff,Staff
Veronica,Marin,E28286,veronica.marin2@apsva.us,Food Services,Food Services Staff,Food Services Staff
Jean-Pierre,Woo,E28287,jeanpierre.woo@apsva.us,Food Services,Staff,Staff
Kristen,Overstreet,E28298,kristen.overstreet@apsva.us,Special Education,Teacher,Instructional Staff
Dulce,Carrillo,E28299,dulce.carrillo@apsva.us,School and Community Relations,Supervisor,Staff
Melissa,Stone,E2830,melissa.stone@apsva.us,Special Education,Teacher,Instructional Staff
Alyssa,Kozma,E28316,alyssa.kozma@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Heather,Carey,E28320,heather.carey2@apsva.us,Adult Education,Staff,Staff
Timothy,McGhee,E28324,timothy.mcghee2@apsva.us,"Substitutes, Special Education","Staff, Staff",Staff
Jesse,Baskin,E28341,jesse.baskin@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Segomotso,Seferian,E28342,segomotso.seferian2@apsva.us,"Substitutes, Division of Instruction","Substitute, Staff",Staff
Heather,Davis,E28344,heather.davis@apsva.us,"Student Services, Division of Instruction","Teacher, Staff, Coordinator",Instructional Staff
Cassandra,Holloway,E28346,cassandra.holloway@apsva.us,Substitutes,Staff,Staff
Jhonatan,Oyola Pastrana,E28352,jhonatan.pastrana@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Zahidul,Khan,E28358,zahidul.khan@apsva.us,Office of Personnel,Analyst,Staff
Maryam,Khalilian,E28369,maryam.khalilian@apsva.us,Substitutes,Staff,Staff
Almaz,Kidane,E28371,almaz.kidane@apsva.us,Transportation,Clerical Specialist,Staff
Hanan,Assamawy,E28372,hanan.assamawy@apsva.us,"Extended Day, Special Education","Staff, Extended Day Staff",Staff
Julie,Pike,E28389,julie.pike@apsva.us,Substitutes,Staff,Staff
Filza,Chattha,E28398,filza.chattha@apsva.us,Substitutes,Substitute,Staff
Ariel,Coburn,E28403,ariel.coburn@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Eden,Yilma,E28404,eden.yilma@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Douglas,Stine,E28410,douglas.stine@apsva.us,Substitutes,Staff,Staff
Amanda,Brundidge,E28414,amanda.brundidge@apsva.us,Adult Education,Staff,Staff
Riovana,Quiroz,E28429,riovana.quiroz@apsva.us,Food Services,Staff,Staff
Woynishet,Woldemariam,E28430,woynishet.woldemaria@apsva.us,"Food Services, Transportation","Staff, Bus Driver",Staff
Leib,Kaminsky,E28434,leib.kaminsky@apsva.us,Substitutes,Staff,Staff
Marcia,Katz,E28435,marcia.katz@apsva.us,Substitutes,Staff,Staff
Sherry,Promise,E28443,sherry.promise@apsva.us,Substitutes,Staff,Staff
Rasha,Al Jerjawi,E28444,rasha.aljerjawi@apsva.us,"Adult Education, Substitutes","Staff, Staff",Staff
Cynthia,Krech,E28447,cynthia.krech2@apsva.us,Substitutes,Substitute,Staff
Bertha,Mitchell,E28453,bertha.mitchell2@apsva.us,"Substitutes, Administrative Services","Staff, Staff",Staff
Alicia,Martinez Flores,E28461,alicia.flores@apsva.us,Student Services,Administrative Assistant,Staff
Marjorie,Blazek,E28462,marjorie.blazek@apsva.us,Student Services,Administrative Assistant,Staff
Ilene,Silverman,E28470,ilene.silverman@apsva.us,Substitutes,Staff,Staff
James,Tyson-Channer,E28471,james.tysonchanner@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Sarah,Johnson,E28473,sarah.johnson2@apsva.us,Information Services,Coordinator,Staff
Siham,Idris,E28482,siham.idris@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Esmeralda,Maldonado,E28487,esmeralda.maldonado@apsva.us,"Office of Personnel, Food Services","Custodian, Staff",Staff
Jennifer,Fuentes,E28516,jennifer.fuentes@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Luis,Duque Leon,E28521,luis.duqueleon2@apsva.us,Transportation,Bus Driver,Transportation Staff
Ivis,Castillo,E28522,ivis.castillo2@apsva.us,Transportation,Bus Driver,Transportation Staff
Kimberly,Wilks,E28524,kimberly.wilks@apsva.us,Facilities and Operations,Director,Staff
Edward,Beidleman,E28526,edward.beidleman@apsva.us,Facilities and Operations,Maintenance Technician,Facilities Staff
Hiwot,Uma,E28529,hiwot.uma@apsva.us,Transportation,Staff,Staff
Jennifer,VanBuren,E28530,jennifer.vanburen@apsva.us,Adult Education,Staff,Staff
Tayseer,Eltayeb,E28532,tayseer.eltayeb@apsva.us,Substitutes,Staff,Staff
Paola,Lopez,E28533,paola.lopez@apsva.us,Extended Day Substitutes,Staff,Staff
Elizabeth,Weston,E28546,elizabeth.weston@apsva.us,Substitutes,Staff,Staff
Jennifer,Colmer,E28550,jennifer.colmer@apsva.us,Special Education,Teacher,Instructional Staff
Carolina,Sorto,E28552,carolina.sorto@apsva.us,Division of Instruction,Clerical Specialist,Staff
Rodolfo,Barrientos,E28561,rodolfo.barrientos@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Meseret,Enyew,E28568,meseret.enyew@apsva.us,Food Services,Staff,Staff
Victoria,Omekam,E28576,victoria.omekam@apsva.us,Extended Day Substitutes,Staff,Staff
Irina,Bartell,E28580,irene.bartell@apsva.us,Substitutes,Staff,Staff
Jason,Mathewson,E28604,jason.mathewson@apsva.us,Information Services,Specialist,Staff
Mary,Dohrenwend,E28614,mary.dohrenwend2@apsva.us,Substitutes,Staff,Staff
Joy,Haley,E28623,joy.haley@apsva.us,Special Education,Teacher,Instructional Staff
Susan,Chase,E28636,susan.chase@apsva.us,Substitutes,Staff,Staff
Rupam,Barua,E28640,rupam.barua2@apsva.us,Food Services,Food Services Staff,Food Services Staff
Sagirah,Biddle,E28642,sagirah.biddle@apsva.us,Extended Day Substitutes,Staff,Staff
Safwa,Mohamed,E28644,safwa.mohamed@apsva.us,"Extended Day, Food Services","Staff, Extended Day Staff",Staff
Sara,Benitez,E28650,sara.benitez@apsva.us,Substitutes,Staff,Staff
Sandra,Doyle,E28653,sandra.doyle@apsva.us,Food Services,Food Services Staff,Food Services Staff
Nazma,Akter,E28654,nazma.akter@apsva.us,Food Services,Staff,Staff
Mark,Beauvais,E28655,mark.beauvais@apsva.us,Information Services,Analyst,Staff
Rasha,Mohamed,E28663,rasha.mohamed@apsva.us,"Special Education, Food Services","Staff, Staff",Staff
Kiera,Willis,E28666,kiera.willis@apsva.us,Extended Day,Staff,Staff
Farrukh,Islam,E28667,farrukh.islam@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Marco,Mandujano Esquivel,E28669,marco.mandujano@apsva.us,REEP Program,Staff,Staff
Suha,Sowwan,E28676,suha.sowwan@apsva.us,"Substitutes, Student Services","Staff, Staff",Staff
Alexandra,Kurian,E28682,alexandra.kurian@apsva.us,Aquatics,Staff,Staff
Desalgne,Taye,E28687,desalgne.taye2@apsva.us,Transportation,Bus Driver,Transportation Staff
Ben,Tesfa,E28688,ben.tesfa@apsva.us,Transportation,Bus Driver,Transportation Staff
Mohamed,Mohamud,E28689,mohamed.mohamud@apsva.us,"Extended Day, Information Services, Extended Day Substitutes","Staff, Staff, Staff",Staff
Juana,Sorto,E28693,juana.sorto@apsva.us,Food Services,Staff,Staff
Younus,Al Dulaimi,E28707,younus.aldulaimi@apsva.us,Intake Center,Staff,Staff
Cynthia,Hilton,E28709,cynthia.hilton@apsva.us,Substitutes,Staff,Staff
Kristine,Gregos,E28717,kristine.gregos@apsva.us,Substitutes,Staff,Staff
Jacqueline,Firster,E2872,jacqueline.firster@apsva.us,Information Services,Analyst,Staff
Frida,Krachenfels,E28730,frida.krachenfels@apsva.us,Special Education,Teacher,Instructional Staff
Raykwan,Russell,E28736,raykwan.russell@apsva.us,Aquatics,Staff,Staff
Santos,Portillo Coreas,E28739,santos.coreas@apsva.us,Extended Day,Staff,Staff
Syed,Ahmed,E28742,syed.ahmed@apsva.us,Food Services,Staff,Staff
Christian,Berrios,E28748,christian.berrios@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Erin,Foster,E28756,erin.foster@apsva.us,Aquatics,Staff,Staff
Marissa,Salad,E28758,marissa.salad@apsva.us,Extended Day,Staff,Staff
Megan,Kellogg,E28760,megan.kellogg@apsva.us,Substitutes,Staff,Staff
Brittany,Thomas,E28761,brittany.thomas@apsva.us,Special Education,Teacher,Instructional Staff
Kimberly,Goodwin,E28782,kimberly.goodwin2@apsva.us,Transportation,Bus Driver,Transportation Staff
Teklay,Kahsay,E28783,teklay.kahsay2@apsva.us,Transportation,Staff,Staff
Carol,Stevens,E28787,carol.stevens@apsva.us,Adult Education,Staff,Staff
Alison,Cummings,E28796,alison.cummings@apsva.us,Substitutes,Staff,Staff
Quintin,Estep,E28812,quintin.estep@apsva.us,Transportation,Staff,Staff
Jalane,Daschke,E28816,jalane.daschke@apsva.us,Substitutes,Staff,Staff
Zaria,Miller,E28817,zaria.miller@apsva.us,Substitutes,Substitute,Staff
Emani,Gray,E28819,emani.gray@apsva.us,Substitutes,Substitute,Staff
Muna,Desta,E28823,desta.muna@apsva.us,Substitutes,Staff,Staff
Nicholas,Natalie,E28825,nicholas.natalie@apsva.us,APS,Teacher,Instructional Staff
Sergio,Rocha,E28827,sergio.rocha2@apsva.us,Transportation,Clerical Specialist,Staff
Angel,Lopez Soto,E28828,angel.lopezsoto@apsva.us,Special Education,Staff,Staff
Erika,Taylor,E28832,erika.taylor@apsva.us,Office of Personnel,Administrator,Staff
Julia,Lagos,E28833,julia.marquez@apsva.us,Food Services,Staff,Staff
Katrina,Van Duyn,E28838,katrina.vanduyn@apsva.us,Adult Education,Staff,Staff
Alberto,Ramos,E28853,alberto.ramos@apsva.us,Facilities and Operations,Maintenance Staff,Facilities Staff
Edwin,Ramirez,E28857,edwin.ramirez2@apsva.us,"Information Services, Administrative Services","Staff, Staff",Staff
Joseph,McMahon,E28859,joseph.mcmahon@apsva.us,Information Services,Analyst,Staff
Berhanu,Abdurhaman,E28868,berhanu.abdurhaman2@apsva.us,Transportation,Bus Driver,Transportation Staff
Tanjuara,Taslim,E28872,tanjuara.taslim@apsva.us,"Substitutes, Extended Day, Food Services","Staff, Staff, Extended Day Staff",Staff
Wendy,Zamora Moran,E28873,wendy.zamoramoran@apsva.us,Relief Custodians,Staff,Staff
Jonathan,Escalera,E28875,jonathan.escalera@apsva.us,Information Services,Staff,Staff
Douglas,Aparicio,E28880,douglas.aparicio@apsva.us,"Information Services, Division of Instruction","Staff, Staff",Staff
Alejandro,Navarro,E28886,alejandro.navarro@apsva.us,Information Services,Staff,Staff
Cathryn,McVicker,E28889,cathryn.mcvicker@apsva.us,Aquatics,Staff,Staff
Xaviar,Goodman,E28909,xaviar.goodman@apsva.us,Information Services,Staff,Staff
Natalie,Edwards,E28919,natalie.edwards@apsva.us,Student Services,Teacher,Instructional Staff
Prarthana,Appaiah,E28921,prarthana.appaiah@apsva.us,Substitutes,Staff,Staff
Kory,Bradley,E28925,kory.bradley@apsva.us,Student Services,Teacher,Instructional Staff
Karin,Bloss,E28928,karin.bloss@apsva.us,Special Education,Teacher,Instructional Staff
Elizabeth,Brady,E28930,elizabeth.brady@apsva.us,Student Services,Teacher,Instructional Staff
Nancy,McKenna,E28955,nancy.mckenna@apsva.us,Special Education,Teacher,Instructional Staff
Cynthia,Kotakis,E28956,cynthia.kotakis@apsva.us,Division of Instruction,Coordinator,Staff
Isabel,Messmore,E28959,isabel.messmore@apsva.us,School and Community Relations,Teacher,Instructional Staff
Amy,Sherman,E28963,amy.sherman@apsva.us,Division of Instruction,Teacher,Instructional Staff
Jennifer,DeCamp,E28974,jennifer.decamp@apsva.us,Special Education,Teacher,Instructional Staff
Anna,Kim,E28976,anna.kim@apsva.us,Student Services,Teacher,Instructional Staff
Cassandra,Class,E28980,cassandra.class@apsva.us,Student Services,Teacher,Instructional Staff
Jennifer,Phillipp,E29029,jennifer.phillipp@apsva.us,Student Services,Teacher,Instructional Staff
Charisse,Berree,E29033,charisse.berree@apsva.us,Student Services,Teacher,Instructional Staff
Anthony,Frazier,E29034,anthony.frazier@apsva.us,Adult Education,Staff,Staff
Claudia,Debose,E29036,claudia.debose@apsva.us,Division of Instruction,Teacher,Instructional Staff
Siobhan,Bowler,E2905,siobhan.bowler@apsva.us,Student Services,Teacher,Instructional Staff
Gregory,Stuart,E29054,gregory.stuart@apsva.us,Adult Education,Staff,Staff
Ron,Crouse,E29057,ron.crouse@apsva.us,Division of Instruction,Coordinator,Staff
William,Roberts,E29074,william.roberts@apsva.us,Information Services,Technical Staff,Staff
Laurie,Artman-McIntosh,E29075,laurie.mcintosh@apsva.us,Division of Instruction,Staff,Staff
Christina,Childress,E29077,christina.childress2@apsva.us,Transportation,Bus Driver,Transportation Staff
Crystal,Harris,E29094,crystal.harris2@apsva.us,Transportation,Bus Driver,Transportation Staff
Solomon,Aga,E29096,solomon.aga2@apsva.us,Transportation,Bus Driver,Transportation Staff
Ayderus,Ahmed,E29097,ayderus.ahmed2@apsva.us,Transportation,Bus Driver,Transportation Staff
Tammie,Jones,E29101,tammie.jones2@apsva.us,Transportation,Bus Driver,Transportation Staff
Meskerem,Woldesemayat,E29104,meskerem.woldesemaya@apsva.us,Food Services,Staff,Staff
Catherine,Hall,E29106,catherine.hall@apsva.us,Student Services,Teacher,Instructional Staff
Lucille,Convery,E29108,lucille.convery@apsva.us,Division of Instruction,Coordinator,Staff
Keiry,Argueta Jarquin,E29111,keiry.arguetajarquin@apsva.us,Extended Day Substitutes,Staff,Staff
Adiya,Enkhtaivan,E29116,adiya.enkhtaivan@apsva.us,Extended Day Substitutes,Staff,Staff
Rahel,Dubale,E29118,rahel.dubale@apsva.us,"Division of Instruction, Intake Center, Extended Day Substitutes","Staff, Staff, Staff",Staff
Stephanie,Strang,E29126,stephanie.strang@apsva.us,Adult Education,Staff,Staff
Pamela,Kopiak,E29132,pamela.kopiak@apsva.us,Office of the Superintendent,LEGAL COUNSEL,Staff
Pamela,Muir,E29138,pamela.muir@apsva.us,Adult Education,Staff,Staff
Alejandro,Padilla Crisostomo,E29143,alejandro.crisostomo@apsva.us,Aquatics,Staff,Staff
Adrian,Francis,E29144,adrian.francis@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Sandy,Quintanilla,E29149,sandy.quintanilla@apsva.us,Substitutes,Substitute,Staff
Berhane,Mengistu,E29159,berhane.mengistu2@apsva.us,Transportation,Bus Attendant,Transportation Staff
Abrehet,Temelso,E29160,abrehet.temelso2@apsva.us,Transportation,Bus Driver,Transportation Staff
Melissa,Hyatt,E29175,melissa.hyatt@apsva.us,Division of Instruction,Coordinator,Staff
Lesli,Estrada,E29184,lesli.estrada@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Nazmun,Nahar,E29193,nazmun.nahar@apsva.us,Food Services,Staff,Staff
Helena,Machado,E292,helena.machado@apsva.us,Facilities and Operations,Director,Staff
Janet,O'Grady,E29200,janet.ogrady2@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Fanny,Sparks,E29211,fanny.sparks@apsva.us,Adult Education,Staff,Staff
Vaughn,Gee,E29212,vaughn.gee@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Christine,Murphy,E29219,christine.murphy@apsva.us,Extended Day Substitutes,Staff,Staff
Ellen,Cathopoulis,E29221,ellen.cathopoulis@apsva.us,Special Education,Teacher,Instructional Staff
John,Oyola Pastrana,E29222,john.oyolapastrana@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Maria,Cabrera,E29227,maria.cabrera@apsva.us,"Extended Day, Food Services","Staff, Extended Day Staff",Staff
Natalie,Cocozza,E29245,natalie.cocozza@apsva.us,Student Services,Teacher,Instructional Staff
Michelle,Hejl,E29250,michelle.hejl@apsva.us,Substitutes,Staff,Staff
Tesfai,Gebreyohannes,E29255,tesfai.gebreyohannes@apsva.us,Transportation,Bus Attendant,Transportation Staff
Michote,Yibekal,E29256,michote.yibekal@apsva.us,Transportation,Bus Driver,Transportation Staff
Brittnee,Stephens,E29264,brittnee.stephens@apsva.us,Student Services,Teacher,Instructional Staff
Frewoini,Giday,E29267,frewoini.giday2@apsva.us,Transportation,Bus Driver,Transportation Staff
Alexis,Lopez,E29271,alexis.lopez@apsva.us,Office of Personnel,Clerical Specialist,Staff
Justin,Clark,E29284,justin.clark@apsva.us,Aquatics,Staff,Staff
John,Reamy,E29286,john.reamy@apsva.us,Information Services,Technical Staff,Staff
Soumaya,Melki,E29288,soumaya.melki@apsva.us,"Adult Education, Substitutes","Staff, Staff",Staff
Linda,Erdos,E2929,linda.erdos@apsva.us,School and Community Relations,Staff,Staff
Corey,Adams,E29292,corey.adams2@apsva.us,Information Services,"Staff, Technical Staff",Staff
Adhanet,Bereketab,E29294,adhanet.bereketab@apsva.us,Food Services,Staff,Staff
Janice,Gerard,E29299,janice.gerard@apsva.us,Adult Education,Staff,Staff
Elizabeth,Hill,E29302,elizabeth.hill@apsva.us,Substitutes,Staff,Staff
Angelia,Thrash-Thomas,E29303,angelia.thrashthomas@apsva.us,Office of Personnel,Staff,Staff
James,Maxstadt,E29304,james.maxstadt@apsva.us,Substitutes,Staff,Staff
Kent,Klein,E29314,kent.klein@apsva.us,Substitutes,Staff,Staff
Shanna,McVey,E29315,shanna.mcvey@apsva.us,Special Education,Teacher,Instructional Staff
Jasmin,Henriquez,E29320,jasmin.henriquez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Amante,Fajardo,E29326,amante.fajardo@apsva.us,REEP Program,Staff,Staff
Catherine,Siles,E29332,catherine.siles@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Sanaa,Alami Rahmouni,E29335,sanaa.alamirahmouni@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kalyan,Barua,E29337,kalyan.barua@apsva.us,Food Services,Staff,Staff
Staur,Harris,E29338,staur.harris@apsva.us,Extended Day,Specialist,Staff
Clara,Gudiel,E29346,clara.gudielygudiel@apsva.us,Family Center,Staff,Staff
Jasper,Little,E29348,jasper.little2@apsva.us,"Administrative Services, Substitutes","Substitute, Staff",Staff
Yvonne,Bingham,E29354,yvonne.bingham@apsva.us,Extended Day,Staff,Staff
Omar,Abu-Ali,E29357,omar.abuali@apsva.us,Substitutes,Staff,Staff
Lawrence,McCarthy,E29375,lawrence.mccarthy@apsva.us,Adult Education,Staff,Staff
Solomon,Nigussie,E29377,solomon.nigussie@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Torin,Ortmayer,E29384,torin.ortmayer@apsva.us,Substitutes,Staff,Staff
Cintia,Johnson,E2939,cintia.johnson@apsva.us,Administrative Services,Staff,Staff
Susan,Omberg,E29400,susan.omberg@apsva.us,Substitutes,Staff,Staff
Zozan,Siso,E29404,zozan.siso2@apsva.us,Extended Day,Staff,Staff
Angela,Turner,E29412,angela.turner@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Alberto,Quezada Campos,E29418,alberto.quezada@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Naudia,Key,E29423,naudia.key@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Shirin,Kabir,E29426,shirin.kabir@apsva.us,"Extended Day, Food Services","Staff, Staff",Staff
Seema,Singh,E29427,seema.singh2@apsva.us,Substitutes,Substitute,Staff
Jerrold,Cohen,E29438,jerrold.cohen@apsva.us,Substitutes,Staff,Staff
Quinn,Smith Perry,E29443,quinn.smithperry@apsva.us,Substitutes,Staff,Staff
Aditya,Singh,E29448,aditya.singh@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Nicole,Brown,E29449,nicole.brown@apsva.us,Special Education,Staff,Staff
Farida,Hamidi,E29454,farida.hamidi@apsva.us,Transportation,Bus Attendant,Transportation Staff
Robert,Davidson,E29480,robert.davidson@apsva.us,Adult Education,Staff,Staff
Tayech,Meseret,E29489,tayech.meseret@apsva.us,"Food Services, Extended Day","Extended Day Staff, Staff",Extended Day Staff
Timeka,Eskridge,E29490,timeka.eskridge@apsva.us,"Substitutes, Extended Day Substitutes","Staff, Staff",Staff
Lee,Thorsen,E29495,lee.thorsen@apsva.us,Substitutes,Staff,Staff
John,Wasserman,E29496,john.wasserman@apsva.us,Aquatics,Manager,Staff
Tazrina,Afrose,E29497,tazrina.afrose2@apsva.us,Food Services,Food Services Staff,Food Services Staff
Djamel,Chetouane,E29498,djamel.chetouane2@apsva.us,Transportation,Bus Driver,Transportation Staff
Girma,Gulilat,E29501,girma.gulilat@apsva.us,Transportation,Bus Driver,Transportation Staff
Desta,Mekonenn,E29502,desta.mekonenn2@apsva.us,"Food Services, Transportation","Bus Driver, Staff",Staff
Phillip,Swarts,E29518,phillip.swarts2@apsva.us,Substitutes,Staff,Staff
Carlo,Traverso,E29521,carlo.traverso@apsva.us,Substitutes,Staff,Staff
Heather,Rothenbuescher,E29528,heather.rothenb@apsva.us,Division of Instruction,Director,Administrative Staff
Bereket,Negash,E29529,bereket.negash2@apsva.us,Transportation,Bus Driver,Transportation Staff
Valeria Camacho,Galdo Camacho,E29539,valeria.galdocamacho@apsva.us,Aquatics,Staff,Staff
Russell,Topp,E29540,russell.topp@apsva.us,Transportation,Staff,Staff
Carlos,Mercado,E29543,carlos.mercado2@apsva.us,Special Education,Staff,Staff
Chi,Liou,E29546,chi.liou@apsva.us,Information Services,Staff,Staff
Gloria,Little,E29547,gloria.little@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Desiree,Velazquez,E29548,desiree.velazquez@apsva.us,Substitutes,Staff,Staff
Tekeste,Adane,E29569,tekeste.adane@apsva.us,Food Services,Staff,Staff
Rubaiyat,Rhidoy,E29570,rubaiyat.rhidoy@apsva.us,School and Community Relations,Producer,Staff
Ashley,Moore,E29582,ashley.moore@apsva.us,Division of Instruction,Staff,Staff
Emperatriz,Guzman,E29586,emperatriz.guzman@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Nelson Matinez,Hernandez Martinez,E29588,nelson.martinez@apsva.us,Facilities and Operations,Electrician,Facilities Staff
Darius,Hunt,E29590,darius.hunt@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Randall,Erwin,E29593,randall.erwin@apsva.us,Substitutes,Staff,Staff
Bethlehem,Tsehaye,E29595,bethlehem.tsehaye@apsva.us,"Food Services, Extended Day","Extended Day Staff, Staff",Staff
Jennifer,Jones,E29596,jennifer.jones@apsva.us,Adult Education,Staff,Staff
Ra'Sean,Lawhorne,E29598,rasean.lawhorne@apsva.us,Transportation,Staff,Staff
Pablo,Barron,E29609,pablo.barron@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Deanna,Beckham,E29610,deanna.beckham@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Fahema,Akter,E29616,fahema.akter@apsva.us,Food Services,Staff,Staff
Aregash,Beyene,E29619,aregash.beyene@apsva.us,"Food Services, Extended Day","Staff, Staff",Staff
Mir,Milani,E29621,mir.milani@apsva.us,Extended Day Substitutes,Staff,Staff
Christine,Van Kirk,E29626,christine.vankirk@apsva.us,Division of Instruction,Staff,Staff
Toni,McGuire,E29638,toni.mcguire@apsva.us,Adult Education,Staff,Staff
Julia,Hutton,E29652,julia.hutton@apsva.us,Aquatics,Staff,Staff
Sandra,Gonzalez Torres,E29658,sandra.torres@apsva.us,Substitutes,Substitute,Staff
Renee,Sturgill,E29664,renee.sturgill@apsva.us,Special Education,Teacher,Instructional Staff
John,Elgin,E29665,john.elgin@apsva.us,Substitutes,Staff,Staff
Shukla,Barua,E29672,shukla.barua@apsva.us,Food Services,Food Services Staff,Food Services Staff
Rokhila,Zakirova,E29680,rokhila.zakirova@apsva.us,Division of Instruction,Staff,Staff
Carson,Brooke,E29685,carson.brooke@apsva.us,Aquatics,Staff,Staff
Maya,Edmondson,E29686,maya.edmondson@apsva.us,REEP Program,Staff,Staff
Amanda,Johnston,E29687,amanda.johnston@apsva.us,Substitutes,Staff,Staff
Pierrine,Hunter,E29690,pierrine.hunter2@apsva.us,Special Education,Staff,Staff
Yoseph,Bekele,E29698,yoseph.bekele2@apsva.us,Transportation,Staff,Staff
Hailu,Jabir,E29700,hailu.jabir2@apsva.us,Transportation,Bus Driver,Transportation Staff
Marta,Gonzalez,E29702,marta.gonzalez@apsva.us,Adult Education,Staff,Staff
Karl,Nelson,E29704,karl.nelson@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Estefany,Van Den Elzen,E29705,estefany.salazar@apsva.us,Division of Instruction,Staff,Staff
Patricia,Hargrove,E29706,patricia.hargrove@apsva.us,Substitutes,Staff,Staff
Shabnam,Rezayee,E29714,shabnam.rezayee@apsva.us,"Food Services, Extended Day","Staff, Staff",Staff
Leon,Young,E29720,leon.young@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Adrianne,McQuillan,E29726,adrianne.mcquillan@apsva.us,Substitutes,Staff,Staff
Laura,Depatch,E2974,laura.depatch@apsva.us,Special Education,Teacher,Instructional Staff
Hanna,Woldemariam,E29743,hanna.woldemariam2@apsva.us,"Food Services, Extended Day Substitutes","Staff, Staff",Staff
Tahira,Rehman,E29756,tahira.rehman@apsva.us,Substitutes,Staff,Staff
Alejandra,Quinonez,E29758,alejandra.quinonez@apsva.us,Substitutes,Staff,Staff
Behzad,Jaffarbhoy,E29759,behzad.jaffarbhoy@apsva.us,Substitutes,Staff,Staff
Fannie,Walker,E29760,fannie.walker@apsva.us,"Substitutes, Special Education","Staff, Staff",Staff
W,Llanos Montenegro,E29773,w.llanosmontenegro@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
L,Lazarte Villaroel,E29777,l.lazartevillaroel2@apsva.us,Transportation,Bus Driver,Transportation Staff
B,Gebreyohanis,E29778,b.gebreyohanis2@apsva.us,Transportation,Bus Driver,Transportation Staff
Abdi,Elias,E29779,abdi.elias2@apsva.us,Transportation,Bus Driver,Transportation Staff
Yoseph,Petros,E29782,yoseph.petros2@apsva.us,Transportation,Bus Driver,Transportation Staff
Tilaye,Aligaz,E29786,tilaye.aligaz2@apsva.us,Transportation,Bus Driver,Transportation Staff
Harriet,Morgan,E29791,harriet.morgan@apsva.us,Substitutes,Staff,Staff
Richard,Delos Reyes,E29792,richard.delosreyes@apsva.us,Adult Education,Staff,Staff
Oliver,Stearns,E29793,oliver.stearns@apsva.us,Aquatics,Staff,Staff
Dashdorj,Danzannyam,E29795,dashdorj.danzannyam@apsva.us,Extended Day Substitutes,Staff,Staff
Lola,Ortiz Torrez,E29796,lola.ortiztorrez@apsva.us,Food Services,Food Services Staff,Food Services Staff
Sarah,Putnam,E298,sarah.putnam@apsva.us,Division of Instruction,Director,Administrative Staff
Enkhtuul,Nyamaakhuu,E29805,enkhtuul.nyamaakhuu@apsva.us,Information Services,Staff,Staff
Melissa,Thenhaus,E29807,melissa.thenhaus@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Mabel,Martinez,E29809,mabel.martinez@apsva.us,Substitutes,Staff,Staff
Dina,Quijano Aroni,E29821,dina.aroni@apsva.us,Food Services,Food Services Staff,Food Services Staff
Sabrina,O'Neil,E29823,sabrina.oneil@apsva.us,Adult Education,Staff,Staff
Flor,Gonzalez,E29828,flor.gonzalez@apsva.us,Student Services,Teacher,Instructional Staff
E,Basto Eyzaguirre,E29839,e.bastoeyzaguirre@apsva.us,Substitutes,Staff,Staff
Catherine,Ashby,E29842,catherine.ashby@apsva.us,Office of the Superintendent,Assistant Superintendent,Administrative Staff
Timothy,Peacock,E29858,timothy.peacock@apsva.us,Substitutes,Staff,Staff
Justin,Robinson,E29859,justin.robinson2@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Marietta,Bradberry,E29860,marietta.bradberry@apsva.us,Substitutes,Staff,Staff
Sandy,Villalta,E29861,sandy.villalta@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Jordan,Luu,E29863,jordan.luu@apsva.us,Substitutes,Substitute,Staff
Lyda,Barrera,E29864,lyda.barrera@apsva.us,Substitutes,Staff,Staff
Garrett,Lane,E29867,garrett.lane@apsva.us,"Information Services, Division of Instruction","Staff, Staff",Staff
Marcus,Lane,E29868,marcus.lane@apsva.us,Information Services,Staff,Staff
Neim,Phe,E29874,neim.phe@apsva.us,Extended Day Substitutes,Staff,Staff
Manizha,Hakimi,E29878,manizha.hakimi@apsva.us,Substitutes,Staff,Staff
Abir,Esmeel,E29882,abir.esmeel@apsva.us,"Substitutes, Extended Day","Extended Day Staff, Staff",Staff
Lucas,Cherry,E29889,lucas.cherry3@apsva.us,Substitutes,Staff,Staff
Jesse,Martell,E29891,jesse.martell@apsva.us,Information Services,Staff,Staff
Fikri,Ramadan,E29898,fikri.ramadan@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Joshua,Martell,E29903,joshua.martell@apsva.us,Information Services,Staff,Staff
Shandana,Shah,E29905,shandana.shah@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Lalla,Idrissi,E29909,lalla.idrissi@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Lila,Ammar,E29910,lila.ammar@apsva.us,Substitutes,Staff,Staff
Carolyn,Kroeger,E29916,carolyn.kroeger@apsva.us,Division of Instruction,Staff,Staff
Leo,Eichers,E29917,leo.eichers@apsva.us,Aquatics,Staff,Staff
Penelope,Beuch,E29921,penelope.beuch@apsva.us,Adult Education,Staff,Staff
Isaiah,Montague,E29925,isaiah.montague@apsva.us,Information Services,Staff,Staff
Thomas,Kargbo,E29926,thomas.kargbo@apsva.us,Substitutes,Staff,Staff
Lukas,Wood,E29941,lukas.wood@apsva.us,Aquatics,Staff,Staff
Christopher,Gulyn,E29942,christopher.gulyn@apsva.us,Aquatics,Staff,Staff
Tyler,Witman,E29961,tyler.witman@apsva.us,Division of Instruction,Coordinator,Staff
Jill,Beadles,E29974,jill.beadles@apsva.us,Special Education,Teacher,Instructional Staff
Marvin,Vides Blanco,E29985,marvin.videsblanco@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Alyssa,Moody,E30003,alyssa.moody@apsva.us,Division of Instruction,Coordinator,Staff
Aida,Haddad,E30010,aida.haddad@apsva.us,Adult Education,Staff,Staff
John,Bacon,E30011,john.bacon@apsva.us,Substitutes,Staff,Staff
Michael,Simpson,E30027,michael.simpson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Janee,McCoy,E30034,janee.mccoy@apsva.us,Substitutes,Staff,Staff
Giselle,Mercado,E30035,giselle.mercado@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Pausha,Monroe,E30040,pausha.monroe@apsva.us,Student Services,Teacher,Instructional Staff
Wendy,Cornejo,E30044,wendy.cornejo@apsva.us,Student Services,Teacher,Instructional Staff
Takiya,Jackson,E30052,takiya.jackson@apsva.us,Student Services,Teacher,Instructional Staff
Vincent,Howell,E30056,vincent.howell@apsva.us,Substitutes,Staff,Staff
Heather,Keppler,E30058,heather.keppler@apsva.us,Substitutes,Staff,Staff
Anna,Harpel,E30061,anna.harpel@apsva.us,Aquatics,Staff,Staff
Abebayehu,Angelo,E30064,abebayehu.angelo2@apsva.us,Transportation,Bus Driver,Transportation Staff
Goytom,Gebrekidan,E30068,goytom.gebrekidan2@apsva.us,Transportation,Bus Driver,Transportation Staff
Aziza,Syed,E30069,aziza.syed@apsva.us,"Extended Day, Substitutes","Extended Day Staff, Staff",Extended Day Staff
Tammy,Treadway,E30081,tammy.treadway@apsva.us,Adult Education,Staff,Staff
Shawn,Valdes,E30090,shawn.valdes2@apsva.us,Transportation,Bus Driver,Transportation Staff
Cassiele,Camacho,E30091,cassiele.camacho@apsva.us,"Information Services, Extended Day Substitutes","Staff, Staff",Staff
Vincent,Smith,E30101,vincent.smith@apsva.us,APS,Teacher,Instructional Staff
Paulette,Rigali,E30118,paulette.rigali2@apsva.us,Student Services,Teacher,Instructional Staff
Lauren,Robertson,E30140,lauren.robertson@apsva.us,Special Education,"Teacher, Staff",Instructional Staff
Maria,Zeigler,E30170,maria.zeigler@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Tamkevin,Dai,E30178,tamkevin.dai@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Asmat,Pervez,E30190,asmat.pervez2@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Barbara,Fisher,E30192,barbara.fisher@apsva.us,Student Services,Teacher,Instructional Staff
Kristen,Evans,E30195,kristen.evans@apsva.us,Special Education,Teacher,Instructional Staff
Susan,Thomas,E30196,susan.thomas@apsva.us,Special Education,Teacher,Instructional Staff
Kathleen,Smith,E30210,kathleen.smith@apsva.us,Special Education,Teacher,Instructional Staff
Lisa,Nemeth,E30215,lisa.nemeth@apsva.us,Student Services,Teacher,Instructional Staff
Brianna,Cobbins,E30228,brianna.cobbins@apsva.us,Office of Personnel,Director,Administrative Staff
Scott,Sharpnack,E30229,scott.sharpnack2@apsva.us,Substitutes,Staff,Staff
Jennifer,Danaceau,E30231,jennifer.danaceau@apsva.us,Substitutes,Staff,Staff
James,Sullivan,E30235,james.sullivan@apsva.us,REEP Program,Staff,Staff
Tyler,Phillips,E30238,tyler.phillips@apsva.us,Aquatics,Staff,Staff
Hiwot,Gizachew,E30241,hiwot.gizachew@apsva.us,Special Education,Teacher,Instructional Staff
Jazmin,Lopez Soto,E30244,jazmin.lopezsoto@apsva.us,Extended Day,Staff,Staff
Heba,Al Bashiereh,E30246,heba.albashiereh@apsva.us,Substitutes,Staff,Staff
Alyse,Oliver,E30248,alyse.oliver@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Subarna,Barua,E30251,subarna.barua@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Elizsa,Key,E30253,elizsa.key@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Sumaiya,Chowdhury,E30261,sumaiya.chowdhury@apsva.us,"Extended Day Substitutes, Special Education","Staff, Staff",Staff
Alexandra,Sheldrake,E30264,alexandra.sheldrake@apsva.us,Aquatics,Staff,Staff
Arminda,Hidalgo Acosta,E30269,arminda.hidalgoa@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Staff
Khory,Moore,E30273,khory.moore@apsva.us,Substitutes,Staff,Staff
Nakeesha,Seneb,E30279,nakeesha.seneb@apsva.us,Adult Education,Staff,Staff
Staci,Downey,E30281,staci.downey@apsva.us,Student Services,Teacher,Instructional Staff
Thomas,Caine,E30283,thomas.caine@apsva.us,Substitutes,Staff,Staff
Kezia,Solano,E30285,kezia.solano@apsva.us,Food Services,Staff,Staff
Ambreen,Kiani,E30286,ambreen.kiani@apsva.us,Substitutes,Substitute,Staff
Sooh-Zun,Hwang,E30289,soohzun.hwang@apsva.us,Information Services,Technical Staff,Staff
Iris,Tomala,E30292,iris.vigil@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Loreto,Karver,E30302,loreto.karver@apsva.us,Substitutes,Staff,Staff
Safiyah,Benlafquih,E30305,safiyah.benlafquih@apsva.us,Substitutes,Staff,Staff
Edna,Mercado,E30306,edna.mercado@apsva.us,Special Education,Staff,Staff
Nicholas,Metzger,E30310,nicholas.metzger@apsva.us,Aquatics,Staff,Staff
Amanda,Hine,E30312,amanda.hine@apsva.us,Adult Education,Staff,Staff
Samiya,Nami,E30318,samiya.nami@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Alassane,Ly,E30319,alassane.ly@apsva.us,Administrative Services,Staff,Staff
David,Tyahla,E30333,david.tyahla@apsva.us,Substitutes,Substitute,Staff
Noreen,Syeda,E30345,noreen.syeda@apsva.us,Extended Day,Staff,Staff
Sankar,Barua,E30347,sankar.barua@apsva.us,Food Services,Staff,Staff
Kala,Cox,E30355,kala.cox@apsva.us,Aquatics,Staff,Staff
James,White,E30357,james.white@apsva.us,Substitutes,Staff,Staff
Carlos,Merly,E30359,carlos.merly@apsva.us,Information Services,Technical Staff,Staff
Twila,Glick,E3036,twila.glick@apsva.us,Substitutes,Staff,Staff
Andrea,Taylor,E30364,andrea.taylor@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Ellen,Clore-Patron,E30372,ellen.clorepatron@apsva.us,REEP Program,Staff,Staff
Mohammed,Alam,E30377,mohammed.alam@apsva.us,Food Services,Staff,Staff
Birkty,Beyene,E30384,birkty.beyene@apsva.us,Transportation,Bus Attendant,Transportation Staff
Elizabeth,Negussie,E30388,elizabeth.negussie@apsva.us,"Food Services, Transportation","Staff, Bus Attendant",Staff
Tyler,Smith,E30392,tyler.smith@apsva.us,Substitutes,Staff,Staff
Martin,Phillips,E30395,martin.phillips@apsva.us,Substitutes,Staff,Staff
Karima,Ibn Elfarouk,E30403,karima.ibnelfarouk@apsva.us,Division of Instruction,Staff,Staff
Rosario,Quijano Aroni,E30405,rosario.quijanoaroni@apsva.us,Food Services,Staff,Staff
Genet,Haileselassie,E30406,genet.haileselassie@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Shaila,Hossain,E30407,shaila.hossain@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Colleen,Greenwood,E3041,colleen.greenwood@apsva.us,Student Services,Teacher,Instructional Staff
Linda,Lopez,E30412,linda.lopez2@apsva.us,"Food Services, Transportation","Staff, Bus Driver",Staff
Nejat,Mohammed,E30413,nejat.mohammed@apsva.us,Food Services,Staff,Staff
Sue,Sarber,E3042,sue.sarber@apsva.us,Office of Personnel,Director,Administrative Staff
Elizabeth,Herrington,E30421,elizabeth.herrington@apsva.us,Adult Education,Staff,Staff
Njideka,Olatunde,E30423,njideka.olatunde@apsva.us,Substitutes,Staff,Staff
M,Villarroel Cordova,E30429,m.villarroelcordova@apsva.us,Substitutes,Staff,Staff
Basant,Edris,E30432,basant.edris@apsva.us,Substitutes,Staff,Staff
Shamson,Nahar,E30435,shamson.nahar@apsva.us,Food Services,Staff,Staff
Dwayne,Herrin,E30440,dwayne.herrin@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Shamsa,Murshid,E30442,shamsa.murshid@apsva.us,Substitutes,Staff,Staff
Maryam,Hakimi,E30444,maryam.hakimi@apsva.us,"Substitutes, Extended Day Substitutes","Substitute, Staff",Staff
Abdul,Mukati,E30455,abdul.mukati@apsva.us,Special Education,Teacher,Instructional Staff
Katelyn,Mathis,E30458,katelyn.mathis@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Roche,Monroe,E30463,roche.monroe@apsva.us,Transportation,Bus Attendant,Transportation Staff
Ethan,Doyle,E30473,ethan.doyle@apsva.us,Aquatics,Staff,Staff
Hugo,Soller,E30474,hugo.soller@apsva.us,Aquatics,Staff,Staff
Vicente,Soto,E30476,vicente.soto@apsva.us,Aquatics,Staff,Staff
Radka,Herndon,E30480,radka.herndon@apsva.us,Substitutes,Staff,Staff
Pattaraporn,Wannalo,E30493,pattaraporn.wannalo@apsva.us,"Food Services, Extended Day","Staff, Staff",Staff
Asia,Mustafa,E30496,asia.mustafa@apsva.us,Food Services,Staff,Staff
Bisrat,Beyene,E30511,bisrat.beyene@apsva.us,Food Services,Staff,Staff
"Hines, Krystal",Bujeiro-Hines,E30512,krystal.hines@apsva.us,Special Education,Teacher,Instructional Staff
Saba,Okbagergish,E30514,saba.okbagergish@apsva.us,Substitutes,Staff,Staff
Najat,Mouzoun,E30515,najat.mouzoun2@apsva.us,Food Services,Staff,Staff
Fatima,Moutaoukil,E30517,fatima.moutaoukil@apsva.us,"Extended Day Substitutes, Food Services","Staff, Staff",Staff
Bishop,McGee,E30518,bishop.mcgee@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Amanda,Jones Coleman,E30524,amanda.jonescoleman@apsva.us,Adult Education,Staff,Staff
Adity,Barua,E30525,adity.barua@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Carlos,Campos,E30526,carlos.campos2@apsva.us,Extended Day Substitutes,Staff,Staff
Deborah,Powells Williams,E30530,deborah.williams@apsva.us,Transportation,Bus Driver,Transportation Staff
Belachew,Mengistu,E30531,belachew.mengistu2@apsva.us,Transportation,Bus Driver,Transportation Staff
Jeanne,Hargett,E30542,jeanne.hargett@apsva.us,Substitutes,Staff,Staff
Maryam,Ghalib,E30547,maryam.ghalib@apsva.us,Substitutes,Staff,Staff
Shahenaz,Abdelbaset,E30552,shahenaz.abdelbaset@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Staff
Jayla,Henry,E30563,jayla.henry@apsva.us,Extended Day,Staff,Staff
Kamila,Mangal,E30567,kamila.mangal@apsva.us,Special Education,Staff,Staff
Leonard,Gradowski,E30569,leonard.gradowski@apsva.us,Substitutes,Staff,Staff
Selma,Ali Ibrahim,E30575,selma.aliibrahim@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Evan,Ross,E30578,evan.ross@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Manjuara,Ahmed,E30585,manjuara.ahmed@apsva.us,Food Services,Staff,Staff
Mossy,Bikoni,E30586,mossy.bikoni@apsva.us,Transportation,Bus Attendant,Transportation Staff
Colleen,Sport,E30591,colleen.sport@apsva.us,Substitutes,Staff,Staff
Janet,Gardner,E30604,janet.gardner@apsva.us,"Substitutes, Administrative Services","Staff, Staff",Staff
Mariam,Alwarith,E30607,mariam.alwarith@apsva.us,Substitutes,Staff,Staff
Walter,Weitz Marholz,E30610,walter.weitzmarholz@apsva.us,Special Education,Staff,Staff
Marianela,Padilla Perez,E30618,marianela.padilla@apsva.us,Substitutes,Staff,Staff
Bruce,Majors,E30621,bruce.majors@apsva.us,Substitutes,Staff,Staff
Nurcan,Cetinbas,E30628,nurcan.cetinbas@apsva.us,Substitutes,Staff,Staff
Jewel,McNeil,E30646,jewel.mcneil@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Imran,Khaliq,E30656,imran.khaliq@apsva.us,Food Services,Staff,Staff
Jacqueline,Arguello,E30659,jacqueline.arguello@apsva.us,Transportation,Clerical Specialist,Staff
Bethany,Banal,E30660,bethany.banal2@apsva.us,Student Services,Teacher,Instructional Staff
Desiree,Armstrong,E30675,desiree.armstrong@apsva.us,Administrative Services,Clerical Assistant,Staff
Alexander,Fernandez Pons,E30678,alexander.fernandez@apsva.us,Substitutes,Staff,Staff
Julius,Townsend,E30685,julius.townsend@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Zoubida,Belfiroud,E30686,zoubida.belfiroud@apsva.us,Food Services,Staff,Staff
Edward,O'Connell,E30690,edward.oconnell@apsva.us,Substitutes,Staff,Staff
Mark,Kataoka,E30691,mark.kataoka@apsva.us,Substitutes,Staff,Staff
Stephanie,Norris,E30696,stephanie.norris@apsva.us,Adult Education,Staff,Staff
Katia,Velasquez Soto,E30710,katia.velasquezsoto@apsva.us,"Information Services, Extended Day","Staff, Extended Day Staff",Extended Day Staff
Shanti,Dsouza,E30725,shanti.dsouza@apsva.us,Substitutes,Staff,Staff
Symon,Cater,E30739,symon.cater@apsva.us,Aquatics,Staff,Staff
Jessy,Bracamonte Peraza,E30740,jessy.bracamonte@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Joshua,Martini,E30742,joshua.martini@apsva.us,Substitutes,Staff,Staff
Mary,Michael,E30744,mary.michael@apsva.us,"Food Services, Substitutes, Extended Day","Staff, Extended Day Staff, Staff",Staff
Megan,Mulvey,E30757,megan.mulvey@apsva.us,Substitutes,Substitute,Staff
Afsaneh,Azizi,E30763,afsaneh.azizi@apsva.us,Substitutes,Staff,Staff
Marco,Lirio Aguillon,E30766,marco.lirioaguillon@apsva.us,REEP Program,Staff,Staff
Erebka,Tsegaye,E30768,erebka.tsegaye@apsva.us,Transportation,Bus Attendant,Transportation Staff
Merkeb,Zemba,E30770,merkeb.zemba@apsva.us,"Substitutes, Intake Center","Staff, Staff",Staff
Mary,Tennant,E30773,mary.tennant@apsva.us,Substitutes,Substitute,Staff
Emily,Radakovich,E30775,emily.radakovich@apsva.us,Special Education,Teacher,Instructional Staff
Sherry,Sposeep,E30778,sherry.sposeep@apsva.us,REEP Program,Staff,Staff
Estela,Reyes,E30789,estela.reyes@apsva.us,Food Services,Food Services Staff,Food Services Staff
Kidane,Gerethgiher,E30793,kidane.gerethgiher@apsva.us,Transportation,Bus Driver,Transportation Staff
Sultan,Muhe,E30794,sultan.muhe@apsva.us,Transportation,Bus Driver,Transportation Staff
Damboba,Jeylu,E30796,damboba.jeylu@apsva.us,Transportation,Bus Driver,Transportation Staff
Stacey,Edington,E30799,stacey.edington@apsva.us,Special Education,Staff,Staff
Shah,Bano,E30800,shah.bano@apsva.us,Substitutes,Staff,Staff
Mohamed,Abdurahman,E30806,mohamed.abdurahman@apsva.us,Transportation,Bus Driver,Transportation Staff
Ikhtyar,Ali,E30810,ikhtyar.ali@apsva.us,"Food Services, Substitutes, Extended Day Substitutes","Staff, Substitute, Staff",Staff
Gabriela,Delcid,E30814,gabriela.delcid2@apsva.us,Intake Center,Staff,Staff
Brian,Mah,E30815,brian.mah@apsva.us,Substitutes,Substitute,Staff
Angela,Pinder,E30817,angela.pinder@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Ana,Aquino,E30834,ana.aquino@apsva.us,"Extended Day, Food Services","Staff, Extended Day Staff",Staff
Binah,Carter,E30835,binah.carter@apsva.us,"Substitutes, Extended Day","Staff, Staff",Staff
Richard,Owens,E30837,richard.owens@apsva.us,Adult Education,Staff,Staff
Gregory,Vallandingham,E30838,greg.vallandingham@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Isabel,Nova De Nunez,E30840,isabel.novadenunez@apsva.us,Adult Education,Staff,Staff
Angelo,Sibilla,E30841,angelo.sibilla@apsva.us,Adult Education,Staff,Staff
Jennifer,Swanson,E30843,jennifer.swanson@apsva.us,Adult Education,Staff,Staff
Lizhe,Han,E30844,lizhe.han@apsva.us,Adult Education,Staff,Staff
Rawshonara,Islam,E30845,rawshonara.islam@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kevin,Glover,E30850,kevin.glover@apsva.us,Office of Personnel,Staff,Staff
Loujayen,Altayeb,E30854,altayeb.loujayen@apsva.us,Substitutes,Staff,Staff
Sumaia,Ahmed,E30859,sumaia.ahmed@apsva.us,Substitutes,Staff,Staff
Samson,Worku,E30860,samson.worku@apsva.us,Transportation,Bus Driver,Transportation Staff
Estifanos,Woldemariam,E30868,estifanos.wold@apsva.us,Transportation,Bus Driver,Transportation Staff
Kari,Gray,E30869,kari.gray@apsva.us,Substitutes,Staff,Staff
Mark,Swadley,E30875,mark.swadley@apsva.us,Transportation,Bus Driver,Transportation Staff
Saqi,Barua,E30880,saqi.barua@apsva.us,Food Services,Staff,Staff
Kamala,Alatmeh,E30881,kamala.alatmeh@apsva.us,Extended Day Substitutes,Staff,Staff
Ellen,Nye,E30883,ellen.nye@apsva.us,Substitutes,Staff,Staff
Radia,Omar,E30884,radia.omar@apsva.us,"Food Services, Transportation","Staff, Bus Attendant",Staff
Elizabeth,Guran,E30889,elizabeth.guran@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Konstantinos,Nikopoulos,E30892,konstantinos.niko@apsva.us,Substitutes,Substitute,Staff
Abdul,Khan,E30894,abdul.khan@apsva.us,School and Community Relations,Staff,Staff
Sophie,Soller,E30895,sophie.soller@apsva.us,Aquatics,Staff,Staff
Amory,Gant,E30899,amory.gant@apsva.us,"Substitutes, Extended Day","Extended Day Staff, Staff",Staff
Fahmida,Sultana,E30900,fahmida.sultana@apsva.us,Food Services,Staff,Staff
Helen,Bard-Sobola,E30906,helen.bardsobola@apsva.us,"Substitutes, Extended Day Substitutes, Administrative Services","Staff, Staff, Staff",Staff
Lakia,Dozier,E30910,lakia.dozier@apsva.us,Office of Personnel,Clerical Specialist,Staff
Regbe,Minas,E30911,regbe.minas@apsva.us,Extended Day,Staff,Staff
Sofia,Marquez,E30912,sofia.marquez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Jazmine,Dominguez,E30913,jazmine.dominguez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Hajira,Mobeen,E30915,hajira.mobeen@apsva.us,Substitutes,Staff,Staff
Robyn,Sweet,E30917,robyn.sweet@apsva.us,Substitutes,Staff,Staff
Sebastiana,Sanchez,E30940,sebastiana.sanchez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kyrah,Smith,E30941,kyrah.smith@apsva.us,Substitutes,Staff,Staff
Naila,Qureshi,E30943,naila.qureshi@apsva.us,Substitutes,Staff,Staff
Thomas,Gray,E30948,thomas.gray@apsva.us,Substitutes,Staff,Staff
Yoshiko,Weick,E30957,yoshiko.weick@apsva.us,Adult Education,Staff,Staff
Denise,Brown,E30959,denise.brown2@apsva.us,Substitutes,Staff,Staff
Tom,Wacker,E30961,tom.wacker@apsva.us,"Aquatics, Substitutes","Staff, Staff",Staff
Lara,Maksymonko,E30962,lara.maksymonko@apsva.us,Special Education,Teacher,Instructional Staff
Sophia,Rivas,E30972,sophia.rivas@apsva.us,Substitutes,Staff,Staff
Mohammad,Emdadullah,E30974,mohammad.emdadullah@apsva.us,Substitutes,Staff,Staff
Deron,Nolan,E30979,deron.nolan@apsva.us,Transportation,Bus Driver,Transportation Staff
Allison,Page,E30989,allison.page@apsva.us,Adult Education,Staff,Staff
Dudy,Martinez Berganza,E30990,dudy.martinez@apsva.us,Special Education,Staff,Staff
Syeda,Choudhury,E30994,syeda.choudhury@apsva.us,Extended Day Substitutes,Staff,Staff
Andrew,Bolfek,E30995,andrew.bolfek@apsva.us,Aquatics,Staff,Staff
Abdalla,Ahmed,E31008,abdalla.ahmed@apsva.us,Substitutes,Staff,Staff
Karen,Nirschl,E31009,karen.nirschl@apsva.us,Special Education,Teacher,Instructional Staff
Kristen,Patterson,E31011,kristen.patterson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Reese,Hall,E31013,reese.hall@apsva.us,Information Services,Staff,Staff
Sondra,Stokes,E31023,sondra.stokes@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Brittany,Alvarado,E31025,brittany.alvardo@apsva.us,"Special Education, Substitutes","Staff, Staff",Staff
Isabella,Rivas,E31027,isabella.rivas@apsva.us,Substitutes,Staff,Staff
Tri,Luu,E31029,tri.luu@apsva.us,Facilities and Operations,Carpenter,Facilities Staff
Gabriel,Ramos-Diaz,E31038,gabriel.ramosdiaz@apsva.us,Information Services,Staff,Staff
Evan,Fillmore,E31040,evan.fillmore@apsva.us,Aquatics,Staff,Staff
Katherine,Marston,E31042,katherine.marston@apsva.us,Aquatics,Staff,Staff
Matthew,Tyson,E31043,matthew.tyson@apsva.us,Aquatics,Staff,Staff
Ellenor,Brown,E31046,ellenor.brown@apsva.us,Transportation,Bus Driver,Transportation Staff
Abdelylah,Touil,E31047,abdelylah.touil@apsva.us,"Food Services, Transportation","Bus Driver, Staff",Staff
Emmanuel,Yeboah,E31048,emmanuel.yeboah@apsva.us,Information Services,Staff,Staff
David,Horak,E31058,david.horak@apsva.us,Administrative Services,Principal,Administrative Staff
Jessica,DaSilva,E31059,jessica.dasilva@apsva.us,Administrative Services,Principal,Administrative Staff
Ibidunni,Osundare,E31060,ibidunni.osundare@apsva.us,Substitutes,Staff,Staff
Kimberly,Pham,E31062,kimberly.pham@apsva.us,Substitutes,Staff,Staff
Alexis,Johnson,E31070,alexis.johnson@apsva.us,APS,Teacher,Instructional Staff
Tigist,Abessa,E31077,tigist.abessa@apsva.us,Transportation,Bus Driver,Transportation Staff
Jeffery,Davis,E31078,jeffrey.davis@apsva.us,Transportation,Bus Driver,Transportation Staff
Rolando,Eguino,E31080,rolando.eguino@apsva.us,Transportation,Bus Driver,Transportation Staff
Janie,Jacobs,E31084,janie.jacobs@apsva.us,Special Education,Teacher,Instructional Staff
Alison,Versaggi,E31086,alison.versaggi@apsva.us,Student Services,Teacher,Instructional Staff
Margaret,Dassira,E31099,margaret.dassira@apsva.us,Student Services,Teacher,Instructional Staff
Flor,Canales Cruz,E31102,flor.canalescruz@apsva.us,Substitutes,Substitute,Staff
Dwayne,Marigny,E31105,dwayne.marigny@apsva.us,Information Services,Staff,Staff
Ameya,Sinha,E31109,ameya.sinha@apsva.us,Aquatics,Staff,Staff
Gabriel,Vazquez,E31110,gabriel.vazquez@apsva.us,Aquatics,Staff,Staff
Alexander,Reed,E31111,alexander.reed@apsva.us,Aquatics,Staff,Staff
Thomas,Leggett,E31112,thomas.leggett@apsva.us,Aquatics,Staff,Staff
Leslie,Martin,E31114,leslie.martin@apsva.us,Division of Instruction,Coordinator,Staff
Merlyn,Rodriguez,E31117,merlyn.rodriguez@apsva.us,Food Services,Staff,Staff
Jon,MacGregor,E31122,jon.macgregor@apsva.us,Aquatics,Staff,Staff
Emma,Bort,E31123,emma.bort@apsva.us,Aquatics,Staff,Staff
Brelynn,Black,E31125,brelynn.black@apsva.us,Transportation,Bus Attendant,Transportation Staff
Wendy,McNally,E31132,wendy.mcnally@apsva.us,Substitutes,Staff,Staff
Ellen,Farnham,E31134,ellen.farnham@apsva.us,Adult Education,Staff,Staff
Keith,Fowler,E31148,keith.fowler@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Meriem,Benhammadi,E31154,meriem.benhammadi@apsva.us,Food Services,Staff,Staff
Tauheda,Tuly,E31158,tauheda.tuly@apsva.us,"Food Services, Extended Day","Staff, Staff",Staff
Rita,Barua,E31159,rita.barua@apsva.us,Food Services,Staff,Staff
Jason,Delozier,E31162,jason.delozier@apsva.us,Extended Day Substitutes,Staff,Staff
Michele,Body,E31163,michele.body@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Sandra,Triveri,E31166,sandra.triveri@apsva.us,Family Center,Specialist,Staff
Yolanda,Machicado,E31176,yolanda.machicado@apsva.us,Special Education,Staff,Staff
Diane,Schwartz,E31181,diane.schwartz@apsva.us,REEP Program,Staff,Staff
Carrie,Butcher,E31197,carrie.butcher@apsva.us,"Extended Day, Food Services","Staff, Staff",Staff
Salma,Cabrera,E31203,salma.cabrera@apsva.us,"Extended Day Substitutes, Substitutes","Staff, Staff",Staff
Anna,Jacobs,E31224,anna.jacobs@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Aicha,Elyamani,E31241,aicha.elyamani@apsva.us,Food Services,Staff,Staff
Katherine,Karns,E31255,katherine.karns@apsva.us,Student Services,Teacher,Instructional Staff
Antoinette,Perry,E31271,antoinette.perry@apsva.us,Special Education,Teacher,Instructional Staff
Elizabeth,Ryan,E31275,elizabeth.ryan@apsva.us,Student Services,Teacher,Instructional Staff
Natalia,McMahan,E31302,natalia.mcmahan@apsva.us,Special Education,Teacher,Instructional Staff
Kelly,Pendleton,E31335,kelly.pendleton@apsva.us,Student Services,Teacher,Instructional Staff
Cody,McCready,E31337,cody.mccready@apsva.us,Substitutes,Staff,Staff
Stephanie,McDaniel,E31340,stephanie.mcdaniel@apsva.us,Student Services,Teacher,Instructional Staff
Rachel,Moore,E31342,rachel.moore2@apsva.us,Special Education,Teacher,Instructional Staff
Ryan,Jackson,E31351,ryan.jackson@apsva.us,Special Education,Teacher,Instructional Staff
Nicole,Reynolds,E31353,nicole.reynolds@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Yvette,Rigdon,E31354,yvette.rigdon@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Jamie,Karlinchak,E31359,jamie.karlinchak@apsva.us,Special Education,Specialist,Staff
Heather,Johnson,E31366,heather.johnson@apsva.us,Aquatics,Staff,Staff
Noe,Urquia Cuadra,E31370,noe.urquiacuadra@apsva.us,Adult Education,Staff,Staff
Mukta,Khatun,E31380,mukta.khatun@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Erika,Porro,E31385,erika.porro@apsva.us,Family Center,Staff,Staff
Marc,Dyer,E31390,marc.dyer@apsva.us,Substitutes,Staff,Staff
Rida,Hussain,E31400,rida.hussain@apsva.us,Substitutes,Staff,Staff
Gayle,Rainbow,E31402,gayle.rainbow@apsva.us,Division of Instruction,Instructional Assistant,Instructional Staff
Jeremy,Wintersteen,E31406,jeremy.wintersteen@apsva.us,Division of Instruction,Teacher,Instructional Staff
Shelby,McDavid,E31412,shelby.mcdavid@apsva.us,Substitutes,Staff,Staff
Lorena,Henriquez,E31419,lorena.henriquez@apsva.us,Transportation,Bus Attendant,Transportation Staff
Mohamed,Bouzhar,E31420,mohamed.bouzhar@apsva.us,Food Services,Staff,Staff
Lady,Aranibar Gallinate,E31423,lady.aranibarg@apsva.us,Extended Day,Staff,Staff
Massiel,Cerna,E31424,massiel.cerna@apsva.us,"Substitutes, Extended Day","Staff, Staff",Staff
Bouria,Hallato,E31425,bouria.hallato@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Mekdes,Wendemu,E31432,mekdes.wendemu@apsva.us,"Food Services, Extended Day","Extended Day Staff, Staff",Staff
Liliana,Paredes,E31437,liliana.paredes@apsva.us,Extended Day Substitutes,Staff,Staff
Niema,Omer,E31438,niema.omer@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Andrea,Golden,E31442,andrea.golden@apsva.us,Substitutes,Staff,Staff
Maysoon,Salih,E31456,maysoon.salih@apsva.us,Division of Instruction,Teacher,Instructional Staff
Ammara,Jabeen,E31457,ammara.jabeen@apsva.us,Substitutes,Staff,Staff
Evangelia,Alataris,E31459,evangelia.alataris@apsva.us,Substitutes,Staff,Staff
Nibal,Hammoudeh,E31471,nibal.hammoudeh@apsva.us,Extended Day,Staff,Staff
Aicha,Ouaajal,E31475,aicha.ouaajal@apsva.us,Special Education,Staff,Staff
Ailis,Brown,E31476,ailis.brown@apsva.us,Aquatics,Staff,Staff
Layne,Kelly,E31477,layne.kelly@apsva.us,Aquatics,Staff,Staff
Mable,Scott,E31479,mable.scott@apsva.us,Special Education,Staff,Staff
Cassandra,Scott,E31480,cassandra.scott@apsva.us,"Special Education, Substitutes","Staff, Staff",Staff
Maria,Yang,E31481,maria.yang@apsva.us,Adult Education,Staff,Staff
Shauna,Corbin,E31482,shauna.corbin@apsva.us,Office of Personnel,Coordinator,Administrative Staff
Samira,El Jaouani,E31484,samira.eljaouani@apsva.us,Food Services,Staff,Staff
Khaleda,Akther,E31489,khaleda.akther@apsva.us,"Food Services, Extended Day","Staff, Extended Day Staff",Extended Day Staff
Jonathan,Wilson,E31495,jonathan.wilson@apsva.us,Substitutes,Substitute,Staff
Gabrielle,Rivas,E31503,gabrielle.rivas@apsva.us,Substitutes,Staff,Staff
Liliana,Castillo,E31507,liliana.castillo@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Tiffany,Rush,E31508,tiffany.rush@apsva.us,Substitutes,Substitute,Staff
Johnny,Lazarte,E31511,johnny.lazarte@apsva.us,Extended Day,Staff,Staff
Gina,Craig,E31512,gina.craig@apsva.us,Substitutes,Staff,Staff
Suad,Omer,E31513,suad.omer@apsva.us,Extended Day Substitutes,Staff,Staff
Troy,Savage,E31515,troy.savage@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Marion,Mattioli,E31516,marion.mattioli@apsva.us,Adult Education,Staff,Staff
Daisy,Portillo-Garcia,E31518,daisy.portillogarcia@apsva.us,Extended Day Substitutes,Staff,Staff
Kristin,Pugh,E31521,kristin.pugh@apsva.us,Substitutes,Staff,Staff
Jennifer,Curtis,E31523,jennifer.curtis@apsva.us,Special Education,Staff,Staff
Emily,Thai,E31524,emily.thai@apsva.us,Special Education,Staff,Staff
Shamim,Akhtar,E31526,shamim.akhtar@apsva.us,Food Services,Staff,Staff
Timothy,Starker,E31540,timothy.starker@apsva.us,Substitutes,Staff,Staff
Brandon,Murray,E31541,brandon.murray@apsva.us,Extended Day Substitutes,Staff,Staff
Khawla,Bougarfaoui,E31542,khawla.bougarfaoui@apsva.us,Substitutes,Staff,Staff
Reba,Bowman,E31543,reba.bowman@apsva.us,Substitutes,Staff,Staff
Asnakech,Worku-Kereta,E31544,asnakech.workukereta@apsva.us,"Food Services, Extended Day","Staff, Staff",Staff
Refath,Karim,E31553,refath.karim@apsva.us,"Extended Day Substitutes, Food Services","Staff, Staff",Staff
Lucia,Perdomo Mejia,E31554,lucia.perdomomejia@apsva.us,Food Services,Staff,Staff
Paulina,Altamirano De Padilla,E31557,paulina.altamirano@apsva.us,Transportation,Bus Attendant,Transportation Staff
Luis,Castellanos Hurtado,E31558,luis.castellano@apsva.us,"Adult Education, Adult Education","Staff, Staff",Staff
Jane,Kone,E31563,jane.kone@apsva.us,REEP Program,Specialist,Staff
Zolzaya,Batkhuu,E31571,zolzaya.batkhuu@apsva.us,Substitutes,Substitute,Staff
Yvonne,Cox Johnson,E31573,yvonne.coxjohnson@apsva.us,Special Education,Staff,Staff
Maria,Deliz,E31576,maria.deliz@apsva.us,"Adult Education, Special Education","Staff, Staff",Staff
Genet,Zacharias,E31580,genet.zacharias@apsva.us,Transportation,Bus Driver,Transportation Staff
Paul,Buckley,E31581,paul.buckley@apsva.us,Aquatics,Staff,Staff
Tsehayensh,W-Mariam,E31582,tsehayensh.wmariam@apsva.us,Transportation,Bus Attendant,Transportation Staff
Shitaye,Fanta,E31583,shitaye.fanta@apsva.us,"Transportation, Food Services","Staff, Bus Attendant",Transportation Staff
Susanna,McIlwaine,E31588,susanna.mcilwaine@apsva.us,Substitutes,Staff,Staff
Rahwea,Teages,E31591,rahwea.teages@apsva.us,Transportation,Bus Attendant,Transportation Staff
Asmaa,Zouaydi,E31593,asmaa.zouaydi@apsva.us,Food Services,Staff,Staff
Lasean,Parks,E31595,lasean.parks@apsva.us,Transportation,Bus Attendant,Transportation Staff
Beverly,Hughes,E31604,beverly.hughes@apsva.us,Substitutes,Staff,Staff
Nicole,Briggs,E31605,nicole.briggs@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Christian,Weddle,E31606,christian.weddle@apsva.us,"Special Education, Extended Day Substitutes, Food Services","Staff, Staff, Staff",Staff
Bardia,Bahrami Zamani,E31607,bardia.bahramizamani@apsva.us,Aquatics,Staff,Staff
Hafida,Faouri,E31610,hafida.faouri@apsva.us,Substitutes,Staff,Staff
Vanessa,DeLeon,E31611,vanessa.deleon@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Sheri,Bloomingburg,E31613,sheri.bloomingburg@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Haley,McReynolds,E31614,haley.mcreynolds@apsva.us,Special Education,Instructional Assistant,Instructional Staff
John,Howard,E31617,john.howard@apsva.us,Substitutes,Substitute,Staff
Derron,Bentley,E31621,derron.bentley@apsva.us,Substitutes,Substitute,Staff
Eileen,Conoboy,E31627,eileen.conoboy@apsva.us,Substitutes,Staff,Staff
Guizen,Del Canto,E31629,guizen.delcanto@apsva.us,Substitutes,Staff,Staff
Maymouna,Al Sayyed,E31635,maymouna.alsayyed@apsva.us,Substitutes,Staff,Staff
Jennifer,Gonzaga,E31636,jennifer.gonzaga@apsva.us,Substitutes,Staff,Staff
Jennifer,Midberry,E31638,jennifer.midberry@apsva.us,Substitutes,Staff,Staff
Travis,Jackson,E31643,travis.jackson@apsva.us,Information Services,Technical Staff,Staff
Abraham,Sharif,E31645,abraham.sharif@apsva.us,Food Services,Food Services Staff,Food Services Staff
Alize,Carrillo,E31650,alize.carrilo@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Azza,Abdelhakam,E31661,azza.abdelhakam@apsva.us,Substitutes,Staff,Staff
Jose,Janampa Huaman,E31666,jose.janampahuaman@apsva.us,Substitutes,Staff,Staff
Yassmina,Hassoun,E31672,yassmina.hassoun@apsva.us,Substitutes,Staff,Staff
Taylor,Ragano,E31673,taylor.ragano@apsva.us,Substitutes,Staff,Staff
Maria,Cruz De Philipp,E31679,maria.cruzdephilipp@apsva.us,Food Services,Staff,Staff
La'Shea,Thomas,E31681,lashea.thomas@apsva.us,"Adult Education, Adult Education","Staff, Staff",Staff
Maria,Orellana Peralta,E31692,maria.orellana2@apsva.us,Food Services,Staff,Staff
Larissa,Vassilkova,E31695,larissa.vassilkova@apsva.us,Adult Education,Staff,Staff
Shavon,Moore,E31696,shavon.moore@apsva.us,Special Education,Staff,Staff
Begashaw,Legese,E31702,begashaw.legese@apsva.us,Transportation,Bus Driver,Transportation Staff
Anna,Eckert,E31709,anna.eckert@apsva.us,Substitutes,Staff,Staff
Rosy,Sahoo,E31713,rosy.sahoo@apsva.us,Substitutes,Staff,Staff
Allison,Arnold,E31714,allison.arnold@apsva.us,Substitutes,Staff,Staff
Julia,Meadows,E31718,julia.meadows@apsva.us,Substitutes,Staff,Staff
Carrie,Cooper,E31721,carrie.cooper@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Nathalie,Reyes,E31724,nathalie.reyes@apsva.us,Extended Day,Clerical Specialist,Staff
Heba,Hamoda,E31725,heba.hamoda@apsva.us,Substitutes,Substitute,Staff
Ulysses,Smith,E31728,ulysses.smith@apsva.us,Substitutes,Staff,Staff
Jennifer,Loayes Delgado,E31736,jennifer.delgado@apsva.us,Substitutes,Staff,Staff
Samira,Kaidi,E31738,samira.kaidi@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Assia,Marouki,E31739,assia.marouki@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Taiwo,Owens,E31740,taiwo.owens@apsva.us,Substitutes,Staff,Staff
Shakeya,Woodson,E31746,shakeya.woodson@apsva.us,Substitutes,Substitute,Staff
Trisha,Madden,E31747,trisha.madden@apsva.us,Substitutes,Staff,Staff
Matthew,Renaldi,E31749,matthew.renaldi@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Faiza,Moutamtia,E31750,faiza.moutamtia@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Michael,Elliot,E31753,michael.elliot@apsva.us,Substitutes,Staff,Staff
Rachida,Elaissaoui,E31760,rachida.elaissaoui@apsva.us,Extended Day Substitutes,Staff,Staff
Windell,Dela Rosa Smith,E31761,windell.delarosa@apsva.us,Substitutes,Staff,Staff
Juan,Gonzalez,E31766,juan.gonzalez@apsva.us,Substitutes,Staff,Staff
Daniel,Grahn,E31768,daniel.grahn@apsva.us,Substitutes,Staff,Staff
George,Obiero,E31772,george.obiero@apsva.us,Extended Day Substitutes,Staff,Staff
Ngozi,Urama,E31773,ngozi.urama@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Matthew,Harker,E31774,matthew.harker@apsva.us,Aquatics,Staff,Staff
Naima,Riad,E31775,naima.riad@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Sofia,Martinez-Gonzalez,E31776,sofia.martnez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Hala,Mohamed,E31777,hala.mohamed@apsva.us,Food Services,Staff,Staff
Amanda,Seas Paniagua,E31778,amanda.seaspaniagua@apsva.us,Food Services,Staff,Staff
Keyonia,Blakeney,E31783,keyonia.blakeney@apsva.us,Office of Personnel,Administrative Specialist,Staff
Pourush,Halamakki Gopal Naik,E31801,pourush.gopal2@apsva.us,Office of Personnel,Analyst,Staff
Joyce,Taylor,E31803,joyce.taylor@apsva.us,"Substitutes, Special Education","Staff, Staff",Staff
Katherine,Pernia,E31808,katherine.pernia@apsva.us,Adult Education,Staff,Staff
Biruk,Ezneh,E31809,biruk.ezneh@apsva.us,Transportation,Bus Driver,Transportation Staff
Hisham,Ali,E31810,hisham.ali@apsva.us,Transportation,Bus Driver,Transportation Staff
Solomon,Kibret,E31811,solomon.kibret@apsva.us,Transportation,Bus Driver,Transportation Staff
Mybinh,Thai,E31812,mybinh.thai@apsva.us,Food Services,Staff,Staff
Vladyslav,Stegniy,E31813,vladyslav.stegniy@apsva.us,REEP Program,Staff,Staff
Leslie,Tierstein,E31815,leslie.tierstein@apsva.us,"Substitutes, Adult Education","Staff, Staff",Staff
Rona,Cox,E3182,rona.cox@apsva.us,Adult Education,Administrative Assistant,Staff
Alexander,Giglio,E31820,alexander.giglio@apsva.us,Food Services,Food Services Staff,Food Services Staff
Lottie,Ligon-Thomas,E31822,lottie.ligonthomas@apsva.us,"Adult Education, Adult Education","Staff, Staff",Staff
Cinthya,Jaldin,E31823,cinthya.jaldin@apsva.us,"Extended Day Substitutes, Special Education","Staff, Staff",Staff
Sahar,Hakimi,E31824,sahar.hakimi@apsva.us,"Extended Day, Division of Instruction","Staff, Staff",Staff
Gloria,Murphy,E31833,gloria.murphy@apsva.us,Transportation,Bus Attendant,Transportation Staff
London,Barnes,E31840,london.barnes@apsva.us,Substitutes,Staff,Staff
Alisha,Potempa,E31841,alisha.potempa@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Zenobia,Salvatierra Valencia,E31842,zenobia.salvatierra@apsva.us,Food Services,Staff,Staff
Jessica,Castillo,E31844,jessica.castillo@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Ariana,Quesada Navarro,E31845,ariana.quesada@apsva.us,Substitutes,Staff,Staff
Nazia,Masrat,E31846,nazia.masrat@apsva.us,Substitutes,Staff,Staff
Naiya,Brooks,E31847,naiya.brooks@apsva.us,Substitutes,Staff,Staff
Rena,Taylor,E31848,rena.taylor@apsva.us,Administrative Services,Staff,Staff
Juana,Torres,E31849,juana.torres@apsva.us,"Extended Day, Food Services","Staff, Staff",Staff
Sakina,Salehi,E31852,sakina.salehi@apsva.us,Extended Day,Staff,Staff
Yancarla,Delgadillo Herrera,E31853,yancarla.delgadillo@apsva.us,"Extended Day Substitutes, Substitutes","Staff, Substitute",Staff
Andrew,Prensky,E31858,andrew.prensky@apsva.us,Substitutes,Substitute,Staff
Nilufar,Yeasmin,E31866,nilufar.yeasmin@apsva.us,Extended Day Substitutes,Staff,Staff
Lucretia,Kebreau,E31876,lucretia.kebreau@apsva.us,"Special Education, Division of Instruction","Staff, Staff",Staff
Nourhan,Hamad,E31878,nourhan.hamad@apsva.us,"Substitutes, Extended Day Substitutes","Staff, Staff",Staff
Mhd,Alhamoui,E31883,mhd.alhamoui@apsva.us,Substitutes,Staff,Staff
Katherine,Ryan,E31884,katherine.ryan@apsva.us,Substitutes,Staff,Staff
Benjamin,Doyle,E31894,benjamin.doyle@apsva.us,Aquatics,Staff,Staff
Cameron,Butcher,E31895,cameron.butcher@apsva.us,Aquatics,Staff,Staff
Darlene,Hammond,E31897,darlene.hammond@apsva.us,"Food Services, Transportation","Staff, Bus Attendant",Transportation Staff
Lemlem,Misghana,E31899,lemlem.misghana@apsva.us,Transportation,Bus Attendant,Transportation Staff
Abebu,Netere,E31901,abebu.netere@apsva.us,Transportation,Bus Attendant,Transportation Staff
Rehana,Rahman,E31902,rehana.rahman@apsva.us,Food Services,Staff,Staff
Shahnaz,Parvin,E31903,shahnaz.parvin@apsva.us,Food Services,Staff,Staff
John,Brandt,E31904,john.brandt@apsva.us,Facilities and Operations,Staff,Staff
Ruth,Rosales Herrera,E31912,ruth.rosalesherrera@apsva.us,Extended Day,Staff,Staff
Katherine,Claggett,E31914,katherine.claggett@apsva.us,Substitutes,Substitute,Staff
Harold,Burch,E31916,harold.burch@apsva.us,Substitutes,Substitute,Staff
Nene,Sangare,E31917,nene.sangare@apsva.us,Substitutes,Substitute,Staff
Melissa,Haberle,E31919,melissa.haberle@apsva.us,Substitutes,Staff,Staff
Christian,Cobbins,E31921,christian.cobbins@apsva.us,Information Services,Technical Staff,Staff
Joanna,Carreto,E31923,joanna.carreto@apsva.us,Information Services,Technical Staff,Staff
Maria-Elvira,Luna Escudero Alie,E31939,mariaelvira.luna@apsva.us,Adult Education,Staff,Staff
Zaid,Tesfaye,E31943,zaid.tesfaye@apsva.us,Food Services,Staff,Staff
Sandra,Quiroga Richter,E31949,sandra.quiro@apsva.us,Adult Education,Staff,Staff
Bryan,Graitge,E3195,bryan.graitge@apsva.us,Information Services,Technical Staff,Staff
Jennifer,Young,E31951,jennifer.young2@apsva.us,Substitutes,Staff,Staff
Colleen,Chambers,E31956,colleen.chambers@apsva.us,Substitutes,Staff,Staff
Asma,Syeda,E31964,asma.syeda@apsva.us,Extended Day,Staff,Staff
Donna,Jones-Kwarteng,E31966,donna.joneskwarteng@apsva.us,Transportation,Clerical Specialist,Staff
Delores,Shirley,E31968,delores.shirley@apsva.us,"Adult Education, Substitutes","Staff, Staff",Staff
Chelsey,Newmyer,E31970,chelsey.hochmuth@apsva.us,Adult Education,Staff,Staff
Fredy,Ramirez,E31978,fredy.ramirez@apsva.us,Substitutes,Substitute,Staff
Dayara,Viveros Leon,E31981,dayara.viverosleon@apsva.us,Transportation,Clerical Specialist,Staff
Eden,Weldemariam,E31982,eden.weldemariam@apsva.us,Food Services,Staff,Staff
Angela,Rosero Rodriguez,E31983,angela.rosero@apsva.us,Adult Education,Staff,Staff
Elizabeth,Ryan,E31986,elizabeth.ryan2@apsva.us,Adult Education,Staff,Staff
Christina,Kaufmann,E31989,christina.kaufmann@apsva.us,REEP Program,Staff,Staff
Chatia,Moore,E31990,chatia.moore@apsva.us,Office of Personnel,Supervisor,Staff
Alexander,Firster,E31993,alexander.firster@apsva.us,Division of Instruction,Staff,Staff
Rene,Marquina,E31995,rene.marquina@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Francisco,Duran,E31996,francisco.duran@apsva.us,Office of Personnel,Superintendent,Staff
Brian,Stockton,E31997,brian.stockton@apsva.us,Office of the Superintendent,Chief of Staff,Administrative Staff
Erin,Tokajer,E31999,erin.tokajer@apsva.us,Special Education,Teacher,Instructional Staff
Bobbie,Hanes,E32003,bobbie.hanes@apsva.us,Student Services,Staff,Staff
Margarita,Zwisler,E32010,margarita.zwisler2@apsva.us,Student Services,Teacher,Instructional Staff
Shelley,Boyd,E32015,shelley.boyd@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Amy,Cannava,E32017,amy.cannava@apsva.us,Student Services,Teacher,Instructional Staff
Pamela,Pipes,E32021,pamela.pipes@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Courtney,Armstrong,E32022,courtney.armstrong@apsva.us,Special Education,Teacher,Instructional Staff
Katherine,Haynes,E32023,katherine.haynes@apsva.us,Substitutes,Staff,Staff
Steven,Jones,E32035,steven.jones@apsva.us,Division of Instruction,Coordinator,Staff
Sarah,Cruz,E32041,sarah.cruz@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Delia,Dowd,E32043,delia.dowd@apsva.us,Student Services,Teacher,Instructional Staff
Amy,Nemeth,E32051,amy.nemeth@apsva.us,Student Services,Teacher,Instructional Staff
Carol,Burka,E32055,carol.burka2@apsva.us,Student Services,Teacher,Instructional Staff
Julianne,Maston,E32058,julianne.maston@apsva.us,Division of Instruction,Teacher,Instructional Staff
Siew,Lee,E32061,siew.lee@apsva.us,Substitutes,Staff,Staff
Christine,Coughlin,E32067,christine.coughlin@apsva.us,Student Services,Staff,Staff
Hind,Ghazlani,E32079,hind.ghazlani@apsva.us,Substitutes,Staff,Staff
Della,Hinn,E32097,della.hinn@apsva.us,Special Education,Teacher,Instructional Staff
Kimberly,Nolasco,E32099,kimberly.nolasco@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Shannon,Joseph,E32101,shannon.joseph@apsva.us,Student Services,Teacher,Instructional Staff
Nathalie,Toomey,E32109,nathalie.toomey@apsva.us,Special Education,Teacher,Instructional Staff
Gretchen,Mills,E32117,gretchen.mills@apsva.us,Extended Day,Staff,Staff
Valerie,Prince,E32118,valerie.prince@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Kaeleigh,Green,E32120,kaeleigh.green@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Jomaris,Correa Mercado,E32132,jomaris.correa@apsva.us,Student Services,Staff,Staff
Graciela,Rosemblat,E32137,graciela.rosemblat@apsva.us,Intake Center,Specialist,Staff
Catherine,Lyons,E32139,catherine.lyons@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Molly,Hague-Ismayilov,E32145,molly.hagueismayilov@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Tara,Newton,E32151,tara.newton@apsva.us,Special Education,Teacher,Instructional Staff
Brianna,Johnson,E32157,brianna.johnson@apsva.us,Substitutes,Staff,Staff
Richard,Hunter,E32158,richard.hunter@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Kelly,Simcik,E32159,kelly.simcik@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Hala,Abdou,E32161,hala.abdou@apsva.us,Substitutes,Staff,Staff
Zain,Hassan,E32164,zain.hassan@apsva.us,Substitutes,Staff,Staff
Michael,Dertke,E32166,michael.dertke@apsva.us,Aquatics,Staff,Staff
Mackenzie,Mangan,E32167,mackenzie.mangan@apsva.us,Aquatics,Staff,Staff
Christine,Jones,E32174,christine.jones@apsva.us,Special Education,Teacher,Instructional Staff
Sean,Lukas,E32176,sean.lukas@apsva.us,Aquatics,Staff,Staff
Aysia,Meyers,E32177,aysia.meyers@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Taylor,Jones,E32185,taylor.jones@apsva.us,Substitutes,Staff,Staff
Joseph,Johnson,E32187,joseph.johnson@apsva.us,Information Services,Technical Staff,Staff
Taj,Fewell,E32188,taj.fewell@apsva.us,Aquatics,Staff,Staff
Eva,Widhalm,E32194,eva.widhalm@apsva.us,Adult Education,Staff,Staff
Sharon,Williams,E32197,sharon.williams2@apsva.us,Office of Personnel,EAP Staff,Staff
Tammie,Lim,E32199,tammie.lim@apsva.us,Adult Education,Staff,Staff
Rihab,Mohamed,E32203,rihab.mohamed@apsva.us,Substitutes,Staff,Staff
Grayson,Dewitt,E32206,grayson.dewitt@apsva.us,Aquatics,Staff,Staff
Sarah,Hallinan,E32207,sarah.hallinan@apsva.us,Aquatics,Staff,Staff
Melaney,Mackin,E32209,melaney.mackin@apsva.us,Administrative Services,Principal,Administrative Staff
Annabel,Rotchford,E32214,annabel.rotchford@apsva.us,Aquatics,Staff,Staff
Lisa,Eckerson,E32228,lisa.eckerson@apsva.us,Division of Instruction,Administrative Assistant,Staff
Nina,Saunders,E32232,nina.saunders@apsva.us,Office of Personnel,EAP Staff,Staff
Gabriela,Enache Sandoz,E32233,gabriela.sandoz@apsva.us,Office of Finance,Analyst,Staff
Dawn,Morgan,E32234,dawn.morgan@apsva.us,Substitutes,Staff,Staff
Ashley,Hidalgo,E32243,ashley.hidalgo@apsva.us,Administrative Services,Staff,Staff
Candice,Barnes,E32244,candice.barnes@apsva.us,Extended Day Substitutes,Staff,Staff
Britney,Olanrewaju,E32260,britney.olanrewaju@apsva.us,Office of Personnel,Coordinator,Administrative Staff
David,Priddy,E32265,david.priddy@apsva.us,Office of the School Board,School Board Member,Staff
Cristina,Diaz-Torres,E32266,cristina.diaztorres@apsva.us,Office of the School Board,School Board Member,Staff
Qing,Wang,E32268,qing.wang@apsva.us,Adult Education,Staff,Staff
Yosef,Boutakov,E32273,yosef.boutakov@apsva.us,Aquatics,Staff,Staff
Samantha,Reilly,E32277,samantha.reilly@apsva.us,Adult Education,Staff,Staff
Keely,Owendoff,E32280,keely.owendoff@apsva.us,Substitutes,Staff,Staff
David,Gerard,E32291,david.gerard@apsva.us,Aquatics,Staff,Staff
Gabriela,Choque Parraga,E32315,gabriela.choque@apsva.us,"Transportation, Food Services","Staff, Staff, Bus Driver",Staff
Julius,Wani,E32321,julius.wani@apsva.us,Transportation,"Bus Attendant, Staff",Staff
Yvonna,Harper,E32323,yvonna.harper@apsva.us,Substitutes,Staff,Staff
Reginald,Gardner,E32324,reginald.gardner@apsva.us,Administrative Services,Staff,Staff
Amber,Taylor,E32328,amber.taylor@apsva.us,Division of Instruction,Staff,Staff
Davonne,Douglas,E32338,davonne.douglas@apsva.us,Substitutes,Staff,Staff
Yoshimi,Muto,E32350,yoshimi.muto@apsva.us,Adult Education,Staff,Staff
Rula,Minawi,E32352,rula.minawi2@apsva.us,Substitutes,Staff,Staff
Andrew,Nguyen,E32355,andrew.nguyen@apsva.us,Substitutes,Staff,Staff
Hamdia,Farhan,E32357,hamdia.farhan@apsva.us,Extended Day,Staff,Staff
Elvira,Prince,E32361,elvira.prince@apsva.us,Substitutes,Staff,Staff
Andreia,Martins De Farias,E32362,andreia.farias@apsva.us,Substitutes,Substitute,Staff
Jakyra,Sweetenburg,E32363,jakyra.sweetenburg@apsva.us,Substitutes,Staff,Staff
Purevsuren,Urtnasan,E32365,purevsuren.urtnasan@apsva.us,Division of Instruction,Staff,Staff
Kristen,Van Sice,E32368,kristen.vansice@apsva.us,Substitutes,Staff,Staff
Kenyata,Swann,E32369,kenyata.swann@apsva.us,Substitutes,Staff,Staff
Woynshet,Asfaw,E32370,woynshet.asfaw@apsva.us,"Transportation, Food Services","Bus Attendant, Staff",Staff
Syeda,Mahmood,E32371,syeda.mahmood@apsva.us,Substitutes,Staff,Staff
Isaiah,Holman,E32374,isaiah.holman@apsva.us,Transportation,Bus Attendant,Transportation Staff
Tianka,Williams,E32378,tianka.williams@apsva.us,Transportation,Staff,Staff
Kleyry,Oyuela Corrales,E32379,kleyry.oyuela@apsva.us,Food Services,Staff,Staff
Timothy,Little,E32385,timothy.little@apsva.us,Adult Education,Staff,Staff
Heidi,Osorio Jaimes,E32390,heidi.jaimes@apsva.us,Extended Day,Staff,Staff
Taylor,Lawrence,E32391,taylor.lawrence2@apsva.us,Extended Day,Staff,Staff
Dianna,Collins,E32392,dianna.collins@apsva.us,Extended Day,Staff,Staff
Sarah,Halverson,E32394,sarah.halverson@apsva.us,Substitutes,Staff,Staff
Daniel,Martinez Moreno,E32396,daniel.martinez@apsva.us,Substitutes,Staff,Staff
Charl,Long,E32407,charl.long@apsva.us,Extended Day,Staff,Staff
Denise,Green Hardy,E32409,denise.hardygreen@apsva.us,Extended Day Substitutes,Staff,Staff
Lee,Kontibon,E32426,lee.kontibon@apsva.us,"Substitutes, Extended Day Substitutes","Substitute, Staff",Staff
Margaret,Wolff,E32429,margaret.wolff@apsva.us,Substitutes,Staff,Staff
Charlotte,Davis,E32440,charlotte.davis@apsva.us,Aquatics,Staff,Staff
Mason,Starr,E32441,mason.starr@apsva.us,Aquatics,Staff,Staff
Luz,Bustillos,E32443,luz.bustillos@apsva.us,Substitutes,Staff,Staff
Deborah,Ryan,E32445,deborah.ryan@apsva.us,Substitutes,Staff,Staff
Natalie,Hodgkiss,E32446,natalie.hodgkiss2@apsva.us,Substitutes,Staff,Staff
Huili,Xie,E32454,huili.xie@apsva.us,Relief Custodians,Custodian,Facilities Staff
Samantha,Lincoln,E32456,samantha.lincoln@apsva.us,Aquatics,Staff,Staff
Yi-Chia,Lin,E32459,yichia.lin@apsva.us,Substitutes,Staff,Staff
Andrea,Mendoza,E32463,andrea.mendoza@apsva.us,Division of Instruction,Teacher,Instructional Staff
Ashley,Rehr,E32467,ashley.rehr@apsva.us,Substitutes,Staff,Staff
Alem,Yohannes,E32472,alem.yohannes@apsva.us,Transportation,Bus Attendant,Transportation Staff
Gisella,Rodriguez Angulo,E32474,gisella.rodriguez@apsva.us,Food Services,Staff,Staff
Jessica,Osher,E32479,jessica.osher2@apsva.us,Substitutes,Staff,Staff
Vladimir,Darakchiev,E32481,vladimir.darakchiev@apsva.us,Aquatics,Staff,Staff
Danielle,Barrera,E32483,danielle.barrera@apsva.us,Food Services,Food Services Staff,Food Services Staff
Nathan,Bynum,E32484,nathan.bynum@apsva.us,Substitutes,Staff,Staff
Wisam,Dodo,E32485,wisam.dodo@apsva.us,Food Services,Staff,Staff
Charles,Moore,E32488,charles.moore@apsva.us,Substitutes,Staff,Staff
Michelle,Stitt,E32490,michelle.stitt2@apsva.us,Student Services,Staff,Staff
Ayan,Abdinor,E32491,ayan.abdinor@apsva.us,Substitutes,Substitute,Staff
Mahanoor,Ruma,E32493,mahanoor.ruma@apsva.us,Food Services,Staff,Staff
Meghan,Chamness,E32494,meghan.chamness@apsva.us,Substitutes,Staff,Staff
Kashaya,Miller,E32495,kashaya.miller@apsva.us,Substitutes,Staff,Staff
Aytaj,Gasimova,E32497,aytaj.gasimova@apsva.us,Substitutes,Staff,Staff
Lisa,Solinger,E32498,lisa.solinger@apsva.us,Substitutes,Staff,Staff
Brandon,Smith,E32499,brandon.smith2@apsva.us,Substitutes,Substitute,Staff
Cheryl,Carter,E32501,cheryl.carter@apsva.us,Transportation,Bus Driver,Transportation Staff
Ayesha,Niazi,E32504,ayesha.niazi@apsva.us,Substitutes,Staff,Staff
Ambra,Xhepa,E32505,ambra.xhepa@apsva.us,Substitutes,Staff,Staff
Madeline,Eisman,E32510,madeline.eisman@apsva.us,Substitutes,Staff,Staff
Micah,Gustafson,E32511,micah.gustafson@apsva.us,Substitutes,Staff,Staff
Christa,Toler,E32512,christa.toler@apsva.us,"Special Education, Substitutes","Staff, Substitute",Staff
Ana,Castegnaro Velasquez,E32517,ana.castegnaro@apsva.us,Substitutes,Staff,Staff
Stephen,McCartney,E32518,stephen.mccartney@apsva.us,Aquatics,Staff,Staff
James,Do,E32520,james.do@apsva.us,Information Services,Staff,Staff
Jared,Greene,E32521,jared.greene@apsva.us,Information Services,Staff,Staff
Daniel,Blakeney-Moore,E32522,daniel.blakeneymoore@apsva.us,Information Services,Staff,Staff
Sean,Moore,E32523,sean.moore@apsva.us,Aquatics,Staff,Staff
Jay,Patel,E32524,jay.patel@apsva.us,Information Services,Staff,Staff
Tate,Mullin,E32526,tate.mullin@apsva.us,Aquatics,Staff,Staff
Ryan,Phillips,E32527,ryan.phillips@apsva.us,Aquatics,Staff,Staff
Andrew,Robinson,E32533,andrew.robinson@apsva.us,School and Community Relations,Specialist,Staff
Aydee,Ferrufino Veizaga,E32540,aydee.ferrufino@apsva.us,Substitutes,Staff,Staff
Marian,Graham,E32543,marian.graham@apsva.us,"Aquatics, REEP Program, Adult Education","Staff, Staff, Staff",Staff
Ella,McHugh,E32547,ella.mchugh@apsva.us,Aquatics,Staff,Staff
Carolyn,Samson,E32554,carolyn.samson@apsva.us,Special Education,Teacher,Instructional Staff
Jacklyn,Rosen,E32561,jacklyn.rosen@apsva.us,Special Education,Staff,Staff
Danielle,Dougherty,E32567,danielle.dougherty@apsva.us,Student Services,Teacher,Instructional Staff
Shanta,Price,E32569,shanta.price@apsva.us,Special Education,Teacher,Instructional Staff
Kimberly,Wilson,E32572,kimberly.wilson@apsva.us,Student Services,Teacher,Instructional Staff
John,Mayo,E32577,john.mayo@apsva.us,Office of the Superintendent,CHIEF OPERATING OFFICER,Administrative Staff
Juliana,Powell,E32580,juliana.powell@apsva.us,Aquatics,Staff,Staff
Jake,O'Brien,E32581,jake.obrien@apsva.us,Aquatics,Staff,Staff
Christian,Hepler,E32582,christian.hepler@apsva.us,Aquatics,Staff,Staff
Keiran,Gibbs,E32583,keiran.gibbs@apsva.us,Aquatics,Staff,Staff
Isabella,Zimering,E32584,isabella.zimering@apsva.us,Adult Education,Staff,Staff
Michele,Tubbs,E32596,michele.tubbs@apsva.us,Aquatics,Staff,Staff
Patricia,Martinez-Lezama,E32605,patricia.martinez@apsva.us,Student Services,Teacher,Instructional Staff
Anna,Tush,E32606,anna.tush@apsva.us,Student Services,Teacher,Instructional Staff
Kimberly,Cocuzzi,E32607,kimberly.cocuzzi@apsva.us,Office of Personnel,Staff,Staff
Antonio,Thompson,E32613,antonio.thompson@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Joseph,Sullivan,E32614,joseph.sullivan@apsva.us,Substitutes,Staff,Staff
Melissa,Szymanski,E32621,melissa.szymanski@apsva.us,APS,Teacher,Instructional Staff
Tihitina,Yimer,E32622,tihitina.yimer@apsva.us,Food Services,Staff,Staff
Lauren,Edwards,E32623,lauren.edwards@apsva.us,Aquatics,Staff,Staff
Nevil,Khurana,E32627,nevil.khurana@apsva.us,Aquatics,Staff,Staff
David,Sandall,E32628,david.sandall@apsva.us,Aquatics,Staff,Staff
Timothy,Flynn,E32631,timothy.flynn@apsva.us,Special Education,Teacher,Instructional Staff
Isabella,Romance,E32634,isabella.romance@apsva.us,Student Services,Staff,Staff
Samuel,Silverman,E32635,samuel.silverman@apsva.us,Special Education,Teacher,Instructional Staff
Mohammed,Ahmed,E32641,mohammed.ahmed@apsva.us,Food Services,Staff,Staff
Naser,Chowdhury,E32642,naser.chowdhury@apsva.us,Food Services,Staff,Staff
Cintia,Branez,E32643,cintia.branez@apsva.us,Food Services,Staff,Staff
Samantha,Gift-Attoh,E32645,samantha.giftattoh@apsva.us,Student Services,Teacher,Instructional Staff
Erdem,Ganbat,E32654,erdem.ganbat@apsva.us,Aquatics,Staff,Staff
Nermine,Ben Hammouda,E32658,nermine.benhammouda@apsva.us,Aquatics,Staff,Staff
Maite,Gutierrez Rodriguez,E32659,maite.gutierrez@apsva.us,Aquatics,Staff,Staff
Francis,Reynolds,E32660,francis.reynolds@apsva.us,Aquatics,Staff,Staff
Terri,Murphy,E32661,terri.murphy@apsva.us,Division of Instruction,Director,Administrative Staff
Mia,Ewell,E32662,mia.ewell@apsva.us,Division of Instruction,Staff,Staff
Ana,Dheming,E32663,ana.dheming@apsva.us,Food Services,Staff,Staff
Tsigereda,Woldemichael,E32664,t.woldemichael@apsva.us,Food Services,Staff,Staff
Courtney,DeLay,E32667,courtney.delay@apsva.us,Special Education,Teacher,Instructional Staff
Vicki,Lyons,E3267,vicki.lyons@apsva.us,Special Education,Teacher,Instructional Staff
Fumie,Maxstadt,E32676,fumie.maxstadt@apsva.us,Substitutes,Staff,Staff
Shonda,Nelson,E32677,shonda.nelson@apsva.us,Special Education,Teacher,Instructional Staff
Linda,Nobrega,E32681,linda.nobrega@apsva.us,Aquatics,Staff,Staff
Idalia,Salmeron Velasquez,E32682,idalia.salmeron@apsva.us,Food Services,Staff,Staff
Gregory,Zubler,E32687,gregory.zubler@apsva.us,Aquatics,Staff,Staff
Max,Eichers,E32688,max.eichers@apsva.us,Aquatics,Staff,Staff
Margaret,Alter,E32689,margaret.alter@apsva.us,Aquatics,Staff,Staff
Lynda,Maguire,E32690,lynda.maguire@apsva.us,Aquatics,Staff,Staff
Emma,Folk,E32692,emma.folk@apsva.us,Special Education,Teacher,Instructional Staff
Gregory,Stickeler,E32696,gregory.stickeler@apsva.us,APS,Teacher,Instructional Staff
Michelle,Shimizu,E32698,michelle.shimizu@apsva.us,Student Services,Teacher,Instructional Staff
Stella,Martinez,E3270,stella.martinez@apsva.us,Intake Center,Specialist,Staff
Daniel,Torg,E32714,daniel.torg@apsva.us,Aquatics,Staff,Staff
Ariadna,Hernandez Blanco,E32715,ariadna.hernandez@apsva.us,Aquatics,Staff,Staff
Daniel,Rozen,E32747,daniel.rozen@apsva.us,Aquatics,Staff,Staff
Melissa,Freimark,E32748,melissa.freimark@apsva.us,Aquatics,Staff,Staff
Nadia,Logan,E32749,nadia.logan@apsva.us,Food Services,Staff,Staff
William,Aviles,E32750,william.aviles@apsva.us,Food Services,Specialist,Staff
Yanira,Aslam-Nina,E32751,yanira.aslamnina@apsva.us,Substitutes,Staff,Staff
Khurram,Jehangiri,E32752,khurram.jehangiri@apsva.us,Substitutes,Substitute,Staff
Amy,Jackson,E32756,amy.jackson@apsva.us,Division of Instruction,Supervisor,Administrative Staff
Leslie,Entwistle,E32757,leslie.entwistle@apsva.us,Aquatics,Staff,Staff
Ehvyn,Wilson,E32761,ehvyn.wilson@apsva.us,Student Services,Teacher,Instructional Staff
Jason,Ottley,E32765,jason.ottley@apsva.us,Office of the Superintendent,CHIEF DEI OFFICER,Administrative Staff
Bich,Nguyen,E32766,bich.nguyen@apsva.us,Substitutes,Staff,Staff
Kelly,Lazarte,E32767,kelly.lazarte@apsva.us,Substitutes,Staff,Staff
Danyelle,Duvall,E32782,danyelle.duvall@apsva.us,"Substitutes, Adult Education","Staff, Staff",Staff
Kareem,Ishmail,E32783,kareem.ishmail@apsva.us,Student Services,Staff,Staff
Evelyn,Soto,E32784,evelyn.soto@apsva.us,Extended Day,Staff,Staff
Ahmed,Bakhit,E32785,ahmed.bakhit@apsva.us,Extended Day Substitutes,Staff,Staff
Tracie,Parker,E32794,tracie.parker@apsva.us,Special Education,Teacher,Instructional Staff
Sharrin,Saintil,E32795,sharrin.saintil@apsva.us,Administrative Services,Coordinator,Administrative Staff
Becca,Reid,E32800,becca.reid@apsva.us,Special Education,Teacher,Instructional Staff
Nadia,Huaracha,E32803,nadia.huaracha@apsva.us,Extended Day Substitutes,Staff,Staff
Rouane,Itani,E32805,rouane.itani@apsva.us,Substitutes,Staff,Staff
Rachel,Simmons,E32806,rachel.simmons@apsva.us,Substitutes,Staff,Staff
Ivan,Maldonado-Maldonado,E32809,ivan.maldonado@apsva.us,Substitutes,Staff,Staff
Damon,Corprew,E32816,damon.corprew@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Isabelle,Kingsley,E32817,isabelle.kingsley@apsva.us,Substitutes,Staff,Staff
Julia,Dragun,E32818,julia.dragun@apsva.us,Aquatics,Staff,Staff
Monalisa,Moore,E32819,monalisa.moore@apsva.us,Extended Day,Staff,Staff
Hadley,Lopez-Solis,E32820,hadley.lopezsolis@apsva.us,Extended Day,Staff,Staff
Deka,Omar-Chirdon,E32821,deka.omarchirdon@apsva.us,Substitutes,Substitute,Staff
Nathan,Robson,E32822,nathan.robson@apsva.us,Extended Day,Staff,Staff
Roua,Abdalla,E32823,roua.abdalla@apsva.us,Extended Day,Staff,Staff
Candice,Umbel,E32824,candice.umbel@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Toshiko,Parrish,E32825,toshiko.parrish@apsva.us,Division of Instruction,Teacher,Instructional Staff
Andre,Williams Lewis,E32829,andre.williamslewis@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Maria,Rojas Rivas,E32832,maria.rojasrivas@apsva.us,Food Services,Staff,Staff
Malia,Washington,E32833,malia.washington@apsva.us,"Substitutes, Extended Day","Staff, Staff",Staff
Sabrina,Khaloua,E32834,sabrina.khaloua@apsva.us,Extended Day,Staff,Staff
Cydney,Caesar-Forde,E32836,cydney.caesarforde@apsva.us,Substitutes,Staff,Staff
Michael,Barnes,E32837,michael.barnes@apsva.us,Aquatics,Staff,Staff
Teneisha,Whitlock,E32846,teneisha.whitlock@apsva.us,Substitutes,Staff,Staff
Katia,Wakim,E32847,katia.wakim@apsva.us,Substitutes,Staff,Staff
Kathryn,Johnson,E32851,bailey.johnson@apsva.us,Student Services,Teacher,Instructional Staff
Leonardo,Fall,E32855,leonardo.fall@apsva.us,Aquatics,Staff,Staff
Christine,Smith,E32856,christine.smith@apsva.us,Office of the Superintendent,LEGAL COUNSEL,Administrative Staff
Creon,Spann,E32858,creon.spann@apsva.us,Substitutes,Staff,Staff
Jennifer,Wolff,E32859,jennifer.wolff@apsva.us,Special Education,Teacher,Instructional Staff
Danielle,Lieneman,E32860,danielle.lieneman@apsva.us,Substitutes,Staff,Staff
Angela,DiBenigno,E32861,angela.dibenigno@apsva.us,Substitutes,Staff,Staff
Sandra,Hickey,E32862,sandra.hickey@apsva.us,Substitutes,Staff,Staff
Genesis,Bustillo Bonilla,E32863,genesis.bustillo@apsva.us,Information Services,Staff,Staff
Zaina,Kibiswa,E32868,zaina.kibiswa@apsva.us,Substitutes,Staff,Staff
Audrey,Ting,E32872,audrey.ting@apsva.us,Substitutes,Staff,Staff
Klen,Moore,E32873,klen.moore@apsva.us,Substitutes,Substitute,Staff
Brianna,Blakeney,E32876,brianna.blakeney@apsva.us,Substitutes,Staff,Staff
Marcos,Silva,E32877,marcos.silva@apsva.us,Substitutes,Staff,Staff
Sophia,Edgel,E32880,sophia.edgel@apsva.us,School and Community Relations,Staff,Staff
Shawayna,Jones,E32883,shawayna.jones@apsva.us,Extended Day,Staff,Staff
Destiny,Dykes,E32884,destiny.dykes@apsva.us,Substitutes,Staff,Staff
Ana,Paz Hernandez,E32885,ana.pazhernandez@apsva.us,Food Services,Staff,Staff
Nancy,Cruz Ortiz,E32888,nancy.cruzortiz@apsva.us,Food Services,Staff,Staff
Amarech,Gebremichael,E32891,amarech.gebremichael@apsva.us,Food Services,Staff,Staff
Ashley,Sanchez,E32893,ashley.sanchez@apsva.us,Extended Day Substitutes,Staff,Staff
Prianka,Akter,E32896,prianka.akter@apsva.us,Food Services,Staff,Staff
Michael,Heredia,E32897,michael.heredia@apsva.us,Aquatics,Staff,Staff
Ryan,York,E32901,ryan.york@apsva.us,Aquatics,Staff,Staff
Sumaya,Estrada Torres,E32902,sumaya.estradatorres@apsva.us,Food Services,Staff,Staff
Dylan,Mohan,E32908,dylan.mohan@apsva.us,Extended Day,Staff,Staff
Marina,Tarantino,E32910,marina.tarantino@apsva.us,Aquatics,Staff,Staff
Konan,Bokosse,E32911,konan.bokosse@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Amira,Damrani,E32914,amira.damrani@apsva.us,Substitutes,Staff,Staff
Carlette,Bryan,E3292,carlette.bryan@apsva.us,Special Education,Teacher,Instructional Staff
Jody,Katz,E32920,jody.katz@apsva.us,Aquatics,Staff,Staff
Kelly,Horan,E32923,kelly.horan@apsva.us,Aquatics,Staff,Staff
Mallory,Latimer,E32924,mallory.latimer@apsva.us,Substitutes,Staff,Staff
LeeAnn,Gedeon,E32925,leeann.gedeon@apsva.us,Substitutes,Staff,Staff
Zachary,Feidner,E32926,zachary.feidner@apsva.us,Substitutes,Staff,Staff
Sunita,Tamang,E32927,sunita.tamang@apsva.us,Extended Day,Staff,Staff
Juliana,Vadala,E32929,juliana.vadala@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Serpil,Rivas,E32930,serpil.rivas@apsva.us,Substitutes,Staff,Staff
Xavier,Friend,E32931,xavier.friend@apsva.us,Food Services,Staff,Staff
Summers,Henderson,E32932,summers.henderson@apsva.us,Substitutes,Substitute,Staff
Lacey,Schmeltzer,E32933,lacey.schmeltzer@apsva.us,Substitutes,Staff,Staff
Andrew,Eschen,E32934,andrew.eschen@apsva.us,Aquatics,Staff,Staff
Wendy,Andreen,E32936,wendy.andreen@apsva.us,Extended Day,Staff,Staff
Karen,Conlin,E32939,karen.conlin@apsva.us,Substitutes,Staff,Staff
Mikala,Shaffer,E32941,mikala.shaffer@apsva.us,Substitutes,Staff,Staff
Katherine,Kalis,E32942,katherine.kalis@apsva.us,Adult Education,Staff,Staff
Jacquelyn,Frend,E32943,jacquelyn.frend@apsva.us,Adult Education,Staff,Staff
Taisha,Estrada-Aponte,E32944,taisha.estradaaponte@apsva.us,Substitutes,Staff,Staff
Zachary,Firestine,E32945,zachary.firestine@apsva.us,Substitutes,Staff,Staff
Danny,Barnes,E32947,danny.barnes@apsva.us,Administrative Services,Staff,Staff
Loribeth,Bosserman,E32950,loribeth.bosserman@apsva.us,Special Education,Teacher,Instructional Staff
Davaadorj,Danzannyam,E32951,davaadorj.danzannyam@apsva.us,Extended Day,Staff,Staff
Cecilia,Escobar Rivera,E32952,cecilia.escobar@apsva.us,Extended Day,Staff,Staff
Daniel,Skoloda,E32953,daniel.skoloda@apsva.us,Extended Day,Staff,Staff
Justin,Rogers,E32960,justin.rogers@apsva.us,Substitutes,Staff,Staff
Kenneth,Golski,E32961,kenneth.golski@apsva.us,Office of the Superintendent,LEGAL COUNSEL,Administrative Staff
Andrew,Davie,E32962,andrew.davie@apsva.us,Adult Education,Staff,Staff
Natalie,Hummel,E32965,natalie.ellis@apsva.us,Substitutes,Staff,Staff
Jarron,McDavid,E32966,jarron.mcdavid@apsva.us,Substitutes,Substitute,Staff
Noah,Elmshaeuser,E32967,noah.elmshaeuser@apsva.us,Substitutes,Staff,Staff
Michael,Podesta,E32969,michael.podesta@apsva.us,Substitutes,Staff,Staff
Heather,Dishongh,E32970,heather.dishongh@apsva.us,Substitutes,Staff,Staff
Zion,Amero,E32974,zion.amero@apsva.us,Food Services,Staff,Staff
Marianna,Fotakos,E32975,marianna.fotakos@apsva.us,Substitutes,Staff,Staff
Deisy,Estevez,E32977,deisy.estevez@apsva.us,Substitutes,Staff,Staff
Rebecca,Yates,E32978,rebecca.yates@apsva.us,Aquatics,Staff,Staff
Angela,Walker,E32979,angela.walker@apsva.us,Substitutes,Staff,Staff
Cynthia,McCluskey,E32982,cynthia.mccluskey@apsva.us,Aquatics,Staff,Staff
Tirhas,Medin,E32983,tirhas.medin@apsva.us,Food Services,Staff,Staff
Randa,Ramadan,E32987,randa.ramadan@apsva.us,Substitutes,Staff,Staff
Litzy,Belteton-Gomez,E32988,litzy.beltetongomez@apsva.us,Extended Day,Staff,Staff
Melony,Iraheta Rojas,E32989,melony.irahetarojas@apsva.us,Extended Day,Staff,Staff
Leslie,Soto-Lopez,E32991,leslie.sotolopez@apsva.us,Extended Day,Staff,Staff
Charlbret,Polk Newton,E32992,charlbret.polknewton@apsva.us,Food Services,Staff,Staff
Chaymaa,Yassine,E32993,chaymaa.yassine@apsva.us,Substitutes,Staff,Staff
Hadia,Ahmed,E32994,hadia.ahmed@apsva.us,Substitutes,Substitute,Staff
Hristina,Ridgway,E32995,hristina.ridgway@apsva.us,Substitutes,Staff,Staff
Nancy,St. Clair,E32996,nancy.stclair@apsva.us,Substitutes,Staff,Staff
Shugoofa,Agha,E32997,shugoofa.agha@apsva.us,Extended Day,Staff,Staff
Marie,Moore,E32998,marie.moore@apsva.us,"Extended Day Substitutes, Food Services","Staff, Staff",Staff
Kerry,Britt,E33000,kerry.britt@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Ghaida,Elsher,E33002,ghaida.elsher@apsva.us,Substitutes,Staff,Staff
Amesha,Smith,E33005,amesha.smith@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Eyreal,Jones-Pratt,E33010,eyreal.jonespratt@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Marilyn,Ruiz Sanchez,E33012,marilyn.ruizsanchez@apsva.us,Food Services,Staff,Staff
Assia,Essaqui,E33013,assia.essaqui@apsva.us,Food Services,Staff,Staff
Noor,Azmat,E33016,noor.azmat@apsva.us,Substitutes,Substitute,Staff
Pamishia,Boyd,E33020,pamishia.boyd@apsva.us,Extended Day Substitutes,Staff,Staff
Jasmine,Fitzgerald,E33021,jasmine.fitzgerald@apsva.us,Extended Day,Staff,Staff
Nyara,Morris,E33022,nyara.morris@apsva.us,"Substitutes, Extended Day","Staff, Staff",Staff
Meaza,Sahlu,E33023,meaza.sahlu@apsva.us,Extended Day,Staff,Staff
Alexander,Herbst,E33027,alexander.herbst@apsva.us,Aquatics,Staff,Staff
Sydney,Purcell,E33028,sydney.purcell@apsva.us,Aquatics,Staff,Staff
Karen,Hannam,E33029,karen.hannam@apsva.us,Aquatics,Staff,Staff
Lonnis,Koontz,E33030,lonnis.koontz@apsva.us,Substitutes,Staff,Staff
Mary,Kalfatovic,E33033,mary.kalfatovic@apsva.us,REEP Program,Staff,Staff
Tracey,Newman,E33034,tracey.newman@apsva.us,"Food Services, Transportation","Bus Attendant, Staff",Staff
Nebiat,Habtemaraym,E33035,nebiat.habtemaraym@apsva.us,Transportation,Staff,Staff
Negussie,Babu,E33036,negussie.babu@apsva.us,Transportation,Bus Attendant,Transportation Staff
Maria,Rios,E33039,maria.rios@apsva.us,Extended Day,Staff,Staff
Ramisa,Imran,E33042,ramisa.imran@apsva.us,Extended Day,Staff,Staff
Liseth,Lopez Salas,E33043,liseth.lopezsalas@apsva.us,Extended Day,Staff,Staff
Madison,Moore,E33044,madison.moore@apsva.us,Extended Day,Staff,Staff
Dazia,Jackson,E33045,dazia.jackson@apsva.us,Extended Day,Staff,Staff
Oliver,Elliot,E33046,oliver.elliot@apsva.us,Extended Day,Staff,Staff
Lucas,Scaros,E33047,lucas.scaros@apsva.us,Substitutes,Staff,Staff
Michaela,Fox,E33049,michaela.fox@apsva.us,Substitutes,Staff,Staff
Joyce,Davis,E33050,joyce.davis@apsva.us,Substitutes,Staff,Staff
Jin,Gunter,E33051,jin.gunter@apsva.us,Substitutes,Staff,Staff
Hadir,Ali,E33052,hadir.ali@apsva.us,Substitutes,Staff,Staff
Fatema,Begum,E33057,fatema.begum@apsva.us,Food Services,Staff,Staff
Janice,O'Day,E33059,janice.oday@apsva.us,"Food Services, Substitutes","Staff, Staff",Staff
Ariane,Montero,E33060,ariane.montero@apsva.us,Extended Day,Staff,Staff
Simon,Alemu,E33062,simon.alemu@apsva.us,Extended Day,Staff,Staff
Kevin,Link,E33063,kevin.link@apsva.us,Extended Day,Staff,Staff
Saliha,Bahfid,E33065,saliha.bahfid@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kirill,Warren,E33066,kirill.warren@apsva.us,Extended Day,Staff,Staff
Tana,Robbins,E33069,tana.robbins@apsva.us,Substitutes,Staff,Staff
Maeve,Quigley,E33070,maeve.quigley@apsva.us,Substitutes,Staff,Staff
Roderick,Mitchener,E33071,roderick.mitchener@apsva.us,Substitutes,Staff,Staff
Miles,Taylor,E33072,miles.taylor@apsva.us,Substitutes,Substitute,Staff
Joanna,Nasser,E33073,joanna.nasser@apsva.us,Substitutes,Staff,Staff
Arianna,Castillo,E33074,arianna.castillo@apsva.us,Extended Day,Staff,Staff
Makhia,Hall,E33082,makhia.hall@apsva.us,Special Education,Staff,Staff
Maria,Medina,E33083,maria.medina2@apsva.us,Substitutes,Staff,Staff
Nathalie,Degraff,E33084,nathalie.degraff@apsva.us,Substitutes,Staff,Staff
Thomas,Williams,E33087,thomas.williams@apsva.us,Extended Day,Staff,Staff
Leah,Atwa,E33088,leah.atwa@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Natalie,Leggett,E33089,natalie.leggett2@apsva.us,Special Education,Teacher,Instructional Staff
Jahan,Puryear,E33090,jahan.puryear@apsva.us,Extended Day,Staff,Staff
Olivia,Flauss,E33091,olivia.flauss@apsva.us,Substitutes,Staff,Staff
Ross,Lott,E33092,ross.lott@apsva.us,Substitutes,Staff,Staff
Amber,Miller,E33093,amber.miller@apsva.us,Substitutes,Staff,Staff
Amy,Hjerstedt,E33094,amy.hjerstedt@apsva.us,Substitutes,Staff,Staff
Jordan,Vincent,E33095,jordan.vincent@apsva.us,Substitutes,Staff,Staff
Karisma,Williams,E33097,karisma.williams@apsva.us,Extended Day,Supervisor,Extended Day Staff
Matthew,Blasi,E33098,matthew.blasi@apsva.us,Substitutes,Staff,Staff
Vi,Chung,E33099,vi.chung@apsva.us,Transportation,Staff,Staff
Earnest,Hargrove,E33100,earnest.hargrove@apsva.us,Substitutes,Staff,Staff
Irene,Jones,E33105,irene.jones@apsva.us,Extended Day,Staff,Staff
Gretchen,Hickey,E33106,gretchen.hickey@apsva.us,Substitutes,Staff,Staff
Cherie,Hagooli,E33107,cherie.hagooli@apsva.us,Substitutes,Staff,Staff
Phoenix,Ayotte,E33109,phoenix.ayotte@apsva.us,Substitutes,Staff,Staff
Alexa,Rawl,E33116,alexa.rawl@apsva.us,Substitutes,Staff,Staff
Hiwot,Atona,E33118,hiwot.atona@apsva.us,Food Services,Staff,Staff
Jaleesa,Tilford,E33119,jaleesa.tilford@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Giselle,Soto C,E33121,giselle.soto@apsva.us,Extended Day,Staff,Staff
Alexander,Rosenberg,E33122,alexander.rosenberg@apsva.us,Extended Day,Staff,Staff
Finn,Loughney,E33123,finn.loughney@apsva.us,Extended Day,Staff,Staff
Binderiya,Enkhtur,E33124,binderiya.enkhtur@apsva.us,Extended Day,Staff,Staff
Feruz,Hussen,E33128,feruz.hussen@apsva.us,Food Services,Staff,Staff
Rolando,Perez Gonzales,E33129,rolando.perez@apsva.us,Food Services,Staff,Staff
Vincent,Marino,E33131,vincent.marino@apsva.us,Substitutes,Staff,Staff
Natalie,Petras,E33132,natalie.petras@apsva.us,Substitutes,Staff,Staff
Maria,Teran-Cabezas,E33133,maria.terancabezas@apsva.us,Substitutes,Staff,Staff
Marques,Wilson,E33134,marques.wilson@apsva.us,Information Services,Technical Staff,Staff
Rachel,Avenick,E33135,rachel.avenick@apsva.us,Substitutes,Staff,Staff
Gabriel,Freckmann,E33139,gabriel.freckmann@apsva.us,Special Education,Staff,Staff
Kathleen,Maconaughey,E33141,kathleen.maconaughey@apsva.us,Substitutes,Staff,Staff
Suzette,Risacher,E33142,suzette.risacher@apsva.us,Substitutes,Staff,Staff
Andrea,Woodward,E33143,andrea.woodward@apsva.us,Substitutes,Staff,Staff
Laura,Posada,E33145,laura.posada@apsva.us,"Substitutes, Administrative Services","Staff, Staff",Staff
Regina,Miller,E33146,regina.miller@apsva.us,Substitutes,Staff,Staff
Sharon,Gundrum,E33147,sharon.gundrum@apsva.us,Substitutes,Staff,Staff
Latrice,Luster,E33148,latrice.luster@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Nardos,Hailu,E33155,nardos.hailu@apsva.us,Extended Day Substitutes,Staff,Staff
Soraya,Giron De Leon,E33160,soraya.girondeleon@apsva.us,Special Education,Staff,Staff
Sierra,Howell,E33161,sierra.howell@apsva.us,Substitutes,Staff,Staff
Murrin,McClafferty,E33162,murrin.mcclafferty@apsva.us,Substitutes,Staff,Staff
Natalie,Hamm,E33164,natalie.hamm@apsva.us,Substitutes,Staff,Staff
Laura,Baracaldo Barragan,E33165,laura.baracaldo@apsva.us,Substitutes,Substitute,Staff
Kateri,Young,E33166,kateri.young@apsva.us,Substitutes,Staff,Staff
Crystal,Haskins,E33168,crystal.haskins@apsva.us,Extended Day,Staff,Staff
Ruth,Hailu,E33170,ruth.hailu@apsva.us,Extended Day,Staff,Staff
Josue,Cisneros,E33171,josue.cisneros@apsva.us,Extended Day,Staff,Staff
Kristin,Wajert,E33172,kristin.wajert@apsva.us,Substitutes,Staff,Staff
Hasna,El Houdaigui,E33173,hasna.elhoudaigui@apsva.us,Substitutes,Staff,Staff
Atsede,Emiru,E33175,atsede.emiru@apsva.us,Food Services,Staff,Staff
Shelia,Robertson,E33177,shelia.robertson@apsva.us,Division of Instruction,Staff,Staff
Mitzi,Dailey,E33178,mitzi.dailey@apsva.us,Substitutes,Staff,Staff
Kristin,Blackburn,E33179,kristin.blackburn@apsva.us,Substitutes,Staff,Staff
Linda,Morehead,E33180,linda.morehead@apsva.us,Extended Day Substitutes,Staff,Staff
Dana,Kappler,E33181,dana.kappler@apsva.us,REEP Program,Staff,Staff
Kelly,Warga,E33183,kelly.warga@apsva.us,Substitutes,Staff,Staff
Kelvina,Leonard,E33184,kelvina.leonard@apsva.us,Substitutes,Staff,Staff
Sandra,Foronda Lemos,E33185,sandra.forondalemos@apsva.us,Food Services,Food Services Staff,Food Services Staff
Lisa,Knapp,E33186,lisa.knapp@apsva.us,Substitutes,Staff,Staff
Michael,Brackman,E33187,michael.brackman@apsva.us,Substitutes,Staff,Staff
Mathew,Coles,E33188,mathew.coles2@apsva.us,Substitutes,Staff,Staff
Christyna,Haskins,E33189,christyna.haskins@apsva.us,Extended Day,Staff,Staff
Ebtisam,Mohamed,E33195,ebtisam.mohamed@apsva.us,Substitutes,Staff,Staff
Yessenia,Turrubiartes,E33196,yessenia.turrubiarte@apsva.us,Special Education,Staff,Staff
Munzer,Kalouda,E33200,munzer.kalouda@apsva.us,Substitutes,Staff,Staff
Angela,Holmes,E33201,angela.holmes@apsva.us,Substitutes,Staff,Staff
Fanaye,Gessesse,E33206,fanaye.gessesse@apsva.us,Special Education,Teacher,Instructional Staff
Henry,Tucker,E33207,henry.tucker@apsva.us,Substitutes,Staff,Staff
Stephanie,Powell,E33208,stephanie.powell@apsva.us,Substitutes,Staff,Staff
Briiane,Miller,E33210,briiane.miller@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Isabel,Levenson,E33211,isabel.levenson@apsva.us,Aquatics,Staff,Staff
Saima,Akther,E33212,saima.akther@apsva.us,Food Services,Staff,Staff
AnTon,Wallace,E33213,anton.wallace@apsva.us,Extended Day,Staff,Staff
Sada,Legas,E33214,sada.legas@apsva.us,Food Services,Staff,Staff
Shanta,Humphrey,E33215,shanta.humphrey@apsva.us,Extended Day,Staff,Staff
Cianna,Fogle,E33216,cianna.fogle@apsva.us,Extended Day,Staff,Staff
German,Contreras,E33218,german.contreras@apsva.us,Substitutes,Staff,Staff
Claire,Rinaldi,E33219,claire.rinaldi@apsva.us,Substitutes,Staff,Staff
Houda,Khalil,E33220,houda.khalil@apsva.us,Substitutes,Staff,Staff
Denisse,Mendez,E33221,denisse.mendez@apsva.us,Substitutes,Staff,Staff
Chia,Camphor,E33222,chia.camphor@apsva.us,Substitutes,Staff,Staff
Andrew,McLeod,E33223,andrew.mcleod@apsva.us,Substitutes,Staff,Staff
Hannah,Martenson,E33224,hannah.martenson2@apsva.us,Substitutes,Staff,Staff
Carly,Hurt,E33225,carly.hurt@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Yuselka,Rodas Sanchez,E33226,yuselka.rodassanchez@apsva.us,Extended Day,Staff,Staff
Houston,Burnside,E33227,houston.burnside@apsva.us,Aquatics,Staff,Staff
Gail,Davis,E33230,gail.davis@apsva.us,Substitutes,Staff,Staff
Alyssa,Marlow,E33231,alyssa.marlow@apsva.us,Substitutes,Staff,Staff
Stacy,Saulnier,E33233,stacy.saulnier@apsva.us,Office of Personnel,Supervisor,Staff
Nicholas,Madamba,E33237,nicholas.madamba@apsva.us,Substitutes,Staff,Staff
Isaac,Endo,E33238,isaac.endo@apsva.us,Extended Day Substitutes,Staff,Staff
Sarah,Krzywicki,E33239,sarah.krzywicki@apsva.us,Substitutes,Staff,Staff
Dwayne,Smith,E33240,dwayne.smith@apsva.us,Substitutes,Staff,Staff
Isaac,Shoultz,E33241,isaac.shoultz@apsva.us,Substitutes,Staff,Staff
Angie,Ebenau,E33242,angie.ebenau@apsva.us,REEP Program,Staff,Staff
Marina,Cisneros,E33245,marina.cisneros@apsva.us,Substitutes,Substitute,Staff
Ding-Lynn,Ledgard,E33246,dinglynn.ledgard@apsva.us,Adult Education,Staff,Staff
Nicholas,Barth,E33247,nicholas.barth@apsva.us,Aquatics,Staff,Staff
Meghan,Garner,E33250,meghan.garner@apsva.us,Substitutes,Staff,Staff
Jorge,Suarez,E33252,jorge.suarez@apsva.us,Substitutes,Staff,Staff
Liam,Mullen,E33255,liam.mullen2@apsva.us,Substitutes,Staff,Staff
Nadia,Merino,E33256,nadia.merino@apsva.us,Substitutes,Staff,Staff
Aida,Alebachew,E33257,aida.alebachew@apsva.us,Transportation,Bus Attendant,Transportation Staff
Mary,Kadera,E33258,mary.kadera@apsva.us,Office of the School Board,School Board Member,Staff
Rachida,El Gharbaoui,E33259,rachida.elgharbaoui@apsva.us,Food Services,Staff,Staff
Lina,Ahearn,E33260,lina.ahearn@apsva.us,Extended Day,Staff,Staff
Cammiel,Hussey,E33263,cammiel.hussey@apsva.us,Substitutes,Staff,Staff
Jan,Gundersen,E33264,jan.gundersen@apsva.us,Substitutes,Staff,Staff
Laurel,Buck,E33265,laurel.buck@apsva.us,Substitutes,Staff,Staff
Paolo,Rivas,E33266,paolo.rivas@apsva.us,Substitutes,Staff,Staff
Melanie,Ortiz,E33267,melanie.ortiz@apsva.us,Extended Day,Staff,Staff
Sam,Lukas,E33270,sam.lukas@apsva.us,Aquatics,Staff,Staff
Cindy,Flores,E33271,cindy.flores@apsva.us,"Extended Day Substitutes, Substitutes","Staff, Staff",Staff
Semira,Mohammed,E33272,semira.mohammed2@apsva.us,Extended Day,Staff,Staff
Rayna,Arias,E33273,rayna.arias@apsva.us,Extended Day,Staff,Staff
Griselda,Andino,E33274,isabel.andino@apsva.us,Division of Instruction,Administrative Assistant,Staff
Finot,Haile,E33277,finot.haile@apsva.us,Extended Day,Staff,Staff
Rebecca,Trytten,E33279,rebecca.trytten@apsva.us,Substitutes,Staff,Staff
Deborah,Ellis,E33280,deborah.ellis@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Imani,Miner,E33281,imani.miner@apsva.us,REEP Program,Staff,Staff
Nicole,Getter,E33282,nicole.getter@apsva.us,Substitutes,Staff,Staff
Muhammad,Abdulrahman,E33284,muhammad.abdulrahman@apsva.us,Substitutes,Staff,Staff
Colette,Daley,E33285,colette.daley@apsva.us,Substitutes,Staff,Staff
Philip,Owens,E33286,philip.owens@apsva.us,Substitutes,Staff,Staff
Samuel,Adu,E33287,samuel.adu@apsva.us,Transportation,Staff,Staff
Mulugeta,Yimer,E33288,mulugeta.yimer@apsva.us,Transportation,Bus Driver,Transportation Staff
Tonia,Reed,E33292,tonia.reed@apsva.us,Substitutes,Staff,Staff
Mason,Paris,E33293,mason.paris@apsva.us,Substitutes,Staff,Staff
Ezban,Morrissette,E33294,ezban.morrissette@apsva.us,Substitutes,Staff,Staff
Sara,Bibi,E33296,sara.bibi@apsva.us,Substitutes,Staff,Staff
Johniece,Marquez,E33298,johniece.marquez@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Sarah,Dempsey,E33299,sarah.dempsey@apsva.us,Substitutes,Substitute,Staff
David,Tevelin,E33300,david.tevelin@apsva.us,Adult Education,Staff,Staff
Trinity,Ewell,E33301,trinity.ewell@apsva.us,Substitutes,Staff,Staff
Katharine,Pulver,E33302,katharine.pulver@apsva.us,Adult Education,Staff,Staff
Jeanne,Kelly,E33303,jeanne.kelly@apsva.us,Administrative Services,Staff,Staff
Jesse,Morrell,E33304,jesse.morrell@apsva.us,Administrative Services,Staff,Staff
Melissa,Simons,E33305,melissa.simons@apsva.us,Administrative Services,Staff,Staff
Luis,Santacruz,E33306,luis.santacruz@apsva.us,Substitutes,Staff,Staff
Kathryn,Koch,E33308,kathryn.koch@apsva.us,Substitutes,Staff,Staff
April,Everett,E33311,april.everett@apsva.us,Substitutes,Staff,Staff
Paula,Cordero Salas,E33312,paula.corderosalas@apsva.us,Substitutes,Staff,Staff
Bailey,Cahill,E33313,bailey.cahill@apsva.us,Substitutes,Staff,Staff
Erin,Carkhuff,E33316,erin.carkhuff@apsva.us,Extended Day,Staff,Staff
Brittany,Kitchen,E33317,brittany.kitchen@apsva.us,Substitutes,Staff,Staff
Martha,Tucker,E33318,martha.tucker@apsva.us,Substitutes,Staff,Staff
Simone,Simien,E33319,simone.simien@apsva.us,Extended Day,Staff,Staff
Domenic,Romanello,E33320,domenic.romanello@apsva.us,Substitutes,Staff,Staff
Logan,Richardson,E33321,logan.richardson@apsva.us,Substitutes,Staff,Staff
Kathryn,Martin,E33322,kathryn.martin@apsva.us,Substitutes,Staff,Staff
Virginia,Long,E33323,virginia.long@apsva.us,Adult Education,Staff,Staff
Alexandra,Savage,E33326,alexandra.savage@apsva.us,Substitutes,Staff,Staff
Alexandra,Wicht,E33327,alexandra.wicht@apsva.us,Substitutes,Substitute,Staff
Xiaoli,Luo,E33328,xiaoli.luo@apsva.us,Administrative Services,Staff,Staff
Arbora,Johnson,E33330,arbora.johnson@apsva.us,Substitutes,Staff,Staff
Carolyn,Fruzzetti,E33331,carolyn.fruzzetti@apsva.us,Substitutes,Staff,Staff
Rania,Salih,E33332,rania.salih@apsva.us,Substitutes,Staff,Staff
Hamed,Hameedi,E33333,hamed.hameedi@apsva.us,Office of Finance,Administrator,Staff
Luke,DiBenigno,E33335,luke.dibenigno@apsva.us,Aquatics,Staff,Staff
Aidan,Singer,E33336,aidan.singer@apsva.us,Aquatics,Staff,Staff
Razia,Tajuddin,E33337,razia.tajuddin@apsva.us,Substitutes,Staff,Staff
Adrian,Horton,E33338,adrian.horton@apsva.us,Substitutes,Staff,Staff
Jessica,Haney,E33339,jessica.haney@apsva.us,Substitutes,Staff,Staff
Emilie,Heller,E33340,emilie.heller@apsva.us,Substitutes,Staff,Staff
Cheryl,Brown,E33341,cheryl.brown@apsva.us,Substitutes,Staff,Staff
James,Stocking,E33342,james.stocking@apsva.us,Substitutes,Staff,Staff
Banafsheh,Kamali,E33343,banafsheh.kamali@apsva.us,Substitutes,Staff,Staff
Emma,Mannino,E33344,emma.mannino2@apsva.us,Substitutes,Staff,Staff
Patrick,Rathbone,E33345,patrick.rathbone@apsva.us,Substitutes,Staff,Staff
Eileen,Wall,E33346,eileen.wall@apsva.us,Aquatics,Staff,Staff
Chelsea,Ross,E33347,chelsea.ross@apsva.us,Substitutes,Staff,Staff
Brooke,McElroy,E33348,brooke.mcelroy@apsva.us,Substitutes,Staff,Staff
Winthrop,Cashdollar,E33349,winthrop.cashdollar@apsva.us,Substitutes,Staff,Staff
Sally,Henrehan,E33350,sally.henrehan@apsva.us,Substitutes,Substitute,Staff
Tanner,Prime,E33351,tanner.prime@apsva.us,Facilities and Operations,Specialist,Staff
Christopher,Pankonin,E33352,christopher.pankonin@apsva.us,Substitutes,Staff,Staff
Allison,Diercks,E33353,allison.diercks@apsva.us,Substitutes,Staff,Staff
Shadman,Rafi,E33354,shadman.rafi@apsva.us,Substitutes,Staff,Staff
Fernando,Silva Sarmiento,E33355,fernando.silva@apsva.us,Substitutes,Staff,Staff
Alyssa,Morris,E33356,alyssa.morris@apsva.us,Substitutes,Staff,Staff
Lisa,Young,E33357,lisa.young2@apsva.us,Substitutes,Staff,Staff
Victor,Celis,E33358,victor.celis@apsva.us,Substitutes,Substitute,Staff
Amy,Rzepka,E33359,amy.rzepka@apsva.us,Substitutes,Staff,Staff
Julian,Eichers,E33360,julian.eichers@apsva.us,Aquatics,Staff,Staff
Charles,Randolph,E33361,charles.randolph2@apsva.us,Aquatics,Staff,Staff
Lee,Gjertsen,E33362,lee.gjertsen@apsva.us,Adult Education,Staff,Staff
Christa,Mansur,E33365,christa.mansur@apsva.us,Substitutes,Staff,Staff
Rebecca,Alvarado,E33366,rebecca.alvarado@apsva.us,Substitutes,Staff,Staff
Walter,Schutz,E33367,walter.schutz@apsva.us,Substitutes,Staff,Staff
Alexander,Al-Hujazi,E33368,alexander.alhujazi@apsva.us,Substitutes,Staff,Staff
Daniel,Israelsson,E33369,daniel.israelsson@apsva.us,Substitutes,Staff,Staff
Sebastian,Blanc,E33375,sebastian.blanc@apsva.us,Adult Education,Staff,Staff
Gloria,Montecino,E33376,gloria.montecino@apsva.us,Substitutes,Staff,Staff
Tracey,McLaughlin,E33377,tracey.mclaughlin@apsva.us,Substitutes,Staff,Staff
Janet,Russell-Hunter,E33379,janet.russellhunter@apsva.us,Substitutes,Staff,Staff
Cindy,Centeno,E33380,cindy.centeno@apsva.us,Substitutes,Staff,Staff
Jacob,Andersen,E33381,jacob.andersen@apsva.us,Substitutes,Staff,Staff
Elton,Hima,E33382,elton.hima@apsva.us,Substitutes,Staff,Staff
Kelly,Schwartz,E33383,kelly.schwartz@apsva.us,Substitutes,Staff,Staff
Pamela,Bernards,E33384,pamela.bernards@apsva.us,Substitutes,Staff,Staff
Max,Golkin,E33385,max.golkin@apsva.us,Substitutes,Staff,Staff
Julie,Smith,E33386,julie.smith@apsva.us,Substitutes,Staff,Staff
Taylor,Christensen,E33387,taylor.christensen@apsva.us,Substitutes,Staff,Staff
Wesley,Hagler,E33388,wesley.hagler@apsva.us,Substitutes,Staff,Staff
Naeem,Darab,E33389,naeem.darab@apsva.us,Substitutes,Staff,Staff
Audra,Phalen,E33390,audra.phalen@apsva.us,Substitutes,Staff,Staff
Ana,Sanchez-Armass,E33391,ana.sanchezarmass@apsva.us,Aquatics,Staff,Staff
Robin,Romack,E33395,robin.romack@apsva.us,Substitutes,Staff,Staff
Della,Lickteig,E33396,della.lickteig@apsva.us,Substitutes,Staff,Staff
Soraya,Ziaeian,E33397,soraya.ziaeian@apsva.us,Substitutes,Staff,Staff
Salma,Mohamed,E33398,salma.mohamed@apsva.us,Substitutes,Staff,Staff
Celestina,Flores,E33399,celestina.flores@apsva.us,Substitutes,Staff,Staff
Miles,Kelley,E33400,miles.kelley@apsva.us,Substitutes,Staff,Staff
Victoria,Parra,E33401,victoria.parra@apsva.us,Substitutes,Staff,Staff
Kerry,Mosley,E33402,kerry.mosley@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Samantha,Laidlaw,E33403,samantha.laidlaw@apsva.us,Substitutes,Staff,Staff
Dan,Benson,E33404,dan.benson@apsva.us,Substitutes,Staff,Staff
Ava,Gonzalez,E33405,ava.gonzalez@apsva.us,Substitutes,Staff,Staff
Erin,Freas-Smith,E33406,erin.freassmith@apsva.us,Substitutes,Staff,Staff
Eric,Lanman,E33407,eric.lanman@apsva.us,Substitutes,Staff,Staff
Nisrin,Nour Aldaiem,E33408,nisrin.nouraldaiem@apsva.us,Substitutes,Staff,Staff
Diana,Ramirez,E33409,diana.ramirez@apsva.us,Substitutes,Substitute,Staff
Riley,Shelton,E33410,riley.shelton@apsva.us,Aquatics,Staff,Staff
Mariam,Makkawi,E33412,mariam.makkawi@apsva.us,Substitutes,Staff,Staff
Noelle,Ng,E33413,noelle.ng@apsva.us,Substitutes,Staff,Staff
Sanaz,Ghodsi,E33414,sanaz.ghodsi@apsva.us,Substitutes,Staff,Staff
David,Witebsky,E33415,david.witebsky@apsva.us,Substitutes,Substitute,Staff
Alya,Elsaid,E33416,alya.elsaid@apsva.us,Substitutes,Staff,Staff
Sylvia,Cacciato,E33417,sylvia.cacciato@apsva.us,Substitutes,Staff,Staff
Monalisa,Moore,E33418,monalisa.moore2@apsva.us,Substitutes,Staff,Staff
Sarah,Davis,E33419,sarah.davis@apsva.us,Substitutes,Staff,Staff
Christie,Nguyen,E33420,christie.nguyen@apsva.us,Substitutes,Staff,Staff
Sydney,Riedl,E33421,sydney.riedl@apsva.us,Substitutes,Staff,Staff
Coreima,Castro Aranda,E33422,coreima.castroaranda@apsva.us,Substitutes,Staff,Staff
Anthony,Long,E33427,anthony.long@apsva.us,Substitutes,Staff,Staff
Lindolfo,Pedraza,E33428,lindolfo.pedraza@apsva.us,Substitutes,Staff,Staff
Anabelle-Jean,Kluge,E33429,anabellejean.kluge@apsva.us,Extended Day,Staff,Staff
Liliana,Sancho,E33431,liliana.sancho@apsva.us,Aquatics,Staff,Staff
Jashon,Wade,E33432,jashon.wade@apsva.us,Facilities and Operations,Staff,Staff
Gustavo,Gonzalez,E33433,gustavo.gonzalez@apsva.us,Substitutes,Staff,Staff
Monica,Flores,E33434,monica.flores@apsva.us,Substitutes,Staff,Staff
Catherine,Hasecke,E33435,catherine.hasecke@apsva.us,Substitutes,Staff,Staff
Danielle,Godfrey,E33437,danielle.godfrey@apsva.us,Office of Finance,Assistant Director,Staff
Dantrese,Canady,E33438,dantrese.canady@apsva.us,Substitutes,Staff,Staff
Joseph,Craig,E33439,joseph.craig@apsva.us,Substitutes,Staff,Staff
Dean,Jordan,E33440,dean.jordan@apsva.us,Substitutes,Substitute,Staff
Morris,Aizenman,E33449,morris.aizenman@apsva.us,Substitutes,Staff,Staff
Basma,Paget,E33450,basma.paget@apsva.us,Substitutes,Staff,Staff
Andrew,Price,E33451,andrew.price@apsva.us,Substitutes,Staff,Staff
Anthea,Fellone,E33452,anthea.fellone@apsva.us,Substitutes,Staff,Staff
Andrew,Spencer,E33453,andrew.spencer@apsva.us,Transportation,Director,Administrative Staff
Rosa,Bravo,E33455,rosa.bravo@apsva.us,Transportation,Staff,Staff
Jeanne,Lonergan,E33456,jeanne.lonergan@apsva.us,Administrative Services,Staff,Staff
Senedu,Gebretsadik,E33457,senedu.gebretsadik@apsva.us,Transportation,Staff,Staff
Nicholas,DuBois,E33458,nicholas.dubois@apsva.us,Extended Day,Staff,Staff
Kathleen,Donovan,E3346,kathleen.donovan@apsva.us,Special Education,Teacher,Instructional Staff
Dori,Kukawa,E33461,dori.kukawa@apsva.us,Substitutes,Staff,Staff
Mira,Khatun,E33462,mira.khatun@apsva.us,Food Services,Staff,Staff
Milagro,Hernandez,E33463,milagro.hernandez@apsva.us,Food Services,Staff,Staff
Angel,Utsey,E33464,angel.utsey@apsva.us,Substitutes,Staff,Staff
Yoon Hye,Ji,E33466,yoonhye.ji@apsva.us,Substitutes,Staff,Staff
Bouchra,Loutfi,E33467,bouchra.loutfi@apsva.us,Food Services,Staff,Staff
Alison,Fesler,E33468,alison.fesler@apsva.us,Substitutes,Staff,Staff
Roel,Escamilla,E33469,roel.escamilla@apsva.us,Substitutes,Staff,Staff
David,Rinaldo,E33470,rinaldo.david@apsva.us,Substitutes,Staff,Staff
Mohamed,Elmasri,E33471,mohamed.elmasri@apsva.us,Substitutes,Staff,Staff
Bayli,Jenkins,E33472,bayli.jenkins@apsva.us,Substitutes,Staff,Staff
Stephanie,Leslie,E33473,stephanie.leslie@apsva.us,Substitutes,Staff,Staff
Amani,Elfadil,E33474,amani.elfadil@apsva.us,Substitutes,Staff,Staff
Travis,Zielinski,E33475,travis.zielinski@apsva.us,Substitutes,Staff,Staff
Jo,Neely,E33481,jo.neely@apsva.us,Substitutes,Staff,Staff
Yesharge,Rahemeto,E33483,yesharge.rahemeto@apsva.us,Transportation,Staff,Staff
Fatima,Dahiri,E33484,fatima.dahiri@apsva.us,Substitutes,Staff,Staff
Kristyn,Lewis,E33486,kristyn.lewis@apsva.us,Substitutes,Staff,Staff
Amber,Bodrick,E33487,amber.bodrick@apsva.us,Substitutes,Staff,Staff
Cynthia,Serraino,E33489,cynthia.serraino@apsva.us,Substitutes,Staff,Staff
Pamela,Bates,E33491,pamela.bates@apsva.us,Substitutes,Staff,Staff
Luisa,Ruiz-Valenzuela,E33492,luisa.ruizvalenzuela@apsva.us,Substitutes,Staff,Staff
Robert,Callahan,E33493,robert.callahan@apsva.us,Substitutes,Staff,Staff
Asma,Hanif,E33494,asma.hanif@apsva.us,Food Services,Staff,Staff
Nicole,Kimball,E33495,nicole.kimball@apsva.us,Substitutes,Staff,Staff
Dennis,Hathorn,E33496,dennis.hathorn@apsva.us,Substitutes,Staff,Staff
Christian,Brown,E33497,christian.brown@apsva.us,Substitutes,Staff,Staff
Earl,Hargrove,E33498,earl.hargrove@apsva.us,Substitutes,Staff,Staff
Angela,Bennett,E33499,angela.bennett@apsva.us,Substitutes,Staff,Staff
Sara,Mulrooney,E335,sara.mulrooney@apsva.us,Adult Education,"Teacher, Staff",Staff
Leah,Ruffner,E33500,leah.ruffner@apsva.us,Substitutes,Staff,Staff
Lillian,Dibenigno,E33501,lillian.dibenigno@apsva.us,Substitutes,Staff,Staff
Alexandra,Pacheco,E33502,alexandra.pacheco@apsva.us,Substitutes,Staff,Staff
Jason,Kiker,E33503,jason.kiker@apsva.us,Substitutes,Staff,Staff
Juana,Eason Ortiz,E33504,juana.easonortiz@apsva.us,Food Services,Staff,Staff
Napaphach,Chitcongkan,E33506,napaphach.chitcong@apsva.us,Food Services,Staff,Staff
Andrew,Cruz Vasquez,E33510,andrew.cruzvasquez@apsva.us,Extended Day,Staff,Staff
Sanayit,Jima,E33511,sanayit.jima@apsva.us,Transportation,Staff,Staff
Shadee,Whiting,E33514,shadee.whiting@apsva.us,Food Services,Staff,Staff
Fesshazion,Ghebrecal,E33515,fesshazion.ghebrecal@apsva.us,Transportation,Staff,Staff
Yetnayet,Wakjira,E33516,yetnayet.wakjira@apsva.us,Extended Day,Staff,Staff
Asmeret,Teclu,E33519,asmeret.teclu@apsva.us,Transportation,Staff,Staff
Keith,Williams,E33521,keith.williams@apsva.us,Office of Personnel,Specialist,Staff
Melanie,Ewell,E33522,melanie.ewell@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Sarah,Yunus,E33528,sarah.yunus@apsva.us,Extended Day,Staff,Staff
 Hiwet,Woldeselassie,E33529,hiwet.woldeselassie@apsva.us,Transportation,Staff,Staff
Maura,Costello,E33530,maura.costello@apsva.us,Special Education,Teacher,Instructional Staff
Derishe,Alambo,E33531,derishe.alambo@apsva.us,Transportation,Staff,Staff
Karina,Hernandez Garcia,E33533,karina.hernandez@apsva.us,Extended Day,Staff,Staff
Lina,Shibeshi,E33534,lina.shibeshi@apsva.us,Extended Day,Staff,Staff
Ellena,Beniam,E33535,ellena.beniam@apsva.us,Extended Day,Staff,Staff
Mark,McLaughlin,E33536,mark.mclaughlin@apsva.us,Office of Finance,Director,Administrative Staff
James,Millson,E33538,james.millson@apsva.us,Substitutes,Staff,Staff
T,Connor,E33539,t.connor@apsva.us,Extended Day Substitutes,Staff,Staff
Claudia,Wilson,E3354,claudia.wilson@apsva.us,Office of Finance,Administrative Assistant,Staff
Wayne,Hicks,E33540,wayne.hicks@apsva.us,Extended Day,Staff,Staff
Paul,Ford,E33542,paul.ford@apsva.us,Extended Day,Staff,Staff
Giuliana,Figueroa,E33543,giuliana.figueroa@apsva.us,Extended Day Substitutes,Staff,Staff
Alaena,Lloyd,E33544,alaena.lloyd@apsva.us,Information Services,Analyst,Staff
Jessica,Nunez Fleitas,E33546,jessica.nunezfleitas@apsva.us,Administrative Services,Staff,Staff
Armani,Price,E33547,armani.price@apsva.us,Extended Day,Staff,Staff
Michael,Magbanua,E33548,michael.magbanua@apsva.us,Extended Day,Staff,Staff
Sonia,Bermudez Zelaya,E33549,sonia.bermudezzelaya@apsva.us,Food Services,Staff,Staff
Nesrine,Guedouari,E33550,nesrine.guedouari@apsva.us,Food Services,Staff,Staff
Brenden,Hogan,E33551,brenden.hogan@apsva.us,Substitutes,Staff,Staff
Akua,Sakyi,E33552,akua.sakyi@apsva.us,Extended Day Substitutes,Staff,Staff
Jacob,Sinton,E33554,jacob.sinton@apsva.us,Aquatics,Staff,Staff
Zehabu,Hussen,E33555,zehabu.hussen@apsva.us,Extended Day Substitutes,Staff,Staff
Jennifer,Lunger,E33556,jennifer.lunger@apsva.us,Substitutes,Staff,Staff
Munroe,Pleasant,E33561,munroe.pleasant@apsva.us,Extended Day,Staff,Staff
Maia,Maitland,E33564,maia.maitland@apsva.us,Administrative Services,Staff,Staff
Amina,Aden,E33565,amina.aden@apsva.us,Extended Day Substitutes,Staff,Staff
John,Abbracciamento,E33570,john.abbracciamento@apsva.us,Substitutes,Staff,Staff
Julissa,Ascencio,E33571,julissa.ascencio@apsva.us,Food Services,Staff,Staff
Abebe,Gebremichael,E33572,abebe.gebremichael@apsva.us,Transportation,Staff,Staff
Diego,Flores,E33573,diego.flores@apsva.us,Information Services,Maintenance Clerk,Facilities Staff
Barbara,Fillip,E33574,barbara.fillip@apsva.us,Adult Education,Staff,Staff
Keun,Lee,E33577,keun.lee@apsva.us,Transportation,Staff,Staff
Leyla,Valdez,E33578,leyla.valdez@apsva.us,Extended Day Substitutes,Staff,Staff
James,Gray,E33581,james.gray@apsva.us,Aquatics,Staff,Staff
Margaret,Kirkpatrick,E33582,margaret.kirkpatrick@apsva.us,Substitutes,Staff,Staff
Genet,Shiferaw,E33584,genet.shiferaw@apsva.us,Transportation,Staff,Staff
Henry,Bello,E3391,henry.bello@apsva.us,Information Services,Analyst,Staff
Sharon,McKay,E3413,sharon.mckay@apsva.us,REEP Program,Staff,Staff
Natalia,Benefiel,E3452,natalia.benefiel@apsva.us,REEP Program,Account Clerk,Staff
Jevon,Howard,E3455,jevon.howard@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Brae,Walker,E3456,brae.walker@apsva.us,Special Education,Administrative Assistant,Staff
Virginia,Pereira,E3460,virginia.pereira@apsva.us,Facilities and Operations,Administrative Assistant,Staff
Pam,Taylor,E3512,pam.taylor@apsva.us,Special Education,Staff,Staff
Mary Beth,Pelosky,E352,marybeth.pelosky@apsva.us,Division of Instruction,Teacher,Instructional Staff
Ana Maria,Te Tan,E3530,anamaria.tetan@apsva.us,Office of Personnel,Administrative Specialist,Staff
Abraham,Tuwafie,E3548,abraham.tuwaffe@apsva.us,Transportation,Bus Driver,Transportation Staff
Edgardo,Mercado,E3579,edgardo.mercado@apsva.us,Student Services,Teacher,Instructional Staff
Dolores,Baldoz,E3580,dolores.baldoz@apsva.us,Division of Instruction,Clerical Specialist,Staff
Chris,Jorss,E3583,chris.jorss@apsva.us,Information Services,Analyst,Staff
Nassif,Ibikounle,E3587,nassif.ibikounle@apsva.us,Substitutes,Staff,Staff
Ann,Reynolds-Kusnitz,E3588,ann.reynolds@apsva.us,Substitutes,Staff,Staff
Margo,Hope,E3598,margo.hope@apsva.us,Division of Instruction,Administrative Assistant,Staff
Dennis,Worthy,E3632,dennis.worthy@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Chip,Bonar,E3633,chip.bonar@apsva.us,Administrative Services,Administrator,Administrative Staff
Erik,Montague,E3735,erik.montague@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Charles,Randolph,E3745,charles.randolph@apsva.us,Division of Instruction,Coordinator,Staff
Hoa,Tran,E3750,hoa.tran@apsva.us,Food Services,Food Services Staff,Food Services Staff
Kathy,Jaffke,E3770,kathy.jaffke@apsva.us,"Division of Instruction, Office of Finance","Staff, Staff",Staff
Diane,Silva,E38,diane.silva@apsva.us,Food Services,Food Services Staff,Food Services Staff
Anthony,Tolliver,E3814,anthony.tolliver@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Lisa,Pellegrino,E3843,lisa.pellegrino@apsva.us,Division of Instruction,Assistant Director,Administrative Staff
Natividad,Roman,E3851,natividad.roman@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Barbara,O'Looney,E3871,barbara.olooney@apsva.us,Substitutes,Staff,Staff
David,Blorstad,E3895,david.blorstad@apsva.us,Office of Finance,Staff,Staff
Katherine,Young,E3928,katherine.young@apsva.us,Office of Personnel,EAP Staff,Staff
Oswaldo,Barahona,E3935,oswaldo.barahona@apsva.us,Relief Custodians,Custodian,Facilities Staff
Sheena,Wilson,E3940,sheena.wilson@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Maria,Perez,E3953,maria.perez@apsva.us,Intake Center,Staff,Staff
Tom,Windsor,E3998,tom.windsor@apsva.us,Division of Instruction,Staff,Staff
Someth,May,E4014,someth.may@apsva.us,Transportation,Bus Driver,Transportation Staff
Pat,Robertson,E4029,pat.robertson@apsva.us,Office of Personnel,Staff,Staff
Amy,Apgar,E4040,amy.apgar@apsva.us,Special Education,Teacher,Instructional Staff
Linda,Anderson,E4082,linda.anderson@apsva.us,Adult Education,Teacher,Instructional Staff
Sharon,Goode,E4108,sharon.goode@apsva.us,Administrative Services,Administrative Assistant,Staff
Tammy,Mills-Mirick,E4124,tammy.mirick@apsva.us,Division of Instruction,Coordinator,Staff
Mary,Vieira,E4138,mary.vieira@apsva.us,Student Services,Administrative Assistant,Staff
Lisa,Craddock,E4140,lisa.craddock@apsva.us,"Food Services, Special Education, Extended Day","Staff, Staff, Extended Day Staff",Extended Day Staff
Anjanette,Brooks,E4146,anjanette.brooks@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Christopher,Williams,E4154,christopher.williams@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Shiree,Ball,E4160,shiree.ball@apsva.us,Transportation,Bus Attendant,Transportation Staff
Kia,Martin,E4188,kia.martin@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
David,McBride,E4203,david.mcbride@apsva.us,Administrative Services,Principal,Administrative Staff
Colin,Brown,E4207,colin.brown@apsva.us,Administrative Services,Principal,Administrative Staff
Judy,Apostolico-Buck,E4239,judy.apostolicobuck@apsva.us,Administrative Services,Principal,Administrative Staff
Steve,Ellis,E4265,steve.ellis@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kevin,Holcombe,E4288,kevin.holcombe@apsva.us,Information Services,Analyst,Staff
Stephen,Moore,E4300,stephen.moore@apsva.us,Transportation,Bus Driver,Transportation Staff
Lillian,Quinteros,E4328,lillian.quinteros@apsva.us,REEP Program,Clerical Specialist,Staff
Liset,Reaves,E4398,liset.reaves@apsva.us,Facilities and Operations,Account Clerk,Staff
Leoman,Green,E4407,leoman.green@apsva.us,Transportation,Bus Driver,Transportation Staff
Tuan,Tran,E4439,tuan.tran@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Mary,Lovatt-McGillivray,E446,mary.mcgillivray@apsva.us,Substitutes,Staff,Staff
Belinda,Watson,E4484,belinda.watson@apsva.us,Extended Day,Specialist,Staff
Michele,Lombard,E4492,michele.lombard@apsva.us,Division of Instruction,Teacher,Instructional Staff
Jacqueline,Nampha,E4528,jacqueline.nampha@apsva.us,Food Services,Food Services Staff,Food Services Staff
Cynthia,Guadalupe,E4536,cynthia.guadalupe@apsva.us,Information Services,Administrative Assistant,Staff
Vanna,Vann,E4592,vanna.vann@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Mariana,Cagua-Cotera,E4607,mariana.cagua@apsva.us,Transportation,Bus Driver,Transportation Staff
Peter,Davis,E4654,peter.davis@apsva.us,Facilities and Operations,Mechanic,Facilities Staff
Rosaura,Palacios,E4676,rosaura.palacios@apsva.us,Relief Custodians,Custodian,Facilities Staff
Henry,Somerville,E4691,henry.somerville@apsva.us,Special Education,Staff,Staff
Lynn,Robinson,E4703,lynn.robinson@apsva.us,Substitutes,Staff,Staff
Joe,Barron,E4730,joe.barron@apsva.us,Office of Personnel,Supervisor,Staff
Lina,Veizaga,E4735,lina.veizaga@apsva.us,Intake Center,Staff,Staff
Candace,Mohrmann,E4750,candace.mohrmann@apsva.us,Substitutes,Staff,Staff
Debbie,McFarland,E4751,debbie.mcfarland@apsva.us,Administrative Services,Staff,Staff
Charles,Robinson,E4765,charles.robinson@apsva.us,Substitutes,Staff,Staff
Judy,Voegler,E4769,judy.voegler@apsva.us,Substitutes,Staff,Staff
Carolyn,Washington,E4799,carolyn.washington@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Deicy,Perez,E4815,deicy.perez@apsva.us,REEP Program,Staff,Staff
Claudia,Merida,E4825,claudia.merida@apsva.us,"Extended Day Substitutes, Food Services","Staff, Staff",Staff
Towanda,Peters,E4835,towanda.peters@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Eiman,Ali,E4847,eiman.ali@apsva.us,Extended Day,Staff,Staff
Rita,Pascual,E4849,rita.pascual@apsva.us,Division of Instruction,Clerical Specialist,Staff
Towana,Murray,E4909,towana.murray@apsva.us,Food Services,Food Services Staff,Food Services Staff
Angeliki,Mike,E4938,angeliki.mike@apsva.us,Food Services,Food Services Staff,Food Services Staff
Tawanda,Perry,E4944,tawanda.perry@apsva.us,Transportation,Bus Attendant,Transportation Staff
Rochelle,Davis,E4948,rochelle.davis@apsva.us,Information Services,Specialist,Staff
Simon,Contreras,E4987,simon.contreras@apsva.us,"Substitutes, Student Services, Adult Education","Staff, Staff, Staff",Staff
Rosa,Smith,E5006,rosa.smith@apsva.us,Transportation,Bus Driver,Transportation Staff
Sonny,Balmores,E5021,sonny.balmores@apsva.us,Information Services,Technical Staff,Staff
Chhern,Yip,E5031,chhern.yip@apsva.us,Facilities and Operations,Glazier/Roofer,Facilities Staff
Maria,Kehaiov,E5037,maria.kehaiov@apsva.us,Food Services,Food Services Staff,Food Services Staff
Jennifer,Powell,E5125,jennifer.powell@apsva.us,Division of Instruction,Teacher,Instructional Staff
Raul,Matos,E5129,raul.matos@apsva.us,Adult Education,Coordinator,Administrative Staff
Carmen,Mejia,E5131,carmen.mejia@apsva.us,Office of the Superintendent,Deputy Clerk Of The Board,Staff
Norma,Perla,E5164,norma.perla@apsva.us,"Transportation, Food Services","Staff, Bus Attendant",Staff
Jeremy,Koller,E5241,jeremy.koller@apsva.us,School and Community Relations,Producer,Staff
Elizabeth,Alvarez,E5299,elizabeth.alvarez@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Olga,Cotom,E5310,olga.cotom@apsva.us,Food Services,Food Services Staff,Food Services Staff
Matt,O'Grady,E5321,matt.ogrady@apsva.us,Office of Finance,Analyst,Staff
Claudia,Mercado,E5325,claudia.mercado@apsva.us,Office of the Superintendent,Clerk,Staff
Diane,Hunter,E5348,diane.hunter@apsva.us,Adult Education,Staff,Staff
Jeffrey,Fishbein,E5360,jeffrey.fishbein@apsva.us,Substitutes,Staff,Staff
Manuel,Delgado,E5364,manuel.delgado@apsva.us,Relief Custodians,Custodian,Facilities Staff
Darlene,Carr-Greene,E5379,darlene.carrgreene@apsva.us,Transportation,Staff,Staff
Halima,Bensaada,E5402,halima.bensaada@apsva.us,Intake Center,Staff,Staff
Elhag,Shamat,E5416,elhag.shamat@apsva.us,Transportation,Bus Driver,Transportation Staff
Gail,Ammons,E5434,gail.ammons@apsva.us,Substitutes,Staff,Staff
Aster,Ejigu,E5489,aster.ejigu@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Chris,Martin,E5525,chris.martin@apsva.us,Transportation,Bus Driver,Transportation Staff
Karen,Archer,E5537,karen.archer@apsva.us,Transportation,Coordinator,Staff
Kenneth,Mifflin,E556,kenneth.mifflin@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Sara,Adams,E5575,sara.adams@apsva.us,Information Services,Specialist,Staff
Eric,Knott,E5580,eric.knott@apsva.us,Information Services,Analyst,Staff
Ana Rosa,Baca De Koski-Karell,E5582,anarosa.karell@apsva.us,Administrative Services,Staff,Staff
Lelia,Roane,E5590,lelia.roane@apsva.us,Substitutes,Staff,Staff
Patrick,Tien,E5601,patrick.tien@apsva.us,Division of Instruction,Coordinator,Staff
Kris,Seldomridge,E5652,kris.seldomridge@apsva.us,Extended Day,Specialist,Staff
Tahied,Rafique,E5694,tahied.rafique@apsva.us,"Substitutes, Extended Day","Staff, Extended Day Staff",Staff
Shahida,Rafique,E5696,shahida.rafique@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Mary,Quattlebaum,E5707,mary.quattlebaum@apsva.us,"Substitutes, Administrative Services","Staff, Staff",Staff
Jeaneth,Mercado,E5733,jeaneth.mercado@apsva.us,Adult Education,Clerical Specialist,Staff
Eleanor,Lewis,E5764,eleanor.lewis@apsva.us,Student Services,Teacher,Instructional Staff
Cheree,David,E5791,cheree.david@apsva.us,Special Education,Teacher,Instructional Staff
Solange,Caovan-Hornbake,E5861,solange.caovan@apsva.us,Student Services,Teacher,Instructional Staff
Emma,Parral-Sanchez,E5877,emma.parralsanchez@apsva.us,Special Education,Administrative Assistant,Staff
Analily,Caballero,E5901,analily.caballero@apsva.us,Administrative Services,Teacher,Instructional Staff
Magali,Valentin,E5924,magali.valentin@apsva.us,"Food Services, Extended Day Substitutes","Food Services Staff, Staff",Food Services Staff
Jasmina,Arguello,E5928,jasmina.arguello@apsva.us,Food Services,Food Services Staff,Food Services Staff
Yolanda,Sotelo,E5937,yolanda.sotelo@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Dominga,Jimenez,E5964,dominga.jimenez@apsva.us,Transportation,Bus Driver,Transportation Staff
Miriam,Ruiz,E5974,miriam.ruiz@apsva.us,Facilities and Operations,Administrative Specialist,Staff
Alba,Chavarria De Salmeron,E5981,alba.chavarria@apsva.us,Extended Day,Supervisor,Extended Day Staff
Juana,Luna,E5996,juana.luna@apsva.us,Intake Center,Clerical Specialist,Staff
Delmy,Elvira,E6007,delmy.elvira@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Betsy,Gutierrez,E6047,betsy.gutierrez@apsva.us,Extended Day Substitutes,Staff,Staff
Lucita,Poliquit,E6078,lucita.poliquit@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Marguarite,Gooden,E6124,marguarite.gooden@apsva.us,Special Education,Staff,Staff
Sonia,Wayar,E6140,sonia.wayar@apsva.us,Intake Center,Registrar,Staff
Iliana,Gutierrez,E6169,iliana.gutierrez@apsva.us,Extended Day,Staff,Staff
Rachel,Reynolds,E6181,rachel.reynolds@apsva.us,Office of Personnel,Staff,Staff
William,Gordon,E6198,william.gordon@apsva.us,Substitutes,Staff,Staff
Kathy,Robinson,E6230,kathy.robinson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Duane,Lomis,E6288,duane.lomis@apsva.us,Information Services,Engineer,Staff
Mila,Vascones-Gatski,E6421,mila.vascones@apsva.us,Student Services,Teacher,Instructional Staff
Angie,Close,E6428,angie.close@apsva.us,Special Education,Supervisor,Administrative Staff
June,Gibbs,E6463,june.gibbs@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Pitter,Chi,E6467,pitter.chi@apsva.us,Information Services,Analyst,Staff
Narciso,Chavez,E6486,narciso.chavez@apsva.us,Transportation,Bus Driver,Transportation Staff
Carolyn,Parker,E6488,carolyn.parker@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Chiante,Brown,E6498,chiante.brown@apsva.us,Substitutes,Staff,Staff
Jorge,Palacios,E6507,jorge.palacios@apsva.us,Relief Custodians,Custodian,Facilities Staff
Julia,Arguello,E6518,julia.arguello@apsva.us,"Food Services, Transportation","Staff, Bus Attendant",Staff
Mynor,Celis,E6549,mynor.celis@apsva.us,Transportation,Bus Driver,Transportation Staff
Anthony,Adamovich,E6575,anthony.adamovich@apsva.us,Division of Instruction,"Staff, Instructional Assistant",Staff
Yna,Walters,E664,yna.walters@apsva.us,Student Services,Teacher,Instructional Staff
Pedro,Hernandez,E6735,pedro.hernandez@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Robert,Stoss,E6752,robert.stoss@apsva.us,Adult Education,Staff,Staff
Tuyet,Ngo-Conklin,E6755,lan.ngo@apsva.us,Food Services,Food Services Staff,Food Services Staff
Sayyada,Khanun,E6757,sayyada.khanun@apsva.us,Substitutes,Staff,Staff
Angela,Lopez,E6767,angela.lopez@apsva.us,Food Services,Food Services Staff,Food Services Staff
Marsha,Mottesheard,E6820,marsha.mottesheard@apsva.us,Division of Instruction,Staff,Staff
Kathy,Clingenpeel,E6835,kathy.clingenpeel@apsva.us,Information Services,Staff,Staff
Jim,Long,E6864,jim.long@apsva.us,School and Community Relations,Printer Supervisor,Staff
Belkis,Hazera,E6882,belkis.hazera@apsva.us,Administrative Services,Staff,Staff
Juanita,Wade,E6919,juanita.wade@apsva.us,Substitutes,Staff,Staff
Randy,Glasner,E692,randy.glasner@apsva.us,APS,Teacher,Instructional Staff
Margaret,Harris,E6949,margaret.harris@apsva.us,Substitutes,Staff,Staff
Jerrilyn,Young,E6978,jerrilyn.young@apsva.us,Adult Education,Specialist,Staff
Phyllis,Gandy,E6979,phyllis.gandy@apsva.us,Adult Education,Supervisor,Administrative Staff
Saundra,Simonson,E6996,saundra.simonson@apsva.us,Food Services,Food Services Staff,Food Services Staff
Renee,Sidberry,E7008,renee.sidberry@apsva.us,Transportation,Staff,Staff
Vanessa,Williams,E7011,vanessa.williams@apsva.us,Facilities and Operations,Administrative Assistant,Staff
Janet,Jackson,E7019,janet.jackson@apsva.us,Information Services,Analyst,Staff
Karen,Miller,E7077,karen.miller@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Valerie,Smith,E7094,valerie.smith@apsva.us,Transportation,Bus Driver,Transportation Staff
Kien,Vuong,E7109,kien.vuong@apsva.us,Office of Finance,Account Clerk,Staff
John,Findley,E7119,john.findley@apsva.us,APS,Teacher,Instructional Staff
Sien,Hem,E7124,sien.hem@apsva.us,Transportation,Staff,Staff
Adrian,Jackson,E7172,adrian.jackson@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Patience,Ossom,E7215,patience.ossom@apsva.us,Food Services,Food Services Staff,Food Services Staff
Silvia,Yzquieta,E7253,silvia.yzquieta@apsva.us,Intake Center,Staff,Staff
Maria,Mercado,E7312,maria.mercado@apsva.us,Office of Personnel,Administrative Specialist,Staff
Joanne,Diamond,E7326,joanne.diamond@apsva.us,Food Services,Food Services Staff,Food Services Staff
Claire,Peters,E7358,claire.peters@apsva.us,Administrative Services,Principal,Administrative Staff
William,Velasquez,E7387,william.velasquez@apsva.us,Transportation,Bus Driver,Transportation Staff
Marjorie,Kohlberg,E739,marjorie.kohlberg@apsva.us,"Substitutes, Division of Instruction","Staff, Staff",Staff
Jorge,Adrian,E7403,jorge.adrian@apsva.us,Information Services,Analyst,Staff
Tatiana,Parra,E7417,tatiana.parra@apsva.us,Extended Day,Staff,Staff
Peter,Khlen,E7435,peter.khlen@apsva.us,Transportation,Bus Driver,Transportation Staff
Critchett,Hodukavich,E7498,critchett.hodukavich@apsva.us,"Administrative Services, Substitutes","Staff, Staff",Staff
Hannah,Oukib,E750,hannah.oukib@apsva.us,Food Services,Food Services Staff,Food Services Staff
Guadalupe,Figueroa,E7535,guadalupe.figueroa@apsva.us,"Food Services, Extended Day Substitutes","Staff, Food Services Staff",Staff
Aleyda,Ramirez,E7547,aleyda.ramirez@apsva.us,Food Services,Food Services Staff,Food Services Staff
Thomas,Gutnick,E755,thomas.gutnick@apsva.us,Adult Education,Staff,Staff
Diane,Hellmuth,E7567,diane.hellmuth@apsva.us,Information Services,Assistant Director,Staff
Sydney,Woodson,E7601,sydney.woodson@apsva.us,Facilities and Operations,Plumber,Facilities Staff
John,Stuhldreher,E7609,john.stuhldreher@apsva.us,School and Community Relations,Producer,Staff
Laverne,Wright,E7617,laverne.wright@apsva.us,Transportation,Bus Driver,Transportation Staff
Singh,Ajrawat,E7635,singh.ajrawat@apsva.us,Information Services,Supervisor,Staff
Jackie,Williams,E7647,jackie.williams@apsva.us,Food Services,Food Services Staff,Food Services Staff
John,Barry,E7713,john.barry@apsva.us,Aquatics,Staff,Staff
Maria,Hernandez,E7730,maria.hernandez2@apsva.us,Food Services,Food Services Staff,Food Services Staff
Anne,Duffy,E7750,anne.duffy@apsva.us,Substitutes,Staff,Staff
Bridget,Loft,E7766,bridget.loft@apsva.us,Office of the Superintendent,CHIEF ACADEMIC OFFICER,Administrative Staff
Mai,Bui,E7777,mai.bui@apsva.us,Substitutes,Staff,Staff
Paul,Maniscalco,E778,paul.maniscalco@apsva.us,Division of Instruction,Coordinator,Staff
Esmeralda,Castillo,E7817,esmeralda.castillo@apsva.us,Division of Instruction,Clerical Specialist,Staff
Farhan,Khan,E7835,farhan.khan@apsva.us,Transportation,Bus Driver,Transportation Staff
David,Kegley,E7868,david.kegley@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Robyn,Kaye,E7903,robyn.kaye@apsva.us,Division of Instruction,Teacher,Instructional Staff
Donna,Fitzgerald,E7912,donna.fitzgerald@apsva.us,Food Services,Staff,Staff
Brian,Prunty,E7959,brian.prunty@apsva.us,Special Education,Staff,Staff
Casey,Robinson,E7969,casey.robinson@apsva.us,Administrative Services,Principal,Administrative Staff
Xenia,Castaneda,E8063,xenia.castaneda@apsva.us,Student Services,Administrative Assistant,Staff
Maria,Castaneda,E8066,maria.castaneda@apsva.us,Student Services,Staff,Staff
Holly,Hawthorne,E8107,holly.hawthorne@apsva.us,Administrative Services,Principal,Administrative Staff
Gladys,Matos,E8122,gladys.matos@apsva.us,Intake Center,Staff,Staff
Kathy,Recinos,E8140,kathy.recinos@apsva.us,Substitutes,Staff,Staff
Gloria,Funes,E817,gloria.funes@apsva.us,Adult Education,Staff,Staff
William,Wheeler,E8180,william.wheeler@apsva.us,Substitutes,Staff,Staff
Rosenia,Peake,E8218,rosenia.peake@apsva.us,Adult Education,Staff,Staff
Susan,Holland,E823,susan.holland@apsva.us,Student Services,Staff,Staff
Terry,Hill,E8268,terry.hill@apsva.us,Substitutes,Staff,Staff
Jilber,Castillo,E8274,jilber.castillo@apsva.us,Food Services,Food Services Staff,Food Services Staff
Doris,Minaya,E8282,doris.minaya@apsva.us,Intake Center,Staff,Staff
Doug,Martin,E8306,doug.martin@apsva.us,Facilities and Operations,Manager,Staff
Gerald,Brandt,E8363,gerald.brandt@apsva.us,Facilities and Operations,Manager,Staff
Jenlene,Nowak,E845,jenlene.nowak@apsva.us,Adult Education,Staff,Staff
Tameka,Martin,E8464,tameka.martin@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Tania,Thomas,E847,tania.thomas@apsva.us,Student Services,Teacher,Instructional Staff
Kimberly,Byrd,E8525,kimberly.byrd@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Kerry,Sterns,E8532,kerry.sterns@apsva.us,Information Services,Analyst,Staff
Bridgett,Doles,E8562,bridgett.doles@apsva.us,Information Services,Specialist,Staff
Regina,Garcia,E8572,regina.garcia@apsva.us,Intake Center,Staff,Staff
Sonia,Zambrano,E8669,sonia.zambrano@apsva.us,Food Services,Food Services Staff,Food Services Staff
Nora,Cuellar-Leigue,E8698,nora.cuellar@apsva.us,Information Services,Specialist,Staff
Alvaro,Hondoy,E8722,alvaro.hondoy@apsva.us,Intake Center,Instructional Assistant,Instructional Staff
Melissa,Marroquin,E8737,melissa.marroquin@apsva.us,Transportation,Clerical Specialist,Staff
Ana,Cabrera,E8746,ana.cabrera@apsva.us,Food Services,Food Services Staff,Food Services Staff
Fidel,Arriaza,E8750,fidel.arriaza@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Sola,Rock,E8781,sola.rock@apsva.us,Food Services,Food Services Staff,Food Services Staff
Wanda,Gant,E8822,wanda.gant@apsva.us,Office of Personnel,Administrative Specialist,Staff
Joyce,Jordan,E8843,joyce.jordan@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Noralee,Nevius,E8871,noralee.nevius@apsva.us,"Special Education, Student Services","Instructional Assistant, Staff",Staff
Robin,Barbour,E8890,robin.barbour@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Venetia,Levenberry,E8893,venetia.levenberry@apsva.us,Division of Instruction,Staff,Staff
Denise,Henderson,E8895,denise.henderson@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Karen,Agate,E8921,karen.agate@apsva.us,Special Education,Teacher,Instructional Staff
Steven,Bernheisel,E893,steven.bernheisel@apsva.us,Facilities and Operations,Assistant Director,Staff
Alicia,Zekan,E8970,alicia.zekan@apsva.us,Media Processing,Clerical Specialist,Staff
Marbea,Tammaro,E8974,marbea.tammaro@apsva.us,Special Education,Teacher,Instructional Staff
Nghia,Huynh,E9009,nghia.huynh@apsva.us,Information Services,Administrator,Staff
Debra,Barnes,E9017,debra.barnes@apsva.us,Transportation,Bus Driver,Transportation Staff
Tanya,Hileman,E9025,tanya.hileman@apsva.us,Information Services,Analyst,Staff
Geoconda,Loza,E9095,geoconda.loza@apsva.us,Transportation,Administrative Specialist,Staff
Hector,Giron,E9211,hector.giron@apsva.us,Transportation,Bus Driver,Transportation Staff
Heidi,Baptista,E9263,heidi.baptista@apsva.us,Student Services,Teacher,Instructional Staff
Lilla,Wise,E929,lilla.wise@apsva.us,Office of the Superintendent,Staff,Staff
Karen,Navarro,E9297,karen.navarro@apsva.us,Food Services,Food Services Staff,Food Services Staff
Jannette,Cabrera,E9339,jannette.cabrera@apsva.us,Transportation,Bus Attendant,Transportation Staff
Sandra,Caceres,E9358,sandra.caceres@apsva.us,"Food Services, Transportation","Staff, Bus Driver",Transportation Staff
Deborah,Hart,E9397,deborah.hart@apsva.us,Special Education,Instructional Assistant,Instructional Staff
Linda,Kelleher,E9399,linda.kelleher@apsva.us,Special Education,Coordinator,Instructional Staff
Jamila,Oufkir,E9404,jamila.oufkir@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Ana,Granados,E9442,ana.granados2@apsva.us,Food Services,Food Services Staff,Food Services Staff
Vanessa,Ventura,E9451,vanessa.ventura@apsva.us,Student Services,Administrative Technician,Staff
Karen,Morales,E9457,karen.morales@apsva.us,Extended Day,Registrar,Staff
Sandra,Vallejos,E9489,sandra.vallejos@apsva.us,"Food Services, Extended Day","Staff, Food Services Staff",Staff
Violeta,Gonzales-Rocha,E9520,violeta.gonzales@apsva.us,Transportation,Bus Driver,Transportation Staff
Gregg,Robertson,E9531,gregg.robertson@apsva.us,Administrative Services,Staff,Staff
Melanie,Siteki,E9615,melanie.siteki@apsva.us,REEP Program,Staff,Staff
Sharon,Sobel,E9620,sharon.sobel@apsva.us,School and Community Relations,Staff,Staff
Wendy,Pilch,E9673,wendy.pilch@apsva.us,Administrative Services,Director,Administrative Staff
Natalia,Nunez,E9703,natalia.nunez@apsva.us,REEP Program,Clerical Specialist,Staff
Donald,Clinger,E9709,donald.clinger@apsva.us,Student Services,Teacher,Instructional Staff
Floyd,Cline,E9733,floyd.cline@apsva.us,Aquatics,Manager,Staff
Tyrone,Byrd,E9772,tyrone.byrd@apsva.us,Administrative Services,Director,Administrative Staff
Renaud,Osterlund,E9802,renaud.osterlund@apsva.us,Facilities and Operations,Maintenance Supervisor,Facilities Staff
Tenita,Chapman,E9805,tenita.chapman@apsva.us,Extended Day,Supervisor,Extended Day Staff
Mariateresa,Enriquez,E9819,mariateresa.enriquez@apsva.us,Substitutes,Staff,Staff
Lorena,Perez,E9926,lorena.perez@apsva.us,Food Services,Food Services Staff,Food Services Staff
Rubina,Jan,E9970,rubina.jan@apsva.us,Extended Day,Extended Day Staff,Extended Day Staff
Linda,Delap,E9985,linda.delap@apsva.us,Substitutes,Staff,Staff
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import json

import markdown
from csv import writer
import time
import os

with open('../../aps_staff.md', 'r') as f:
    text = f.read()
    html = markdown.markdown(text)

with open('aps_staff_lst.html', 'w') as f:
    f.write(html)

# Read the list with all the links
aps_list_path = os.getcwd() + "/aps_staff_lst.html"

aps_list_html_page = BeautifulSoup(open(aps_list_path, encoding="utf8"), "html.parser")

# All the links to aps websites
all_links_aps = []

for a in aps_list_html_page.find_all('a', href=True):
    all_links_aps.append(a['href'])

def arrayToString(array):
    items = str(array)
    items = items.replace("[", "")
    items = items.replace("]", "")
    items = items.replace("\'", "")
    return items


class Educator:
    def __init__(self, firstName, lastName, aps_id, email, worksite, title, category):
        self.firstName = firstName
        self.lastName =  lastName
        self.aps_id = aps_id 
        self.email = email
        self.worksite = worksite
        self.title = title
        self.category = category
        
        

with open('aps_data.csv', 'w', encoding='utf8', newline='') as f:
    thewriter = writer(f)
    header = ['First Name', 'Last Name', 'APS ID','Email', 'Worksite', 'Title', 'Category']
    thewriter.writerow(header)
    all_educators = []
    for link in all_links_aps:
            html = urllib.request.urlopen(link).read()
            soup = BeautifulSoup(html, 'html.parser')
            scripts = soup.find_all('script')
            
            found_data = 0

            for script in scripts:
                result = script.text.find('aps_directory_data')
                if (result != -1):
                    raw_data = script.text.strip()[21:-1]
                    
            data = json.loads(raw_data)

           

            for key in data:
                first_name = data[key]['name_f']
                last_name = data[key]['name_l']
                aps_id = data[key]['apsid']
                email = data[key]['email']
                #position = data[key]['title']        
                worksite = arrayToString(data[key]['site'])
                worksite = worksite.replace("5001", "Virtual Elementary School")
                worksite = worksite.replace("5002", "Virtual Middle School")
                worksite = worksite.replace("5003", "Virtual High School")
                try:
                    title = arrayToString(data[key]['title'])
                except:
                    title = 'n/a'

                category = arrayToString(data[key]['category'])             

                #for each in data[key]['site']:
                    #worksite+= each + ", "
                #create educator object
                all_educators.append(Educator(first_name, last_name, aps_id, email, worksite, title, category))
            # now try the opposite where we exc
        
        '''
        if (each.worksite == 'Abingdon ES'):
            if (each.title == 'Teacher'):
                header = [each.firstName, each.lastName, each.aps_id, each.email, each.worksite, each.title, each.category]
                thewriter.writerow(header)
        '''

        #taking out redundant data using a for loop
    '''
        k = [[1, 2], [4], [5, 6, 2], [1, 2], [3], [4]]
        new_k = []
        for elem in k:
            if elem not in new_k:
                new_k.append(elem)
        k = new_k
        print k'''
        
        '''if (each.worksite != 'Abingdon ES'):
            header = [each.firstName, each.lastName, each.aps_id, each.email, each.worksite, each.title, each.category]
            thewriter.writerow(header)'''
        #i'm going to make a copy of this file that way we don't mess up already working code, and can focus only on this portion

    cleaned_data = []
    for each in all_educators:
        if each.aps_id not in cleaned_data:
            cleaned_data.append(each)
        all_educators = cleaned_data
        
        


    


        
            

      

    

    





    
    
    
    
            
            
            





            
            
            
           