import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import json

import markdown
from csv import writer
import time
import os

with open('../../aps_staff.md', 'r') as f:
    text = f.read()
    html = markdown.markdown(text)

with open('aps_staff_lst.html', 'w') as f:
    f.write(html)

# Read the list with all the links
aps_list_path = os.getcwd() + "/aps_staff_lst.html"

aps_list_html_page = BeautifulSoup(open(aps_list_path, encoding="utf8"), "html.parser")

# All the links to aps websites
all_links_aps = []

for a in aps_list_html_page.find_all('a', href=True):
    all_links_aps.append(a['href'])


with open('aps_data.csv', 'w', encoding='utf8', newline='') as f:
    thewriter = writer(f)
    header = ["First Name", "Last Name",'Email', "APS id", "Site", 'Position', "Category",  'Team', 'Courses', "Grade"]
    thewriter.writerow(header)

    for link in all_links_aps:

        header = [" "]
        thewriter.writerow(header)
        header = ["Now looking at the data from " + link]
        thewriter.writerow(header)
        header = [" "]
        thewriter.writerow(header)

        html = urllib.request.urlopen(link).read()
        soup = BeautifulSoup(html, 'html.parser')
        scripts = soup.find_all('script')

        found_data = 0

        for script in scripts:
            result = script.text.find('aps_directory_data')
            if (result != -1):
                raw_data = script.text.strip()[21:-1]

        data = json.loads(raw_data)


        for key in data:
            print(len(data))
            staff_info = data[key]
            
            
            this_staff = []

            for dict in staff_info:
                print(key, ":", staff_info[dict])
                this_staff.append(staff_info[dict])
            
            try:
                header = [this_staff[0], this_staff[1], this_staff[2], this_staff[3], this_staff[4], this_staff[5], this_staff[6], this_staff[7], this_staff[8], this_staff[9]]
                thewriter.writerow(header)
            except:
                print("could write data for employee", this_staff[0], this_staff[1])


            print("\n\n\n")
