import asyncio
from bs4 import BeautifulSoup
from pyppeteer import launch

async def main():
    # launch chromium browser in the background
    browser = await launch()
    # open a new tab in the browser
    page = await browser.newPage()
    # add URL to a new page and then open it
    print("we here")
    await page.goto("https://careercenter.apsva.us/staff-directory/")
    # create a screenshot of the page and save it
    page_content = await page.content()

    print(page_content.text)

    soup = BeautifulSoup(page_content)
    content = soup.find("section", id="sd-content")

    print(content.prettify())

    # close the browser
    await browser.close()

asyncio.get_event_loop().run_until_complete(main())
