import requests 
from bs4 import BeautifulSoup

URL = "https://realpython.github.io/fake-jobs/"
page = requests.get(URL)

soup = BeautifulSoup(page.content, "html.parser")

results = soup.find(id="ResultsContainer")

print(results.prettify())

job_elements = results.find_all("div", class_="card-content")

for job_element in job_elements:
    position = job_element.find("h2", class_ = "title is-5")
    name = job_element.find("h3", class_ = "subtitle is-6 company")
    location = job_element.find("p", class_ = "location")
    print(position.text.strip())
    print(name.text.strip())
    print(location.text.strip())
    print()

print()

python_jobs = results.find_all("h2", string="Python")

print(python_jobs)

print()

python_jobs = results.find_all(
    "h2", string=lambda text: "python" in text.lower()
)

print(len(python_jobs))

