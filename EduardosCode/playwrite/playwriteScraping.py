from bs4 import BeautifulSoup
from playwright.sync_api import sync_playwright

# Use sync version of Playwright
with sync_playwright() as p:
    # Launch the browser
    browser = p.chromium.launch()

    # Open a new browser page
    page = browser.new_page()

    # Create a URI for our test file
    url = "https://careercenter.apsva.us/staff-directory/"

    # Open our test file in the opened page
    page.goto(url)
    page_content = page.content()

    # Process extracted content with BeautifulSoup
    soup = BeautifulSoup(page_content)
    print(soup.prettify())

    # Close browser
    browser.close()