
#### This program scrapes naukri.com's page and gives our result as a 
#### list of all the job_profiles which are currently present there. 
  
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
import time
  
#url of the page we want to scrape
url = "https://careercenter.apsva.us/staff-directory/"
  
# initiating the webdriver. Parameter includes the path of the webdriver.
service = Service('./Drivers/chromedriver')
driver = webdriver.Chrome(service=service) 
driver.get(url) 
  
# this is just to ensure that the page is loaded
time.sleep(5) 
  
html = driver.page_source
  
# this renders the JS code and stores all
# of the information in static HTML code.
  
# Now, we could simply apply bs4 to html variable
soup = BeautifulSoup(html, "html.parser")
  
driver.close() # closing the webdriver