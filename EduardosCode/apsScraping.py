from playwright.sync_api import sync_playwright
from bs4 import BeautifulSoup
#import markdown
import time
import os

# Import the aps staff list and store it on a file
# Commented for now since the list wont change much but will be usefull if more links are added
"""
with open('../aps_staff.md', 'r') as f:
    text = f.read()
    html = markdown.markdown(text)

with open('aps_staff_lst.html', 'w') as f:
    f.write(html)
"""

# Read the list with all the links
aps_list_path = os.getcwd() + "/aps_staff_lst.html"

aps_list_html_page = BeautifulSoup(open(aps_list_path, encoding="utf8"), "html.parser")

# All the links to aps websites
all_links_aps = []

for a in aps_list_html_page.find_all('a', href=True):
    all_links_aps.append(a['href'])

# 
with sync_playwright() as p:

        # Launch the browser
        browser = p.chromium.launch()

        # Open a new browser page
        page = browser.new_page()

        # URL to go to a website
        url = "https://careercenter.apsva.us/staff-directory/"

        # Open our webpage
        page.goto(url)
        page_content = page.content()

        soup = BeautifulSoup(page_content, features="html.parser")

        start_content = soup.find("section", id="sd-content")

        staff_listing = start_content.find("div", class_ = "listings")

        all_staff = staff_listing.find_all("div", class_ = "row")

        for staff in all_staff:
            name = staff.find("div", class_ = "name")
            position = staff.find("div", class_ = "title")
            team = staff.find("div", class_ = "team")
            courses = staff.find("div", class_= "courses")
            try:
                print("Position:\t", position.text.strip())
                print("Name:\t", name.text.strip())
                print("Team:\t", team.text.strip())
                print("Courses:\t", courses.text.strip())
                print()
            except:
                print()


# This code should iterate through all the websites stored in all_links_aps list
# This code is a WIP (Work In Progress) and will eventually be integrated into the main proggram 
# Whithin this loop the program should add the information to the database
"""
for url in all_links_aps:

    with sync_playwright() as p:

        # Launch the browser
        browser = p.chromium.launch()

        # Open a new browser page
        page = browser.new_page()

        # Open our webpage
        page.goto(url)
        page_content = page.content()

        soup = BeautifulSoup(page_content, features="html.parser")

        start_content = soup.find("section", id="sd-content")

        staff_listing = start_content.find("div", class_ = "listings")

        all_staff = staff_listing.find_all("div", class_ = "row")

        for staff in all_staff:
            name = staff.find("div", class_ = "name")
            position = staff.find("div", class_ = "title")
            team = staff.find("div", class_ = "team")
            courses = staff.find("div", class_= "courses")
            try:
                print("Position:\t", position.text.strip())
                print("Name:\t", name.text.strip())
                print("Team:\t", team.text.strip())
                print("Courses:\t", courses.text.strip())
                print()
            except:
                print()
"""


